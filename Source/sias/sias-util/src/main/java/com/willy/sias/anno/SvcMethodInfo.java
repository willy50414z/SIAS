package com.willy.sias.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashMap;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SvcMethodInfo {
	public String methodDesc = "";
	public String[] argsDesc = new String[0];
	public String resultDesc = "";
	public HashMap<String, String> defaultArgsDesc = new HashMap<String, String>() {
		private static final long serialVersionUID = 1L;

		{
			put("sid", "標的ID");
			put("baseDate", "計算基準日");
			put("fieldId", "欄位ID");
			put("maDays", "MA期間(5,10,20)");
			put("turnTsList", "轉折點tsList");
			put("psTsList", "支壓tsList");
			put("interval", "計算期數");
		}
	};
	String[] argsDesc() default {""};
	String resultDesc() default "";
	String methodDesc() default "";
}
