package com.willy.sias.backtest.service;

import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.config.Const;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChipDmsService {

  @Autowired
  private DataTableDao dtDao;
  @Autowired
  private ChipIdxService ciSvc;

  public List<TimeSeriesDTO<BigDecimal>> getItHoldRatio(Integer sid) {
    return getItHoldRatio(sid, null, null);
  }

  public List<TimeSeriesDTO<BigDecimal>> getItHoldRatio(Integer sid, Date date) {
    return getItHoldRatio(sid, date, date);
  }

  public List<TimeSeriesDTO<BigDecimal>> getItHoldRatio(Integer sid, Date startDate, Date endDate) {
    //撈出IT_HOLD
    List<TimeSeriesDTO<BigDecimal>> itHoldTsList = dtDao.findTsList(sid,
        Const._FIELDID_LP_IT_HOLD, startDate, endDate);

    //get capital
    List<TimeSeriesDTO<BigDecimal>> csMsList = ciSvc.getIssuedSharesList(sid, startDate, endDate);

    List<TimeSeriesDTO<BigDecimal>> itHoldRatioTsList = new ArrayList<>();
    itHoldTsList.forEach(itHoldTs -> {
      TimeSeriesDTO<BigDecimal> cs = csMsList.stream()
          .filter(csMs -> csMs.getDate().compareTo(itHoldTs.getDate()) == 0).findAny().orElse(null);
      if (itHoldTs.getDate() != null && itHoldTs.getValue() != null && cs != null) {
        itHoldRatioTsList.add(new TimeSeriesDTO(itHoldTs.getDate(),
            itHoldTs.getDecimalValue().divide(cs.getValue(), 6, RoundingMode.HALF_UP)));
      }
    });
    return itHoldRatioTsList;
  }
}
