package com.willy.sias.backtest.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradeDetailDTO;
import com.willy.sias.backtest.util.TradeUtil;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.util.BigDecimalUtil;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;

@Service
public class ProfitDTOService {

  @Autowired
  private TradeUtil tu;
  @Autowired
  private StatisticService ss;
  @Autowired
  private CalendarRepository caRepo;

  public final Comparator<TradeDetailDTO> _COMPARATOR_TRADEDETAILPO_TRADEPRICE = new Comparator<TradeDetailDTO>() {
    @Override
    public int compare(TradeDetailDTO o1, TradeDetailDTO o2) {
      // TODO Auto-generated method stub
      return o1.getTradePrice().compareTo(o2.getTradePrice());
    }
  };

  /**
   * 依日期設定本類別所有統計數據
   *
   * @param profitDTO
   */
  public void setSimpleStatisticFields(ProfitDTO profitDTO) {
    setSimpleStatisticFields(profitDTO, profitDTO.getLastTradeDate());
  }

  public void setComplexStatisticFields(ProfitDTO profitDTO) {
    setComplexStatisticFields(profitDTO, profitDTO.getLastTradeDate());
  }

  /**
   * 短時間能算出的統計資料
   *
   * @param profitDTO
   * @param calBaseDate
   */
  public void setSimpleStatisticFields(ProfitDTO profitDTO, Date calBaseDate) {
    // 已實現毛利
    profitDTO.setGrossProfit(this.getGrossProfit(profitDTO, calBaseDate));
    // 已實現毛利率
    profitDTO.setGrossProfitRatio(this.getGrossProfitRatio(profitDTO, calBaseDate));
    // 獲利率(勝率)
    profitDTO.setProfitRatio(this.getProfitRatio(profitDTO, calBaseDate));
    // 獲利因子(總獲利/總損失)
    profitDTO.setProfitFactor(this.getProfitFactor(profitDTO, calBaseDate));
    // 勝率
    profitDTO.setProfitRatio(this.getProfitRatio(profitDTO, calBaseDate));
    // 期望值
    profitDTO.setExpectValue(this.getExceptedValue(profitDTO, calBaseDate));
  }

  /**
   * 要算比較久的統計資料
   *
   * @param profitDTO
   * @param calBaseDate
   */
  public void setComplexStatisticFields(ProfitDTO profitDTO, Date calBaseDate) {
    //淨值
    try{ profitDTO.setNetValueTsList(this.getNetValueList(profitDTO)); } catch (Exception e ) { WLog.error(e);}
    //Beta value
    try{ profitDTO.setBetaRatio(ss.getBeltaValue(profitDTO)); } catch (Exception e ) { WLog.error(e);}
    // Alpha value
    try{ profitDTO.setAlfaRatio(ss.getAlphaValue(profitDTO)); } catch (Exception e ) { WLog.error(e);}

//		this.setMaxLossAmt(this.getMaxLossAmt(calBaseDate));
//		this.setMaxLossAmtRatio(this.getMaxLossRatio(calBaseDate));
//		this.setMaxConLossCount(this.getMaxConLossCount(calBaseDate));
//		this.setMaxProfitAmt(this.getMaxProfitAmt(calBaseDate));
//		this.setMaxProfitAmtRatio(this.getMaxProfitRatio(calBaseDate));
//		this.setMaxConLossAmt(this.getMaxConLossAmt(calBaseDate));
//		this.setMaxConLossRatio(this.getMaxConLossAmtRatio(calBaseDate));
//		this.setRiskRatio(this.getRiskRatio(calBaseDate));
  }

  /**
   * 毛利
   *
   * @param baseDate
   * @return
   */
  public BigDecimal getGrossProfit(ProfitDTO profitDTO, Date baseDate) {
    List<BigDecimal> grossProfitList = profitDTO.getTradeDetailList().stream()
        .filter(
            td -> td.getGrossProfit() != null && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList());
    return BigDecimalUtil.sum(grossProfitList);
  }

  /**
   * 毛利率(毛利 / 成本)
   *
   * @param baseDate
   * @return
   */
  public BigDecimal getGrossProfitRatio(ProfitDTO profitDTO, Date baseDate) {
    // 毛利
    BigDecimal grossProfit = getGrossProfit(profitDTO, baseDate);
    // 成本
    BigDecimal cost = new BigDecimal(0);
    // 先買後賣
    List<TradeDetailDTO> tdList = profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && (td.getTradeType().equals(TradeType.sell) || td.getTradeType()
            .equals(TradeType.shortSelling))
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate)).collect(Collectors.toList());
    for (TradeDetailDTO td : tdList) {
      cost = cost.subtract(td.getGrossProfit());
      cost = cost.add(td.getTradeAmt());
    }
    tdList = profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && (td.getTradeType().equals(TradeType.buy) || td.getTradeType()
            .equals(TradeType.shortBuying))
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .collect(Collectors.toList());
    for (TradeDetailDTO td : tdList) {
      cost = cost.add(td.getGrossProfit());
      cost = cost.subtract(td.getTradeAmt());
    }
    if (cost.compareTo(Const._BIGDECIMAL_0) > 0) {
      return grossProfit.divide(cost, 2, RoundingMode.HALF_UP);
    } else {
      return Const._BIGDECIMAL_m1;
    }
  }

  /**
   * @param profitDTO
   * @param baseDate
   * @return
   */
  public BigDecimal getProfitRatio(ProfitDTO profitDTO, Date baseDate) {
    // 投資次數
    BigDecimal investCount = new BigDecimal(profitDTO.getTradeDetailList().stream()
        .filter(
            td -> td.getGrossProfit() != null && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .count());
    // 獲利次數
    BigDecimal profitCount = new BigDecimal(profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) > 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .count());
    if (investCount.compareTo(Const._BIGDECIMAL_0) > 0) {
      return profitCount.divide(investCount, 2, RoundingMode.HALF_UP);
    } else {
      return Const._BIGDECIMAL_m1;
    }

  }

  /**
   * 獲利因子(總獲利/總損失)
   *
   * @param baseDate
   * @return
   */
  public BigDecimal getProfitFactor(ProfitDTO profitDTO, Date baseDate) {
    BigDecimal totalProfit = new BigDecimal(0), totalLoss = new BigDecimal(0);
    // 總獲利
    List<BigDecimal> profitList = profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) > 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList());
    // 總損失
    List<BigDecimal> lossList = profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) <= 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList());

    for (BigDecimal profit : profitList) {
      totalProfit = totalProfit.add(profit);
    }

    for (BigDecimal loss : lossList) {
      totalLoss = totalLoss.subtract(loss);
    }
    // 總損失>0才計算，避免除數為0回傳無限大
    return (totalLoss.compareTo(Const._BIGDECIMAL_0) > 0) ? totalProfit.divide(totalLoss, 2,
        RoundingMode.HALF_UP)
        : null;
  }

  public BigDecimal getAcctBalance(ProfitDTO profitDTO) {
    return getAcctBalance(profitDTO, new Date());
  }

  /**
   * 帳戶餘額
   *
   * @param calBaseDate 計算基準日
   */
  public BigDecimal getAcctBalance(ProfitDTO profitDTO, Date calBaseDate) {
    BigDecimal acctBalance = BigDecimalUtil.sum(profitDTO.getTradeDetailList().stream()
        .filter(tradeDetail -> WDate.beforeOrEquals(tradeDetail.getSettleDate(), calBaseDate))
        .map(td -> td.getTradeAmt()).collect(Collectors.toList()));
    return acctBalance.add(profitDTO.getOriginalFunds());
  }

  public List<TradeDetailDTO> getInventoryList(ProfitDTO profitDTO, Date calBaseDate) {
    TradeDetailDTO tmpTd = null, tmpInv1 = null;
    List<TradeDetailDTO> invList1, invList2 = null, inventoryList = new ArrayList<>();
    List<TradeDetailDTO> tdList = profitDTO.getTradeDetailList().stream()
        .filter(td -> WDate.beforeOrEquals(td.getTradeDate(), calBaseDate))
        .collect(Collectors.toList());
    for (TradeDetailDTO td : tdList) {
      int count1 = 0, count2 = 0;
      BigDecimal cost = new BigDecimal(0), tradeAmt = new BigDecimal(0), tmpCost = new BigDecimal(
          0);
      int units = td.getTradeUnit().intValue();
      tradeAmt = tu.getTradeAmt(units, td.getTradePrice(), td.getTradeType());
      switch (td.getTradeType()) {
        case buy:
          tmpInv1 = inventoryList.stream()
              .filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                  && iv.getTradeDate().compareTo(td.getTradeDate()) == 0
                  && iv.getTradeType().equals(TradeType.buy)
                  && iv.getTradePrice().compareTo(td.getTradePrice()) == 0)
              .findFirst().orElse(null);
          if (tmpInv1 == null) {
            inventoryList.add(td.newInstance());
          } else {
            tmpInv1.setTradeUnit(tmpInv1.getTradeUnit().add(td.getTradeUnit()));
            tmpInv1.setTradeAmt(tmpInv1.getTradeAmt().add(td.getTradeAmt()));
          }
          break;
        case shortBuying:
          // 沖賣數量
          invList1 = inventoryList.stream()
              .filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                  && iv.getTradeDate().equals(td.getTradeDate())
                  && iv.getTradeType().equals(TradeType.shortSelling))
              .sorted(_COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
          Collections.reverse(invList1);// 沖掉'賣'的存貨，價高優先
          count1 = BigDecimalUtil.sum(
                  invList1.stream().map(iv -> iv.getTradeUnit()).collect(Collectors.toList()))
              .intValue();
          // 調整存貨
          int remainUnit;
          // 有當日沖賣庫存
          if (CollectionUtils.isNotEmpty(invList1)) {
            for (TradeDetailDTO inv : invList1) {
              // 沖賣庫存>0 && 交易數量>0
              if (count1 > 0 && units > 0) {
                remainUnit = Math.max(0, inv.getTradeUnit().intValue() - units);
                if (remainUnit > 0) {
                  // 未完銷才需計算金額，否則後面會被remove
                  tmpCost = inv.getTradeAmt().multiply(new BigDecimal(units))
                      .divide(inv.getTradeUnit(),
                          0, RoundingMode.CEILING);
                  inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
                  cost = cost.add(tmpCost);
                  tradeAmt = tradeAmt.subtract(tmpCost);
                } else {
                  cost = cost.add(inv.getTradeAmt());
                  tradeAmt = tradeAmt.subtract(inv.getTradeAmt());
                }
                // 剩餘庫存(沖賣庫存扣除本次消除庫存)
                count1 -= inv.getTradeUnit().intValue();
                // 剩餘待交易數
                units -= inv.getTradeUnit().intValue();
                inv.setTradeUnit(new BigDecimal(remainUnit));
              }
            }
          }

          // 沖買及當日買的庫存用完後，單純的沖賣
          if (units > 0) {
            tmpInv1 = inventoryList.stream()
                .filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                    && iv.getTradeDate().compareTo(td.getTradeDate()) == 0
                    && iv.getTradeType().equals(TradeType.shortBuying)
                    && iv.getTradePrice().compareTo(td.getTradePrice()) == 0)
                .findFirst().orElse(null);
            if (tmpInv1 == null) {
              tmpTd = td.newInstance();
              tmpTd.setTradeAmt(tradeAmt);
              tmpTd.setTradeUnit(new BigDecimal(units));
              inventoryList.add(tmpTd);
            } else {
              tmpInv1.setTradeAmt(tmpInv1.getTradeAmt().add(tradeAmt));
              tmpInv1.setTradeUnit(tmpInv1.getTradeUnit().add(new BigDecimal(units)));
            }
          }
          break;
        case sell:
          invList1 = this.getSellableTdList(td.getSid(), td.getTradeDate(), inventoryList);
          // 調整存貨
          for (TradeDetailDTO inv : invList1) {
            if (units > 0) {
              if (inv.getTradeUnit().intValue() > units) {
                tmpCost = inv.getTradeAmt().multiply(new BigDecimal(units))
                    .divide(inv.getTradeUnit(), 0,
                        RoundingMode.CEILING);
                inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
                inv.setTradeUnit(inv.getTradeUnit().subtract(new BigDecimal(units)));
                cost = cost.add(tmpCost);
                units = 0;
              } else {
                units -= inv.getTradeUnit().intValue();
                inv.setTradeUnit(Const._BIGDECIMAL_0);
                cost = cost.add(inv.getTradeAmt());
              }
            }
          }
          break;
        case shortSelling:
          // 從存貨中取出沖買交易(優先)
          invList1 = inventoryList.stream()
              .filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                  && iv.getTradeDate().equals(td.getTradeDate())
                  && iv.getTradeType().equals(TradeType.shortBuying))
              .sorted(_COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
          count1 = BigDecimalUtil.sum(
                  invList1.stream().map(iv -> iv.getTradeUnit()).collect(Collectors.toList()))
              .intValue();
          // 存貨中沖賣數量不夠負擔，找同日普賣存貨
          if (count1 < units) {
            invList2 = inventoryList.stream().filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                    && iv.getTradeDate().equals(td.getTradeDate()) && iv.getTradeType()
                    .equals(TradeType.buy))
                .sorted(_COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
            count2 = BigDecimalUtil
                .sum(invList2.stream().map(iv -> iv.getTradeUnit()).collect(Collectors.toList()))
                .intValue();
          }

          // 調整存貨
          // 有當日沖賣庫存
          if (CollectionUtils.isNotEmpty(invList1)) {
            for (TradeDetailDTO inv : invList1) {
              // 沖賣庫存>0 && 待交易數量>0
              if (count1 > 0 && units > 0) {
                remainUnit = Math.max(0, inv.getTradeUnit().intValue() - units);
                if (remainUnit > 0) {
                  // 未完銷才需計算金額，否則後面會被remove
                  tmpCost = inv.getTradeAmt().multiply(new BigDecimal(units))
                      .divide(inv.getTradeUnit(),
                          0, RoundingMode.CEILING);
                  inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
                  cost = cost.add(tmpCost);
                  tradeAmt = tradeAmt.add(tmpCost);
                } else {
                  cost = cost.add(inv.getTradeAmt());
                  tradeAmt = tradeAmt.add(inv.getTradeAmt());
                }
                // 剩餘庫存(沖賣庫存扣除本次消除庫存)
                count1 -= inv.getTradeUnit().intValue();
                // 剩餘待交易數
                units -= inv.getTradeUnit().intValue();
                inv.setTradeUnit(new BigDecimal(remainUnit));
              }
            }
          }
          // 抵銷同日賣
          if (CollectionUtils.isNotEmpty(invList2)) {
            // 買入數量 > 沖賣數量
            for (TradeDetailDTO inv : invList2) {
              // 同日普賣庫存>0 && 待交易數量>0
              if (count2 > 0 && units > 0) {
                remainUnit = Math.max(0, inv.getTradeUnit().intValue() - units);
                if (remainUnit > 0) {
                  // 未完銷才需計算金額，否則後面會被remove
                  tmpCost = inv.getTradeAmt().multiply(new BigDecimal(units))
                      .divide(inv.getTradeUnit(),
                          0, RoundingMode.CEILING);
                  inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
                  cost = cost.add(tmpCost);
                  tradeAmt = tradeAmt.add(tmpCost);
                } else {
                  cost = cost.add(inv.getTradeAmt());
                  tradeAmt = tradeAmt.add(inv.getTradeAmt());
                }
                // 剩餘庫存(沖賣庫存扣除本次消除庫存)
                count2 -= inv.getTradeUnit().intValue();
                // 剩餘待交易數
                units -= inv.getTradeUnit().intValue();
                inv.setTradeUnit(new BigDecimal(remainUnit));
              }
            }
          }

          if (units > 0) {
            tmpInv1 = inventoryList.stream()
                .filter(iv -> iv.getSid().compareTo(td.getSid()) == 0
                    && iv.getTradeDate().compareTo(td.getTradeDate()) == 0
                    && iv.getTradeType().equals(TradeType.shortSelling)
                    && iv.getTradePrice().compareTo(td.getTradePrice()) == 0)
                .findFirst().orElse(null);
            if (tmpInv1 == null) {
              tmpTd = td.newInstance();
              tmpTd.setTradeAmt(tradeAmt);
              tmpTd.setTradeUnit(new BigDecimal(units));
              inventoryList.add(tmpTd);
            } else {
              tmpInv1.setTradeAmt(tmpInv1.getTradeAmt().add(tradeAmt));
              tmpInv1.setTradeUnit(tmpInv1.getTradeUnit().add(new BigDecimal(units)));

            }
          }
          break;
        default:
          WLog.error(
              new IllegalArgumentException("TradeType[" + td.getTradeType() + "] is invalid"));
          break;
      }
    }
    List<TradeDetailDTO> newTdList = new ArrayList<>();
    inventoryList.stream().filter(td -> td.getTradeUnit().compareTo(Const._BIGDECIMAL_0) > 0)
        .forEach(td -> newTdList.add(td.newInstance()));
    return newTdList;
  }

  public List<TimeSeriesDTO<BigDecimal>> getNetValueList(ProfitDTO profitDTO) {
    List<CalendarPO> caList = caRepo.findByDateTypeIsAndDateBetween(
        Const._CFG_ID_CALENDAR_STOCK_BUS_DATE, profitDTO.getFirstTradeDate(),
        profitDTO.getLastTradeDate());
    List<TimeSeriesDTO<BigDecimal>> netValueTsList = new ArrayList<>();
    for (CalendarPO ca : caList) {
      netValueTsList.add(
          new TimeSeriesDTO<BigDecimal>(ca.getDate(), this.getNetValue(profitDTO, ca.getDate())));
    }
    return netValueTsList;
  }

  public BigDecimal getNetValue(ProfitDTO profitDTO, Date baseDate) {

    List<TradeDetailDTO> tdList = profitDTO.getTradeDetailList().stream()
        .filter(td -> WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .collect(Collectors.toList());
    // 建立一個與profitPo相同的TradeDetailList
    ProfitDTO p = new ProfitDTO();
    p.setOriginalFunds(profitDTO.getOriginalFunds());
    for (TradeDetailDTO td : tdList) {
      tu.trade(td.getSid(), td.getTradeDate(), td.getTradeUnit().intValue(), td.getTradeType(),
          td.getTradePrice(), p);
    }
    p.getInventoryList().forEach(inv -> {
      tu.trade(inv.getSid(), baseDate,
          this.getShortBuyableCount(p, inv.getSid(), baseDate).intValue(),
          TradeType.shortBuying, PriceType.CLOSE, p);
      tu.trade(inv.getSid(), baseDate,
          this.getShortSellableCount(p, inv.getSid(), baseDate).intValue(),
          TradeType.shortSelling, PriceType.CLOSE, p);
      tu.trade(inv.getSid(), baseDate, this.getSellableCount(inv.getSid(), baseDate, p).intValue(),
          TradeType.sell, PriceType.CLOSE, p);
    });
    BigDecimal netValue = new BigDecimal(0);
    for (TradeDetailDTO td : p.getTradeDetailList()) {
      if (td.getGrossProfit() != null) {
        netValue = netValue.add(td.getGrossProfit());
      }
    }
    netValue = netValue.add(p.getOriginalFunds());
    return netValue;
  }

  /**
   * 期望值(獲利*獲利率+虧損*虧損率)
   *
   * @param profitDTO
   * @param baseDate
   * @return
   */
  public BigDecimal getExceptedValue(ProfitDTO profitDTO, Date baseDate) {
    // 獲利金額List
    BigDecimal profitGrossProfit = BigDecimalUtil.sum(profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) > 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList()));

    // 虧損金額List
    BigDecimal lossGrossProfit = BigDecimalUtil.sum(profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) < 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList()));

    // 勝率
    BigDecimal profitRatio = this.getProfitRatio(profitDTO, baseDate);
    return profitGrossProfit.multiply(profitRatio)
        .add(lossGrossProfit.multiply(Const._BIGDECIMAL_1.subtract(profitRatio)));
  }

  /**
   * @param profitDTO
   * @return
   */
  public BigDecimal getProfitAmtRatio(ProfitDTO profitDTO) {
    return getProfitAmtRatio(profitDTO, new Date());
  }

  public BigDecimal getProfitAmtRatio(ProfitDTO profitDTO, Date baseDate) {
    // 獲利金額List
    BigDecimal profitGrossProfit = BigDecimalUtil.sum(profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) > 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList()));

    // 虧損金額List
    BigDecimal lossGrossProfit = BigDecimalUtil.sum(profitDTO.getTradeDetailList().stream()
        .filter(td -> td.getGrossProfit() != null
            && td.getGrossProfit().compareTo(Const._BIGDECIMAL_0) < 0
            && WDate.beforeOrEquals(td.getTradeDate(), baseDate))
        .map(td -> td.getGrossProfit()).collect(Collectors.toList()));
    if (lossGrossProfit.compareTo(Const._BIGDECIMAL_0) == 0) {
      return null;
    } else {
      return profitGrossProfit.divide(lossGrossProfit, 4, RoundingMode.HALF_UP);
    }

  }


  /**
   * 取得可沖買存貨
   *
   * @param sid
   * @param tradeDate
   * @return
   */
  public List<TradeDetailDTO> getShortBuyableTdList(ProfitDTO profitDTO, int sid, Date tradeDate) {
    return profitDTO
        .getInventoryList().stream().filter(
            td -> td.getSid().compareTo(sid) == 0 && td.getTradeDate().equals(tradeDate)
                && td.getTradeUnit().compareTo(Const._BIGDECIMAL_0) > 0
                && (td.getTradeType().equals(TradeType.sell)
                || td.getTradeType().equals(TradeType.shortSelling)))
        .collect(Collectors.toList());
  }

  /**
   * 取得可沖買存貨數量
   *
   * @param sid
   * @param tradeDate
   * @return
   */
  public BigDecimal getShortBuyableCount(ProfitDTO profitDTO, int sid, Date tradeDate) {
    BigDecimal tmpUnit = new BigDecimal(0);
    List<BigDecimal> unitList = getShortBuyableTdList(profitDTO, sid, tradeDate).stream()
        .map(td -> td.getTradeUnit()).collect(Collectors.toList());
    for (BigDecimal unit : unitList) {
      tmpUnit = tmpUnit.add(unit);
    }
    return tmpUnit;
  }

  /**
   * 取得可普賣存貨(不含可沖賣數)
   *
   * @param sid
   * @param tradeDate
   * @return
   */
  public List<TradeDetailDTO> getSellableTdList(int sid, Date tradeDate, ProfitDTO profitDTO) {
    return getSellableTdList(sid, tradeDate, profitDTO.getInventoryList());
  }

  public List<TradeDetailDTO> getSellableTdList(int sid,
      Date tradeDate, List<TradeDetailDTO> inventoryList) {
    return inventoryList.stream()
        .filter(iv -> iv.getSid().compareTo(sid) == 0 && !iv.getTradeDate().equals(tradeDate)
            && iv.getTradeType().equals(TradeType.buy)
            && iv.getTradeUnit().compareTo(Const._BIGDECIMAL_0) > 0)
        .sorted(this._COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
  }

  public BigDecimal getSellableCount(int sid, Date tradeDate, ProfitDTO profitDTO) {
    List<TradeDetailDTO> tdList = this.getSellableTdList(sid, tradeDate, profitDTO);
    if (CollectionUtils.isEmpty(tdList)) {
      return new BigDecimal(0);
    } else {
      return BigDecimalUtil.sum(
          tdList.stream().map(td -> td.getTradeUnit()).collect(Collectors.toList()));
    }
  }

  /**
   * 取得可沖賣存貨
   *
   * @param sid
   * @param tradeDate
   * @return
   */
  public List<TradeDetailDTO> getShortSellableTdList(ProfitDTO profitDTO, int sid, Date tradeDate) {
    return profitDTO.getInventoryList().stream()
        .filter(td -> td.getSid().compareTo(sid) == 0 && td.getTradeDate().equals(tradeDate)
            && (td.getTradeType().equals(TradeType.buy) || td.getTradeType()
            .equals(TradeType.shortBuying)))
        .collect(Collectors.toList());
  }

  /**
   * 取得可沖賣存貨數量
   *
   * @param sid
   * @param tradeDate
   * @return
   */
  public BigDecimal getShortSellableCount(ProfitDTO profitDTO, int sid, Date tradeDate) {
    BigDecimal tmpUnit = new BigDecimal(0);
    List<BigDecimal> unitList = getShortSellableTdList(profitDTO, sid, tradeDate).stream()
        .map(td -> td.getTradeUnit()).collect(Collectors.toList());
    for (BigDecimal unit : unitList) {
      tmpUnit = tmpUnit.add(unit);
    }
    return tmpUnit;
  }

  public String getTradeDetailToString(ProfitDTO profitDTO) {
    StringBuilder sb = new StringBuilder();
    for (TradeDetailDTO tradeDetail : profitDTO.getTradeDetailList()) {
      sb.append(tradeDetail.toString()).append("\r\n");
    }
    return sb.toString();
  }
}
