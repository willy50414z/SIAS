package com.willy.sias.backtest.service;

import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class OperatorServiceTest {

  @Autowired
  private OperatorService operSvc;

  @Test
  public void test_add_with2TsList_shouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = operSvc.add(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("5"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("4"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("3")))
        , Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("6"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("7"))
            , new TimeSeriesDTO<>(WType.strToDate("20201103"), new BigDecimal("8"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("9"))
            , new TimeSeriesDTO<>(WType.strToDate("20201105"), new BigDecimal("10"))));

    List<TimeSeriesDTO<BigDecimal>> expectResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("11")),
        new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("11")),
        new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("12")));

    Assert.assertEquals(result, expectResult);
  }

  @Test
  public void test_subtract_with2TsList_shouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = operSvc.subtract(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("5"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("4"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("3")))
        , Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("6"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("7"))
            , new TimeSeriesDTO<>(WType.strToDate("20201103"), new BigDecimal("8"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("9"))
            , new TimeSeriesDTO<>(WType.strToDate("20201105"), new BigDecimal("10"))));

    List<TimeSeriesDTO<BigDecimal>> expectResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("-1")),
        new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("-3")),
        new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("-6")));

    Assert.assertEquals(result, expectResult);
  }

  @Test
  public void test_multiply_with2TsList_shouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = operSvc.multiply(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("5"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("4"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("3")))
        , Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("6"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("7"))
            , new TimeSeriesDTO<>(WType.strToDate("20201103"), new BigDecimal("8"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("9"))
            , new TimeSeriesDTO<>(WType.strToDate("20201105"), new BigDecimal("10"))));

    List<TimeSeriesDTO<BigDecimal>> expectResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("30")),
        new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("28")),
        new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("27")));

    Assert.assertEquals(result, expectResult);
  }

  @Test
  public void test_divide_with2TsList_shouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = operSvc.divide(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("5"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("4"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("3")))
        , Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("6"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("7"))
            , new TimeSeriesDTO<>(WType.strToDate("20201103"), new BigDecimal("8"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("9"))));

    List<TimeSeriesDTO<BigDecimal>> expectResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("0.8333")),
        new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("0.5714")),
        new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("0.3333")));

    Assert.assertEquals(result, expectResult);
  }

  @Test
  public void test_divide_with2TsListAndScaleAndRoundingMode_shouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = operSvc.divide(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("5"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("4"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("3")))
        , Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("6"))
            , new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("7"))
            , new TimeSeriesDTO<>(WType.strToDate("20201103"), new BigDecimal("8"))
            , new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("9"))
            , new TimeSeriesDTO<>(WType.strToDate("20201105"), new BigDecimal("10"))), 2,
        RoundingMode.HALF_UP);

    List<TimeSeriesDTO<BigDecimal>> expectResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20201101"), new BigDecimal("0.83")),
        new TimeSeriesDTO<>(WType.strToDate("20201102"), new BigDecimal("0.57")),
        new TimeSeriesDTO<>(WType.strToDate("20201104"), new BigDecimal("0.33")));

    Assert.assertEquals(result, expectResult);
  }
}
