package com.willy.sias.web.controller;

import com.willy.sias.web.service.CategoryEditorService;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/categoryEditor")
public class CategoryEditorController {
  @Autowired
  private CategoryEditorService ceSvc;

  @RequestMapping("/")
  public ModelAndView view() {
    ModelAndView mv = new ModelAndView("CategoryEditor");

//    mv.addObject("catNameList",
//        fiRepo.findAll().stream().map(fi -> fi.getFieldName().split(":")[0])
//            .sorted().collect(Collectors.toCollection(LinkedHashSet::new)));
    return mv;
  }

  @ResponseBody
  @RequestMapping("/getFormattedString")
  public String getFormattedString(String unFormattedCatString) {
    return ceSvc.getFormattedCateString(unFormattedCatString);
  }

  @ResponseBody
  @RequestMapping("/saveCatToDb")
  public String saveCatToDb(String unFormattedCatString) {
    return ceSvc.saveCatToDb(unFormattedCatString);
  }

  @ResponseBody
  @RequestMapping("/queryCompanyCatListBySidCodeAndCatName")
public String queryCompanyCatListBySidCodeAndCatName(String qryStr, String qryType)
      throws JSONException {
    return ceSvc.queryCompanyCatListByQryStrAndQryType(qryStr, qryType);
  }
}
