package com.willy.sias.db.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * BatchLog
 * 
 * @author
 */
@Table(name = "BACK_TEST_LOG_D")
@Entity
@Data
public class BackTestLogDPO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "\"TEST_ID_D\"")
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private Integer testIdD;
	@Column(name = "\"TEST_ID\"")
	private Integer testId;
	@Column(name = "\"TRADE_DETAIL\"")
	private byte[] tradeDetail;
}