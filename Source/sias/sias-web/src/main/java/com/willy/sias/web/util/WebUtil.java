package com.willy.sias.web.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.NoSuchFileException;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import com.willy.util.log.WLog;

public class WebUtil {
	public static ResponseEntity<byte[]> getDownloadObject(String filePath, String downloadedFileName) {
		try {
			File file = new File(filePath);
			if (!file.exists()) {
				throw new NoSuchFileException(filePath);
			}
			HttpHeaders headers = new HttpHeaders();
			//下載顯示的檔名，解決中文名稱亂碼問題
			String downloadFileName;
			downloadFileName = new String(downloadedFileName.getBytes("UTF-8"),"iso-8859-1");
			//通知瀏覽器以attachment（下載方式）開啟圖片
			headers.setContentDispositionFormData("attachment", downloadFileName);
			//application/octet-stream:二進位制流資料（最常見的檔案下載）
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			//201 HttpStatus.CREATED
			return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),headers,HttpStatus.CREATED);
		} catch (IOException e) {
			WLog.error(e);
			return null;
		}
	}
}
