package com.willy.sias.backtest.service;

import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.config.Const;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ChipDmsServiceTest {

  @Autowired
  private ChipDmsService chipDmsSvc;
  @MockBean
  private DataTableDao dtDao;
  @MockBean
  private ChipIdxService ciSvc;

  @Before
  public void init() {
    List<TimeSeriesDTO<BigDecimal>> itHoldTsList_20160331 = Arrays.asList(
        //連買5天,空2天後繼續買
        new TimeSeriesDTO<>(WType.strToDate("20160331"), new BigDecimal("90153"))
    );
    List<TimeSeriesDTO<BigDecimal>> itHoldTsList_20160401 = Arrays.asList(
        //連買5天,空2天後繼續買
        new TimeSeriesDTO<>(WType.strToDate("20160401"), new BigDecimal("51374"))
    );
    List<TimeSeriesDTO<BigDecimal>> itHoldTsList_20160406 = Arrays.asList(
        //連買5天,空2天後繼續買
        new TimeSeriesDTO<>(WType.strToDate("20160406"), new BigDecimal("1054669"))
    );
    List<TimeSeriesDTO<BigDecimal>> capitalStockList = Arrays.asList(
        //連買5天,空2天後繼續買
        new TimeSeriesDTO<>(WType.strToDate("20160301"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160302"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160303"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160304"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160305"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160306"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160307"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160308"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160309"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160310"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160311"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160312"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160313"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160314"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160315"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160316"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160317"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160318"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160319"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160320"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160321"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160322"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160323"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160324"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160325"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160326"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160327"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160328"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160329"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160330"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160331"), new BigDecimal("100000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160401"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160402"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160403"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160404"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160405"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160406"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160407"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160408"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160409"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160410"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160411"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160412"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160413"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160414"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160415"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160416"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160417"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160418"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160419"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160420"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160421"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160422"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160423"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160424"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160425"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160426"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160427"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160428"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160429"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160430"), new BigDecimal("200000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160501"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160502"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160503"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160504"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160505"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160506"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160507"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160508"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160509"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160510"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160511"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160512"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160513"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160514"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160515"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160516"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160517"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160518"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160519"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160520"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160521"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160522"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160523"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160524"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160525"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160526"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160527"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160528"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160529"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160530"), new BigDecimal("300000000")),
        new TimeSeriesDTO<>(WType.strToDate("20160531"), new BigDecimal("300000000"))
    );
    Mockito.when(
        dtDao.findTsList(494, Const._FIELDID_LP_IT_HOLD, WType.strToDate("20160331"),
            WType.strToDate("20160331"))).thenReturn(itHoldTsList_20160331);
    Mockito.when(
        dtDao.findTsList(494, Const._FIELDID_LP_IT_HOLD, WType.strToDate("20160401"),
            WType.strToDate("20160401"))).thenReturn(itHoldTsList_20160401);
    Mockito.when(
        dtDao.findTsList(494, Const._FIELDID_LP_IT_HOLD, WType.strToDate("20160406"),
            WType.strToDate("20160406"))).thenReturn(itHoldTsList_20160406);
    Mockito.when(
            ciSvc.getIssuedSharesList(494, WType.strToDate("20160331"), WType.strToDate("20160331")))
        .thenReturn(capitalStockList.stream()
            .filter(cs -> WType.dateToStr(cs.getDate(), "yyyyMM").equals("201603")).collect(
                Collectors.toList()));
    Mockito.when(
            ciSvc.getIssuedSharesList(494, WType.strToDate("20160401"), WType.strToDate("20160401")))
        .thenReturn(capitalStockList.stream()
            .filter(cs -> WType.dateToStr(cs.getDate(), "yyyyMM").equals("201604")).collect(
                Collectors.toList()));
    Mockito.when(
            ciSvc.getIssuedSharesList(494, WType.strToDate("20160406"), WType.strToDate("20160406")))
        .thenReturn(capitalStockList.stream()
            .filter(cs -> WType.dateToStr(cs.getDate(), "yyyyMM").equals("201604")).collect(
                Collectors.toList()));

  }

  @Test
  public void testGetItHoldRatioShouldSuccess() {
    BigDecimal result = chipDmsSvc.getItHoldRatio(494, WType.strToDate("20160331"),
        WType.strToDate("20160331")).get(0).getDecimalValue();
    Assert.assertEquals(result, new BigDecimal("0.000902"));

    result = chipDmsSvc.getItHoldRatio(494, WType.strToDate("20160401"),
        WType.strToDate("20160401")).get(0).getDecimalValue();
    Assert.assertEquals(result, new BigDecimal("0.000257"));

    result = chipDmsSvc.getItHoldRatio(494, WType.strToDate("20160406"),
        WType.strToDate("20160406")).get(0).getDecimalValue();
    Assert.assertEquals(result, new BigDecimal("0.005273"));
  }
}
