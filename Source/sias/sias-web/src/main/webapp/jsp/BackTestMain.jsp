<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>歷史回測</title>
<%@ include file="/jsp/Header.jsp"%>
<%@ include file="/jsp/HighChartPlugin.jsp"%>
<script>
function downloadCandleChart(sid, date) {
    $.blockUI();
    $.ajax({
        type: "POST",
        url: getContextPath() + "/BackTestMain/downloadCandleChart",
        data: {
            "sid": sid,
            "date": date
        },
        success: function(fileName) {
            if (fileName == '') {
                alert('檔案產生失敗');
            }
            $("#candleChartImg").attr(
                "src",
                getContextPath() +
                "/BackTestMain/getCandleChart?fileName=" +
                fileName);
            $("#candleChartDiv").dialog("open");
        },
        complete: function() {
            $.unblockUI();
        }
    });
}

function setTradeDetailEvent() {
    $('.tradeDetailBtn')
        .each(
            function() {
                $(this).on("click",function() {
	                $("#tradeDetailDiv").hide("blind", 500);
	                $.ajax({
	                    type: "POST",
	                    url: getContextPath() +
	                        "/BackTestMain/QueryTradeDetail",
	                    data: {
	                        testId: $(this).val()
	                    },
	                    success: function(tradeDetailVO) {
	                        var tradeDetailVOList = tradeDetailVO.tradeLogList;
// 	                        $.each(tradeDetailVO.custColList, function(rowIndex, rowValue){
// 	                        	$.each(rowValue, function(colIndex, colValue){
// 		                        	console.log(colValue);
// 		                        })
// 		                        console.log("----------------------------");
// 	                        })
	                        
	                        tradeDetailHtml = "<table id = 'tradeDetailTable'  class='display'><thead><tr>";
	                        tradeDetailHtml += "<th width=\"100px\">標的</th><th width=\"100px\">建倉日</th><th width=\"90px\">建倉別</th>";
	                        tradeDetailHtml += "<th width=\"90px\">建倉價</th><th width=\"100px\">平倉日</th><th width=\"90px\">平倉別</th>";
	                     	tradeDetailHtml += "<th width=\"90px\">平倉價</th><th width=\"100px\">淨利</th><th width=\"100px\">淨利率</th>";
	                     	//客製化欄位
	                     	if (tradeDetailVO.custColList != null) {
	                     		$.each(tradeDetailVO.custColList[0], function(colIndex, colValue){
		                        	tradeDetailHtml += "<th width=\"90px\">"+colValue+"</th>";
		                        })
	                     	}
	                     	tradeDetailHtml += "</tr></thead><tbody>";
	                        for (var tradeDetailIndex = 0; tradeDetailIndex < tradeDetailVOList.length; tradeDetailIndex++) {
	                            var grossProfit = parseInt(tradeDetailVOList[tradeDetailIndex].grossProfit
	                                .replace( "$", "") .replace(  ",", ""));
	                            tradeDetailHtml += "<tr><td>" + tradeDetailVOList[tradeDetailIndex].stockName + "</td>";
	                            
	                            //建倉日
	                            tradeDetailHtml += "<td>";
	                            $.each(tradeDetailVOList[tradeDetailIndex].buyDateList, function(i,buyDate){
	                            	tradeDetailHtml += "<a onClick='downloadCandleChart(\"" +
	                                tradeDetailVOList[tradeDetailIndex].sid + "\", \"" +
	                                buyDate + "\")'>" +
	                                buyDate + "</a><br/>";
	                            });
                                tradeDetailHtml += "</td><td>";
                                
                                //建倉別
                                $.each(tradeDetailVOList[tradeDetailIndex].buyTypeList, function(i,buyType){
                                	tradeDetailHtml += buyType;
                                	tradeDetailHtml += "<br/>";
                                });
                            	tradeDetailHtml += "</td><td>";
	                            
                            	//建倉價
                              $.each(tradeDetailVOList[tradeDetailIndex].buyPriceList, function(i,buyPrice){
                                  tradeDetailHtml += buyPrice;
                                  tradeDetailHtml += "<br/>";
                              });
                            	tradeDetailHtml += "</td>";
                            	
                            	//平倉日
	                            tradeDetailHtml += "<td><a onClick='downloadCandleChart(\"" +
	                                tradeDetailVOList[tradeDetailIndex].sid + "\", \"" +
	                                tradeDetailVOList[tradeDetailIndex].sellDate + "\")'>" +
	                                tradeDetailVOList[tradeDetailIndex].sellDate + "</a></td>";
	                                
	                            //平倉別
	                            tradeDetailHtml += "<td>" + tradeDetailVOList[tradeDetailIndex].sellType + "</td>";
	                            
	                          	//平倉價
	                            tradeDetailHtml += "<td>" + tradeDetailVOList[tradeDetailIndex].sellPrice + "</td>";
	                            
	                            //淨利
	                            tradeDetailHtml += "<td>" + tradeDetailVOList[tradeDetailIndex].grossProfit + "</td>";
	                            
	                          	//淨利率
	                            tradeDetailHtml += "<td>" + tradeDetailVOList[tradeDetailIndex].grossProfitRatio + "</td>";
	                            
	                            //客製化欄位
	                            if (tradeDetailVO.custColList != null) {
	                            	$.each(tradeDetailVO.custColList[tradeDetailIndex + 1], function(colIndex, colValue){
			                        	tradeDetailHtml += "<td>"+colValue+"</td>";
			                        })
	                            }
	                            tradeDetailHtml += "</tr>";
	                        }
	                        tradeDetailHtml += "</tbody></table>";
	                        $("#tradeDetailDiv").show();
	                        $("#tradeDetailTableDiv").html(tradeDetailHtml);
	                        $("#tradeDetailTable").DataTable({"pageLength": 50, "order": [[ 1, 'asc' ]]});
	                        
	                        //未實現損益淨值圖
	                        setHighChart('unRealizeNetValue', "未實現損益淨值圖", "Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in"
	                        		,"淨值","Net Value", tradeDetailVO.netValueTsJsonArray)
                       		//已實現損益淨值圖
   	                        setHighChart('realizeNetValue', "已實現損益淨值圖", "Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in"
   	                        		,"淨值","Net Value", tradeDetailVO.rnetValueTsJsonArray)
	                    },
	                    error: function(data) {
	                        alert("查詢交易明細失敗");
	                    }
	                });
	            })
            });

}

function showBackTestResultByTestDesc(testDesc) {
    $.ajax({
        type: "POST",
        url: getContextPath() +
            "/BackTestMain/findBackTestResultByTestDesc",
        data: {
            "testDesc": testDesc
        },
        success: function(res) {
            if (res.errMsg == "") {
                alert(res.errMsg);
                return;
            }
            //顯示查詢結果區
            $("#testResultTable").html("");
            $("#testResultTable").append(res.dataTableHtml);
            setTradeDetailEvent();
        }
    });
}

function setHistoryTestSelectEvent() {
    $("#historyTestSelect").selectmenu({
        change: function(event, data) {
            if (data.item.value === null) {
                return;
            }
            showBackTestResultByTestDesc(data.item.value);
            $("#tradeDetailDiv").hide();
            $("#tradeDetailTableDiv").html("");
        }
    });
}
function downloadStrategySource(testId){
	window.location.href = getContextPath() + "/BackTestMain/downloadStrategySource/" + testId;
}
$(function() {
    setHistoryTestSelectEvent();

    $("#candleChartDiv").dialog({
        height: 700,
        width: 1100,
        autoOpen: false,
        modal: true
    });

    $("#refreshBackTest").click(
        function() {
            $("#historyTestSelect>option").each(function() {
                this.remove();
            })
            $("#historyTestSelect").selectmenu('destroy');
            $.ajax({
                type: "POST",
                url: getContextPath() +
                    "/BackTestMain/getBackTestIdList",
                success: function(res) {
                    $("#historyTestSelect").append(
                        "<option selected></option>");
                    $.each(res, function(index, value) {
                        $("#historyTestSelect").append(
                            "<option value=\"" + value + "\">" + value +
                            "</option>");
                    })
                    setHistoryTestSelectEvent();
                }
            });
        })

    if ("${testId}" != "") {
        showBackTestResultByTestDesc("${testId} -");
    }
});
</script>
<style>
.sidItem {
	border: 0.5px gray solid;
	margin: 2px;
	border-radius: 3px;
	text-align: center
}

.sidItem:hover {
	background-color: #E0E0E0;
}

.selectedSidItem {
	background-color: #E0E0E0;
}

.sidSelectedRegion {
	border: 1px black solid;
	border-radius: 3px;
	width: 40%;
}

.chooseSidBtn {
	width: 20px;
	heigth: 10px;
	size: 10px;
}
</style>
</head>
<body>
	<div id="testResultDiv">
		<fieldset>
			<legend>回測結果</legend>
			<table>
				<tr>
					<select id="historyTestSelect">
						<option></option>
						<c:forEach var="histTestId" items="${histTestIdList}">
							<option value="${histTestId}">
								<c:out value="${histTestId}" />
							</option>
						</c:forEach>
					</select>
					<button type="button" class="btn btn-primary" id="refreshBackTest">刷新</button>
				</tr>
			</table>
			<table>
				<div id="testResultTable"></div>
			</table>
		</fieldset>
	</div>
	<div id="tradeDetailDiv" title="交易明細" style="display: none">
		<fieldset>
			<legend>交易明細</legend>
			<figure class="highcharts-figure">
			  <div id="unRealizeNetValue"></div>
			</figure>
			<figure class="highcharts-figure">
			  <div id="realizeNetValue"></div>
			</figure>
			<div id="tradeDetailTableDiv" style="width: 100%"></div>
		</fieldset>
	</div>
	<div id="candleChartDiv">
		<img id="candleChartImg" />
	</div>
	<!-- 	<form id="downloadCandleChartForm" action="downloadCandleChart" method="POST" type="hidden"> -->
	<!-- 		<input id="sid" name="sid" type="hidden"/> -->
	<!-- 		<input id="date" name="date" type="hidden"/> -->
	<!-- 	</form> -->

</body>
</html>