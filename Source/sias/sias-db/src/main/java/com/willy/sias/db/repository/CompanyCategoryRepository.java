package com.willy.sias.db.repository;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.CompanyCategoryPO;

@Repository
public interface CompanyCategoryRepository extends JpaRepository<CompanyCategoryPO, Integer> {
	@Modifying
	@Transactional
	@Query("delete from CompanyCategoryPO where dataDate = ?1 and sid in (?2)")
	void deleteByDataDateAndSidIn(Date dataDate, Set<Integer> sidSet);
	Set<CompanyCategoryPO> findByDataDateAndCategoryIdEquals(Date dataDate, int categoryid);
	Set<CompanyCategoryPO> findByDataDateAndCategoryIdIn(Date dataDate, List<Integer> categoryidList);
	List<CompanyCategoryPO> findByCategoryIdIs(Integer categoryId);
	CompanyCategoryPO findTop1ByDataDateLessThanEqualAndSidIsAndCategoryIdIs(Date dataDate, Integer sid, Integer categoryId);
	@Query("select cc from CompanyCategoryPO cc "
			+ "where cc.dataDate = (select max(cc1.dataDate) from CompanyCategoryPO cc1) "
			+ " and cc.categoryId in (:categoryIdSet)")
	List<CompanyCategoryPO> findLastestCompanyCategoryByDataDateAndCategoryIdEquals(Collection<Integer> categoryIdSet);
	
	@Query("select sid from CompanyCategoryPO cc where cc.categoryId in (" + 
			"	select sc.cfgId from SysConfigPO sc where sc.parentId in (1,48) " + 
			"	and sc.cfgDesc not in ('管理股票','受益證券','指數投資證券(ETN)','台灣存託憑證(TDR)','認購售權證','認股權憑證','牛熊證(不含展延型牛熊證)','展延型牛熊證')" + 
			") group by sid")
	List<Integer> findSidWithoutDerivative();

	List<CompanyCategoryPO> findByDataDateIsAndSidIs(Date date, Integer sid);
}