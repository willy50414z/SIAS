package com.willy.sias.util;

import java.io.File;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.willy.net.mail.MsgType;
import com.willy.net.mail.WMail;
import com.willy.util.log.WLog;

@Service
public class MailUtil {
	private WMail wMail;
	@Value("${spring.mail.receiver.info}")
	private String infoReceiver;
	@Value("${spring.mail.receiver.error}")
	private String errorReceiver;

	public MailUtil(@Autowired WMail wMail, @Value("${spring.mail.sender}") String sender, @Value("${spring.mail.sendFrom}") String sendFrom) {
		super();
		this.wMail = wMail;
		wMail.setSender(sender);
		wMail.setSendFrom(sendFrom);
	}
	
	public void info(String subject, String content) {
		try {
			wMail.setContent(MsgType.Html, content);
			wMail.send("INFO: " +  subject, Arrays.asList(infoReceiver.split(";")));
		} catch (Exception e) {
			WLog.error(e);
		}
	}
	public void info(String subject, String content, File... attachments) {
		try {
			for(File attachment : attachments) {
				wMail.addAttachment(attachment);
			}
			wMail.setContent(MsgType.Html, content);
			wMail.send("INFO: " +  subject, Arrays.asList(infoReceiver.split(";")));
		} catch (Exception e) {
			WLog.error(e);
		}
	}
	public void error(String subject, String content) {
		try {
			wMail.setContent(MsgType.Html, content);
			wMail.send("ERROR: " +  subject, Arrays.asList(errorReceiver.split(";")));
		} catch (Exception e) {
			WLog.error(e);
		}
	}
	
	
	
}
