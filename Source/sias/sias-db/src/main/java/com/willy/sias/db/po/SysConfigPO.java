package com.willy.sias.db.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * FieldMap
 * @author 
 */
@Table(name="SYS_Config")
@Entity
@Getter @Setter
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SysConfigPO implements Serializable {
    @Id
    @Column(name = "\"CFG_ID\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)  
    private Integer cfgId;

    @EqualsAndHashCode.Include
    @Column(name = "\"PARENT_ID\"")
    private Integer parentId;

    @EqualsAndHashCode.Include
    @Column(name = "\"CFG_VALUE\"")
    private String cfgValue;
    
    @Column(name = "\"CFG_DESC\"")
    private String cfgDesc;

    private static final long serialVersionUID = 1L;
}