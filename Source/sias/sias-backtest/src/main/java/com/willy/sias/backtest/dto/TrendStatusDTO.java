package com.willy.sias.backtest.dto;

import com.willy.sias.backtest.service.TechDmsService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.util.log.WLog;

public class TrendStatusDTO {
	private TechIdxService tiSvc;
	private TechDmsService tdSvc;
	private List<List<TimeSeriesDTO<BigDecimal>>> turningPointList;
	List<TimeSeriesDTO<BigDecimal>> aTurnTsList;
	List<TimeSeriesDTO<BigDecimal>> vTurnTsList;

	public TrendStatusDTO(TechIdxService ts, List<List<TimeSeriesDTO<BigDecimal>>> turningPointList) {
		super();
		this.tiSvc = ts;
		if (turningPointList == null || turningPointList.size() != 2) {
			WLog.error(
					"turningPointList.size()[" + (turningPointList == null ? "null" : turningPointList.size()) + "]");
		}
		this.turningPointList = turningPointList;
		this.aTurnTsList = turningPointList.get(0);
		this.vTurnTsList = turningPointList.get(1);
	}

	public int getPressureTrend() {
		List<TimeSeriesDTO<BigDecimal>> pressureTsList = new ArrayList<>();
		return tdSvc.getPSStatus(aTurnTsList, pressureTsList);
	}
	
	public int getSupportTrend() {
		List<TimeSeriesDTO<BigDecimal>> supportTsList = new ArrayList<>();
		return tdSvc.getPSStatus(vTurnTsList, supportTsList);
	}

	public BigDecimal getPressurePrice(Date date) {
		return tdSvc.getPSPrice(date, aTurnTsList);
	}

	public BigDecimal getSupportPrice(Date date) {
		return tdSvc.getPSPrice(date, vTurnTsList);
	}
	
}
