package com.willy.sias.web.vo;

import java.util.List;

import org.json.JSONArray;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TradeDetailVO {
	private String errMsg;
	private List<TradeLog_EachTrade> tradeLogList;
	private Object[] netValueTsJsonArray;
	private Object[] rNetValueTsJsonArray;
	private List<List<String>> custColList;
}
