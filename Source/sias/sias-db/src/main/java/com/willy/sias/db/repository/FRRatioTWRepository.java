package com.willy.sias.db.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.willy.sias.db.po.FRRatioTWPO;
import com.willy.sias.db.po.FRRatioTWPOKey;

public interface FRRatioTWRepository extends JpaRepository<FRRatioTWPO, FRRatioTWPOKey> {
	public List<FRRatioTWPO> findByFrDateLike(String frDate);
}
