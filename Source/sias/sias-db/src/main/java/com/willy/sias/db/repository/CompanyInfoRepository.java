package com.willy.sias.db.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.CompanyInfoPO;
import com.willy.sias.db.po.CompanyInfoPOKey;

@Repository
public interface CompanyInfoRepository extends JpaRepository<CompanyInfoPO, CompanyInfoPOKey> {
	@Query(value = "select ci.* from CompanyInfoPO ci where ci.dataDate = (select max(ci1.datatDate) from CompanyInfoPO ci1)", nativeQuery = true)
	public Set<CompanyInfoPO> findLastestCompanyInfo();
}
