package com.willy.sias.batch.processor;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.BatchLogPO;
import com.willy.sias.db.po.IdxDailyUsPO;
import com.willy.sias.db.repository.BatchLogRepository;
import com.willy.sias.db.repository.IdxDailyUsRepository;
import com.willy.sias.util.EnumSet.BatchName;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Setter;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@Service
@Setter
@ConfigurationProperties(prefix = "sias.batch.cboe-vix")
public class CBOE_VIX extends BaseBatchProcessor {

  @Autowired
  private WJsoup js;

  @Autowired
  private IdxDailyUsRepository idxRepo;
  @Autowired
  private BatchLogRepository batchLogRepo;

  @Override
  public void cleanHisData() throws Exception {

  }

  @Override
  protected void exec() throws Exception {
    this.putMapParam("startyyyy", WDate.getDateInfo(this.dataDate, Calendar.YEAR));
    this.putMapParam("startMM", WDate.getDateInfo(this.dataDate, Calendar.MONTH));
    this.putMapParam("startdd", WDate.getDateInfo(this.dataDate, Calendar.DAY_OF_MONTH));

    Date yesterday = WDate.addDay(new Date(), -1);
    this.putMapParam("endyyyy", WDate.getDateInfo(yesterday, Calendar.YEAR));
    this.putMapParam("endMM", WDate.getDateInfo(yesterday, Calendar.MONTH));
    this.putMapParam("enddd", WDate.getDateInfo(yesterday, Calendar.DAY_OF_MONTH));

    IdxDailyUsPO idxPo;
    Set<IdxDailyUsPO> idxSet = new HashSet<>();
    List<String> rawDataList;
    List<String> prePageRawDataList = null;

    //限制最多跑500頁，避免無窮迴圈，20000101-20221118也才28x頁
    for (int pageIdx = 0; pageIdx < 500; pageIdx++) {
      //逐頁爬出VIX資料
      this.putMapParam("pageIdx", pageIdx);
      List<Element> dataTable = js.get(
              this.parseMapParamToString(this.urlTempl),
              Const._ENCODING_UTF8)
          .getElementsByTag("div").stream().filter(div -> div.attr("data-tab-pane").equals("Daily"))
          .findAny().get().getElementsByTag("tbody");

      //回空資料表示已經到最後一頁
      if (dataTable.size() == 0) {
        break;
      } else {
        rawDataList = dataTable.get(0).getElementsByTag("tbody").get(0)
            .getElementsByTag("div").stream()
            .filter(div -> div.className().equals("cell__content") || div.className()
                .equals("cell__content u-secondary")).map(Element::text).collect(
                Collectors.toList());
      }

      //取出資料與前頁相同，也表示已到最後一頁
      if (rawDataList.equals(prePageRawDataList)) {
        break;
      }

      for (int i = 0; i < rawDataList.size(); i += 5) {
        idxPo = new IdxDailyUsPO();
        idxPo.setSid(Const._CFG_ID_VIX_INDEX);
        idxPo.setDataDate(WType.strToDate(rawDataList.get(i), "MM/dd/yyyy"));
        idxPo.setOpen(new BigDecimal(rawDataList.get(i + 1)));
        idxPo.setHigh(new BigDecimal(rawDataList.get(i + 2)));
        idxPo.setLow(new BigDecimal(rawDataList.get(i + 3)));
        idxPo.setClose(new BigDecimal(rawDataList.get(i + 4)));
        idxSet.add(idxPo);
      }
      idxRepo.saveAll(idxSet);
      batchLogRepo.saveAll(idxSet.stream().map(
              idx -> new BatchLogPO(idx.getDataDate(), BatchName.CBOE_VIX.toString(), 1, new Date()))
          .collect(
              Collectors.toList()));
      idxSet.clear();
      prePageRawDataList = rawDataList;
    }
  }
}
