package com.willy.sias.db.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Table(name="MARGIN_TRADE_INFO")
@Entity
@IdClass(MarginTradeInfoPOKey.class)
@Data
public class MarginTradeInfoPO {
	@Id
	@Column(name = "SID")
	private int sid;
	
	@Id
	@Column(name = "DATA_DATE")
	private Date dataDate;

	@Column(name = "FIN_BUY")
	private int finBuy;

	@Column(name = "FIN_SELL")
	private int finSell;

	@Column(name = "FIN_REPAY")
	private int finRepay;

	@Column(name = "FIN_BALANCE")
	private int finBalance;

	@Column(name = "FIN_QUOTA")
	private BigDecimal finQuota;

	@Column(name = "LEND_BUY")
	private int lendBuy;

	@Column(name = "LEND_SELL")
	private int lendSell;

	@Column(name = "LEND_REPAY")
	private int lendRepay;

	@Column(name = "LEND_BALANCE")
	private int lendBalance;

	@Column(name = "LEND_QUOTA")
	private BigDecimal lendQuota;

	@Column(name = "MEMO")
	private String memo ;
}
