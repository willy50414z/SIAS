package com.willy.sias.db.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Table(name="MONTHLY_REVENUE_TW")
public class MonthlyRevenueTWPOKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 股票代號
     */
    @Column(name = "SID")
    private int sid;
    
    /**
     * 營收所屬日期
     */
    @Column(name = "REVENUE_DATE")
    private String revenueDate;
}
