package com.willy.sias.backtest.service;

import com.willy.sias.backtest.dto.BackTestInfoDTO;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradePlanDTO;
import com.willy.sias.backtest.strategy.TradeStrategy;
import com.willy.sias.backtest.util.TradeUtil;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfitAnalyzeService {
	@Autowired
	private CalendarRepository caRepo;
	@Autowired
	private TradeUtil tu;

	/**
	 *
	 * @param btInfo
	 * @return
	 * @throws ParseException
	 * @throws BeansException
	 * @throws MalformedURLException
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public ProfitDTO getStrategyTradeResult(BackTestInfoDTO btInfo) throws ParseException, BeansException, MalformedURLException, ClassNotFoundException, IllegalArgumentException, IllegalAccessException, InstantiationException {
		/**
		 * 初始化profitPo
		 */
		ProfitDTO profitDTO = new ProfitDTO();
		// 設定初始資金
		profitDTO.setOriginalFunds(btInfo.getFunds());
		profitDTO.setAcctBalance(btInfo.getFunds());

		// 撈出交易日
		List<CalendarPO> caList;
		if (btInfo.getStartDate() == null && btInfo.getEndDate() == null) {
			caList = caRepo.findByDateTypeIsOrderByDate(Const._CFG_ID_CALENDAR_STOCK_BUS_DATE);
		} else if (btInfo.getStartDate() == null) {
			caList = caRepo.findByDateTypeIsAndDateLessThanEqual(Const._CFG_ID_CALENDAR_STOCK_BUS_DATE,
					btInfo.getEndDate());
		} else if (btInfo.getStartDate() == null) {
			caList = caRepo.findByDateTypeIsAndDateGreaterThanEqual(Const._CFG_ID_CALENDAR_STOCK_BUS_DATE,
					btInfo.getStartDate());
		} else {
			caList = caRepo.findByDateTypeIsAndDateBetween(Const._CFG_ID_CALENDAR_STOCK_BUS_DATE, btInfo.getStartDate(),
					btInfo.getEndDate());
		}
		List<Date> allTradeDateList = caList.stream().map(ca -> ca.getDate()).collect(Collectors.toList());
		profitDTO.setFirstTradeDate(allTradeDateList.get(0));
		profitDTO.setLastTradeDate(allTradeDateList.get(allTradeDateList.size() - 1));
		profitDTO.setTradeDetailList(new ArrayList<>());
		profitDTO.setSidSet(btInfo.getSidSet());

		/**
		 * 多執行續執行交易策略
		 */
		// 依交易邏輯建立買賣資訊
		TradeStrategy bsStrategy = null;
		//Muti-Thread
		List<Future<Integer>> resultList = new ArrayList<>();
		ExecutorService fixedThreadPool = Executors.newFixedThreadPool(20);
		for(int sid : profitDTO.getSidSet()) {
			bsStrategy = getTradeStrategy(btInfo.getStrategyName());
			bsStrategy.setSid(sid);
			bsStrategy.setTradeDateList(allTradeDateList);
			resultList.add(fixedThreadPool.submit(bsStrategy));
		}
		long completeCount = 0;
		int totalCount = profitDTO.getSidSet().size();
		while(completeCount < totalCount) {
			completeCount = resultList.stream().filter(r -> r.isDone()).count();
			WLog.info("交易計畫蒐集進度: " + new BigDecimal(completeCount).divide(new BigDecimal(totalCount), 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).setScale(2) + "% ("+completeCount+"/"+totalCount+")");
			try {Thread.sleep(10000);} catch (InterruptedException e) {}
		}

//		// Single Thread
//		bsStrategy = getTradeStrategy(strategyClassName);
//		PrintClass.total = profitPo.getSidSet().size();
//		new Thread(new PrintClass()).start();
//		for (int sid : profitPo.getSidSet()) {
//			bsStrategy.setSid(sid);
//			bsStrategy.setTradeDateList(allTradeDateList);
//			bsStrategy.collectTradePlanList();
//			PrintClass.current++;
//		}
//		PrintClass.isRun = false;
		
		//Log 買賣計畫
		WLog.info("Strategy Trade Size[" + TradeStrategy.getTradePlanList().size() + "]");
		TradeStrategy.getTradePlanList().stream().sorted((o1, o2) -> {
			if (o1.getSid() == o2.getSid()) {
				return new Long(o1.getTradeDate().getTime()).compareTo(
						new Long(o2.getTradeDate().getTime()));
			} else {
				return new Long(o1.getSid()).compareTo(new Long(o2.getSid()));
			}
		}).forEach(tp -> {
			WLog.info("sid[" + tp.getSid() + "]tradeDate[" + tp.getTradeDate() + "]tradeType["
					+ tp.getTradeType() + "]tradePrice[" + tp.getTradePrice()
					+ "]tradeShares[" + tp.getTradeShares() + "]");
		});
		
		//將交易計畫進行回測
		new Thread(new ProcessListener(TradeStrategy.getTradePlanList().size())).start();
		TradeStrategy.getTradePlanList().stream().sorted(new Comparator<TradePlanDTO>() {
			@Override
			public int compare(TradePlanDTO o1, TradePlanDTO o2) {
				return o1.getTradeDate().compareTo(o2.getTradeDate());
			}
		}).forEach(tp -> {
			tu.trade(tp.getSid(), tp.getTradeDate(), 1000, tp.getTradeType(), tp.getTradePrice(),
					profitDTO);
			ProcessListener.currentProcess++;
//			try {
//				Thread.sleep(3000);
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		});
		ProcessListener.isRun = false;
		
		if(bsStrategy != null) {
			bsStrategy.collectTradePlanInfo(profitDTO);
		}
		
		return profitDTO;
	}

	private TradeStrategy getTradeStrategy(String strategyClassName) {
		try {
			return (TradeStrategy) BeanFactory.getBean(Class.forName(Const._STRATEGY_PACKAGE_NAME + "." + strategyClassName));
		} catch (BeansException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		;
		return null;
	}
	
//	private TradeStrategy getTradeStrategy(String strategyClassName) {
//		try {
//			return (TradeStrategy) context.getBean(Class.forName(strategyClassName));
//		} catch (BeansException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		;
//		return null;
//	}

	private int getAnalyzeType(TradeStrategy ts) {
		try {
			ts.getClass().getDeclaredMethod("setTradeDetail", Date.class, ProfitDTO.class);
			return 1;
		} catch (Exception e) {
		}
		try {
			ts.getClass().getDeclaredMethod("setTradeDetail", Integer.class, List.class, ProfitDTO.class);
			return 2;
		} catch (Exception e) {
		}
		return -1;
	}

//	public List<TradeDetailPO> getProfitReport(int sid, List<TradeDetailPO> bsInfoList, boolean toNextTrade)
//			throws ParseException {
//		// 填入買賣價格
//		fillBuyAndSellPrice(sid, bsInfoList, toNextTrade);
//		return bsInfoList;
//	}

//	public List<MonthlyProfitPO> getMonthlyProfit(int sid, List<TradeDetailPO> tradeInfoList, boolean toNextTrade)
//			throws ParseException {
//		// 填入買賣價格
//		fillBuyAndSellPrice(sid, tradeInfoList, toNextTrade);
//		// 將各期成長率依月份做分類
//		Integer month;
//		Map<Integer, List<BigDecimal>> monthlyGrowthMap = new HashMap<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			month = WDate.getDateInfo(tradeInfo.getBuyDate(), Calendar.MONTH);
//			WLog.debug(tradeInfo.toString());
//			if (monthlyGrowthMap.containsKey(month)) {
//				monthlyGrowthMap.get(month).add(tradeInfo.getGrossProfitRatio());
//			} else {
//				List<BigDecimal> growthList = new ArrayList<>();
//				growthList.add(tradeInfo.getGrossProfitRatio());
//				monthlyGrowthMap.put(month, growthList);
//			}
//		}
//
//		// 將各月份成長率作統計計算，並依月分做排序
//		MonthlyProfitPO monthlyProfit;
//		List<MonthlyProfitPO> monthlyProfitList = new ArrayList<>();
//		for (Integer growthMonth : monthlyGrowthMap.keySet()) {
//			monthlyProfit = applicationContext.getBean(MonthlyProfitPO.class);
//			monthlyProfit.setMonth(growthMonth);
//			monthlyProfit.setGrowthList(monthlyGrowthMap.get(growthMonth));
//			monthlyProfitList.add(monthlyProfit);
//		}
//		Collections.sort(monthlyProfitList, new Comparator<MonthlyProfitPO>() {
//			@Override
//			public int compare(MonthlyProfitPO o1, MonthlyProfitPO o2) {
//				// TODO Auto-generated method stub
//				return Integer.compare(o1.getMonth(), o2.getMonth());
//			}
//		});
//		return monthlyProfitList;
//	}
//
//		public String getMonthlyProfitReport(List<MonthlyProfitPO> monthlyProfitList) throws ParseException {
//		StringBuffer sb = new StringBuffer();
//		sb.append("月份成長率分析\r\n");
//
//		sb.append("月份").append("\t").append("平均成長率").append("\t").append("成長率標準差").append("\t").append("單月勝率\r\n");
//
//		for (MonthlyProfitPO monthlyProfitPo : monthlyProfitList) {
//			sb.append(monthlyProfitPo.getMonth()).append("\t").append(monthlyProfitPo.getMean()).append("\t")
//					.append(monthlyProfitPo.getSd()).append("\t").append(monthlyProfitPo.getGrowRatio()).append("\r\n");
//		}
//		return sb.toString();
//	}

//	public List<WeeklyProfitPO> getWeeklyProfit(int sid, List<TradeDetailPO> tradeInfoList, boolean toNextTrade)
//			throws ParseException {
//		// 填入買賣價格
//		fillBuyAndSellPrice(sid, tradeInfoList, toNextTrade);
//		// 將各期成長率依月份做分類
//		Integer month;
//		Integer partOfMonth;
//		String growthMapKey;
//		Map<String, List<BigDecimal>> weeklyGrowthMap = new HashMap<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			month = WDate.getDateInfo(tradeInfo.getBuyDate(), Calendar.MONTH);
//			partOfMonth = WDate.getPartOfMonth(tradeInfo.getBuyDate());
//			growthMapKey = month + "-" + partOfMonth;
//			WLog.debug(tradeInfo.toString());
//			if (weeklyGrowthMap.containsKey(growthMapKey)) {
//				weeklyGrowthMap.get(growthMapKey).add(tradeInfo.getGrossProfitRatio());
//			} else {
//				List<BigDecimal> growthList = new ArrayList<>();
//				growthList.add(tradeInfo.getGrossProfitRatio());
//				weeklyGrowthMap.put(growthMapKey, growthList);
//			}
//		}
//
//		// 將各月份成長率作統計計算，並依月分做排序
//		WeeklyProfitPO weeklyProfit;
//		List<WeeklyProfitPO> weeklyProfitList = new ArrayList<>();
//		for (String weekGrowthKey : weeklyGrowthMap.keySet()) {
//			weeklyProfit = applicationContext.getBean(WeeklyProfitPO.class);
//			weeklyProfit.setMonth(Integer.valueOf(weekGrowthKey.split("-")[0]));
//			weeklyProfit.setWeek(Integer.valueOf(weekGrowthKey.split("-")[1]));
//			weeklyProfit.setGrowthList(weeklyGrowthMap.get(weekGrowthKey));
//			weeklyProfitList.add(weeklyProfit);
//		}
//		Collections.sort(weeklyProfitList, new Comparator<WeeklyProfitPO>() {
//			@Override
//			public int compare(WeeklyProfitPO o1, WeeklyProfitPO o2) {
//				// TODO Auto-generated method stub
//				return Integer.compare(o1.getMonth(), o2.getMonth()) * 3 + Integer.compare(o1.getWeek(), o2.getWeek());
//			}
//		});
//		return weeklyProfitList;
//	}

//	public String getWeeklyProfitReport(List<WeeklyProfitPO> weeklyProfitList) throws ParseException {
//		StringBuffer sb = new StringBuffer();
//		sb.append("月份成長率分析\r\n");
//
//		sb.append("月份").append("\t").append("月初/中/末").append("\t").append("平均成長率").append("\t").append("成長率標準差")
//				.append("\t").append("單月勝率\r\n");
//
//		for (WeeklyProfitPO weeklyProfitPo : weeklyProfitList) {
//			sb.append(weeklyProfitPo.getMonth()).append("\t").append(weeklyProfitPo.getWeek()).append("\t")
//					.append(weeklyProfitPo.getMean()).append("\t").append(weeklyProfitPo.getSd()).append("\t")
//					.append(weeklyProfitPo.getGrowRatio()).append("\r\n");
//		}
//		return sb.toString();
//	}
//
//	public String getWeeklyProfitReport(int sid, Set<TradeDetailPO> bsInfoList, boolean toNextTrade)
//			throws ParseException {
//		return getWeeklyProfitReport(getWeeklyProfit(sid, bsInfoList, toNextTrade));
//	}
//
//	public List<List<String>> monthlyProfitAnalyze(int sid, Set<TradeDetailPO> tradeInfoList, boolean toNextTrade)
//			throws ParseException {
//		// 填入買賣價格
//		fillBuyAndSellPrice(sid, tradeInfoList, toNextTrade);
//		// Group growth by month
//		Integer month;
//		Map<Integer, List<BigDecimal>> monthlyGrowthMap = new HashMap<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			month = WDate.getDateInfo(tradeInfo.getTradeDate(), Calendar.MONTH);
//			LOGGER.debug(tradeInfo.toString());
//			if (monthlyGrowthMap.containsKey(month)) {
//				monthlyGrowthMap.get(month).add(tradeInfo.getGrowthRatio());
//			} else {
//				List<BigDecimal> growthList = new ArrayList<>();
//				growthList.add(tradeInfo.getGrowthRatio());
//				monthlyGrowthMap.put(month, growthList);
//			}
//		}
//		// 組出分析結果
//		List<String> row;
//		List<List<String>> result = new ArrayList<>();
//
//		// 分析Header
//		row = new ArrayList<>();
//		row.add("月份成長率分析");
//		result.add(row);
//
//		// Title
//		row = new ArrayList<>();
//		row.add("月份");
//		row.add("平均成長率");
//		row.add("成長率標準差");
//		row.add("單月勝率");
//		result.add(row);
//
//		// Content
//		List<List<String>> contentList = new ArrayList<>();
//		for (Integer growthMonth : monthlyGrowthMap.keySet()) {
//			if (monthlyGrowthMap.get(growthMonth).size() < 10) {
//				LOGGER.info(growthMonth + "月樣本數過少，排除計算");
//				continue;
//			}
//			row = new ArrayList<>();
//			// 月份
//			row.add(String.valueOf(growthMonth));
//			// 平均成長率
//			row.add(statisticService.getMean(monthlyGrowthMap.get(growthMonth)).toString());
//			// 成長率標準差
//			row.add(statisticService.getSd(monthlyGrowthMap.get(growthMonth)).toString());
//			// 單月勝率
//			row.add(statisticService.getSuccessRatio(monthlyGrowthMap.get(growthMonth), ">0.005").toString());
//			contentList.add(row);
//		}
//		contentList.sort(new Comparator<List<String>>() {
//			@Override
//			public int compare(List<String> o1, List<String> o2) {
//				// TODO Auto-generated method stub
//				return Integer.valueOf(o1.get(0)).compareTo(Integer.valueOf(o2.get(0)));
//			}
//		});
//		result.addAll(contentList);
//
//		// 清除空間
//		monthlyGrowthMap.clear();
//
//		// 分析Header
//		row = new ArrayList<>();
//		row.add("月份最佳投資期間分析");
//		result.add(row);
//
//		// Title
//		row = new ArrayList<>();
//		row.add("月份");
//		row.add("期間");
//		row.add("平均成長率");
//		row.add("成長率標準差");
//		row.add("單月勝率");
//		result.add(row);
//
//		// Group growth by 月初 月中 月底
//		int dayOfMonth;
//		String periodKey;
//		Map<String, List<BigDecimal>> monthPeriodGrowthMap = new HashMap<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			month = WDate.getDateInfo(tradeInfo.getTradeDate(), Calendar.MONTH);
//			dayOfMonth = WDate.getDateInfo(tradeInfo.getTradeDate(), Calendar.DAY_OF_MONTH);
//			if (dayOfMonth < 11) {
//				// 月初
//				periodKey = "Ear";
//			} else if (dayOfMonth > 10 && dayOfMonth < 21) {
//				// 月中
//				periodKey = "Mid";
//			} else {
//				// 月底
//				periodKey = "End";
//			}
//
//			String monthPeriodKey = month + periodKey;
//			if (monthPeriodGrowthMap.containsKey(monthPeriodKey)) {
//				monthPeriodGrowthMap.get(monthPeriodKey).add(tradeInfo.getGrowthRatio());
//			} else {
//				List<BigDecimal> growthList = new ArrayList<>();
//				growthList.add(tradeInfo.getGrowthRatio());
//				monthPeriodGrowthMap.put(monthPeriodKey, growthList);
//			}
//		}
//
//		// Content
//		contentList = new ArrayList<>();
//		for (String growthMonth : monthPeriodGrowthMap.keySet()) {
//			if (monthPeriodGrowthMap.get(growthMonth).size() < 10) {
//				LOGGER.info(growthMonth + "月樣本數過少，排除計算");
//				continue;
//			}
//			row = new ArrayList<>();
//			// 月份
//			row.add(growthMonth.substring(0, growthMonth.length() - 3));
//			// 期間
//			row.add(growthMonth.substring(growthMonth.length() - 3));
//			// 平均成長率
//			row.add(statisticService.getMean(monthPeriodGrowthMap.get(growthMonth)).toString());
//			// 成長率標準差
//			row.add(statisticService.getSd(monthPeriodGrowthMap.get(growthMonth)).toString());
//			// 單月勝率
//			row.add(statisticService.getSuccessRatio(monthPeriodGrowthMap.get(growthMonth), ">0.005").toString());
//			contentList.add(row);
//		}
//		contentList.sort(new Comparator<List<String>>() {
//			@Override
//			public int compare(List<String> o1, List<String> o2) {
//				// TODO Auto-generated method stub
//				return Integer.valueOf(o1.get(0)).compareTo(Integer.valueOf(o2.get(0)));
//			}
//		});
//		result.addAll(contentList);
//		return result;
//	}

//	/**
//	 * 填入買賣價格
//	 * 
//	 * @param sid
//	 * @param tradeDetailList
//	 * @param toNextTradeDate
//	 * @throws ParseException
//	 */
//	public List<TradeDetailPO> fillBuyAndSellPrice(List<TradeDetailPO> tradeDetailList, boolean toNextTradeDate)
//			throws ParseException {
//		// 依sid排序tradeDetail，將同sid的聚集在一起
//		tradeDetailList.sort(new Comparator<TradeDetailPO>() {
//			@Override
//			public int compare(TradeDetailPO o1, TradeDetailPO o2) {
//				// TODO Auto-generated method stub
//				return o1.getSid().compareTo(o2.getSid());
//			}
//		});
//
//		// 依sid分別填入實際交易日
//		Integer sid = 0;
//		List<TradeDetailPO> tempTradeDetailList = null;
//		List<TradeDetailPO> realTradeDetailList = new ArrayList<>();
//		for (TradeDetailPO tradeDetail : tradeDetailList) {
//			// 新標的
//			if (tradeDetail.getSid().compareTo(sid) != 0) {
//				// 填入金額並加入List
//				if (tempTradeDetailList != null && tempTradeDetailList.size() > 0) {
//					fillBuyAndSellPrice(sid, tempTradeDetailList, toNextTradeDate);
//					realTradeDetailList.addAll(tempTradeDetailList);
//				}
//
//				// 初始化新List
//				sid = tradeDetail.getSid();
//				tempTradeDetailList = new ArrayList<>();
//			} else {
//				tempTradeDetailList.add(tradeDetail);
//			}
//		}
//
//		// 填入最後一組TradeDetail Price
//		fillBuyAndSellPrice(sid, tempTradeDetailList, toNextTradeDate);
//		realTradeDetailList.addAll(tempTradeDetailList);
//
//		return realTradeDetailList;
//	}

//	/**
//	 * 填入買賣價格
//	 * 
//	 * @param sid
//	 * @param tradeDetailList
//	 * @param toNextTradeDate
//	 * @throws ParseException
//	 */
//	public void fillBuyAndSellPrice(int sid, List<TradeDetailPO> tradeDetailList, boolean toNextTradeDate)
//			throws ParseException {
//		// 將預計交易日轉換為實際交易日
//		tradeDetailList = toNextTradeDate ? adjustToRealTradeDate(sid, tradeDetailList, 1)
//				: adjustToRealTradeDate(sid, tradeDetailList, -1);
//
//		// 將交易金額設定為收盤價
//		TimeSeriesDTO<BigDecimal> tsDto = null;
//		HashMap<Date, TimeSeriesDTO<BigDecimal>> closeMap = new HashMap<>();
//		List<TimeSeriesDTO<BigDecimal>> closePriceTsList = dtDao.findTdsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE);
//		for (TradeDetailPO tradeDetail : tradeDetailList) {
//			// 買價
//			if (closeMap.containsKey(tradeDetail.getBuyDate())) {
//				tsDto = closeMap.get(tradeDetail.getBuyDate());
//			} else {
//				tsDto = closePriceTsList.stream().filter(ts -> ts.getDate().equals(tradeDetail.getBuyDate()))
//						.findFirst().orElse(null);
//				closeMap.put(tradeDetail.getBuyDate(), tsDto);
//			}
//			if (tsDto == null) {
//				throw new IllegalArgumentException("未找到當日股價 tradeDetail[" + WString.toString(tradeDetail) + "]");
//			}
//			tradeDetail.setBuyPrice(tsDto.getDecimalValue());
//
//			// 賣價
//			if (closeMap.containsKey(tradeDetail.getSellDate())) {
//				tsDto = closeMap.get(tradeDetail.getSellDate());
//			} else {
//				tsDto = closePriceTsList.stream().filter(ts -> ts.getDate().equals(tradeDetail.getSellDate()))
//						.findFirst().orElse(null);
//				closeMap.put(tradeDetail.getSellDate(), tsDto);
//			}
//			if (tsDto == null) {
//				throw new IllegalArgumentException("未找到當日股價 tradeDetail[" + WString.toString(tradeDetail) + "]");
//			}
//			tradeDetail.setSellPrice(tsDto.getDecimalValue());
//		}
//	}
//	public void fillBuyAndSellPrice(int sid, Set<TradeDetailPO> tradeDetailList, boolean toNextTradeDate)
//			throws ParseException {
//		// 將預計交易日轉換為實際交易日
//		tradeDetailList = toNextTradeDate ? adjustToRealTradeDate(sid, tradeDetailList, 1)
//				: adjustToRealTradeDate(sid, tradeDetailList, -1);
//
//		// 將交易金額設定為收盤價
//		TimeSeriesDTO<BigDecimal> tsDto = null;
//		HashMap<Date, TimeSeriesDTO<BigDecimal>> closeMap = new HashMap<>();
//		List<TimeSeriesDTO<BigDecimal>> closePriceTsList = dtDao.findTdsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE);
//		for (TradeDetailPO tradeDetail : tradeDetailList) {
//			// 買價
//			if (closeMap.containsKey(tradeDetail.getBuyDate())) {
//				tsDto = closeMap.get(tradeDetail.getBuyDate());
//			} else {
//				tsDto = closePriceTsList.stream().filter(ts -> ts.getDate().equals(tradeDetail.getBuyDate()))
//						.findFirst().orElse(null);
//				closeMap.put(tradeDetail.getBuyDate(), tsDto);
//			}
//			if (tsDto == null) {
//				throw new IllegalArgumentException("未找到當日股價 tradeDetail[" + WString.toString(tradeDetail) + "]");
//			}
//			tradeDetail.setBuyPrice(tsDto.getDecimalValue());
//
//			// 賣價
//			if (closeMap.containsKey(tradeDetail.getSellDate())) {
//				tsDto = closeMap.get(tradeDetail.getSellDate());
//			} else {
//				tsDto = closePriceTsList.stream().filter(ts -> ts.getDate().equals(tradeDetail.getSellDate()))
//						.findFirst().orElse(null);
//				closeMap.put(tradeDetail.getSellDate(), tsDto);
//			}
//			if (tsDto == null) {
//				throw new IllegalArgumentException("未找到當日股價 tradeDetail[" + WString.toString(tradeDetail) + "]");
//			}
//			tradeDetail.setSellPrice(tsDto.getDecimalValue());
//		}
//	}
//
//	/**
//	 * 預計買入日若不為交易日，將延後至下一交易日
//	 * 
//	 * @param sid
//	 * @param tradeInfoList
//	 * @return
//	 * @throws ParseException
//	 */
//	public List<TradeDetailPO> adjustToRealTradeDate(int sid, List<TradeDetailPO> tradeInfoList, int adjustNum)
//			throws ParseException {
//		int maxGap = 5;
//		Date realTradeDate;
//		Set<Date> allTradeDateList = dtDao.getTradeDateSetOfTheTarget(sid);
//		// 逐個讀取預期購買日
//		Set<TradeDetailPO> noTradeDateList = new HashSet<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			// 預計buyDate及sell已是交易日則跳過
//			if (!allTradeDateList.contains(tradeInfo.getBuyDate())
//					&& !allTradeDateList.contains(tradeInfo.getSellDate())) {
//				// 買入日
//				realTradeDate = dateUtil.getTradeDate(allTradeDateList, tradeInfo.getBuyDate(), adjustNum);
//				if (realTradeDate == null) {
//					noTradeDateList.add(tradeInfo);
//					WLog.warn("沒有此預期交易日[" + WType.dateToStr(tradeInfo.getBuyDate()) + "]的實際交易日");
//					continue;
//				} else if (realTradeDate.after(WDate.calDate_Day(tradeInfo.getBuyDate(), maxGap))) {
//					WLog.warn("預計交易日(" + WType.dateToStr(tradeInfo.getBuyDate()) + ")與實際交易日("
//							+ WType.dateToStr(realTradeDate) + ")差異過大");
//					noTradeDateList.add(tradeInfo);
//					continue;
//				}
//				WLog.debug("買入日由[" + WType.dateToStr(tradeInfo.getBuyDate()) + "]調整為[" + realTradeDate + "]");
//				tradeInfo.setBuyDate(realTradeDate);
//
//				// 賣出日
//				realTradeDate = dateUtil.getTradeDate(allTradeDateList, tradeInfo.getSellDate(), adjustNum);
//				if (realTradeDate == null) {
//					noTradeDateList.add(tradeInfo);
//					WLog.warn("沒有此預期交易日[" + WType.dateToStr(tradeInfo.getSellDate()) + "]的實際交易日");
//					continue;
//				} else if (realTradeDate.after(WDate.calDate_Day(tradeInfo.getSellDate(), maxGap))) {
//					WLog.warn("預計交易日(" + WType.dateToStr(tradeInfo.getSellDate()) + ")與實際交易日("
//							+ WType.dateToStr(realTradeDate) + ")差異過大");
//					noTradeDateList.add(tradeInfo);
//					continue;
//				}
//				WLog.debug("賣出日由[" + WType.dateToStr(tradeInfo.getSellDate()) + "]調整為[" + realTradeDate + "]");
//				tradeInfo.setSellDate(realTradeDate);
//			}
//		}
//		// 移除沒有預期交易日的tradeDetailInfo
//		for (TradeDetailPO noTradeBSInfo : noTradeDateList) {
//			tradeInfoList.remove(noTradeBSInfo);
//		}
//		return tradeInfoList;
//	}
//	public Set<TradeDetailPO> adjustToRealTradeDate(int sid, Set<TradeDetailPO> tradeInfoList, int adjustNum)
//			throws ParseException {
//		int maxGap = 5;
//		Date realTradeDate;
//		Set<Date> allTradeDateList = dtDao.getTradeDateSetOfTheTarget(sid);
//		// 逐個讀取預期購買日
//		Set<TradeDetailPO> noTradeDateList = new HashSet<>();
//		for (TradeDetailPO tradeInfo : tradeInfoList) {
//			// 預計buyDate及sell已是交易日則跳過
//			if (!allTradeDateList.contains(tradeInfo.getBuyDate())
//					&& !allTradeDateList.contains(tradeInfo.getSellDate())) {
//				// 買入日
//				realTradeDate = dateUtil.getTradeDate(allTradeDateList, tradeInfo.getBuyDate(), adjustNum);
//				if (realTradeDate == null) {
//					noTradeDateList.add(tradeInfo);
//					WLog.warn("沒有此預期交易日[" + WType.dateToStr(tradeInfo.getBuyDate()) + "]的實際交易日");
//					continue;
//				} else if (realTradeDate.after(WDate.calDate_Day(tradeInfo.getBuyDate(), maxGap))) {
//					WLog.warn("預計交易日(" + WType.dateToStr(tradeInfo.getBuyDate()) + ")與實際交易日("
//							+ WType.dateToStr(realTradeDate) + ")差異過大");
//					noTradeDateList.add(tradeInfo);
//					continue;
//				}
//				WLog.debug("買入日由[" + WType.dateToStr(tradeInfo.getBuyDate()) + "]調整為[" + realTradeDate + "]");
//				tradeInfo.setBuyDate(realTradeDate);
//
//				// 賣出日
//				realTradeDate = dateUtil.getTradeDate(allTradeDateList, tradeInfo.getSellDate(), adjustNum);
//				if (realTradeDate == null) {
//					noTradeDateList.add(tradeInfo);
//					WLog.warn("沒有此預期交易日[" + WType.dateToStr(tradeInfo.getSellDate()) + "]的實際交易日");
//					continue;
//				} else if (realTradeDate.after(WDate.calDate_Day(tradeInfo.getSellDate(), maxGap))) {
//					WLog.warn("預計交易日(" + WType.dateToStr(tradeInfo.getSellDate()) + ")與實際交易日("
//							+ WType.dateToStr(realTradeDate) + ")差異過大");
//					noTradeDateList.add(tradeInfo);
//					continue;
//				}
//				WLog.debug("賣出日由[" + WType.dateToStr(tradeInfo.getSellDate()) + "]調整為[" + realTradeDate + "]");
//				tradeInfo.setSellDate(realTradeDate);
//			}
//		}
//		// 移除沒有預期交易日的tradeDetailInfo
//		for (TradeDetailPO noTradeBSInfo : noTradeDateList) {
//			tradeInfoList.remove(noTradeBSInfo);
//		}
//		return tradeInfoList;
//	}
}

class ProcessListener implements Runnable {
	public volatile static int currentProcess = 0;
	public int totalProcess = 0;
	public volatile static boolean isRun = true;

	public ProcessListener(int total) {
		super();
		this.totalProcess = total;
	}

	@Override
	public void run() {
		while (isRun && totalProcess > 0) {
			WLog.info("交易回測進度:" + new BigDecimal(currentProcess).divide(new BigDecimal(totalProcess), 2, RoundingMode.HALF_UP)
					.multiply(new BigDecimal(100)).setScale(2) + "% (" + currentProcess + "/" + totalProcess + ")");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}