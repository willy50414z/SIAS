package com.willy.sias.db.po;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;


@Table(name = "EXCHANGE")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ExchangePOKey implements Serializable
{
	/**
	 * 標的代號
	 */
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 資料日期
	 */
	@Column(name = "DATA_DATE")
	private Date dataDate;

	private static final long serialVersionUID = 1L;
}
