package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Table(name="SHARES_DISPERSION")
@Getter @Setter
public class SharesDispersionPOKey implements Serializable {
    /**
     * 標的代號
     */
    @Column(name = "SID")
    private Integer sid;

    /**
     * 資料發布日期
     */
    @Column(name = "DATA_DATE")
    private Date dataDate;

    private static final long serialVersionUID = 1L;
}
