package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.BatchLogPO;

@Repository
public interface BatchLogRepository extends JpaRepository<BatchLogPO, String> {
	public BatchLogPO findByBatchNameAndDataDate(String batchName,Date dataDate);
	public BatchLogPO findTop1ByBatchNameIsAndDataDateLessThan(String batchName,Date dataDate);
	public List<BatchLogPO> findByBatchNameInAndDataDateIs(List<String> batchNameList,Date dataDate);
	public List<BatchLogPO> findByStatusIs(Integer status);
	public BatchLogPO findFirstByBatchNameIsOrderByDataDateDesc(String batchName);
	public BatchLogPO findFirstByDataDateLessThanOrderByDataDateDesc(Date dataDate);
}
