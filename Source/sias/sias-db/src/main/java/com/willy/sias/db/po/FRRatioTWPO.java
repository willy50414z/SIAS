package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

/**
 * LP_FUTURE_TW
 * @author 
 */
@Table(name="FR_RATIO_TW")
@Entity
@Data
@IdClass(FRRatioTWPOKey.class)
public class FRRatioTWPO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 股票代號
     */
    @Id
    @Column(name = "SID")
    private int sid;
    
    /**
     * 財報所屬日期
     */
    @Id
    @Column(name = "FR_DATE")
    private String frDate;
    
    /**
     * 財報項目ID
     */
    @Id
    @Column(name = "FR_KEY")
    private int frKey;
    
    /**
     * 財報項目值
     */
    @Column(name = "FR_VALUE")
    private BigDecimal frValue;
    
    /**
     * 財報公布日
     */
    @Column(name = "FR_RELEASE_DATE")
    private String frReleaseDate;
}
