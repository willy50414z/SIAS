package com.willy.sias.batch.processor;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.IdxDailyUsPO;
import com.willy.sias.db.repository.IdxDailyUsRepository;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Setter;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@Setter
@Service
@ConfigurationProperties("sias.batch.idx-usd")
public class IDX_USD extends BaseBatchProcessor {

  @Autowired
  private WJsoup jsoup;

  @Autowired
  private IdxDailyUsRepository idxDailyUsRepo;

  @Override
  public void cleanHisData() throws Exception {

  }

  @Override
  protected void exec() throws Exception {
    Document doc = jsoup.get(parseMapParamToString(urlTempl),
        Const._ENCODING_UTF8);
    List<String> dataList = doc.getElementById("main3").getElementsByTag("div").get(7)
        .getElementsByTag("td").stream().map(
            Element::text).collect(Collectors.toList());

    if (dataList.size() == 1) {
      return;
    }

    if (validDataDate(doc.getElementById("main3").getElementsByTag("div").get(6).text())) {
      IdxDailyUsPO idxUsd;

      List<IdxDailyUsPO> idxUsdList = new ArrayList<>();
      idxUsd = new IdxDailyUsPO();
      idxUsd.setSid(Const._CFG_ID_USD_INDEX);
      idxUsd.setDataDate(this.dataDate);
      idxUsd.setOpen(new BigDecimal(StringUtil.cleanDigitData(dataList.get(0))));
      idxUsd.setHigh(new BigDecimal(StringUtil.cleanDigitData(dataList.get(1))));
      idxUsd.setLow(new BigDecimal(StringUtil.cleanDigitData(dataList.get(2))));
      idxUsd.setClose(new BigDecimal(StringUtil.cleanDigitData(dataList.get(3))));
      idxDailyUsRepo.save(idxUsd);
    } else {
      throw new IllegalStateException("data date is not corresponding");
    }

  }

  private boolean validDataDate(String dataStrDate) {
    String year = dataStrDate.substring(0, dataStrDate.indexOf("年"));
    String month = dataStrDate.substring(dataStrDate.indexOf("年") + 1, dataStrDate.indexOf("月"));
    String day = dataStrDate.substring(dataStrDate.indexOf("月") + 1, dataStrDate.indexOf("日"));
    Date realDataDate = WType.strToDate(
        year + WString.fillStrLength(month, "0", -2) + WString.fillStrLength(day, "0", -2));
    return realDataDate.compareTo(this.dataDate) == 0;
  }
}
