package com.willy.sias.web.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.willy.sias.web.service.BackTestSummaryService;
import com.willy.sias.web.vo.TradeDetailVO;

@Controller
@RequestMapping("/BackTestSummary")
public class BackTestSummaryController {
	@Autowired
	private BackTestSummaryService bTSmyService;

	@RequestMapping("/")
	public ModelAndView visit() throws ParseException {
		ModelAndView view = new ModelAndView("frames/MainFrame");
		view.addObject("homePageUrl", "query");
		return view;
	}

	@RequestMapping("/query")
	public ModelAndView query(String testIdCreteria, String testDateCreteria, String testStartDateCreteria,
			String testEndDateCreteria, String strategyNameCreteria, String strategyDescCreteria,
			String grossProfitCriteria, String grossProfitRatioCriteria, String profitRatioCriteria)
			throws ParseException {
		ModelAndView view = new ModelAndView("BackTestSummary");

//		if (!StringUtils.isBlank(testIdCreteria)) {
//			view.addObject("testId", testIdCreteria);
//		}
//
//		List<BachTestSummaryDTO> btsList = new ArrayList<>();
//		try {
//			btsList = bTSmyService.getBackTestSummary(testIdCreteria, testDateCreteria, testStartDateCreteria,
//					testEndDateCreteria, strategyNameCreteria, strategyDescCreteria, grossProfitCriteria,
//					grossProfitRatioCriteria, profitRatioCriteria);
//		} catch (Exception e) {
//			WLog.error(e);
//			view.addObject("errMsg", e.getMessage());
//		}
//		view.addObject("backTestSummaryList", btsList);
		return view;
	}

	@ResponseBody
	@RequestMapping("/QueryTradeDetail")
	public List<TradeDetailVO> queryTradeDetail(String testId) throws Exception {
		return null;
//		try {
//			List<TradeDetailVO> tmpTradeDetailVOList = new ArrayList<>();
//			List<TradeDetailVO> tradeDetailVOList = new ArrayList<>();
//			List<TradeDetailPO> tradeDetailList;
//			ProfitPO profitPo = bTSmyService.getBackTestTradeDetail(Integer.valueOf(testId));
//			if (profitPo == null) {
//				throw new NullPointerException("未查到該筆交易明細");
//			} else {
//				tradeDetailList = profitPo.getTradeDetailList();
//			}
//			tradeDetailList.forEach(td -> {
//				try {
//					if (td.getBuyDate() != null) {
//						tmpTradeDetailVOList.add(new TradeDetailVO(WType.dateToStr(td.getBuyDate(), "yyyy-MM-dd"), true,
//								td.getBuyPrice().doubleValue(), 1, td.getBuyAmt().toString(), ""));
//					}
//					if (td.getSellDate() != null) {
//						tmpTradeDetailVOList.add(new TradeDetailVO(WType.dateToStr(td.getSellDate(), "yyyy-MM-dd"),
//								false, td.getSellPrice().doubleValue(), 1, td.getSellAmt().toString(),
//								td.getBuyDate() == null ? "" : td.getGrossProfit().toString()));
//					}
//				} catch (ParseException e) {
//					WLog.error(e);
//				}
//			});
//
//			// 依交易日期排序
//			tmpTradeDetailVOList.sort(new Comparator<TradeDetailVO>() {
//				@Override
//				public int compare(TradeDetailVO o1, TradeDetailVO o2) {
//					// TODO Auto-generated method stub
//					int tradeDateComparedValue = Integer.valueOf(WString.rmStr(o1.getTradeDate(), "-"))
//							.compareTo(Integer.valueOf(WString.rmStr(o2.getTradeDate(), "-")));
//					if (tradeDateComparedValue == 0) {
//						return o1.isBuy() ? -1 : 1;
//					} else {
//						return tradeDateComparedValue;
//					}
//				}
//			});
//
//			// group by tradeDate and tradeType
//			tmpTradeDetailVOList.forEach(tmpVo -> {
//				TradeDetailVO lastTradeDetailVO = tradeDetailVOList.size() > 0
//						? tradeDetailVOList.get(tradeDetailVOList.size() - 1)
//						: null;
//				if (lastTradeDetailVO != null && lastTradeDetailVO.getTradeDate().equals(tmpVo.getTradeDate())
//						&& lastTradeDetailVO.isBuy() == tmpVo.isBuy()
//						&& lastTradeDetailVO.getTradePrice() == tmpVo.getTradePrice()) {
//					lastTradeDetailVO.setTradeAmt(new BigDecimal(lastTradeDetailVO.getTradeAmt())
//							.add(new BigDecimal(tmpVo.getTradeAmt())).toString());
//					lastTradeDetailVO.setTradeCount(lastTradeDetailVO.getTradeCount() + 1);
//					if (StringUtils.isNotEmpty(tmpVo.getGrossProfit())
//							&& StringUtils.isNotEmpty(lastTradeDetailVO.getGrossProfit())) {
//						lastTradeDetailVO.setGrossProfit(new BigDecimal(lastTradeDetailVO.getGrossProfit())
//								.add(new BigDecimal(tmpVo.getGrossProfit())).toString());
//					}
//				} else {
//					tradeDetailVOList.add(new TradeDetailVO(tmpVo.getTradeDate(), tmpVo.isBuy(), tmpVo.getTradePrice(),
//							tmpVo.getTradeCount(), tmpVo.getTradeAmt(), tmpVo.getGrossProfit()));
//				}
//			});
//
//			NumberFormat formatter = new DecimalFormat("$###,###,###,###.##");
//			tradeDetailVOList.forEach(td -> {
//				td.setTradeAmt(formatter.format(Double.valueOf(td.getTradeAmt())));
//				if (StringUtils.isNoneBlank(td.getGrossProfit())) {
//					td.setGrossProfit(formatter.format(Double.valueOf(td.getGrossProfit())));
//				}
//			});
//
//			return tradeDetailVOList;
//		} catch (Exception e) {
//			WLog.error(e);
//			throw e;
//		}
	}
}
