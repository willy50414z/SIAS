package com.willy.sias.db.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "MARKET_INDEX_TW")
@IdClass(MarketIndexTwPOKey.class)
public class MarketIndexTwPO {
	/**
	 * 收盤指數
	 */
	@Column(name = "\"CLOSE\"")
	private BigDecimal close;

	/**
	 * 標的代號
	 */
	@Id
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 資料日期
	 */
	@Id
	@Column(name = "DATA_DATE")
	private Date dataDate;
}
