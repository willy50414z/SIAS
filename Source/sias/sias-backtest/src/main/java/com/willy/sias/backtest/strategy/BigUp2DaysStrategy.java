//package com.willy.sias.backtest.strategy;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.text.ParseException;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.db.dto.TimeSeriesDTO;
//import com.willy.sias.db.po.PriceDailyTwPO;
//import com.willy.sias.db.repository.PriceDailyTwRepository;
//import com.willy.sias.util.EnumSet.PriceType;
//import com.willy.sias.util.EnumSet.TradeType;
//import com.willy.sias.util.config.Const;
//import com.willy.util.log.WLog;
//import com.willy.util.string.WString;
//
//@Component
//public class BigUp2DaysStrategy extends TradeStrategy {
//	private int continueDays = 2;
//	private BigDecimal upRatio = BigDecimal.valueOf(1.05);
//	@Autowired
//	private PriceDailyTwRepository priceRepo;
//
//	@Override
//	public void setTradeDetail() {
//		
//		//如果當天漲，則隔天再買回
////		if()
////		profitPo.getTradeDetailList().stream().filter(td -> !td.isTradeComplete() && td.getSellDate().before(tradeDate)).forEach(td -> {
////			tu.trade(td.getSid(), tradeDate, 1, TradeType.shortBuying.getKey(), profitPo);
////		});
//		
//		
////		List<Integer> sidList = profitPo.getSidList();
////		for (int sid : sidList) {
////			// 取得近3天股價
////			List<TimeSeriesDTO<BigDecimal>> recentDaysPrice;
////			try {
////				recentDaysPrice = getRecentDaysPrice(sid, tradeDate, -4);
////				if(recentDaysPrice == null || recentDaysPrice.size()<4) {
////					System.out.println("SID["+sid+"]tradeDate["+tradeDate+"]size["+(recentDaysPrice == null ? 0 :recentDaysPrice.size())+"]");
////					continue;
////				}
////				
////				boolean meetCriteria = true;
////				// 連續2天漲幅>5% && 第2天紅K>5% && 第2天開高
////				for (int dayIndex = 0; dayIndex < continueDays; dayIndex++) {
////					if (recentDaysPrice.get(dayIndex + 1).getDecimalValue()
////							.divide(recentDaysPrice.get(dayIndex).getDecimalValue(), 2, RoundingMode.FLOOR)
////							.compareTo(upRatio) < 0) {
////						meetCriteria = false;
////					}
////				}
////				if (!meetCriteria) {
////					continue;
////				}
////
//////				// 第2天紅K>5%
////				List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(sid,
////						recentDaysPrice.get(0).getDate(), recentDaysPrice.get(3).getDate());
////				if (priceList.size() != 4) {
////					continue;
////				}
////				if (priceList.get(2).getClose().divide(priceList.get(2).getOpen(), 2, RoundingMode.FLOOR)
////						.compareTo(upRatio) <= 0) {
////					continue;
////				}
//////
//////				// 第2天開高
////				if (priceList.get(2).getOpen().compareTo(priceList.get(1).getClose()) <= 0) {
////					continue;
////				}
////				if (priceList.get(3).getOpen().compareTo(priceList.get(2).getClose()) <= 0) {
////					continue;
////				}
////				
////				//成交量>2000
////				if (priceList.get(3).getTradingShares()<2000000L) {
////					continue;
////				}
////
////				tu.trade(sid, tradeDate, 1, TradeType.shortSelling, PriceType.OPEN, profitPo);
////
////			} catch (ParseException e) {
////				WLog.error(e);
////			}
//
////		}
//	}
//
//	private List<TimeSeriesDTO<BigDecimal>> getRecentDaysPrice(int sid, Date tradeDate, int getPriceDay) throws ParseException {
//		// 取得近3天的股價
//		List<Date> recentBusDateList = dtDao.findBusDate(tradeDate, getPriceDay);
//		List<TimeSeriesDTO<BigDecimal>> priceTsList = dtDao.findTdsList(sid,
//				Const._FIELDID_PRICE_DAILY_TW_CLOSE, recentBusDateList.get(0),
//				recentBusDateList.get(recentBusDateList.size() - 1));
//		if (priceTsList.size() != Math.abs(getPriceDay)) {
//			return null;
//		} else {
//			return priceTsList;
//		}
//	}
//
//	@Override
//	public String getStrategyDesc() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String checkArgs() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Integer call() throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
