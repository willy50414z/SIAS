package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

/**
 * LP_FUTURE_TW
 * @author 
 */
@Table(name="MONTHLY_REVENUE_TW")
@Entity
@Data
@IdClass(MonthlyRevenueTWPOKey.class)
public class MonthlyRevenueTWPO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 股票代號
     */
    @Id
    @Column(name = "SID")
    private int sid;
    
    /**
     * 營收所屬日期
     */
    @Id
    @Column(name = "REVENUE_DATE")
    private String revenueDate;
    
    /**
     * 營收
     */
    @Column(name = "REVENUE")
    private BigDecimal revenue;
    
    /**
     * 上個月營收
     */
    @Column(name = "REVENUE_LAST_MONTH")
    private BigDecimal revenueLastMonth;
    
    /**
     * 去年同期營收
     */
    @Column(name = "REVENUE_LAST_YEAR")
    private BigDecimal revenueLastYear;
    
    /**
     * 累計營收
     */
    @Column(name = "SUM_REVENUE")
    private BigDecimal sumRevenue;
    
    /**
     * 去年同期累計營收
     */
    @Column(name = "SUM_REVENUE_LAST_YEAR")
    private BigDecimal sumRevenueLastYear;
    
    
}
