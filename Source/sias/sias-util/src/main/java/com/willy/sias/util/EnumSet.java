package com.willy.sias.util;

import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EnumSet {

  public static enum ParentKey {
    Category_TWSE(1),//TWSE分類
    MarketType(2),//市場別
    SID(3),//股票代碼
    BSFRKey(4),//資產負債表會計科目代碼
    ISFRKey(5),//損益表會計科目代碼
    CFFRKey(6),
    MarketIndex(7),
    FutureType(86),
    OptionType(87),
    CustomerizedCompanyCategory(126082);//自定義分類

    private int key;

    private ParentKey(int key) {
      this.key = key;
    }

    public int getKey() {
      return this.key;
    }
  }

  public static enum PriceType {
    OPEN(Const._FIELDID_PRICE_DAILY_TW_OPEN), HIGH(Const._FIELDID_PRICE_DAILY_TW_HIGH), LOW(
        Const._FIELDID_PRICE_DAILY_TW_LOW), CLOSE(Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    private int key;

    private PriceType(int key) {
      this.key = key;
    }

    public int getKey() {
      return this.key;
    }
  }

  /**
   * 1: 買入 2: 融資 3: 補券 4: 賣出 5:融券 6: 融資賣
   *
   * @author Willy
   */
  public static enum TradeType {
    buy(1, "買入"),//買入
    marginBuy(2, "融資"),//融資
    shortBuying(3, "補券"),//補券
    sell(4, "賣出"),//賣出
    marginSell(5, "融資賣"),//融資賣
    shortSelling(6, "融券賣");//融券賣

    private Integer key;
    private String desc;

    private TradeType(Integer key, String desc) {
      this.key = key;
      this.desc = desc;
    }

    public Integer getKey() {
      return this.key;
    }

    public String getDesc() {
      return this.desc;
    }

    public TradeType getLiquidateTradeType() {
      switch (this) {
        case buy:
          return sell;
        case marginBuy:
          return marginSell;
        case shortBuying:
          return shortSelling;
        case sell:
          return buy;
        case marginSell:
          return marginBuy;
        case shortSelling:
          return shortBuying;
        default:
          return null;
      }
    }
  }

  public static int getFRKey(String frType) {
    switch (frType) {
      case "IS":
        return ParentKey.ISFRKey.getKey();
      case "BS":
        return ParentKey.BSFRKey.getKey();
      case "CF":
        return ParentKey.CFFRKey.getKey();
      default:
        throw new NullPointerException("找不到財務報表對應的Key");
    }
  }

  public static enum AnalyzeCacheKey {
    _ValidDateList;
  }

  public static enum PriceCheckPointType {
    MA5("周均線", PriceCheckPointTypeType.MA), MA10("10日均線", PriceCheckPointTypeType.MA),
    MA20("月均線", PriceCheckPointTypeType.MA), MA60("季均線", PriceCheckPointTypeType.MA),
    MA120("半年均線", PriceCheckPointTypeType.MA), MA240("年均線", PriceCheckPointTypeType.MA),
    BIG_VOL("大量交易成本區", PriceCheckPointTypeType.VOL), GIANT_VOL("超大量交易成本區",
        PriceCheckPointTypeType.VOL),
    W("W底部型態", PriceCheckPointTypeType.PATTERN), M("M頂", PriceCheckPointTypeType.PATTERN),
    TRI("三角收斂", PriceCheckPointTypeType.PATTERN), GAP("跳空缺口",
        PriceCheckPointTypeType.GAP), GAP_MA5("突破MA5跳空缺口", PriceCheckPointTypeType.GAP),
    GAP_MA10("突破MA10跳空缺口", PriceCheckPointTypeType.GAP), GAP_MA20("突破MA20跳空缺口",
        PriceCheckPointTypeType.GAP),
    GAP_MA60("突破MA60跳空缺口", PriceCheckPointTypeType.GAP), GAP_MA120("突破MA120跳空缺口",
        PriceCheckPointTypeType.GAP),
    GAP_MA240("突破MA240跳空缺口", PriceCheckPointTypeType.GAP),
    GAP_TREND_SUPPORT("突破支撐趨勢跳空缺口", PriceCheckPointTypeType.GAP),
    GAP_TREND_PRESSURE("突破壓力跳空缺口", PriceCheckPointTypeType.GAP), GAP_GAP(
        "突破前方缺口跳空缺口", PriceCheckPointTypeType.GAP),
    GAP_BIG_VOL("大量交易成本區缺口", PriceCheckPointTypeType.GAP), GAP_GIANT_VOL(
        "超大量交易成本區缺口", PriceCheckPointTypeType.GAP);

    public String getDesc() {
      return desc;
    }

    String desc;
    PriceCheckPointTypeType type;

    PriceCheckPointType(String desc, PriceCheckPointTypeType type) {
      this.desc = desc;
      this.type = type;
    }

    public PriceCheckPointTypeType getType() {
      return type;
    }

    public PriceCheckPointType getPriceCheckPointGapType() {
      switch (this) {
        case MA5:
          return GAP_MA5;
        case MA10:
          return GAP_MA10;
        case MA20:
          return GAP_MA20;
        case MA60:
          return GAP_MA60;
        case MA120:
          return GAP_MA120;
        case MA240:
          return GAP_MA240;
        case BIG_VOL:
          return GAP_BIG_VOL;
        case GIANT_VOL:
          return GAP_GIANT_VOL;
        default:
          return null;
      }
    }
  }

  public static enum PriceCheckPointTypeType {
    MA, GAP, VOL, PATTERN;
  }

  public static enum BatchName {
    DIVIDEND_TW('D',
        "股利政策"),//LP_FUTURE_TW('D', "三大法人買賣超_期貨"), LP_OPTION_TW('D', "三大法人買賣超_選擇權"),
    FR_IS_TW('Q', "損益表_TW"), LP_TXN_LOG_TW_TWSE('D', "三大法人買賣超_上市"), LP_TXN_LOG_TW_TPEX(
        'D', "三大法人買賣超_櫃買"), MARGIN_TRADE_INFO_TW_TPEX('D', "信用交易資訊_上櫃"),
    FI_HOLDING_TPEX('D', "外資持有_上櫃", new BatchName[]{LP_TXN_LOG_TW_TPEX}),
    FI_HOLDING_TWSE('D', "外資持有_上市", new BatchName[]{LP_TXN_LOG_TW_TWSE}),
    LP_FUTURE_TW('D', "法人持有期貨口數"), LP_OPTION_TW('D', "法人持有選擇權口數"),
    MARGIN_TRADE_INFO_TW_TWSE('D', "信用交易資訊_上市"), MARKET_INDEX_TW_TWSE('D',
        "加權指數"), MONTHLY_REVENUE_TW('M', "月營收"),
    MARKET_INDEX_TW_TWSE_TXN_INFO('D', "加權指數成交資訊", new BatchName[]{MARKET_INDEX_TW_TWSE}),
    STOCK_PRICE_TW_TWSE('D', "股價_上市(日)"), STOCK_PRICE_TW_TPEX('D', "股價_上櫃(日)"), IT_HOLD(
        'T', "投信持有股數"), IDX_USD('D', "美元指數"), EXCHANGE_USD_TWD('D', "匯率"), CBOE_VIX('N', "芝加哥交易所恐慌指數"), MARGIN_RATIO_TW('N', "大盤融資維持率");
    private char frequence;
    private String batchDesc;
    private BatchName[] frontBatchs;

    BatchName(char frequence, String batchDesc) {
      this.frequence = frequence;
      this.batchDesc = batchDesc;
      this.frontBatchs = null;
    }

    BatchName(char frequence, String batchDesc, BatchName[] frontBatchs) {
      this.frequence = frequence;
      this.batchDesc = batchDesc;
      this.frontBatchs = frontBatchs;
    }

    public char getFrequence() {
      return this.frequence;
    }

    public String getBatchDesc() {
      return batchDesc;
    }

    public BatchName[] getFrontBatchs() {
      return frontBatchs;
    }

    public String getFrequenceDesc() {
      switch (this.frequence) {
        case 'D':
          return "每日";
        case 'W':
          return "每周";
        case 'M':
          return "每月";
        case 'Q':
          return "每季";
        case 'N':
          return "需要才跑";
        default:
          return "*";
      }
    }
  }

  public static enum KBar {
    CHANGE("變盤線", "天前出現變盤線，待隔日開盤確認趨勢是否轉向"),
    BLACK_AND_RED("長黑後長紅收一樣", "天前出現長黑後長紅，若位處低檔且明日開高，股價準備向上"),
    RED_AND_BLACK("長紅後長黑收一樣", "天前出現長紅後長黑，若位處高檔且明日開低，股價準備向下"),
    //3033 20200320
    //2207 20200320
    //6448 20200319
    RED_EAT_BLACK("長紅吞噬", "天前出現長紅吞噬長黑棒，若位處低檔且明日開高，股價準備向上"),
    //3189 20200817
    //4908 20200707 20200724
    BLACK_EAT_RED("長黑吞噬", "天前出現長黑吞噬長紅棒，若位處高檔且明日開低，股價準備向下"),
    BLACK_BUT_TRED("長黑後長下影線", "天前出現長黑後長下影，若位處低檔且明日開高，股價準備向上"),
    //2489 20200717
    RED_BUT_RTBLACK("長紅後長上影線", "天前出現長紅後長上影，若位處高檔且明日開低，股價準備向下");
    private String desc;
    private String trendDesc;

    KBar(String desc, String trendDesc) {
      this.desc = desc;
      this.trendDesc = trendDesc;
    }

    public String getTrendDesc() {
      return trendDesc;
    }

    public String getDesc() {
      return desc;
    }
  }

  public static enum TrendType {
    UP_WARD("上升趨勢"), DOWN_TREND("下降趨勢"), TRIANGLE_NARROW("三角收縮"), TRIANGLE_WIDEN(
        "三角發散"), HORIZON_TIDY("水平整理");
    String desc;
    int breakUdTrackStatus;

    TrendType(String desc) {
      this.desc = desc;
    }

    public String getDesc() {
      return desc;
    }

    public void setBreakUdTrackStatus(int breakUdTrackStatus) {
      this.breakUdTrackStatus = breakUdTrackStatus;
    }

    public int getBreakUdTrackStatus() {
      return breakUdTrackStatus;
    }
  }

  public static enum FrReleaseDate {
    Q1("0515"), Q2("0814"), Q3("1114"), Q4("0331");
    private String mmdd;//財報發布日

    FrReleaseDate(String mmdd) {
      this.mmdd = mmdd;
    }

    public String getMmdd() {
      return mmdd;
    }

    public static Date getNextFrReleaseDate(Date date) {
      int year = WDate.getDateInfo(date, Calendar.YEAR);

     String nextFrRelMmdd = Arrays.stream(FrReleaseDate.values())
          .filter(relDate -> WType.strToDate(year + relDate.getMmdd()).compareTo(date) > 0)
          .map(relDate -> relDate.getMmdd()).min(Const._DEFAULT_COMPARATOR_STRING).orElse(null);

      String strNextFrRelDate;
     if(nextFrRelMmdd == null) {
       //next year Q1
       strNextFrRelDate = (year + 1) + Q4.getMmdd();
     } else {
       strNextFrRelDate = year + nextFrRelMmdd;
     }
      return WType.strToDate(strNextFrRelDate);
    }

    public static List<Date> getFrTimeSlot(Date date) {
      Date nextFrReleaseDate = FrReleaseDate.getNextFrReleaseDate(date);
      int frRelQuarterFirstMonth = WDate.getQuarter(nextFrReleaseDate) * 3 - 2;
      String strFrRelQuarterFirstDate = WDate.getDateInfo(nextFrReleaseDate, Calendar.YEAR) + WString.fillStrLength(
          frRelQuarterFirstMonth, "0", -2) + "01";
      return Arrays.asList(WDate.addDay(WType.strToDate(strFrRelQuarterFirstDate), -1), nextFrReleaseDate);
    }
  }
}
