package com.willy.sias.db.repository;

import java.util.Date;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.willy.sias.db.po.MarginTradeInfoPO;
import com.willy.sias.db.po.MarginTradeInfoPOKey;

@Repository
public interface MarginTradeInfoRepository extends JpaRepository<MarginTradeInfoPO, MarginTradeInfoPOKey> {
	@Transactional
	public void deleteByDataDateIsAndSidIn(Date dataDate, Set<Integer> sidSet);
}
