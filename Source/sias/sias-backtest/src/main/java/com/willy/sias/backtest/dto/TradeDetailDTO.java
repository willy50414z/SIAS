package com.willy.sias.backtest.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.willy.sias.util.EnumSet.TradeType;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TradeDetailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String tradeId;//單次交易的TradeId一樣
	private TradeType tradeType;
	private Integer sid;// 標的
	private Date tradeDate;// 交易日
	private Date settleDate;// 結算日
	private BigDecimal tradeUnit;// 交易單位
	private BigDecimal tradePrice;// 交易價格
	private BigDecimal tradeAmt;// 交易金額
	private BigDecimal grossProfit;
	private BigDecimal cost;
	private Set<String> parentTradeIdSet;
	
	public TradeDetailDTO newInstance() {
		TradeDetailDTO td = new TradeDetailDTO();
		td.setTradeId(this.tradeId);
		td.setTradeType(this.tradeType);
		td.setSid(this.sid);
		td.setTradeDate(this.tradeDate);
		td.setTradeUnit(this.tradeUnit);
		td.setTradePrice(this.tradePrice);
		td.setTradeAmt(this.tradeAmt);
		td.setSettleDate(this.settleDate);
		td.setGrossProfit(this.grossProfit);
		return td;
	}
	
	

	public TradeDetailDTO(TradeType tradeType, Integer sid, Date tradeDate, BigDecimal tradeUnit, BigDecimal tradePrice,
			BigDecimal tradeAmt, Date settleDate) {
		super();
		this.tradeType = tradeType;
		this.sid = sid;
		this.tradeDate = tradeDate;
		this.tradeUnit = tradeUnit;
		this.tradePrice = tradePrice;
		this.tradeAmt = tradeAmt;
		this.settleDate = settleDate;
	}

	public TradeDetailDTO(TradeType tradeType, Integer sid, Date tradeDate, BigDecimal tradeUnit, BigDecimal tradePrice,
			BigDecimal tradeAmt, Date settleDate, BigDecimal grossProfit) {
		super();
		this.tradeType = tradeType;
		this.sid = sid;
		this.tradeDate = tradeDate;
		this.tradeUnit = tradeUnit;
		this.tradePrice = tradePrice;
		this.tradeAmt = tradeAmt;
		this.settleDate = settleDate;
		this.grossProfit = grossProfit;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TradeDetailDTO other = (TradeDetailDTO) obj;
		if (grossProfit == null) {
			if (other.grossProfit != null)
				return false;
		} else if (!grossProfit.equals(other.grossProfit))
			return false;
		if (settleDate == null) {
			if (other.settleDate != null)
				return false;
		} else if (settleDate.compareTo(other.settleDate)!=0)
			return false;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		if (tradeAmt == null) {
			if (other.tradeAmt != null)
				return false;
		} else if (!tradeAmt.equals(other.tradeAmt))
			return false;
		if (tradeDate == null) {
			if (other.tradeDate != null)
				return false;
		} else if (!tradeDate.equals(other.tradeDate))
			return false;
		if (tradePrice == null) {
			if (other.tradePrice != null)
				return false;
		} else if (!tradePrice.equals(other.tradePrice))
			return false;
		if (tradeType != other.tradeType)
			return false;
		if (tradeUnit == null) {
			if (other.tradeUnit != null)
				return false;
		} else if (!tradeUnit.equals(other.tradeUnit))
			return false;
		return true;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((grossProfit == null) ? 0 : grossProfit.hashCode());
		result = prime * result + ((settleDate == null) ? 0 : settleDate.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		result = prime * result + ((tradeAmt == null) ? 0 : tradeAmt.hashCode());
		result = prime * result + ((tradeDate == null) ? 0 : tradeDate.hashCode());
		result = prime * result + ((tradePrice == null) ? 0 : tradePrice.hashCode());
		result = prime * result + ((tradeType == null) ? 0 : tradeType.hashCode());
		result = prime * result + ((tradeUnit == null) ? 0 : tradeUnit.hashCode());
		return result;
	}
	
	public void addParentTradeId(String parentId) {
		if(parentTradeIdSet == null) {
			parentTradeIdSet = new HashSet<>();
		}
		parentTradeIdSet.add(parentId);
	}
}
