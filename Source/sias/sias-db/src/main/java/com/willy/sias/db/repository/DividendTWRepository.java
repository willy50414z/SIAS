package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.willy.sias.db.po.DividendTWPO;
import com.willy.sias.db.po.DividendTWPOKey;

public interface DividendTWRepository extends JpaRepository<DividendTWPO, DividendTWPOKey> {
	@Modifying
	@Transactional
	public void deleteByDataDateBetween(Date startDate, Date endDate);
	public List<DividendTWPO> findBySidIn(List<Integer> sidList);
	@Modifying
	@Transactional
	@Query("SELECT D FROM DividendTWPO D WHERE D.sid in ?1 AND (YEAR(D.exSDivDate)=?2 OR YEAR(exCDivDate)=?2)")
	public List<DividendTWPO> findBySidInAndExSDivDateOrExCDivDateEqualsYear(List<Integer> sidList, int year);
}
