--建立SIAS
USE [master]
CREATE DATABASE [SIAS] ON  PRIMARY 
( NAME = N'SIAS', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS.mdf' , SIZE = 4194304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SIAS_log', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS_log.ldf' , SIZE = 26816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
--建立SIAS_PRICE
USE [master]
CREATE DATABASE [SIAS_PRICE] ON  PRIMARY 
( NAME = N'SIAS_PRICE', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS_PRICE.mdf' , SIZE = 4194304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SIAS_PRICE_log', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS_PRICE.ldf' , SIZE = 26816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
--建立SIAS_PRICE
USE [master]
CREATE DATABASE [SIAS_BATCH_LOG] ON  PRIMARY 
( NAME = N'SIAS_BATCH_LOG', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS_BATCH_LOG.mdf' , SIZE = 4194304KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SIAS_PRICE_log', FILENAME = N'D:\Code\MSSQL\workspace\DATA\SIAS_BATCH_LOG.ldf' , SIZE = 26816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
--建立PRICE_DAILY_TW TABLE
/****** Object:  Table [dbo].[PRICE_DAILY_TW]    Script Date: 2021/9/2 下午 09:27:25 ******/
USE [SIAS_PRICE]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRICE_DAILY_TW](
	[SID] [int] NOT NULL,
	[DATA_DATE] [datetime] NOT NULL,
	[OPEN] [decimal](9, 2) NULL,
	[HIGH] [decimal](9, 2) NULL,
	[LOW] [decimal](9, 2) NULL,
	[CLOSE] [decimal](9, 2) NULL,
	[TURN_OVER] [decimal](18, 0) NULL,
	[TRADING_SHARES] [decimal](18, 0) NULL,
	[TRADING_NUM] [int] NULL,
	[LAST_BUY_PRICE] [decimal](9, 2) NULL,
	[LAST_BUY_VOL] [int] NULL,
	[LAST_SOLD_PRICE] [decimal](9, 2) NULL,
	[LAST_SOLD_VOL] [int] NULL,
 CONSTRAINT [PK_PRICE_DAILY_TW] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[DATA_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--建立BATCH_LOG TABLE
USE [SIAS_BATCH_LOG]
/****** Object:  Table [dbo].[BATCH_LOG]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BATCH_LOG](
	[BATCH_LOG_ID] [int] IDENTITY(1,1) NOT NULL,
	[DATA_DATE] [datetime] NOT NULL,
	[BATCH_NAME] [nvarchar](50) NOT NULL,
	[STATUS] [int] NOT NULL,
	[ERR_MSG] [nvarchar](max) NULL,
	[CREATE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_BATCH_LOG] PRIMARY KEY CLUSTERED 
(
	[BATCH_LOG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
--建立BATCH_LOG同義字
USE [SIAS]
CREATE SYNONYM [dbo].[PRICE_DAILY_TW] FOR [SIAS_PRICE].[dbo].[PRICE_DAILY_TW]
GO
CREATE SYNONYM [dbo].[BATCH_LOG] FOR [SIAS_BATCH_LOG].[dbo].[BATCH_LOG]
GO

USE [SIAS]
GO
/****** Object:  Sequence [dbo].[BACK_TEST_SEQ]    Script Date: 2022/4/30 下午 10:33:07 ******/
CREATE SEQUENCE [dbo].[BACK_TEST_SEQ] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
/****** Object:  Table [dbo].[BACK_TEST_LOG_D]    Script Date: 2021/9/2 下午 09:27:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BACK_TEST_LOG_D](
	[TEST_ID] [int] NULL,
	[TEST_ID_D] [int] IDENTITY(1,1) NOT NULL,
	[TRADE_DETAIL] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[TEST_ID_D] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BACK_TEST_LOG_H]    Script Date: 2021/9/2 下午 09:27:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BACK_TEST_LOG_H](
	[TEST_ID] [int] NOT NULL,
	[TEST_DESC] [nvarchar](200) NULL,
	[ERR_MSG] [nvarchar](500) NULL,
	[STRATEGY_FILE_PATH] [NVARCHAR](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[TEST_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CALENDAR]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CALENDAR](
	[DATE] [datetime] NOT NULL,
	[DATE_TYPE] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DATE] ASC,
	[DATE_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMPANY_CATEGORY]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMPANY_CATEGORY](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATA_DATE] [datetime] NOT NULL,
	[SID] [int] NOT NULL,
	[CATEGORY_ID] [int] NULL,
 CONSTRAINT [PK_COMPANY_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[COMPANY_INFO]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COMPANY_INFO](
	[DATA_DATE] [datetime] NOT NULL,
	[SID] [int] NOT NULL,
	[CHAIR_MAN] [nvarchar](20) NULL,
	[GEN_MANAGER] [nvarchar](20) NULL,
	[EST_DATE] [datetime] NULL,
	[CAPITAL] [decimal](18, 0) NULL,
	[PUBL_SHARES] [decimal](18, 0) NULL,
	[SE_DATE] [datetime] NULL,
	[OTC_DATE] [datetime] NULL,
	[ES_DATE] [datetime] NULL,
	[PUB_DATE] [datetime] NULL,
 CONSTRAINT [PK_COMPANY_INFO] PRIMARY KEY CLUSTERED 
(
	[DATA_DATE] ASC,
	[SID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DIVIDEND_TW]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DIVIDEND_TW](
	[SID] [int] NOT NULL,
	[DIV_BELONGS] [nvarchar](25) NOT NULL,
	[DATA_DATE] [datetime] NULL,
	[S_DIV_REV] [decimal](9, 6) NULL,
	[S_DIV_LEG_RES] [decimal](9, 6) NULL,
	[S_DIV]  AS ([S_DIV_REV]+[S_DIV_LEG_RES]),
	[EX_S_DIV_DATE] [datetime] NULL,
	[C_DIV_REV] [decimal](9, 6) NULL,
	[C_DIV_LEG_RES] [decimal](9, 6) NULL,
	[C_DIV]  AS ([C_DIV_REV]+[C_DIV_LEG_RES]),
	[EX_C_DIV_DATE] [datetime] NULL,
	[PU_C_DIV_DATE] [datetime] NULL,
	[PU_RIGHT_BASE_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_DIVIDEND_TW] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[DIV_BELONGS] ASC,
	[PU_RIGHT_BASE_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FIELD_INFO]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FIELD_INFO](
	[FIELD_ID] [int] NOT NULL,
	[FIELD_NAME] [varchar](50) NULL,
	[TABLE_NAME] [varchar](50) NULL,
	[FIELD_KEY] [int] NULL,
	[KEY_COL_NAME] [varchar](50) NULL,
	[VAL_COL_NAME] [varchar](200) NULL,
	[DATE_COL_NAME] [varchar](50) NULL,
	[FIX_FUNC] [varchar](200) NULL,
	[PROCESS_INFO] [nvarchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FR_IS_TW]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FR_IS_TW](
	[SID] [varchar](20) NOT NULL,
	[FR_DATE] [char](6) NOT NULL,
	[FR_KEY] [int] NOT NULL,
	[FR_VALUE] [decimal](18, 2) NULL,
	[FR_RELEASE_DATE] [char](8) NULL,
 CONSTRAINT [PK_FR_IS_TW] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[FR_DATE] ASC,
	[FR_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FR_RATIO_TW]    Script Date: 2021/9/2 下午 09:27:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FR_RATIO_TW](
	[SID] [varchar](20) NOT NULL,
	[FR_DATE] [char](6) NOT NULL,
	[FR_KEY] [int] NOT NULL,
	[FR_VALUE] [decimal](18, 2) NULL,
	[FR_RELEASE_DATE] [char](8) NULL,
 CONSTRAINT [PK_FR_RATIO_TW] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[FR_DATE] ASC,
	[FR_KEY] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LP_FUTURE_TW]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LP_FUTURE_TW](
	[DATA_DATE] [datetime] NOT NULL,
	[FUTURE_TYPE] [int] NOT NULL,
	[FI_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[IT_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[DL_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[FI_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[IT_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[DL_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[NET_TXN_LOT]  AS ((([FI_LONG_TXN_LOT]+[IT_LONG_TXN_LOT])+[DL_LONG_TXN_LOT])-(([FI_SHORT_TXN_LOT]+[IT_SHORT_TXN_LOT])+[DL_SHORT_TXN_LOT])),
	[FI_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[IT_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[DL_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[FI_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[IT_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[DL_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[NET_BAL_LOT]  AS ((([FI_LONG_BAL_LOT]+[IT_LONG_BAL_LOT])+[DL_LONG_BAL_LOT])-(([FI_SHORT_BAL_LOT]+[IT_SHORT_BAL_LOT])+[DL_SHORT_BAL_LOT])),
 CONSTRAINT [PK_LP_FUTURE_TW] PRIMARY KEY CLUSTERED 
(
	[DATA_DATE] ASC,
	[FUTURE_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LP_OPTION_TW]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LP_OPTION_TW](
	[DATA_DATE] [datetime] NOT NULL,
	[OPTION_TYPE] [int] NOT NULL,
	[FI_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[IT_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[DL_LONG_TXN_LOT] [decimal](9, 2) NOT NULL,
	[FI_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[IT_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[DL_SHORT_TXN_LOT] [decimal](9, 2) NOT NULL,
	[NET_TXN_LOT]  AS ((([FI_LONG_TXN_LOT]+[IT_LONG_TXN_LOT])+[DL_LONG_TXN_LOT])-(([FI_SHORT_TXN_LOT]+[IT_SHORT_TXN_LOT])+[DL_SHORT_TXN_LOT])),
	[FI_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[IT_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[DL_LONG_BAL_LOT] [decimal](9, 2) NOT NULL,
	[FI_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[IT_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[DL_SHORT_BAL_LOT] [decimal](9, 2) NOT NULL,
	[NET_BAL_LOT]  AS ((([FI_LONG_BAL_LOT]+[IT_LONG_BAL_LOT])+[DL_LONG_BAL_LOT])-(([FI_SHORT_BAL_LOT]+[IT_SHORT_BAL_LOT])+[DL_SHORT_BAL_LOT])),
 CONSTRAINT [PK_LP_OPTION_TW] PRIMARY KEY CLUSTERED 
(
	[DATA_DATE] ASC,
	[OPTION_TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LP_TRANS_LOG_TW]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LP_TRANS_LOG_TW](
	[SID] [int] NOT NULL,
	[TRANS_DATE] [datetime] NOT NULL,
	[FI_NS_BUY] [int] NULL,
	[FI_NS_SELL] [int] NULL,
	[FI_NS_NET]  AS (isnull([FI_NS_BUY],(0))-isnull([FI_NS_SELL],(0))),
	[FI_SELF_BUY] [int] NULL,
	[FI_SELF_SELL] [int] NULL,
	[FI_SELF_NET]  AS (isnull([FI_SELF_BUY],(0))-isnull([FI_SELF_SELL],(0))),
	[FI_BUY] [int] NULL,
	[FI_SELL] [int] NULL,
	[FI_NET]  AS ((isnull([FI_NS_BUY],(0))+isnull([FI_SELF_BUY],(0)))-(isnull([FI_NS_SELL],(0))+isnull([FI_SELF_SELL],(0)))),
	[IT_BUY] [int] NULL,
	[IT_SELL] [int] NULL,
	[IT_NET]  AS (isnull([IT_BUY],(0))-isnull([IT_SELL],(0))),
	[DL_HEDGING_BUY] [int] NULL,
	[DL_HEDGING_SELL] [int] NULL,
	[DL_HEDGING_NET]  AS (isnull([DL_HEDGING_BUY],(0))-isnull([DL_HEDGING_SELL],(0))),
	[DL_SELF_BUY] [int] NULL,
	[DL_SELF_SELL] [int] NULL,
	[DL_SELF_NET]  AS (isnull([DL_SELF_BUY],(0))-isnull([DL_SELF_SELL],(0))),
	[DL_BUY] [int] NULL,
	[DL_SELL] [int] NULL,
	[DL_NET]  AS ((isnull([DL_HEDGING_BUY],(0))+isnull([DL_SELF_BUY],(0)))-(isnull([DL_HEDGING_SELL],(0))+isnull([DL_SELF_SELL],(0)))),
	[LP_BUY]  AS ((isnull([FI_BUY],(0))+isnull([IT_BUY],(0)))+isnull([DL_BUY],(0))),
	[LP_SELL]  AS ((isnull([FI_SELL],(0))+isnull([IT_SELL],(0)))+isnull([DL_SELL],(0))),
	[LP_NET]  AS (((((isnull([FI_BUY],(0))+isnull([IT_BUY],(0)))+isnull([DL_BUY],(0)))-isnull([FI_SELL],(0)))+isnull([IT_SELL],(0)))+isnull([DL_SELL],(0))),
	[FI_HOLDING] [decimal](14, 0) NULL,
	[FI_BUYABLE] [decimal](14, 0) NULL,
	[FI_BUYABLE_MDF_REASON] [nvarchar](50) NULL,
	[IT_HOLDING] [decimal](14, 0) NULL,
	[PUBLISH_COUNT] [decimal](14, 0) NULL,
 CONSTRAINT [PK_LP_TRANS_LOG_TW] PRIMARY KEY CLUSTERED 
(
	[TRANS_DATE] ASC,
	[SID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MARGIN_TRADE_INFO]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MARGIN_TRADE_INFO](
	[SID] [int] NOT NULL,
	[DATA_DATE] [datetime] NOT NULL,
	[FIN_BUY] [int] NULL,
	[FIN_SELL] [int] NULL,
	[FIN_REPAY] [int] NULL,
	[FIN_BALANCE] [int] NULL,
	[FIN_QUOTA] [decimal](10, 2) NULL,
	[LEND_BUY] [int] NULL,
	[LEND_SELL] [int] NULL,
	[LEND_REPAY] [int] NULL,
	[LEND_BALANCE] [int] NULL,
	[LEND_QUOTA] [decimal](10, 2) NULL,
	[MEMO] [nchar](20) NULL,
 CONSTRAINT [PK_MARGIN_TRADE_INFO] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[DATA_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MONTHLY_REVENUE_TW]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MONTHLY_REVENUE_TW](
	[SID] [varchar](20) NOT NULL,
	[REVENUE_DATE] [char](6) NOT NULL,
	[REVENUE] [decimal](15, 0) NOT NULL,
	[REVENUE_LAST_MONTH] [decimal](15, 0) NOT NULL,
	[REVENUE_LAST_YEAR] [decimal](15, 0) NOT NULL,
	[SUM_REVENUE] [decimal](15, 0) NOT NULL,
	[SUM_REVENUE_LAST_YEAR] [decimal](15, 0) NOT NULL,
 CONSTRAINT [PK_MONTHLY_REVENUE_TW] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[REVENUE_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SHARES_DISPERSION]    Script Date: 2021/9/2 下午 09:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SHARES_DISPERSION](
	[SID] [int] NOT NULL,
	[DATA_DATE] [datetime] NOT NULL,
	[SHARES_COUNT] [int] NOT NULL,
	[SHAREHOLDER_COUNT] [int] NOT NULL,
	[AVG_SHARES_EACH_PERSON] [int] NOT NULL,
	[HOLDING_COUNT_1] [int] NOT NULL,
	[PERSON_COUNT_1] [int] NOT NULL,
	[PERSON_COUNT_2] [int] NOT NULL,
	[PERSON_COUNT_3] [int] NOT NULL,
	[PERSON_COUNT_4] [int] NOT NULL,
	[PERSON_COUNT_5] [int] NOT NULL,
	[BATCH_DATE] [datetime] NOT NULL,
 CONSTRAINT [PK_SHARES_DISPERSION] PRIMARY KEY CLUSTERED 
(
	[SID] ASC,
	[DATA_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SYS_CONFIG]    Script Date: 2021/9/2 下午 09:27:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SYS_CONFIG](
	[CFG_ID] [int] IDENTITY(1,1) NOT NULL,
	[PARENT_ID] [int] NULL,
	[CFG_VALUE] [nvarchar](50) NULL,
	[CFG_DESC] [nvarchar](max) NULL,
 CONSTRAINT [PK_SYS_CONFIG] PRIMARY KEY CLUSTERED 
(
	[CFG_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次LOG ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'BATCH_LOG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'BATCH_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次狀態(0:正常, 1:異常)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'STATUS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'錯誤信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'ERR_MSG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建立日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG', @level2type=N'COLUMN',@level2name=N'CREATE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次LOG' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BATCH_LOG'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_CATEGORY', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_CATEGORY', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_CATEGORY', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'分類ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_CATEGORY', @level2type=N'COLUMN',@level2name=N'CATEGORY_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司分類' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_CATEGORY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'董事長' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'CHAIR_MAN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'總經理' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'GEN_MANAGER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司建立日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'EST_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'實收資本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'CAPITAL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'已發行股本' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'PUBL_SHARES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上市日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'SE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上櫃日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'OTC_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'興櫃日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'ES_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公開發行日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO', @level2type=N'COLUMN',@level2name=N'PUB_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司基本資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'COMPANY_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股利所屬期間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'DIV_BELONGS'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公告時間' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票股利-盈餘轉增資配股(元/股)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'S_DIV_REV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票股利-資本公積轉增資配股(元/股)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'S_DIV_LEG_RES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票股利' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'S_DIV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'除權交易日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'EX_S_DIV_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'現金股利-盈餘轉增資配股(元/股)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'C_DIV_REV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'現金股利-資本公積轉增資配股(元/股)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'C_DIV_LEG_RES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'現金股利' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'C_DIV'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'除息交易日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'EX_C_DIV_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'除息分派日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'PU_C_DIV_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'權利分派基準日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW', @level2type=N'COLUMN',@level2name=N'PU_RIGHT_BASE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股利發布資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DIVIDEND_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位資訊ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'FIELD_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'FIELD_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'TABLE_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'該欄位對應的Key(不在KV Table的資料，此欄位null)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'FIELD_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Key欄位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'KEY_COL_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'值欄位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'VAL_COL_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日期欄位名稱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'DATE_COL_NAME'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'固定Func' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO', @level2type=N'COLUMN',@level2name=N'FIX_FUNC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'欄位資訊，供動態查詢使用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FIELD_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報所屬日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW', @level2type=N'COLUMN',@level2name=N'FR_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報項目ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW', @level2type=N'COLUMN',@level2name=N'FR_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報項目值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW', @level2type=N'COLUMN',@level2name=N'FR_VALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報公布日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW', @level2type=N'COLUMN',@level2name=N'FR_RELEASE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'損益表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_IS_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報所屬日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW', @level2type=N'COLUMN',@level2name=N'FR_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報項目ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW', @level2type=N'COLUMN',@level2name=N'FR_KEY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報項目值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW', @level2type=N'COLUMN',@level2name=N'FR_VALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財報公布日' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW', @level2type=N'COLUMN',@level2name=N'FR_RELEASE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'財務比率表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FR_RATIO_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'期貨類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'FUTURE_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'FI_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'IT_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'DL_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'FI_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'IT_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'DL_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'淨交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'NET_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'FI_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'IT_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'DL_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'FI_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'IT_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'DL_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'淨未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW', @level2type=N'COLUMN',@level2name=N'NET_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'期貨三大法人交易結果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_FUTURE_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選擇權類別' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'OPTION_TYPE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'FI_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'IT_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商多方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'DL_LONG_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'FI_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'IT_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商空方交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'DL_SHORT_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'淨交易口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'NET_TXN_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'FI_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'IT_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商多方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'DL_LONG_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'FI_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'IT_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商空方未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'DL_SHORT_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'淨未平倉口數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW', @level2type=N'COLUMN',@level2name=N'NET_BAL_LOT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'選擇權三大法人交易結果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_OPTION_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'交易日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'TRANS_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資非自營商買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_NS_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資非自營商賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_NS_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資非自營商買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_NS_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資自營商買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_SELF_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資自營商賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_SELF_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資自營商買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_SELF_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'外資買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'FI_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'IT_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'IT_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'IT_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商買進股數(避險)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_HEDGING_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商賣出股數(避險)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_HEDGING_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投信買賣超(避險)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_HEDGING_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商買進股數(自行買賣)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_SELF_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商賣出股數(自行買賣)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_SELF_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商買賣超(自行買賣)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_SELF_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'自營商買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'DL_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'三大法人買進股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'LP_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'三大法人賣出股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'LP_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'三大法人買賣超' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW', @level2type=N'COLUMN',@level2name=N'LP_NET'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'法人買賣超資訊' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LP_TRANS_LOG_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融資買(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'FIN_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融資賣(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'FIN_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'現金償還(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'FIN_REPAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融資餘額(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'FIN_BALANCE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融資限額(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'FIN_QUOTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融券買(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'LEND_BUY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融券賣(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'LEND_SELL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'現券償還(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'LEND_REPAY'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融券餘額(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'LEND_BALANCE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'融券限額(張)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'LEND_QUOTA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'註記(O：停止融資, X：停止融券, @：融資分配, %：融券分配, !：停止買賣, Y：未取得信用交易資格, V：不得借券交易且無借券餘額停止借券賣出, Z：借券賣出餘額已達總量控管標準或初次上櫃無賣出額度暫停借券賣出, !：停止買賣)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO', @level2type=N'COLUMN',@level2name=N'MEMO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資券餘額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MARGIN_TRADE_INFO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'股票代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營收所屬月份' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'REVENUE_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'REVENUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'上個月營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'REVENUE_LAST_MONTH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'去年同期營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'REVENUE_LAST_YEAR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'累計營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'SUM_REVENUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'去年同期累計營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW', @level2type=N'COLUMN',@level2name=N'SUM_REVENUE_LAST_YEAR'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'月營收' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'MONTHLY_REVENUE_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'標的代號' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'SID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'開盤價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'OPEN'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最高價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'HIGH'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最低價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'LOW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收盤價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成交金額' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'TURN_OVER'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成交股數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'TRADING_SHARES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成交筆數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'TRADING_NUM'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後買價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'LAST_BUY_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後買量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'LAST_BUY_VOL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後賣價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'LAST_SOLD_PRICE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最後賣量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW', @level2type=N'COLUMN',@level2name=N'LAST_SOLD_VOL'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'TWSE股價' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PRICE_DAILY_TW'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'資料日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'DATA_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'集保總張數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'SHARES_COUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'總股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'SHAREHOLDER_COUNT'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'平均張數/人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'AVG_SHARES_EACH_PERSON'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'>400張大股東持有張數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'HOLDING_COUNT_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'>400張大股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'PERSON_COUNT_1'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'400-600張大股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'PERSON_COUNT_2'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'600-800張大股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'PERSON_COUNT_3'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'800-1000張大股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'PERSON_COUNT_4'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'>1000張大股東人數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'PERSON_COUNT_5'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'批次日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SHARES_DISPERSION', @level2type=N'COLUMN',@level2name=N'BATCH_DATE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參數ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CONFIG', @level2type=N'COLUMN',@level2name=N'CFG_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參數父ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CONFIG', @level2type=N'COLUMN',@level2name=N'PARENT_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參數值' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CONFIG', @level2type=N'COLUMN',@level2name=N'CFG_VALUE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'參數描述' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CONFIG', @level2type=N'COLUMN',@level2name=N'CFG_DESC'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系統參數' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SYS_CONFIG'
GO
