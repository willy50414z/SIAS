package com.willy.sias.db.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.BeanFactory;

@Repository
public class CompanyDao {
	public static Map<Integer, String> getAllCompanyCategoryMap(){
		SysConfigRepository configRepo = BeanFactory.getBean(SysConfigRepository.class);
		List<SysConfigPO> configList = configRepo.findByParentIdIsOrderByCfgId(2);
		
		Map<Integer, String> companyCategoryMap = new HashMap<> ();
		for (SysConfigPO config : configList) {
			companyCategoryMap.put(config.getCfgId(), config.getCfgDesc());
		}
		return companyCategoryMap;
	}
	public List<Integer> findSidListByCategoryId(Set<Integer> categoryIdSet){
		CompanyCategoryRepository ccRepo = BeanFactory.getBean(CompanyCategoryRepository.class);
		List<CompanyCategoryPO> ccList = ccRepo.findLastestCompanyCategoryByDataDateAndCategoryIdEquals(categoryIdSet);
		return ccList.stream().map(cc -> cc.getSid()).collect(Collectors.toList());
	}
}
