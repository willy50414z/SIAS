package com.willy.sias.web.controller;

import com.willy.sias.backtest.service.FieldFilterService;
import com.willy.sias.web.service.FieldInfoService;
import com.willy.sias.db.po.FieldInfoPO;
import com.willy.sias.db.repository.FieldInfoRepository;
import com.willy.sias.web.vo.FieldInfoVO;
import com.willy.util.type.WType;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/fieldQuery")
public class FieldEditorController {

  @Autowired
  private FieldInfoRepository fiRepo;

  @Autowired
  private FieldInfoService fiService;

  @Autowired
  private FieldFilterService ffSvc;

  @RequestMapping("/")
  public ModelAndView view() {
    ModelAndView mv = new ModelAndView("FieldEditor");
    mv.addObject("fieldInfoCatNameList",
        fiRepo.findAll().stream().map(fi -> fi.getFieldName().split(":")[0])
            .sorted().collect(Collectors.toCollection(LinkedHashSet::new)));
    return mv;
  }

  @ResponseBody
  @RequestMapping("/getSubFieldInfo")
  public FieldInfoVO getSubFieldInfo(String catName) {
    List<FieldInfoPO> fiList = fiRepo.findByFieldNameLike(catName + "%");
    if (fiList.stream().filter(fi -> fi.getFieldName().equals(catName)).count() > 0) {
      return fiService.getFieldInfoVo(fiList.get(0));
    } else {
      return fiService.getSubCatOrFieldNameList(catName, fiList);
    }
  }

  @ResponseBody
  @RequestMapping("/getFieldBlockDesc")
  public String getFieldBlockDesc(String fieldBlock) throws JSONException {
    return fiService.getFieldDesc(fieldBlock);
  }

  @ResponseBody
  @RequestMapping("/execFieldFilter")
  public String execFieldFilter(String fieldFilter) throws Exception {
    List<Date> dateList = ffSvc.executeProcBlocks(fieldFilter);
    JSONArray ja = new JSONArray();
    dateList.forEach(date -> ja.put(WType.dateToStr(date)));
    return ja.toString();
  }
}
