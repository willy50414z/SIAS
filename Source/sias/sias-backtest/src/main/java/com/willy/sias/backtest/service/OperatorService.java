package com.willy.sias.backtest.service;

import com.willy.sias.db.dto.TimeSeriesDTO;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class OperatorService {

  public List<TimeSeriesDTO<BigDecimal>> add(List<TimeSeriesDTO<BigDecimal>> tsList1,
      List<TimeSeriesDTO<BigDecimal>> tsList2) {
    Map<Date, BigDecimal> ts1Map = tsList1.stream()
        .filter(ts -> ts.getValue() != null)
        .collect(Collectors.toMap(TimeSeriesDTO::getDate, TimeSeriesDTO::getDecimalValue));

    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (TimeSeriesDTO<BigDecimal> ts2 : tsList2) {
      if (ts1Map.containsKey(ts2.getDate())) {
        result.add(new TimeSeriesDTO<>(ts2.getDate(),
            ts1Map.get(ts2.getDate()).add(ts2.getDecimalValue())));
      }
    }
    return result;
  }

  public List<TimeSeriesDTO<BigDecimal>> subtract(List<TimeSeriesDTO<BigDecimal>> tsList1,
      List<TimeSeriesDTO<BigDecimal>> tsList2) {
    Map<Date, BigDecimal> ts1Map = tsList1.stream()
        .filter(ts -> ts.getValue() != null)
        .collect(Collectors.toMap(TimeSeriesDTO::getDate, TimeSeriesDTO::getDecimalValue));

    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (TimeSeriesDTO<BigDecimal> ts2 : tsList2) {
      if (ts1Map.containsKey(ts2.getDate())) {
        result.add(new TimeSeriesDTO<>(ts2.getDate(),
            ts1Map.get(ts2.getDate()).subtract(ts2.getDecimalValue())));
      }
    }
    return result;
  }

  public List<TimeSeriesDTO<BigDecimal>> multiply(List<TimeSeriesDTO<BigDecimal>> tsList1,
      List<TimeSeriesDTO<BigDecimal>> tsList2) {
    Map<Date, BigDecimal> ts1Map = tsList1.stream()
        .filter(ts -> ts.getValue() != null)
        .collect(Collectors.toMap(TimeSeriesDTO::getDate, TimeSeriesDTO::getDecimalValue));

    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (TimeSeriesDTO<BigDecimal> ts2 : tsList2) {
      if (ts1Map.containsKey(ts2.getDate())) {
        result.add(new TimeSeriesDTO<>(ts2.getDate(),
            ts1Map.get(ts2.getDate()).multiply(ts2.getDecimalValue())));
      }
    }
    return result;
  }

  public List<TimeSeriesDTO<BigDecimal>> divide(List<TimeSeriesDTO<BigDecimal>> tsList1,
      List<TimeSeriesDTO<BigDecimal>> tsList2) {
    return divide(tsList1, tsList2, 4, RoundingMode.HALF_UP);
  }

  public List<TimeSeriesDTO<BigDecimal>> divide(List<TimeSeriesDTO<BigDecimal>> tsList1,
      List<TimeSeriesDTO<BigDecimal>> tsList2, int scale, RoundingMode roundingMode) {
    Map<Date, BigDecimal> ts1Map = tsList1.stream()
        .filter(ts -> ts.getValue() != null)
        .collect(Collectors.toMap(TimeSeriesDTO::getDate, TimeSeriesDTO::getDecimalValue));

    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (TimeSeriesDTO<BigDecimal> ts2 : tsList2) {
      if (ts1Map.containsKey(ts2.getDate())) {
        result.add(new TimeSeriesDTO<>(ts2.getDate(),
            ts1Map.get(ts2.getDate()).divide(ts2.getDecimalValue(), scale, roundingMode)));
      }
    }
    return result;
  }
}
