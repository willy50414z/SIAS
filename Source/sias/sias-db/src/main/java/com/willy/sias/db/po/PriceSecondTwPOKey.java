package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "PRICE_SECOND_TW")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class PriceSecondTwPOKey implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name = "SID")
	private int sid;
    @Column(name = "DATA_DATE")
	private Date dataDate;
}
