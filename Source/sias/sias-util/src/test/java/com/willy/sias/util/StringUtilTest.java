package com.willy.sias.util;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StringUtilTest {

  @Test
  public void testGetProccessBlock_whenContains2ObjectArgs_shouldSuccess() {
    Set<String> result = StringUtil.getProccessBlock("[123(1,2,[456(7,8,9)],[1011(12,13)])]");
    Set<String> expectedResult = new LinkedHashSet<>(Arrays.asList("[1011(12,13)]", "[456(7,8,9)]",
        "[123(1,2,${1},${0})]"));
    Assert.assertEquals(expectedResult, result);
  }
}
