package com.willy.sias.web.controller;

import com.willy.file.dictionary.WDictionary;
import com.willy.sias.web.service.BackTestMainService;
import com.willy.sias.web.util.WebUtil;
import com.willy.sias.web.vo.BackTestResultVO;
import com.willy.sias.web.vo.TradeDetailVO;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/BackTestMain")
public class BackTestMainController {
	@Autowired
	private BackTestMainService backTestService;
	@Value("${sias.candleChart.dir}")
	private String candleChartDir;

	@RequestMapping("/")
	public ModelAndView visit() throws ParseException {
		ModelAndView view = new ModelAndView("frames/MainFrame");
		view.addObject("homePageUrl", "viewBackTestMain");
		return view;
	}

	@RequestMapping("/viewBackTestMain")
	public ModelAndView viewBackTestMain() throws ParseException {
		ModelAndView view = new ModelAndView("BackTestMain");
		view.addObject("histTestIdList", backTestService.getLatest10TestId());
		return view;
	}

	@RequestMapping("/{testId}")
	public ModelAndView visit(@PathVariable("testId") String testId) throws ParseException {
		ModelAndView view = new ModelAndView("frames/MainFrame");
		view.addObject("homePageUrl", "viewBackTestMain/" + testId);
		return view;
	}

	@RequestMapping("/viewBackTestMain/{testId}")
	public ModelAndView viewBackTestMain(@PathVariable("testId") String testId) throws ParseException {
		ModelAndView view = new ModelAndView("BackTestMain");
		view.addObject("histTestIdList", backTestService.getLatest10TestId());
		view.addObject("testId", testId.split(" ")[0]);
		return view;
	}

	@ResponseBody
	@RequestMapping("/getSidListByCategory")
	public Map<Integer, String> getSidListByCategory(Integer categoryId) {
		if (categoryId == null) {
			return null;
		} else {
			return backTestService.getSidAndCompanyDescMapByCompanyCategory(categoryId);
		}
	}

	@ResponseBody
	@RequestMapping("/findBackTestResultByTestDesc")
	public BackTestResultVO findBackTestResultByTestDesc(String testDesc, HttpServletRequest request) {
		BackTestResultVO btResult = new BackTestResultVO();
		try {
			if (StringUtils.isNotEmpty(testDesc)) {
				btResult = backTestService.getBackTestResultVO(Integer.parseInt(testDesc.split(" ")[0]),
						request.getContextPath());
			} else {
				btResult.setErrMsg("TestDesc[" + testDesc + "] is empty");
				WLog.error("TestDesc[" + testDesc + "] is empty");
			}
		} catch (Exception e) {
			WLog.error(e);
			btResult.setErrMsg(WType.exceptionToString(e));
		}
		return btResult;
	}

	@ResponseBody
	@RequestMapping("/QueryTradeDetail")
	public TradeDetailVO queryTradeDetail(Integer testId) throws Exception {
		return backTestService.getBackTestTradeDetail(testId);
	}

	@ResponseBody
	@RequestMapping("/getBackTestIdList")
	public List<String> getBackTestIdList() throws Exception {
		return backTestService.getLatest10TestId();
	}

	@ResponseBody
	@RequestMapping(value = "/downloadCandleChart")
	public String downloadCandleChart(int sid, String date) throws Exception {
		String filePath = backTestService.exportCandleChart(sid, date);
		if (filePath == null) {
			return "";
		} else {
			return WDictionary.getFileName(filePath);
		}
	}

	@ResponseBody
	@RequestMapping(value = "/downloadStrategySource/{testId}")
	public ResponseEntity<byte[]> downloadStrategySource(@PathVariable("testId") Integer testId) throws Exception {
		String strategySourcePath = backTestService.getStrategySourcePath(testId);
		File file = new File(strategySourcePath);
		if (!file.exists()) {
			WLog.error("strategySourcePath is not exist, testId[" + testId + "]");
			return null;
		}
		ResponseEntity<byte[]> rs = WebUtil.getDownloadObject(strategySourcePath, file.getName());
		return rs;
	}

	@ResponseBody
	@RequestMapping(value = "/getCandleChart", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getCandleChart(String fileName) throws IOException {
		return IOUtils.toByteArray(new FileInputStream(candleChartDir + fileName));
	}

}
