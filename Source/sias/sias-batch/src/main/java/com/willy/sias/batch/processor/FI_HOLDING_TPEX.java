package com.willy.sias.batch.processor;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WRegex;
import com.willy.util.string.WString;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.fi-holding-tpex")
public class FI_HOLDING_TPEX extends BaseBatchProcessor {
	@Autowired
	private WJsoup js;
	@Autowired
	private WFile_txt fileTxt;
	@Autowired
	private LPTransLog_TW_TWSERepository lpRepo;
	private String urlTempl;
	private String csvFileNameTempl;
	
	@Override
	public void cleanHisData() throws Exception {
		
	}

	@Override
	public void exec() throws Exception {
		//下載資料檔
		String filePath = downloadFIHoldingTwseTable();
		
		//當天沒資料
		if(filePath.equals("")) {
			WLog.info("DataDate["+this.dataDate+"] has no data");
			return;
		}
		
		//讀取資料檔
		String[][] fiHoldingAr = fileTxt.readToAr(filePath);
		
		mergeSid(fiHoldingAr);
		
		//撈出當天的lp資料更新進去
		List<LPTransLogTwPO> lpList = lpRepo.findByTransDateIs(this.dataDate);
		
		for(String[] fiHolding : fiHoldingAr) {
			LPTransLogTwPO lpPo = lpList.stream().filter(lp -> lp.getSid().compareTo(sidMap.get(fiHolding[0])) == 0).findFirst().orElse(null);
			if(lpPo==null) {
				continue;
			}
			lpPo.setPublishCount(new BigDecimal(WString.cleanIntegerData(fiHolding[2])));
			lpPo.setFiHolding(new BigDecimal(WString.cleanIntegerData(fiHolding[4])));
			lpPo.setFiBuyable(new BigDecimal(WString.cleanIntegerData(fiHolding[3])));
			if(StringUtils.isNotEmpty(fiHolding[9])) {
				List<String> matchStrList = WRegex.getMatchedStrList(fiHolding[9], ">.{1}<");
				if(CollectionUtils.isEmpty(matchStrList)) {
					lpPo.setFiBuyableMdfReason(fiHolding[9]);
				} else {
					if(matchStrList.get(0).length()>1) {
						lpPo.setFiBuyableMdfReason(matchStrList.get(0).substring(1, 2));
					}
				}
			}
		}
		lpRepo.saveAll(lpList);
	}
	
	private String downloadFIHoldingTwseTable() throws IOException, InterruptedException, ExecutionException {
		int years = WDate.getDateInfo(this.dataDate, Calendar.YEAR);
		int months = WDate.getDateInfo(this.dataDate, Calendar.MONTH);
		int days = WDate.getDateInfo(this.dataDate, Calendar.DATE);
		this.putMapParam("years", String.valueOf(years));
		this.putMapParam("months", WString.fillStrLength(String.valueOf(months), "0", -2));
		this.putMapParam("days", WString.fillStrLength(String.valueOf(days), "0", -2));
		this.putMapParam("strDataDate", this.strDataDate);

		String filePath = fileDownloadDir + parseMapParamToString(csvFileNameTempl);
		if(new File(filePath).exists()) {
			return filePath;
		}
		Document doc = js.getBody(parseMapParamToString(urlTempl), "MS950");
		if(doc.html().contains("查無所需資料")) {
			WLog.warn("查無所需資料 filePath["+filePath+"]");
			return "";
		}
		
		StringBuffer sb = new StringBuffer();
		doc.getElementsByTag("table").first().getElementsByTag("tr").forEach(tr -> {
			tr.getElementsByTag("td").forEach(td -> sb.append("\"").append(td.text().trim()).append("\","));
			if(sb.length()>1) {
				sb.setLength(sb.length()-1);
				sb.append("\r\n");
			}
		});
		fileTxt.writeByStr(filePath, sb.toString());
		return filePath;
	}
}
