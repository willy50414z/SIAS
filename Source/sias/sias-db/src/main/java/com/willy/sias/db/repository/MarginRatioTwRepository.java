package com.willy.sias.db.repository;

import com.willy.sias.db.po.MarginRatioTwPO;
import com.willy.sias.db.po.MarginRatioTwPOKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MarginRatioTwRepository extends
    JpaRepository<MarginRatioTwPO, MarginRatioTwPOKey> {

}
