//package com.willy.sias.backtest.strategy;
//
//import java.text.ParseException;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.backtest.po.TradeDetailPO;
//import com.willy.sias.backtest.util.TradeUtil;
//import com.willy.sias.db.po.SysConfigPO;
//import com.willy.sias.db.repository.SysConfigRepository;
//import com.willy.sias.util.EnumSet.TradeType;
//import com.willy.sias.util.config.Const;
//import com.willy.util.date.WDate;
//import com.willy.util.log.WLog;
//import com.willy.util.string.WString;
//import com.willy.util.type.WType;
//
//import lombok.Getter;
//
//@Component
//@Getter
//public class SeasonStrategy extends TradeStrategy {
//	@StrategyArg(desc = "買入月份(1~12)")
//	private Integer month;
//	@StrategyArg(desc = "買入週數(1~3)")
//	private Integer partOfMonth;
//	@StrategyArg(desc = "每個月投資單位數")
//	private Integer buyCountForEachTrade;// 每筆交易投資筆數
//	@StrategyArg(desc = "買入持有工作日數")
//	private Integer keepDays;// 買入後存放幾天後賣掉
//	@Autowired
//	private TradeUtil su;
//	@Autowired
//	private SysConfigRepository config;
//
//	public SeasonStrategy() {
//		super();
//		this.setSingleTarget(true);
//	}
//
//	@Override
//	public void setTradeDetail(Date tradeDate, ProfitPO profitPo) {
//		// TODO Auto-generated method stub
//		try {
//			tradeJudgeLog.setLength(0);
//			for (Integer sid : this.getSidSet()) {
//				// 資料檢核
//				if (partOfMonth > 3 || partOfMonth < 0) {
//					return;
//				}
//
//				int tradeYear = WDate.getDateInfo(tradeDate, Calendar.YEAR);
//				int tradeMonth = WDate.getDateInfo(tradeDate, Calendar.MONTH);
//				// 該SID的預期交易日
//				Date targetTradeDate = WType.strToDate(tradeYear + WString.fillStrLength(String.valueOf(month), "0", -2)
//						+ "" + WString.fillStrLength(String.valueOf((partOfMonth - 1) * 10 + 5), "0", -2));
//				List<TradeDetailPO> tradeDetailList = profitPo.getTradeDetailList();
//
//				// 交易日>=目標日
//				// 同個期間之前沒有交易過(一個月只取一個交易時間點)
//				tradeJudgeLog.append("今天日期[" + WType.dateToStr(tradeDate) + "]，目標買入日[")
//						.append(WType.dateToStr(targetTradeDate)).append("]");
//				if (WDate.afterOrEquals(tradeDate, targetTradeDate) && tradeDetailList.stream()
//						.filter(td -> WDate.getDateInfo(td.getBuyDate(), Calendar.YEAR) == tradeYear
//								&& WDate.getDateInfo(td.getBuyDate(), Calendar.MONTH) == tradeMonth)
//						.count() == 0L && month == tradeMonth) {
//					su.trade(sid, tradeDate, buyCountForEachTrade, TradeType.buy, profitPo);
//					tradeJudgeLog.append("買入SID[").append(sid).append("]張數[").append(buyCountForEachTrade).append("]");
//				} else {
//					tradeJudgeLog.append("不符合交易條件。");
//				}
//				tradeJudgeLog.append("\r\n");
//
//				// 賣出[買入20天以上]數量的股票
//				int sellNum = new Long(
//						tradeDetailList.stream()
//								.filter(td -> WDate.afterOrEquals(tradeDate,
//										WDate.calDate_Day(td.getBuyDate(), keepDays)) && td.getSellDate() == null)
//								.count()).intValue();
//				su.trade(sid, tradeDate, sellNum, TradeType.sell, profitPo);
//				tradeJudgeLog.append("今天日期[" + WType.dateToStr(tradeDate) + "]持有天數>").append(keepDays).append("天的張數有[")
//						.append(sellNum).append("]張，賣出[").append(sellNum).append("]張");
//				tradeJudgeLog.append("\r\n");
//			}
//			WLog.info(tradeJudgeLog.toString());
//		} catch (Exception e) {
//			try {
//				WLog.error("tradeDate[" + WType.dateToStr(tradeDate) + "]profitPo[" + WString.toString(profitPo)
//						+ "]設定存貨失敗", e);
//			} catch (ParseException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//		}
//	}
//
//	public String getStrategyDesc() {
//		try {
//			StringBuffer descSb = new StringBuffer();
//			for (Integer sid : this.getSidSet()) {
//				SysConfigPO coompanyConfig = config.findById(sid).orElse(null);
//				descSb.append("SID[").append(sid).append("]TargetCode[").append(coompanyConfig.getCfgValue())
//						.append("]TargetName[").append(coompanyConfig.getCfgDesc()).append("]於每年").append(month)
//						.append("月").append((partOfMonth - 1) * 10 + 5).append("日(非交易日則向後順延)買入")
//						.append(buyCountForEachTrade).append("張股票，並於20個交易日後賣出\r\n");
//			}
//			return descSb.toString();
//		} catch (Exception e) {
//			WLog.error(e);
//			return "";
//		}
//
//	}
//
//	public void setSu(TradeUtil su) {
//		this.su = su;
//	}
//
//	public void setBuyCountForEachTrade(Integer buyCountForEachTrade) {
//		this.buyCountForEachTrade = buyCountForEachTrade;
//	}
//
//	@Override
//	public String checkArgs() {
//		// TODO Auto-generated method stub
//		if (month < 1 || month > 12) {
//			return "month[" + month + "] 不可<1 或 >12";
//		}
//		if (partOfMonth < 1 || partOfMonth > 3) {
//			return "partOfMonth[" + partOfMonth + "] 不可<1 或 >3";
//		}
//		return "";
//	}
//
//	@Override
//	public void init() {
//	}
//}
