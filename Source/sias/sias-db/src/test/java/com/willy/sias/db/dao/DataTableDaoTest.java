package com.willy.sias.db.dao;

import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.FieldInfoPO;
import com.willy.sias.db.repository.FieldInfoRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class DataTableDaoTest {

  @Autowired
  private DataTableDao dtDao;
  private final int sid = 494;
  private final int fieldId = Const._FIELDID_PRICE_DAILY_TW_CLOSE;

  @MockBean
  private WJDBC jdbcRepo;
  @MockBean
  private FieldInfoRepository fiRepo;

  private List<TimeSeriesDTO<BigDecimal>> tsList;
  private Set<Date> dateSet;

  @Before
  public void init() {
    tsList = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20200529"), new BigDecimal("292.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200601"), new BigDecimal("295.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200602"), new BigDecimal("296.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200603"), new BigDecimal("301.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200604"), new BigDecimal("306.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200605"), new BigDecimal("311.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200608"), new BigDecimal("318.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200609"), new BigDecimal("319.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200616"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200617"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200618"), new BigDecimal("314.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200619"), new BigDecimal("314.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200622"), new BigDecimal("312.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200623"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200624"), new BigDecimal("317.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200629"), new BigDecimal("312.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200630"), new BigDecimal("313.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200701"), new BigDecimal("317.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200702"), new BigDecimal("322.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200703"), new BigDecimal("329.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200706"), new BigDecimal("338.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200707"), new BigDecimal("338.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200708"), new BigDecimal("341.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200709"), new BigDecimal("345.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200710"), new BigDecimal("348.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200713"), new BigDecimal("354.5")));

    dateSet = new HashSet<>(Arrays.asList(WType.strToDate("20200529"),
        WType.strToDate("20200601"),
        WType.strToDate("20200602"),
        WType.strToDate("20200603"),
        WType.strToDate("20200604"),
        WType.strToDate("20200605"),
        WType.strToDate("20200608"),
        WType.strToDate("20200609"),
        WType.strToDate("20200610"),
        WType.strToDate("20200611"),
        WType.strToDate("20200612"),
        WType.strToDate("20200615"),
        WType.strToDate("20200616"),
        WType.strToDate("20200617"),
        WType.strToDate("20200618"),
        WType.strToDate("20200619"),
        WType.strToDate("20200622"),
        WType.strToDate("20200623"),
        WType.strToDate("20200624"),
        WType.strToDate("20200629"),
        WType.strToDate("20200630"),
        WType.strToDate("20200701"),
        WType.strToDate("20200702"),
        WType.strToDate("20200703"),
        WType.strToDate("20200706"),
        WType.strToDate("20200707"),
        WType.strToDate("20200708"),
        WType.strToDate("20200709"),
        WType.strToDate("20200710"),
        WType.strToDate("20200713")));
    Mockito.when(fiRepo.findAllById(Arrays.asList(fieldId))).thenReturn(
        Arrays.asList(
            new FieldInfoPO(fieldId, "股價 - 收盤價(日)", "[PRICE_DAILY_TW]", null, null, "[Close]",
                "[DATA_DATE]", null, null)));
    Mockito.when(fiRepo.findById(fieldId)).thenReturn(
        Optional.of(
            new FieldInfoPO(fieldId, "股價 - 收盤價(日)", "[PRICE_DAILY_TW]", null, null, "[Close]",
                "[DATA_DATE]", null, null)));

    Mockito.when(jdbcRepo.<TimeSeriesDTO<Double>>queryForBeanList(
        Mockito.anyString(),
        Mockito.any(RowMapper.class))).thenReturn(tsList);

    Mockito.when(jdbcRepo.queryForBeanSet(Mockito.anyString(), Mockito.any(RowMapper.class))).thenReturn(dateSet);

  }

  @Test
  public void testfindTsListWithSidAndFieldKeyListAndClassShouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = dtDao.findTsList(sid,
        Arrays.asList(fieldId), BigDecimal.class);
    Assert.assertEquals(tsList, result);
  }

  @Test
  public void testfindTsListWithSidAndFieldKeyAndClassShouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = dtDao.findTsList(sid,
        fieldId, BigDecimal.class);
    Assert.assertEquals(tsList, result);
  }

  @Test
  public void testfindTsListWithSidAndFieldKeyListAndClassAndStartDateAndEndDateShouldSuccess() {
    List<TimeSeriesDTO<BigDecimal>> result = dtDao.findTsList(sid,
        Arrays.asList(fieldId), BigDecimal.class, WType.strToDate("20200610"), WType.strToDate("20200630"));

    List<TimeSeriesDTO<BigDecimal>> expectedResult = Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200616"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200617"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200618"), new BigDecimal("314.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200619"), new BigDecimal("314.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200622"), new BigDecimal("312.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200623"), new BigDecimal("315.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200624"), new BigDecimal("317.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200629"), new BigDecimal("312.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200630"), new BigDecimal("313.0")));
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void testfindRecentTsListWithSidAndFieldKeyAndBaseDateAndIntervalShouldSuccess(){
    List<TimeSeriesDTO<BigDecimal>> result = dtDao.findRecentTsList(sid, fieldId, WType.strToDate("20200615"), -5);
    List<TimeSeriesDTO<BigDecimal>> expectedResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200609"), new BigDecimal("319.0"))
    );
    Assert.assertEquals(expectedResult, result);

    expectedResult = Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20200609"), new BigDecimal("319.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")));
    result = dtDao.findRecentTsList(sid, fieldId, WType.strToDate("20200609"), 5);
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void testFindRecentTsListWithSidAndFieldKeyAndBaseDateAndIntervalShouldSuccess(){
    List<TimeSeriesDTO<BigDecimal>> result = dtDao.findRecentTsList(sid, Arrays.asList(fieldId), BigDecimal.class, WType.strToDate("20200615"), -5);
    List<TimeSeriesDTO<BigDecimal>> expectedResult = Arrays.asList(
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200609"), new BigDecimal("319.0"))
    );
    Assert.assertEquals(expectedResult, result);

    expectedResult = Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20200609"), new BigDecimal("319.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200610"), new BigDecimal("322.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200611"), new BigDecimal("320.5")),
        new TimeSeriesDTO<>(WType.strToDate("20200612"), new BigDecimal("316.0")),
        new TimeSeriesDTO<>(WType.strToDate("20200615"), new BigDecimal("309.5")));
    result = dtDao.findRecentTsList(sid, Arrays.asList(fieldId), BigDecimal.class, WType.strToDate("20200609"), 5);
    Assert.assertEquals(expectedResult, result);
  }

  @Test
  public void testFindDateSetByFieldIdAndSidListWithFieldIdAndSidListShouldSuccess(){
    Set<Date> result =  dtDao.findDateSetByFieldIdAndSidList(fieldId, Arrays.asList(sid));
    Assert.assertEquals(dateSet, result);
  }

}
