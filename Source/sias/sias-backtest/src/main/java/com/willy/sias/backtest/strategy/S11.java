package com.willy.sias.backtest.strategy;

import com.willy.sias.backtest.dto.BreakUdTrackDTO;
import com.willy.sias.backtest.dto.TradePlanDTO;
import com.willy.sias.backtest.service.TechDmsService;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradeDetailDTO;
import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.OHLCDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;

@Service
@Scope("prototype")
public class S11 extends TradeStrategy {
	@Autowired
	private CalendarRepository caRepo;
	@Autowired
	private DataTableDao dtDao;
	@Autowired
	private TechIdxService tiSvc;
	@Autowired
	private TechDmsService tdSvc;

	@Override
	public void collectTradePlanList() {
		try {
			List<TimeSeriesDTO<BigDecimal>> ma20TsList = tiSvc.getSMA(this.getSid(), 20, Const._FIELDID_PRICE_DAILY_TW_CLOSE);
			List<TimeSeriesDTO<BigDecimal>> ma60TsList = tiSvc.getSMA(this.getSid(), 60, Const._FIELDID_PRICE_DAILY_TW_CLOSE);

			//VOL MA20>2000
			List<TimeSeriesDTO<BigDecimal>> volMa20TsList = tiSvc.getSMA(this.getSid(), 20, Const._FIELDID_PRICE_DAILY_TW_TRADING_SHARES);
			Set<Date> volLt2000DateSet = volMa20TsList.stream()
					.filter(ts -> ts.getValue() != null && ts.getDecimalValue().compareTo(new BigDecimal(2000000)) > 0)
					.map(TimeSeriesDTO::getDate).collect(Collectors.toSet());
			
			BreakUdTrackDTO preBtDto = null;
			for (Date tradeDate : this.getTradeDateList()) {
				if(!volLt2000DateSet.contains(tradeDate)) {
					continue;
				}
				
				BreakUdTrackDTO b = tdSvc.getBreakUdTrackStatus(this.getSid(), tradeDate);
				//b = null表示可能沒有該公司股價資料
				if(b == null) {
					break;
				}
				
				if (b.getBreakUdStatus() < 0) {
					preBtDto = b;
				}
				
				
				//當天MA20/60價格
				TimeSeriesDTO<BigDecimal> ma201 = ma20TsList.stream().filter(m -> m.getDate().compareTo(tradeDate) == 0)
						.findFirst().orElse(null);
				TimeSeriesDTO<BigDecimal> ma601 = ma60TsList.stream().filter(m -> m.getDate().compareTo(tradeDate) == 0)
						.findFirst().orElse(null);
				
				//MA20死叉60天數
				List<TimeSeriesDTO<BigDecimal>> subMa20TsList = ma20TsList.stream().filter(ma20 -> WDate.beforeOrEquals(ma20.getDate(), tradeDate)).collect(Collectors.toList());
				List<TimeSeriesDTO<BigDecimal>> subMa60TsList = ma60TsList.stream().filter(ma60 -> WDate.beforeOrEquals(ma60.getDate(), tradeDate)).collect(Collectors.toList());
				int crossDays = -1;
				for(int i=subMa20TsList.size()-1; i>=0;i--) {
					if(subMa20TsList.get(i).getDecimalValue().compareTo(subMa60TsList.get(i).getDecimalValue())>0) {
						crossDays = subMa20TsList.size()-1-i;
						break;
					}
				}
				
				//買進: 向下突破趨勢線後，再向上突破趨勢線，T日為紅K，MA20/60死叉>20天
				BigDecimal tOpenPrice = this.tu.getPrice(this.getSid(), PriceType.OPEN, tradeDate);
				BigDecimal tClosePrice = this.tu.getPrice(this.getSid(), PriceType.CLOSE, tradeDate);
				if (preBtDto != null && preBtDto.getBreakUdStatus() < 0 && b.getBreakUdStatus() >= 0 && ma201 != null
						&& ma601 != null && ma201.getDecimalValue().compareTo(ma601.getDecimalValue()) < 0
						&& crossDays > 20 && tClosePrice.compareTo(tOpenPrice) > 0) {
					this.addTradePlanList(new TradePlanDTO(this.getSid(), tradeDate, TradeType.buy, 1000, tClosePrice));
					
//					//賣出: 突破前向下突破低點
					List<TimeSeriesDTO<BigDecimal>>  tsList = dtDao.findRecentTsList(this.getSid(), Const._FIELDID_PRICE_DAILY_TW_LOW, preBtDto.getBaseDate(), preBtDto.getBreakUdStatus());
					BigDecimal stopLossPrice = tsList.stream().map(ts -> ts.getDecimalValue()).min(Const._DEFAULT_COMPARATOR_BIGDECIMAL).orElse(tClosePrice.multiply(new BigDecimal("0.95")));
					
					//賣出: 投報率>10%
					BigDecimal stopEarnPrice = tClosePrice.multiply(new BigDecimal("1.1"));
					
					//賣出: 持有超過20天直接賣
					BigDecimal sellPrice;
					List<TimeSeriesDTO<OHLCDTO>>  ohlcTsList = dtDao.<OHLCDTO>findRecentTsList(this.getSid(), Const._FIELDID_LIST_OHLC, OHLCDTO.class, tradeDate, 20);
					TimeSeriesDTO<OHLCDTO> sellTs = ohlcTsList.stream()
							.filter(ts -> ts.getValue().getHigh().compareTo(stopEarnPrice) >= 0).findFirst().orElse(null);
					sellPrice = sellTs == null ? null : stopEarnPrice;
					sellTs = sellPrice!=null ? sellTs : ohlcTsList.stream()
									.filter(ts -> ts.getValue().getClose().compareTo(stopLossPrice) < 0).findFirst()
									.orElse(ohlcTsList.get(ohlcTsList.size() - 1));
					sellPrice = sellTs.getValue().getClose();
					this.addTradePlanList(new TradePlanDTO(this.getSid(), sellTs.getDate(), TradeType.sell, 1000,
							sellPrice));
					
					preBtDto = null;
				}
			}
		} catch (Exception e) {
			WLog.error(e);
		}
	}

	@Override
	public void collectTradePlanInfo(ProfitDTO po) {
		List<List<String>> tableData = new ArrayList<>();
		tableData.add(Arrays.asList("前一日紅K","當日紅K","前一日漲幅","今日漲幅","近2日漲幅","MA交叉死亡天數","MA交叉幅度"));
		po.getTradeDetailList().stream().filter(td -> td.getGrossProfit() != null).forEach(td -> {
			try {
				List<String> rowData = new ArrayList<>();
				TradeDetailDTO buyTd = po.getTradeDetailList().stream().filter(tdd -> td.getParentTradeIdSet().contains(tdd.getTradeId())).findFirst().orElse(null);
				List<TimeSeriesDTO<OHLCDTO>> ohlcTsList = dtDao.findTsList(buyTd.getSid(), Const._FIELDID_LIST_OHLC, OHLCDTO.class, null, buyTd.getTradeDate());
				List<TimeSeriesDTO<BigDecimal>> ma20TsList = tiSvc.getSMA(ohlcTsList.stream().map(ohlc -> new TimeSeriesDTO<BigDecimal>(ohlc.getDate(), ohlc.getValue().getClose())).collect(Collectors.toList()), 20);
				List<TimeSeriesDTO<BigDecimal>> ma60TsList = tiSvc.getSMA(ohlcTsList.stream().map(ohlc -> new TimeSeriesDTO<BigDecimal>(ohlc.getDate(), ohlc.getValue().getClose())).collect(Collectors.toList()), 60);
				TimeSeriesDTO<OHLCDTO> tTs = ohlcTsList.get(ohlcTsList.size() - 1);
				TimeSeriesDTO<OHLCDTO> tm1Ts = ohlcTsList.get(ohlcTsList.size() - 2);
				TimeSeriesDTO<OHLCDTO> tm2Ts = ohlcTsList.get(ohlcTsList.size() - 3);
				//前一日紅K
				rowData.add((tm1Ts.getValue().getClose().compareTo(tm1Ts.getValue().getOpen()) > 0)?"Y":"N");
				//當日紅K
				rowData.add((tTs.getValue().getClose().compareTo(tTs.getValue().getOpen()) > 0)?"Y":"N");
				//前一日漲幅
				rowData.add(tm1Ts.getValue().getClose().divide(tm2Ts.getValue().getClose(), 4, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1).multiply(Const._BIGDECIMAL_100).setScale(2).toString() + "%");
				//今日漲幅
				rowData.add(tTs.getValue().getClose().divide(tm1Ts.getValue().getClose(), 4, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1).multiply(Const._BIGDECIMAL_100).setScale(2).toString() + "%");
				//近2日漲幅
				rowData.add(tTs.getValue().getClose().divide(tm2Ts.getValue().getClose(), 4, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1).multiply(Const._BIGDECIMAL_100).setScale(2).toString() + "%");
				//MA交叉死亡天數
				int crossDays = -1;
				for(int i=ma20TsList.size()-1; i>=0;i--) {
					if(ma20TsList.get(i).getDecimalValue().compareTo(ma60TsList.get(i).getDecimalValue())>0) {
						crossDays = ma20TsList.size()-1-i;
						break;
					}
				}
				rowData.add(""+crossDays);
				//MA交叉幅度
				TimeSeriesDTO<BigDecimal> ma20Ts = ma20TsList.stream().filter(ma20 -> ma20.getDate().compareTo(buyTd.getTradeDate())==0).findFirst().orElse(null);
				TimeSeriesDTO<BigDecimal> ma60Ts = ma60TsList.stream().filter(ma60 -> ma60.getDate().compareTo(buyTd.getTradeDate())==0).findFirst().orElse(null);
				if(ma20Ts != null && ma60Ts != null) {
					rowData.add(ma60Ts.getDecimalValue().divide(ma20Ts.getDecimalValue(), 2, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1).multiply(Const._BIGDECIMAL_100).setScale(2).toString() + "%");
				} else {
					rowData.add("");
				}
				tableData.add(rowData);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		WLog.info(tableData.toString());
		po.setCustColList(tableData);
	}
}
