package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.sias.db.po.LPFutureTWPO;
import com.willy.sias.db.repository.LP_FUTURE_TWRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.FileUtil;
import com.willy.util.date.WDate;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.lp-future-tw")
public class LP_FUTURE_TW extends BaseBatchProcessor {

  @Autowired
  private LP_FUTURE_TWRepository lftRepo;

  @Override
  public void cleanHisData() throws Exception {
//		lftRepo.deleteById(this.dataDate);
  }

  @Override
  protected void exec() throws Exception {
    //add slash date
    SimpleDateFormat sf = new SimpleDateFormat("yyyy/MM/dd");
    String slashDate = sf.format(dataDate);
    this.putMapParam("strDataDate_slash", slashDate);

    //下載CSV檔
    this.filePath = FileUtil.downloadByJsoup(parseMapParamToString(this.urlTempl),
        parseMapParamToString(this.csvFileNameTempl));

    //讀檔
    String[][] data = txtFile.readToAr(this.filePath, "日期", null, true);

    //組PO
    Set<LPFutureTWPO> lpfSet = compose(data);

    //save
    lftRepo.saveAll(lpfSet);
  }

  private Set<LPFutureTWPO> compose(String[][] data) throws ParseException {
    String strDate = "";
    String futureType = "";
    LPFutureTWPO lpf = null;
    Set<LPFutureTWPO> lpfSet = new HashSet<>();
    HashMap<String, Integer> futureTypeMap = cfgDao.findFielddescAndKeyMapByParentkey(
        ParentKey.FutureType.getKey());
    for (String[] rowData : data) {
      if (!strDate.equals(rowData[0]) || !futureType.equals(rowData[1])) {
        strDate = rowData[0];
        futureType = rowData[1];
        if (futureType.contains("已下市")) {
          continue;
        }
        lpf = new LPFutureTWPO();
        lpf.setDataDate(WType.strToDate(strDate, WDate.dateFormat_yyyyMMdd_Slash));
        lpf.setFutureType(futureTypeMap.get(futureType));
        if (lpf.getFutureType() == null) {
          throw new IllegalArgumentException(
              "futureType[" + futureType + "]dataDate[" + this.dataDate
                  + "] has not register FutureType in sysConfig");
        }
      }
      if (rowData[2].contains("外資")) {
        lpf.setFiLongTxnLot(new BigDecimal(WString.cleanDigitData(rowData[3])));
        lpf.setFiShortTxnLot(new BigDecimal(WString.cleanDigitData(rowData[5])));
        lpf.setFiLongBalLot(new BigDecimal(WString.cleanDigitData(rowData[9])));
        lpf.setFiShortBalLot(new BigDecimal(WString.cleanDigitData(rowData[11])));
      } else if (rowData[2].contains("投信")) {
        lpf.setItLongTxnLot(new BigDecimal(WString.cleanDigitData(rowData[3])));
        lpf.setItShortTxnLot(new BigDecimal(WString.cleanDigitData(rowData[5])));
        lpf.setItLongBalLot(new BigDecimal(WString.cleanDigitData(rowData[9])));
        lpf.setItShortBalLot(new BigDecimal(WString.cleanDigitData(rowData[11])));
      }
      if (rowData[2].contains("自營商")) {
        lpf.setDlLongTxnLot(new BigDecimal(WString.cleanDigitData(rowData[3])));
        lpf.setDlShortTxnLot(new BigDecimal(WString.cleanDigitData(rowData[5])));
        lpf.setDlLongBalLot(new BigDecimal(WString.cleanDigitData(rowData[9])));
        lpf.setDlShortBalLot(new BigDecimal(WString.cleanDigitData(rowData[11])));
      }
      lpfSet.add(lpf);
    }
    return lpfSet;
  }
}
