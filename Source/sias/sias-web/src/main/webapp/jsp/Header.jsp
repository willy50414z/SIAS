<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-3.4.1.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-ui-1.12.1.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-blockUI.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-datatable-1.10.20.js"></script>
	<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/common.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/bootstrap.min-3.2.0.css"
	type="text/css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/jquery.dataTables.min-1.10.20.css"
	type="text/css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/jquery-ui1.12.1.css"
	type="text/css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/common.css"
	type="text/css" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.caret-1.3.7.js"></script>
</head>
</html>