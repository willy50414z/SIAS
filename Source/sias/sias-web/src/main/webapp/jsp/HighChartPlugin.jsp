<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
.highcharts-figure, .highcharts-data-table table {
	min-width: 360px;
	max-width: 800px;
	margin: 1em auto;
}

.highcharts-data-table table {
	font-family: Verdana, sans-serif;
	border-collapse: collapse;
	border: 1px solid #ebebeb;
	margin: 10px auto;
	text-align: center;
	width: 100%;
	max-width: 500px;
}

.highcharts-data-table caption {
	padding: 1em 0;
	font-size: 1.2em;
	color: #555;
}

.highcharts-data-table th {
	font-weight: 600;
	padding: 0.5em;
}

.highcharts-data-table td, .highcharts-data-table th,
	.highcharts-data-table caption {
	padding: 0.5em;
}

.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even)
	{
	background: #f8f8f8;
}

.highcharts-data-table tr:hover {
	background: #f1f7ff;
}
</style>
<script>
	function setHighChart(id, xTitle, subXTitle, yTitle, yValueName, data){
		Highcharts.chart(id, {
	      chart: {
	        zoomType: 'x'
	      },
	      title: {//Title
	        text: xTitle
	      },
	      subtitle: {//副Title
	        text: subXTitle
	      },
	      xAxis: {
	        type: 'datetime'
	      },
	      yAxis: {
	        title: {
	          text: yTitle//Y軸Title
	        }
	      },
	      legend: {
	        enabled: false
	      },
	      plotOptions: {
	        area: {
	          fillColor: {
	            linearGradient: {
	              x1: 0,
	              y1: 0,
	              x2: 0,
	              y2: 1
	            },
	            stops: [
	              [0, Highcharts.getOptions().colors[0]],
	              [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
	            ]
	          },
	          marker: {
	            radius: 2
	          },
	          lineWidth: 1,
	          states: {
	            hover: {
	              lineWidth: 1
	            }
	          },
	          threshold: null
	        }
	      },
	
	      series: [{
	        type: 'area',
	        name: yValueName,
	        data: data
	      }]
	    });
	}
</script>
</head>
