package com.willy.sias.backtest.processor;

import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import java.util.Date;

import com.willy.util.log.WLog;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

@Getter @Setter
public abstract class CurrentStatusProcessor {
	Integer sid;
	Date analyzedate;

	@Autowired
	SysConfigRepository scRepo;
	@Autowired
	PriceDailyTwRepository priceRepo;
	PriceDailyTwPO todayPrice;
	@Autowired
	DataTableDao dtDao;

	abstract String getProcessorName();

	public void process() {
		WLog.info("start analyze [" + this.getProcessorName() + "]  processor");
		exec();
		
	}
	public abstract void exec();
}
