<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SIAS</title>
<%@ include file="/jsp/Header.jsp"%>
</head>
<script>
$(function(){
  $("#qryBtn").on("click", function() {
    $.ajax({
      type: "POST",
      url: getContextPath() +
          "/categoryEditor/queryCompanyCatListBySidCodeAndCatName",
      data: {
       "qryStr": $("#qryTxt").val(),
       "qryType": $("#qryType").val()
      },
      success: function(result) {
        $("#catQryResult").val(result)
      }
    })
  })
  $("#qryType").selectmenu();
  $(".btn-secondary").on("click", function(){
    var cursorPos = $("#catQryResult").caret()
    var oldVal = $("#catQryResult").val()
    var newVal = oldVal.substr(0, cursorPos) + $(this).text() + oldVal.substring(cursorPos, oldVal.length)
    $("#catQryResult").val(newVal)
    $("#catQryResult").caret(cursorPos + 1)
  })
  $("#saveBtn").on("click", function(){
  var unFormattedCatString = $("#catQryResult").val();
    //get format category string
    $.ajax({
      type: "POST",
      url: getContextPath() +
          "/categoryEditor/getFormattedString",
      data: {
       "unFormattedCatString": unFormattedCatString
      },
      success: function(result) {
        if(confirm("CategoryDate: \r\n" + result)) {
          $.ajax({
                type: "POST",
                url: getContextPath() +
                    "/categoryEditor/saveCatToDb",
                data: {
                 "unFormattedCatString": unFormattedCatString
                },
                success: function(result) {
                  alert(result);
                }
          });
        }
      },
      error: function(e) {
        alert("error");
      }
    });
  })
  $("#add").on("click", function(){
    var catJs = JSON.parse($("#catQryResult").val());
    var firstKey = Object.keys(catJs)[0]
    catJs[firstKey].push("")
    var catStr = JSON.stringify(catJs)
    $("#catQryResult").val(catStr)
    $("#catQryResult").caret(catStr.indexOf("\"\"") + 1)
  })
})

</script>
<body>
  <table>
    <tr>
      <td><input id="qryTxt" type="text"/></td>
      <td><select id="qryType">
        <option value="sid">SID</option>
        <option value="cat">Category</option>
      </select></td>
      <!-- <td><select id="catNameSelect">
        <option value=""></option>
        <option value="all">ALL</option>
        <c:forEach var="catName" items="${catNameList}">
          <option value="${catName}">
            <c:out value="${catName}" />
          </option>
        </c:forEach>
      </select></td>
      <td><select id="subCatNameSelect"></select></td> -->
      <td><button type="button" class="btn btn-primary" id="qryBtn">查詢</button></td>
    </tr>
    </table>
    <table>
    <tr>
      <td><textarea id="catQryResult" style='width: 1184px; height: 93px;font-size: 25px;'></textarea></td>
    </tr>
    <tr>
      <td>
        <button type="button" class="btn btn-secondary">{}</button>
        <button type="button" class="btn btn-secondary">[]</button>
        <button type="button" class="btn btn-secondary">""</button>
        <button type="button" class="btn btn-secondary">:</button>
        <button type="button" class="btn btn-secondary">,</button>
        <button type="button" class="btn btn-primary" id="add">Add</button>
        <button type="button" class="btn btn-primary" id="saveBtn">儲存</button>
      </td>
    </tr>
   </table>
</body>
</html>