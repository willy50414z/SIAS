package com.willy.sias.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.LPOptionTWPO;
import com.willy.sias.db.po.LPOptionTwPOKey;


@Repository
public interface LP_OPTION_TWRepository extends JpaRepository<LPOptionTWPO, LPOptionTwPOKey> {
}