package com.willy.sias.util;

import java.math.BigDecimal;
import java.util.List;

public class BigDecimalUtil {
	public static BigDecimal sum(List<BigDecimal> bdList) {
		BigDecimal sum = new BigDecimal(0);
		for(BigDecimal bd : bdList) {
			sum = sum.add(bd);
		}
		return sum;
	}
}
