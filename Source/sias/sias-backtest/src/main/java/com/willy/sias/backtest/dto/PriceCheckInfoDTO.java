package com.willy.sias.backtest.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.willy.sias.util.EnumSet.PriceCheckPointType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PriceCheckInfoDTO {
	private Date date;
	private PriceCheckPointType pcpType;
	private BigDecimal price;
}
