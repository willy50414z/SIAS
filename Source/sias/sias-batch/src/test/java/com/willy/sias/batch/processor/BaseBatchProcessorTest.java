package com.willy.sias.batch.processor;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BaseBatchProcessorTest {
  @Autowired
  @Qualifier("STOCK_PRICE_TW_TWSE")
  BaseBatchProcessor bbProcessor;
  @Test
  public void testParseMapParamToString_withStringTemplate_shouldSuccess (){
    bbProcessor.putMapParam("aa", "a1");
    bbProcessor.putMapParam("bb", "b2");
    bbProcessor.putMapParam("cc", "c3");

    String actualResult = bbProcessor.parseMapParamToString("${aa}-${bb}-${cc}");
    String expectedResult = "a1-b2-c3";
    Assert.assertEquals(actualResult, expectedResult);
  }
}
