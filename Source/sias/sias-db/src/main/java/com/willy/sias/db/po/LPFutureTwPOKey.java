package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * LPTransLog_TW_TWSE
 * @author 
 */
@AllArgsConstructor
@NoArgsConstructor
@Table(name="LP_FUTURE_TW")
public class LPFutureTwPOKey implements Serializable {
    /**
     * 資料日期
     */
    @Column(name = "DATA_DATE")
    private Date dataDate;

    /**
     * 標的代號
     */
    @Column(name = "FUTURE_TYPE")
    private Integer futureType;

    private static final long serialVersionUID = 1L;
}