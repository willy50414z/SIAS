package com.willy.sias.backtest.util;

import static org.junit.Assert.assertTrue;

import com.willy.sias.backtest.service.StatisticService;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.willy.r.dao.WRStatics;

@SpringBootTest
@ComponentScan("com.willy")
@RunWith(SpringRunner.class)
public class StatisticServiceTest {
	@Autowired
	public StatisticService statSvc;
	@Autowired
	public WRStatics wr;
	
	/**
	 * 	標準差
	 * @throws ParseException
	 */
	@Test
	public void getCorrelation() throws ParseException {
		List<BigDecimal> bdList1 = new ArrayList<>();
		bdList1.add(new BigDecimal("3.55"));
		bdList1.add(new BigDecimal("5.88"));
		bdList1.add(new BigDecimal("1.22"));
		bdList1.add(new BigDecimal("9.33"));
		bdList1.add(new BigDecimal("4.02"));
		
		List<BigDecimal> bdList2 = new ArrayList<>();
		bdList2.add(new BigDecimal("5"));
		bdList2.add(new BigDecimal("5.1"));
		bdList2.add(new BigDecimal("5.2"));
		bdList2.add(new BigDecimal("5.3"));
		bdList2.add(new BigDecimal("5.4"));
		assertTrue(statSvc.getCorrelation(bdList1, bdList2).compareTo(new BigDecimal("0.2292355")) == 0);
	}
	
	/**
	 * 	標準差
	 * @throws ParseException
	 */
	@Test
	public void getSd() throws ParseException {
		List<BigDecimal> bdList1 = new ArrayList<>();
		bdList1.add(new BigDecimal("1"));
		bdList1.add(new BigDecimal("2"));
		bdList1.add(new BigDecimal("3"));
		bdList1.add(new BigDecimal("4"));
		bdList1.add(new BigDecimal("5"));
		bdList1.add(new BigDecimal("6"));
		bdList1.add(new BigDecimal("7"));
		bdList1.add(new BigDecimal("8"));
		bdList1.add(new BigDecimal("9"));
		bdList1.add(new BigDecimal("10"));
		assertTrue(statSvc.getSd(bdList1).compareTo(new BigDecimal("3.0276504")) == 0);
	}
}
