package com.willy.sias.backtest.service;

import com.willy.sias.anno.SvcMethodInfo;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.repository.FRISTWRepository;
import com.willy.sias.util.BigDecimalUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 籌碼相關分析工具
 *
 * @author Willy
 */
@Service
public class ChipIdxService {

  @Autowired
  private FRISTWRepository frIsRepo;

  @Autowired
  private DataTableDao dtDao;

  /**
   * 取得股本
   * @param sid
   * @return
   */
  @SvcMethodInfo(methodDesc = "股本", resultDesc = "每個月的股本List")
  public List<TimeSeriesDTO<BigDecimal>> getIssuedSharesList(int sid) {
    return getIssuedSharesList(sid, null, null);
  }

  @SvcMethodInfo(methodDesc = "股本", resultDesc = "每個月的股本List")
  public List<TimeSeriesDTO<BigDecimal>> getIssuedSharesList(int sid, Date date) {
    return getIssuedSharesList(sid, date, date);
  }

  @SvcMethodInfo(methodDesc = "股本", resultDesc = "每個月的股本List")
  public List<TimeSeriesDTO<BigDecimal>> getIssuedSharesList(int sid, Date startDate,
      Date endDate) {
    Integer startYm =
        startDate == null ? null : Integer.parseInt(WType.dateToStr(startDate, "yyyyMM"));
    Integer endYm = endDate == null ? null : Integer.parseInt(WType.dateToStr(endDate, "yyyyMM"));

    //get raw data
    List<Object[]> cptStkList = frIsRepo.findIssuedSharesListBySid(sid);
    cptStkList = cptStkList.stream().filter(cpt ->
        (startYm == null || Integer.parseInt(cpt[0].toString()) >= startYm) && (endYm == null
            || Integer.parseInt(cpt[0].toString()) <= endYm)).collect(Collectors.toList());

    //convert monthly data to dailt data
    List<TimeSeriesDTO<BigDecimal>> issueSharesTsList = new ArrayList<>();
    for (Object[] cptStk : cptStkList) {
      Date tmpStartDate = WType.strToDate(cptStk[0] + "01");
      Date tmpEndDate = WDate.addDate(tmpStartDate, Calendar.MONTH, 1);
      while (tmpStartDate.before(tmpEndDate)) {
        if ((startDate != null && startDate.before(tmpStartDate)) || (endDate != null
            && endDate.after(tmpStartDate))) {
          continue;
        }
        issueSharesTsList.add(
            new TimeSeriesDTO<>(tmpStartDate, new BigDecimal(cptStk[1].toString())));
        tmpStartDate = WDate.addDay(tmpStartDate, 1);
      }
    }
    return issueSharesTsList;
  }

  /**
   *
   * @param sid
   * @param days 連續天數 (>0: 買超, <0: 賣超)
   * @return
   */
  @SvcMethodInfo(methodDesc = "取得連續N日買/賣超的日期List", resultDesc = "連續超買N日的日期List")
  public List<Date> getITContinueDaysDateList(int sid, int days) {
//    //get LP_TRADE_LOG
    List<TimeSeriesDTO<BigDecimal>> itTsList = dtDao.findTsList(sid, Const._FIELDID_LP_IT_NET);

    //get continue Days
    int reserve = 0;
    int continueDays = 0;
    List<Date> resultList = new ArrayList<>();
    for (int i = 0; i < itTsList.size(); i++) {
      int itLog = itTsList.get(i).getDecimalValue().intValue();
      if ((days > 0) == (itLog > 0) && itLog != 0) {
        // 連續N天都符合
        if (reserve == 0 && ++continueDays >= Math.abs(days)) {
          resultList.add(itTsList.get(i).getDate());
        } else if (reserve > 0 && (continueDays + reserve) >= Math.abs(days)) {
          //符合條件，但前面有幾天買賣超為0
          for (int j = i - Math.max(0, reserve); j <= i; j++) {
            resultList.add(itTsList.get(j).getDate());
            ++continueDays;
          }
          reserve = 0;
        }
      } else {
        //
        if(continueDays == 0) {
          continue;
        }else if(itTsList.get(i).getDecimalValue().intValue() == 0 && reserve < 2){
          //前2次為0可接受
          reserve++;
        } else if (itTsList.get(i).getDecimalValue().intValue() == 0 && reserve == 2){
          //第三次為0 => 不符條件
          continueDays = 0;
          reserve = 0;
        } else {
          continueDays = 0;
        }
      }
    }
    return resultList;
  }

  @SvcMethodInfo(methodDesc = "取得連續超買/賣天數", resultDesc = ">0: 買超, <0: 賣超")
  public Integer getITContinueDays(int sid, Date date) {
    //get LP_TRADE_LOG
    List<TimeSeriesDTO<BigDecimal>> itTsList = dtDao.findTsList(sid, Const._FIELDID_LP_IT_NET, null, date);

    //get continue Days
    int continueDays = 0;
    int reserve = 0;

    for(TimeSeriesDTO<BigDecimal> itTs : itTsList) {
      if(itTs.getDecimalValue().compareTo(BigDecimal.ZERO) > 0) {
        continueDays += (reserve + 1);
        reserve = 0;
      } else if(itTs.getDecimalValue().compareTo(BigDecimal.ZERO) < 0) {
        continueDays += (reserve - 1);
        reserve = 0;
      } else {
        if(Math.abs(reserve) == 2){
          reserve = 0;
          continueDays = 0;
          continue;
        }
        if(continueDays < 0) {
          reserve--;
        } else if (continueDays > 0) {
          reserve++;
        }
      }
    }
    return continueDays + reserve;
  }

  @SvcMethodInfo(methodDesc = "最近2,3,5,10,20,60,120天的投信淨交易加總")
  public Map<Integer, BigDecimal> getRecentDaysItNetSum(int sid, Date date) {
    return getRecentDaysItNetSum(sid, date, 2, 3, 5, 10, 20, 60, 120);
  }

  @SvcMethodInfo(methodDesc = "最近幾天的投信淨交易加總")
  public Map<Integer, BigDecimal> getRecentDaysItNetSum(int sid, Date date, Integer... days)
       {
    //get max day count and query it net data
    Integer maxDay = Arrays.<Integer>asList(days).stream().max(Const._DEFAULT_COMPARATOR_INTEGER).orElse(null);
    List<TimeSeriesDTO<BigDecimal>> itTsList = dtDao.<BigDecimal>findRecentTsList(sid,
        Const._FIELDID_LP_IT_NET, date, maxDay * -1);
    Map<Integer, BigDecimal> result = new LinkedHashMap<>();

    List<BigDecimal> itNetList = itTsList.stream().map(ts -> ts.getDecimalValue()).collect(
        Collectors.toList());
    for(int day : days) {
      if(itTsList.size() < day) {
        break;
      }
      result.put(day, BigDecimalUtil.sum(itNetList.subList(0, day)));
    }
    return result;
  }
}
