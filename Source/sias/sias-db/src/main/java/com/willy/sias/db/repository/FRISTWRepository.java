package com.willy.sias.db.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.willy.sias.db.po.FRISTWPO;
import com.willy.sias.db.po.FRISTWPOKey;
import org.springframework.data.jpa.repository.Query;

public interface FRISTWRepository extends JpaRepository<FRISTWPO, FRISTWPOKey> {
	List<FRISTWPO> findByFrDate(String strDate);

	//淨利 / 發行股數 = EPS => 淨利 / EPS = 發行股數
	//DB的淨利單位是(仟元)
	@Cacheable(value = "sias:FRISTWRepository", key = "{#root.methodName, #sid}")
	@Query("SELECT fr1.frDate,(fr1.frValue * 1000) / (SELECT fr2.frValue FROM FRISTWPO fr2 WHERE fr2.frKey = 30930 and fr2.frDate=fr1.frDate and fr1.sid = fr2.sid) FROM FRISTWPO fr1 WHERE fr1.frKey = 30952 and fr1.sid=?1 ORDER BY 1")
	List<Object[]> findIssuedSharesListBySid(Integer sid);
}
