package com.willy.sias.db.config;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class DataSourcesConfig {
	//第一數據源
		@Primary
		@Bean(name = "JPADS")
		@ConfigurationProperties("spring.datasource.jpa")
		public DataSource firstDataSource() {
			return DataSourceBuilder.create().build();
		}
		
		//第二數據源
		@Bean(name = "JDBCDS")
		@ConfigurationProperties("spring.datasource.jdbc")
		public DataSource secondDataSource() {
			return DataSourceBuilder.create().build();
		}
		
		//JPA設置
		@Autowired
		private JpaProperties jpaProperties;
		@Autowired
		private HibernateProperties hibernateProperties;
		@Bean(name = "vendorProperties")
		public Map<String, Object> getVendorProperties() {
		    return hibernateProperties.determineHibernateProperties(jpaProperties.getProperties(), new HibernateSettings());
		}
}
