package com.willy.sias.util;

import com.willy.util.string.WRegex;
import com.willy.util.string.WString;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

public class StringUtil {
	public static String cleanDigitData(String rawData) {
		if(StringUtils.isEmpty(rawData)) {return "-1";}
		rawData = WString.rmStr(rawData, ",","\"","\t"," ","⊙", "⊕");
		rawData=(StringUtils.containsAny(rawData, "--", "---") || StringUtils.isEmpty(rawData))?"-1":rawData;
		return rawData;
	}
	public static List<String> retrieveUniqueParamKeyList(String str){
		return new ArrayList<>(WRegex.getMatchedStrSet(str, WRegex._REGEX_PARAMETER_WRAPED));
	}

	public static Set<String> retrieveParamKeySet(String str){
		return WRegex.getMatchedStrSet(str, WRegex._REGEX_PARAMETER_WRAPED);
	}

	public static Set<String> getProccessBlock(String processStr) {
		int argIndex = 0;
		Set<String> result = new LinkedHashSet<>();
		Set<String> tmpMatchStrSet = WRegex.getMatchedStrSet(processStr,
				WRegex._REGEX_SQUARE_BRACKETS_WRAPED);

		while (tmpMatchStrSet.size() > 0) {
			result.addAll(tmpMatchStrSet);
			for (String tmpMatchStr : tmpMatchStrSet) {
				processStr = processStr.replace(tmpMatchStr, "${" + argIndex++ + "}");
			}
			tmpMatchStrSet = WRegex.getMatchedStrSet(processStr, WRegex._REGEX_SQUARE_BRACKETS_WRAPED);
		}
		return result;
	}
}
