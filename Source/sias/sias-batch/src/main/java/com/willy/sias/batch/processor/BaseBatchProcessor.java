package com.willy.sias.batch.processor;

import com.willy.file.txt.WFile_txt;
import com.willy.sias.db.dao.ConfigDao;
import com.willy.sias.db.po.BatchLogPO;
import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.BatchLogRepository;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.EnumSet.BatchName;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Setter
@Service
public abstract class BaseBatchProcessor{
	@Autowired
	protected WFile_txt txtFile;
	protected Date dataDate;
	protected String strDataDate;
	protected String strDataDateTW;
	protected String startDate;// 資料起始日
	protected String filePath;
	String urlTempl;
	String csvFileNameTempl;
	protected HashMap<String, Integer> sidMap;
	@Value("${sias.downloadFile.dir}")
	String fileDownloadDir;
	@Autowired
	protected ConfigDao cfgDao;
	@Autowired
	private BatchLogRepository batchLogRepo;
	@Autowired
	private CompanyCategoryRepository ccRepo;
	@Autowired
	SysConfigRepository scRepo;
	private HashMap<String, String> paramsMap = new HashMap<>();
	private String batchName;

	public void process() throws Exception {
		try {
			commonPreExec();

			//檢核前置排程是否已執行
			if(BatchName.valueOf(this.batchName).getFrontBatchs() != null){
				List<String> frontBatchNameList = Arrays.asList(BatchName.valueOf(this.batchName).getFrontBatchs()).stream().map(bns -> bns.toString()).collect(Collectors.toList());
				List<BatchLogPO> blList = batchLogRepo.findByBatchNameInAndDataDateIs(frontBatchNameList, dataDate);
				if(blList.size() != frontBatchNameList.size()) {
					//前置排成未執行，先執行前置排程
					WLog.info("Front Batch is not complete, BatchName[" + this.batchName + "]frontBatchNameList["+frontBatchNameList+"]blList["+blList+"]");
					for(String frontBatchName : frontBatchNameList) {
						if(!blList.stream().map(bl -> bl.getBatchName()).collect(Collectors.toList()).contains(frontBatchName)) {
							Class<?> batchPorcessorClass = Class.forName(Const.batchProcessorPackage + frontBatchName);
							BaseBatchProcessor batchPorcessor = (BaseBatchProcessor) BeanFactory.getBean(batchPorcessorClass);
							batchPorcessor.setStrDataDate(strDataDate);
							batchPorcessor.process();
						}
					}
					WLog.info("Front Batch is complete");
				}
			}
			
			BatchLogPO batchLog = batchLogRepo.findByBatchNameAndDataDate(this.batchName, dataDate);
			if ((startDate != null && dataDate.before(WType.strToDate(startDate))) || dataDate.after(new Date())
					|| (batchLog != null && Const._BATCH_STATUS_SUCESS == batchLog.getStatus())) {
				WLog.info("Skip - Batch[" + this.batchName + "]DataDate[" + this.strDataDate + "]startDate[" + startDate
						+ "]");
				return;
			}
			WLog.info("Start - Batch[" + this.batchName + "]DataDate[" + this.strDataDate + "]");

			// 清除舊資料
			cleanHisData();

			exec();
			WLog.info("Complete - Batch[" + this.batchName + "]DataDate[" + this.strDataDate + "]");
			if (batchLog != null && Const._BATCH_STATUS_FAIL == batchLog.getStatus()) {
				batchLog.setErrMsg(null);
				batchLog.setStatus(Const._BATCH_STATUS_SUCESS);
				batchLogRepo.save(batchLog);
			} else {
				saveBatchExecLog(null);
			}
		} catch (Exception e) {
 			handleBatchException(e);
// 			throw e;
		}

	}

	String getStrFromArray(String[] targetAr, Integer index) {
		WLog.debug("LENGTH: "+String.valueOf(targetAr.length));
		WLog.debug("index: "+index);
		WLog.debug("STR: "+WString.toString(targetAr));
		if (targetAr.length > index && targetAr[index] != null) {
			WLog.debug("return: "+targetAr[index]);
			return targetAr[index];
		} else {
			WLog.debug("return: -1");
			return "-1";
		}
	}
	
	void handleBatchException(Exception e) {
		WLog.info("執行失敗 - 批次[" + this.batchName + "]日期[" + this.strDataDate + "]");
		String errMsg = WType.exceptionToString(e); 
		WLog.error(errMsg);
		saveBatchExecLog(errMsg);
	}

	private void commonPreExec() throws Exception {
		txtFile.setEncoding("MS950");
		txtFile.setSeparator("\",\"");

		strDataDateTW = WDate.convertCE(strDataDate);
		dataDate = WType.strToDate(strDataDate);

		sidMap = cfgDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
		this.batchName = this.getClass().getSimpleName();
		putMapParam("strDataDate", strDataDate);
		putMapParam("strDataDateTw", strDataDateTW);
		putMapParam("processorName", this.batchName);
	}

	private void saveBatchExecLog(String errMsg) {
		BatchLogPO batchLog = batchLogRepo.findByBatchNameAndDataDate(this.batchName, dataDate);
		if (batchLog == null) {
			batchLog = new BatchLogPO();
			batchLog.setBatchName(this.batchName);
			batchLog.setDataDate(dataDate);
		}
		batchLog.setStatus(StringUtils.isEmpty(errMsg) ? 1 : 0);
		batchLog.setCreateDate(new Date());
		batchLog.setErrMsg(errMsg);
		batchLogRepo.save(batchLog);
	}
	
	//確認新舊分類有無差異
	Set<CompanyCategoryPO> filtCCList(Set<CompanyCategoryPO> ccSet) {
		CompanyCategoryPO dbCc;
		Set<CompanyCategoryPO> newCcSet = new HashSet<>();
		for(CompanyCategoryPO cc : ccSet) {
			dbCc = ccRepo.findTop1ByDataDateLessThanEqualAndSidIsAndCategoryIdIs(dataDate, cc.getSid(), cc.getCategoryId());
			if(dbCc == null) {
				newCcSet.add(cc);
			}
		}
		return newCcSet;
	}
	void mergeSid(String[][] spAr, int sidCodeIndex, int sidNameIndex) {
		SysConfigPO sysConfig;
		HashMap<String, Integer> sidMap = cfgDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
		HashSet<SysConfigPO> fmList = new HashSet<SysConfigPO>();
		for (String[] spRow : spAr) {
			if (spRow.length > sidCodeIndex && spRow.length > sidNameIndex
					&& !sidMap.containsKey(spRow[sidCodeIndex].trim())) {
				sysConfig = new SysConfigPO();
				sysConfig.setParentId(ParentKey.SID.getKey());
				sysConfig.setCfgValue(spRow[sidCodeIndex].trim());
				sysConfig.setCfgDesc(spRow[sidNameIndex].trim());
				fmList.add(sysConfig);
			}
		}
		scRepo.saveAll(fmList);
		refreshSidMap();
	}
	
	void mergeSid(JSONArray spJs, int sidCodeIndex, int sidNameIndex) throws JSONException {
		SysConfigPO sysConfig;
		HashMap<String, Integer> sidMap = cfgDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
		HashSet<SysConfigPO> fmList = new HashSet<SysConfigPO>();
		for(int i=0;i<spJs.length();i++) {
			JSONArray spJs1 = spJs.getJSONArray(i);
			if (spJs1.length() > sidCodeIndex && spJs1.length() > sidNameIndex
					&& !sidMap.containsKey(spJs1.get(sidCodeIndex))) {
				sysConfig = new SysConfigPO();
				sysConfig.setParentId(ParentKey.SID.getKey());
				sysConfig.setCfgValue(spJs1.getString(sidCodeIndex));
				sysConfig.setCfgDesc(spJs1.getString(sidNameIndex));
				fmList.add(sysConfig);
			}
		}
		scRepo.saveAll(fmList);
		refreshSidMap();
	}
	
	void mergeSid(String[][] spAr) {
		mergeSid(spAr, 0, 1);
	}
	
	void refreshSidMap() {
		sidMap = cfgDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
	}
	
	public abstract void cleanHisData() throws Exception;

	protected abstract void exec() throws Exception;
	
	String getMapParam(String key) {
		return this.paramsMap.get(this.batchName+"_"+key);
	}

	public String putMapParam(String key, Integer value) {
		return this.paramsMap.put(this.batchName+"_"+key, String.valueOf(value));
	}
	public String putMapParam(String key, String value) {
		return this.paramsMap.put(this.batchName+"_"+key, value);
	}
	String parseMapParamToString(String strTempl) {
		String key;
		List<String> matchedStrList = StringUtil.retrieveUniqueParamKeyList(strTempl);
		for (String matchedStr : matchedStrList) {
			key = matchedStr.substring(2, matchedStr.length() - 1);// 去掉括號
			String param = paramsMap.get(this.batchName+"_"+key);
			if(param == null) {
				WLog.error(this.batchName+"_"+key+" is not exist in paramsMap");
				return "";
			}else {
				strTempl = strTempl.replace(matchedStr, param.toString());
			}
			
		}
		return strTempl;
	}
	
}
