package com.willy.sias.backtest.service;

import com.google.gson.Gson;
import com.willy.sias.anno.SvcInfo;
import com.willy.sias.anno.SvcMethodInfo;
import com.willy.sias.backtest.dto.BreakUdTrackDTO;
import com.willy.sias.backtest.dto.PriceCheckInfoDTO;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.OHLCDTO;
import com.willy.sias.db.dto.OHLCVDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.util.EnumSet.KBar;
import com.willy.sias.util.EnumSet.PriceCheckPointType;
import com.willy.sias.util.EnumSet.PriceCheckPointTypeType;
import com.willy.sias.util.EnumSet.TrendType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@SvcInfo("技術構面")
public class TechDmsService {

  @Autowired
  private TechIdxService techIdxSvc;
  @Autowired
  private CalendarRepository caRepo;
  @Autowired
  private Gson gs;
  @Autowired
  private PriceDailyTwRepository priceRepo;
  @Autowired
  public DataTableDao dtDao;
  @Autowired
  private StatisticService staticSvc;

  /**
   * 底底高 1 / 頭頭低 -1 / 其他回 null
   * @return
   */
  public List<TimeSeriesDTO<BigDecimal>> get3HOr2BLStatus (Integer sid){
    return get3HOr2BLStatus(sid, Const._AV_TURN_CHECK_TICKS);
  }

  public List<TimeSeriesDTO<BigDecimal>> get3HOr2BLStatus(Integer sid, int checkTicks) {
    List<TimeSeriesDTO<OHLCDTO>> ohlcList = dtDao.findTsList(sid, Const._FIELDID_LIST_OHLC,
        OHLCDTO.class);

    List<List<TimeSeriesDTO<BigDecimal>>> turningPointList = techIdxSvc.getTurningPointList(
        checkTicks, ohlcList.stream()
            .map(ohlc -> new TimeSeriesDTO<>(
                ohlc.getDate(), ohlc.getValue().getHigh())).collect(
                Collectors.toList()), ohlcList.stream()
            .map(ohlc -> new TimeSeriesDTO<>(
                ohlc.getDate(), ohlc.getValue().getLow())).collect(
                Collectors.toList()));

    List<TimeSeriesDTO<BigDecimal>> aTurnTsList = turningPointList.get(0);
    List<TimeSeriesDTO<BigDecimal>> vTurnTsList = turningPointList.get(1);
    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (int i = 0; i < ohlcList.size(); i++) {
      if (aTurnTsList.get(i).getValue() != null) {
        // 20天改10天,AA間差距至少1%
        for (int j = i + checkTicks + 1;
            j < Math.min(i + checkTicks + Const._3H_2BL_CHECK_DAYS, ohlcList.size() - 1);
            j++) {
          if (ohlcList.get(j).getValue().getClose()
              .compareTo(ohlcList.get(j - 1).getValue().getLow()) < 0 //收盤低於T-1 L
              &&
              ohlcList.get(j - 1).getValue().getHigh()
                  .compareTo(ohlcList.get(i).getValue().getLow())
                  < 0 //At最高價 < 前At最低價
              && ohlcList.get(j - 1).getValue().getHigh().compareTo(
              ohlcList.get(j - 2).getValue().getHigh().max(ohlcList.get(j - Const._AV_TURN_CHECK_TICKS).getValue().getHigh()
                  .max(ohlcList.get(j - 4).getValue().getHigh()))) > 0 //At > 前3日最高價
          ) {
            result.add(new TimeSeriesDTO<>(ohlcList.get(j).getDate(), BigDecimal.valueOf(-1)));
            break;
          }
        }
      } else if (vTurnTsList.get(i).getValue() != null) {
        for (int j = i + checkTicks + 1;
            j < Math.min(i + checkTicks + Const._3H_2BL_CHECK_DAYS, ohlcList.size() - 1);
            j++) {
          if (ohlcList.get(j).getValue().getClose()
              .compareTo(ohlcList.get(j - 1).getValue().getHigh()) > 0//收盤高於T-1 H
              &&
              ohlcList.get(j - 1).getValue().getLow()
                  .compareTo(ohlcList.get(i).getValue().getHigh())
                  > 0//Vt最低價 > 前Vt最高價
              && ohlcList.get(j - 1).getValue().getLow()
              .compareTo(ohlcList.get(j - 2).getValue().getLow().min(
                  ohlcList.get(j - Const._AV_TURN_CHECK_TICKS).getValue().getLow()
                      .min(ohlcList.get(j - 4).getValue().getLow()))) < 0 //Vt < 前3日最低價
          ) {
            result.add(new TimeSeriesDTO<>(ohlcList.get(j).getDate(), BigDecimal.valueOf(1)));
            break;
          }
        }
      } else {
        result.add(new TimeSeriesDTO<>(ohlcList.get(i).getDate(), null));
      }
    }
    return result;
  }

  public List<TimeSeriesDTO<BigDecimal>> getKeepUpDnDays(List<TimeSeriesDTO<BigDecimal>> tsList) {
    int trendDay = 0;
    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (int i = 1; i < tsList.size(); i++) {
      if (tsList.get(i).getValue() == null) {
        result.add(new TimeSeriesDTO<>(tsList.get(i).getDate(), null));
      } else {
        if (tsList.get(i - 1).getValue() == null) {
          result.add(new TimeSeriesDTO<>(tsList.get(i).getDate(), Const._BIGDECIMAL_0));
        } else {
          int isMaGrowth = tsList.get(i).getDecimalValue()
              .compareTo(tsList.get(i - 1).getDecimalValue());
          if (isMaGrowth == 0) {
            trendDay = 0;
            result.add(new TimeSeriesDTO<>(tsList.get(i).getDate(), Const._BIGDECIMAL_0));
          } else {
            if (trendDay == 0 || ((isMaGrowth > 0) == (trendDay > 0))) {
              trendDay += isMaGrowth;
              result.add(
                  new TimeSeriesDTO<>(tsList.get(i).getDate(), BigDecimal.valueOf(trendDay)));
            } else {
              trendDay = isMaGrowth;
              result.add(
                  new TimeSeriesDTO<>(tsList.get(i).getDate(), BigDecimal.valueOf(isMaGrowth)));
            }
          }
        }
      }
    }
    return result;
  }

  @SvcMethodInfo(methodDesc = "判斷為上升/下降趨勢", resultDesc = "{1:上升,-1:下降}")
  public int getPSStatus(List<TimeSeriesDTO<BigDecimal>> turnTsList,
      List<TimeSeriesDTO<BigDecimal>> psTsList) {
    int status = 0;
    List<TimeSeriesDTO<BigDecimal>> turnTsListNotNull = turnTsList.stream()
        .filter(turn -> turn.getDecimalValue() != null).collect(Collectors.toList());
    if (turnTsListNotNull.size() < 2) {
      WLog.error("turnTsListNotNull.size()<2[" + turnTsListNotNull.size() + "]");
      return 0;
    }
    for (int i = 1; i < turnTsListNotNull.size(); i++) {
      // 本次轉折高於上次
      if (turnTsListNotNull.get(i).getDecimalValue()
          .compareTo(turnTsListNotNull.get(i - 1).getDecimalValue()) > 0) {
        // 本次轉折高於上次 且 前次趨勢為0
        if (status == 0) {
          status = 1;
          if (psTsList.size() == 2) {
            psTsList.remove(0);
          }
          psTsList.add(turnTsListNotNull.get(i));//1
          // 本次轉折高於上次 且 前次趨勢為-1
        } else if (status == -1) {
          // 以T、T-2決定
          status = turnTsListNotNull.get(i).getDecimalValue()
              .compareTo(turnTsListNotNull.get(i - 2).getDecimalValue());
          psTsList.clear();
          psTsList.add(turnTsListNotNull.get(i - 2));
          psTsList.add(turnTsListNotNull.get(i));
        } else {
          if (!turnTsListNotNull.get(i).equals(psTsList.get(psTsList.size() - 1))) {
            if (psTsList.size() > 1) {
              psTsList.remove(0);
            }
            psTsList.add(turnTsListNotNull.get(i));
          }
        }
      } else if (turnTsListNotNull.get(i).getDecimalValue()
          .compareTo(turnTsListNotNull.get(i - 1).getDecimalValue()) < 0) {
        if (status == 0) {
          status = -1;
          if (psTsList.size() == 2) {
            psTsList.remove(0);
          }
          psTsList.add(turnTsListNotNull.get(i));
        } else if (status == 1) {
          // 以T、T-2決定
          status = turnTsListNotNull.get(i).getDecimalValue()
              .compareTo(turnTsListNotNull.get(i - 2).getDecimalValue());
          psTsList.clear();
          psTsList.add(turnTsListNotNull.get(i - 2));
          psTsList.add(turnTsListNotNull.get(i));
        } else {
          if (!turnTsListNotNull.get(i).equals(psTsList.get(psTsList.size() - 1))) {
            if (psTsList.size() > 1) {
              psTsList.remove(0);
            }
            psTsList.add(turnTsListNotNull.get(i));
          }
        }
      }
    }

    // 補足最後一個支撐點未被計算到問題
    // 若-1支撐點與-2趨勢與先前同，則以-1及-2繪製壓力支撐線
    if (turnTsListNotNull.get(turnTsListNotNull.size() - 1).getDate()
        .compareTo(psTsList.get(psTsList.size() - 1).getDate()) != 0
        && status == turnTsListNotNull.get(turnTsListNotNull.size() - 1).getDecimalValue()
        .compareTo(turnTsListNotNull.get(turnTsListNotNull.size() - 2).getDecimalValue())) {
      psTsList.remove(0);
      psTsList.add(turnTsListNotNull.get(turnTsListNotNull.size() - 1));
    } else if (psTsList.size() == 1) {
      TimeSeriesDTO<BigDecimal> secodePs = psTsList.get(0);
      psTsList.clear();
      psTsList.add(turnTsListNotNull.get(0));
      psTsList.add(secodePs);
    }
    return status;
  }

  @SvcMethodInfo(methodDesc = "趨勢類型", argsDesc = {"", "",
      "長(T)/短(F)期"}, resultDesc = "TrendType(上升/下降/箱型/三角)")
  public TrendType getTrendType(int sid, Date analyzedate, boolean longTerm) throws ParseException {
    List<List<TimeSeriesDTO<BigDecimal>>> turningPointList = techIdxSvc.getTurningPointList(sid,
        analyzedate);
    if (turningPointList == null || turningPointList.size() != 2) {
      WLog.error(
          "turningPointList.size()[" + (turningPointList == null ? "null" : turningPointList.size())
              + "]");
    }
    List<TimeSeriesDTO<BigDecimal>> aTurnTsList = turningPointList.get(0);
    List<TimeSeriesDTO<BigDecimal>> vTurnTsList = turningPointList.get(1);

    // 用來評斷壓力/支撐線的關鍵2點位
    List<TimeSeriesDTO<BigDecimal>> pressureTsList = new ArrayList<>();
    List<TimeSeriesDTO<BigDecimal>> supportTsList = new ArrayList<>();

    // 判斷壓力/支撐線趨勢
    int pressureTrend = this.getPSStatus(aTurnTsList, pressureTsList);
    int supportTrend = this.getPSStatus(vTurnTsList, supportTsList);
    // 是否有突破趨勢線
    BreakUdTrackDTO budDto = this.getBreakUdTrackStatus(sid, analyzedate, aTurnTsList, vTurnTsList);
    if (longTerm) {
      WLog.info(pressureTrend + "/" + supportTrend + "/" + WString.toString(budDto));
      return trendToString(pressureTrend, supportTrend, budDto.getBreakUdStatus());
    }

    List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(sid,
        aTurnTsList.get(aTurnTsList.size() - 1).getDate(),
        aTurnTsList.get(aTurnTsList.size() - 1).getDate());
    if (!CollectionUtils.isEmpty(priceList)) {
      int todayPsPoint = 0;// 判斷今天應作為壓力點還是支撐點(1: 今天為a點 / -1: 今天為v點)
      int psPointCount = 0;// 前面壓力/支撐點數量
      // 確認前3個壓力/支撐點，今天補第4個
      for (int i = aTurnTsList.size() - 1; i >= 0; i--) {
        if (todayPsPoint == 0) {
          todayPsPoint = (aTurnTsList.get(i).getValue() != null) ? -1
              : (vTurnTsList.get(i).getValue() != null) ? 1 : 0;
          psPointCount = (todayPsPoint != 0) ? 1 : 0;
        } else if (todayPsPoint == -1) {
          todayPsPoint = (vTurnTsList.get(i).getValue() != null) ? 1 : -1;
          psPointCount = (todayPsPoint == 1) ? psPointCount + 1 : psPointCount;
        } else if (todayPsPoint == 1) {
          todayPsPoint = (aTurnTsList.get(i).getValue() != null) ? -1 : 1;
          psPointCount = (todayPsPoint == -1) ? psPointCount + 1 : psPointCount;
        }
        if (psPointCount == 4) {
          aTurnTsList = aTurnTsList.subList(i, aTurnTsList.size());
          vTurnTsList = vTurnTsList.subList(i, vTurnTsList.size());
          break;
        }
      }
      pressureTsList.clear();
      supportTsList.clear();
      pressureTrend = this.getPSStatus(aTurnTsList, pressureTsList);
      supportTrend = this.getPSStatus(vTurnTsList, supportTsList);
      budDto = this.getBreakUdTrackStatus(sid, analyzedate, aTurnTsList, vTurnTsList);
    }
    return trendToString(pressureTrend, supportTrend, budDto.getBreakUdStatus());
  }

  @SvcMethodInfo(methodDesc = "計算突破趨勢DTO", resultDesc = "BreakUdTrackDTO")
  public BreakUdTrackDTO getBreakUdTrackStatus(int sid, Date date) throws ParseException {
    List<List<TimeSeriesDTO<BigDecimal>>> turningPointList = null;
    //取股價轉折點，先用><近3日高低點做為轉折點，若只取到一個轉折點，改為><2日...1日
    turningPointList = techIdxSvc.getTurningPointList(sid, date);
    if (turningPointList == null || turningPointList.size() != 2) {
      WLog.error(
          "turningPointList.size()[" + (turningPointList == null ? "null" : turningPointList.size())
              + "]");
      return null;
    }
    List<TimeSeriesDTO<BigDecimal>> aTurnTsList = turningPointList.get(0);
    List<TimeSeriesDTO<BigDecimal>> vTurnTsList = turningPointList.get(1);
    return getBreakUdTrackStatus(sid, date, aTurnTsList, vTurnTsList);
  }

  @SvcMethodInfo(methodDesc = "計算突破趨勢DTO", argsDesc = {"", "", "上轉折點List",
      "下轉折點List"}, resultDesc = "BreakUdTrackDTO")
  public BreakUdTrackDTO getBreakUdTrackStatus(int sid, Date date,
      List<TimeSeriesDTO<BigDecimal>> aTurnTsList,
      List<TimeSeriesDTO<BigDecimal>> vTurnTsList) throws ParseException {
    Date lastPSDate = aTurnTsList.stream().filter(ts -> ts.getDecimalValue() != null)
        .map(ts -> ts.getDate())
        .max(Const._DEFAULT_COMPARATOR_DATE).orElse(aTurnTsList.get(0).getDate());
    WLog.debug("lastPSDate[" + lastPSDate + "]date[" + date + "]");
    List<TimeSeriesDTO<BigDecimal>> closeTdList = dtDao.findTsList(sid,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE, BigDecimal.class, lastPSDate, date);
    int breakUdTrackStatus = 0;
    BigDecimal pressPrice, supportPrice;
    BreakUdTrackDTO budDto = new BreakUdTrackDTO();
    for (int i = closeTdList.size() - 1; i > -1; i--) {
      try {
        pressPrice = this.getPSPrice(closeTdList.get(i).getDate(), aTurnTsList, budDto);
        supportPrice = this.getPSPrice(closeTdList.get(i).getDate(), vTurnTsList, budDto);
      } catch (Exception e) {
        WLog.error(e);
        continue;
      }

      WLog.debug("Date[" + closeTdList.get(i).getDate() + "]pressPrice[" + pressPrice + "]close["
          + closeTdList.get(i).getDecimalValue() + "]");
      if (closeTdList.get(i) == null || closeTdList.get(i).getDecimalValue() == null
          || supportPrice == null) {
        WLog.error(new Exception("[sid]" + sid + "[Date]" + date + " NULL"));
      }
      if (closeTdList.get(i).getDecimalValue().compareTo(pressPrice) > 0) {
        // 檢測是否向上突破軌道壓力線
        if (breakUdTrackStatus < 0) {
          break;
        }
        breakUdTrackStatus++;
      } else if (closeTdList.get(i).getDecimalValue().compareTo(supportPrice) < 0) {
        // 檢測是否向下突破軌道支撐線
        if (breakUdTrackStatus > 0) {
          break;
        }
        breakUdTrackStatus--;
      } else if (closeTdList.get(i).getDecimalValue().compareTo(supportPrice) > 0
          && closeTdList.get(i).getDecimalValue().compareTo(pressPrice) < 0) {
        // 還在軌道線內
        break;
      }
    }
    budDto.setBreakUdStatus(breakUdTrackStatus);
    budDto.setBaseDate(date);
    return budDto;
  }

  @SvcMethodInfo(methodDesc = "計算趨勢線延伸到某日的價格", argsDesc = {"",
      "轉折點List"}, resultDesc = "趨勢線延伸到某日的價格")
  public BigDecimal getPSPrice(Date date, List<TimeSeriesDTO<BigDecimal>> turnTsList) {
    return getPSPrice(date, turnTsList, null);
  }

  @SvcMethodInfo(methodDesc = "計算趨勢線延伸到某日的價格", argsDesc = {"", "轉折點List",
      "突破趨勢DTO"}, resultDesc = "趨勢線延伸到某日的價格")
  public BigDecimal getPSPrice(Date date, List<TimeSeriesDTO<BigDecimal>> turnTsList,
      BreakUdTrackDTO budDto) {
    List<TimeSeriesDTO<BigDecimal>> pressureTsList = new ArrayList<>();
    this.getPSStatus(turnTsList, pressureTsList);
    if (budDto != null) {
      if (budDto.getPressureTsList() == null) {
        budDto.setPressureTsList(pressureTsList);
      } else {
        budDto.setSupportTsList(pressureTsList);
      }
    }
    if (pressureTsList.size() != 2) {
      throw new IllegalArgumentException(
          "pressureTsList.size()[" + pressureTsList.size() + "]date[" + date
              + "]gs.toJson(pressureTsList)["
              + gs.toJson(pressureTsList) + "]gs.toJson(turnTsList)[" + gs.toJson(turnTsList)
              + "]");
    }

    // 計算壓力點連線
    // 取得2壓力點相差天數
    List<Date> dateList = new ArrayList<>(
        pressureTsList.stream().map(pt -> pt.getDate()).collect(Collectors.toList()));
    dateList.add(date);
    dateList = dateList.stream().sorted(Const._DEFAULT_COMPARATOR_DATE)
        .collect(Collectors.toList());
    List<CalendarPO> cusCaList = caRepo.findByDateTypeIsAndDateBetween(
        Const._CFG_ID_CALENDAR_STOCK_BUS_DATE,
        dateList.get(0), dateList.get(dateList.size() - 1));

    long indexDiff = cusCaList.stream()
        .filter(ca -> WDate.afterOrEquals(ca.getDate(), pressureTsList.get(0).getDate())
            && WDate.beforeOrEquals(ca.getDate(), pressureTsList.get(1).getDate())).count() - 1;
    long indexDiff2 = cusCaList.stream()
        .filter(ca -> WDate.afterOrEquals(ca.getDate(), pressureTsList.get(0).getDate())
            && WDate.beforeOrEquals(ca.getDate(), date)).count() - 1;

    WLog.debug(
        "Diff days between [" + pressureTsList.get(0).getDate() + "] and [" + pressureTsList.get(1)
            .getDate()
            + "] is [" + indexDiff + "]");

    if (indexDiff <= 0) {
      WLog.error("indexDiff is ngt 0[" + indexDiff + "]date[" + date + "]");
    }

    // 價差/天數
    BigDecimal diffPerDay = pressureTsList.get(1).getDecimalValue()
        .subtract(pressureTsList.get(0).getDecimalValue())
        .divide(new BigDecimal(indexDiff), 4, RoundingMode.HALF_UP);
    WLog.debug("diffPerDay[" + pressureTsList.get(1).getDecimalValue() + "-"
        + pressureTsList.get(0).getDecimalValue() + "/" + indexDiff + "=" + diffPerDay + "]");

    return diffPerDay.multiply(new BigDecimal(indexDiff2))
        .add(pressureTsList.get(0).getDecimalValue());
  }

  private TrendType trendToString(int pressureTrend, int supportTrend, int breakUdTrackStatus) {
    TrendType trendDirection = null;
    switch (pressureTrend) {
      case 1:
        switch (supportTrend) {
          case 1:
            trendDirection = TrendType.UP_WARD;
            break;
          case 0:
          case -1:
            trendDirection = TrendType.TRIANGLE_WIDEN;
            break;
        }
      case 0:
        switch (supportTrend) {
          case 1:
            trendDirection = TrendType.TRIANGLE_NARROW;
            break;
          case 0:
            trendDirection = TrendType.HORIZON_TIDY;
            break;
          case -1:
            trendDirection = TrendType.TRIANGLE_WIDEN;
            break;
        }
      case -1:
        switch (supportTrend) {
          case 1:
          case 0:
            trendDirection = TrendType.TRIANGLE_NARROW;
            break;
          case -1:
            trendDirection = TrendType.DOWN_TREND;
            break;
        }
    }
    trendDirection.setBreakUdTrackStatus(breakUdTrackStatus);
    return trendDirection;
  }

  @SvcMethodInfo(methodDesc = "均線多/空頭排列 (MA5/10/20)", resultDesc = "(return>0?多:空) 頭排列")
  public int getMAArgmtDays(int sid, Date baseDate) {
    return getMAArgmtDays(sid, baseDate, 5, 10, 20);
  }

  @SvcMethodInfo(methodDesc = "均線多/空頭排列", resultDesc = "(return>0?多:空) 頭排列")
  public int getMAArgmtDays(int sid, Date baseDate, int... maDays) {
    int argmtDays = 0;
    List<TimeSeriesDTO<BigDecimal>> tdList = null;
    List<List<TimeSeriesDTO<BigDecimal>>> smaList = new ArrayList<>();
    // 撈出資料集(要向前取排列天數)
    tdList = dtDao.findTsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE);

    if (CollectionUtils.isEmpty(tdList)) {
      WLog.error("sid[" + sid + "] tdList is empty");
      return argmtDays;
    } else {
      tdList = tdList.stream().filter(td -> WDate.beforeOrEquals(td.getDate(), baseDate))
          .collect(Collectors.toList());
      if (CollectionUtils.isEmpty(tdList)) {
        WLog.error("sid[" + sid + "] baseDate[" + baseDate + "] tdList is empty");
        return argmtDays;
      }
    }

    for (int maDay : maDays) {
      smaList.add(techIdxSvc.getSMA(tdList, maDay));
    }

    int smaSize = 0;
    for (List<TimeSeriesDTO<BigDecimal>> sma : smaList) {
      if (smaSize == 0) {
        smaSize = sma.size();
      }
      if (smaSize != sma.size()) {
        throw new IllegalArgumentException(
            "SID[" + sid + "]baseDate[" + baseDate + "]maDays[" + WString.toString(maDays)
                + "] smaSize["
                + smaSize + "] sma.size()[" + sma.size() + "] sma size is not match");
      }
    }

    // 均線排列狀態
    int argmtStatus = Const.ERROR_INT_RC;
    for (int i = smaSize - 1; i >= 0; i--) {
      BigDecimal sma = null;
      int compareStatus = 0;
      for (List<TimeSeriesDTO<BigDecimal>> smaTs : smaList) {
        // 取第一條MA值
        if (sma == null) {
          sma = smaTs.get(i).getDecimalValue();
          continue;
        }
        // 取第二條與第一條MA比較值
        if (argmtStatus == Const.ERROR_INT_RC) {
          argmtStatus = sma.compareTo(smaTs.get(i).getDecimalValue());
          continue;
        }
        if (smaTs.get(i).getDecimalValue() == null) {
          break;
        }
        // 其餘MA與前一條比較狀態相同 ? 繼續比較 : 回傳排列天數
        compareStatus = sma.compareTo(smaTs.get(i).getDecimalValue());
        if (compareStatus != argmtStatus) {
          return argmtDays;
        } else {
          sma = smaTs.get(i).getDecimalValue();
        }
      }
      argmtDays += compareStatus;
    }
    return argmtDays;
  }

  @SvcMethodInfo(methodDesc = "支撐/壓力資訊Set", resultDesc = "Set<PriceCheckInfo>")
  public Set<PriceCheckInfoDTO> getPriceCheckPoints(int sid, Date baseDate) {
    Set<PriceCheckInfoDTO> pciSet = new LinkedHashSet<>();

    // 均線支撐
    pciSet.addAll(getMAPriceCheckPoints(sid, baseDate));

    // 交易量大
    pciSet.addAll(getVolPriceCheckPoints(sid, baseDate));

    // 跳空缺口區
    getGAPPriceCheckPoints(sid, baseDate, pciSet);

    pciSet.stream().sorted(new Comparator<PriceCheckInfoDTO>() {
      @Override
      public int compare(PriceCheckInfoDTO o1, PriceCheckInfoDTO o2) {
        return o1.getDate().compareTo(o2.getDate());
      }
    });
    return pciSet;
  }

  @SvcMethodInfo(methodDesc = "支撐/壓力點(前一天的MA)", resultDesc = "Set<PriceCheckInfo>")
  public Set<PriceCheckInfoDTO> getMAPriceCheckPoints(int sid, Date baseDate) {
    List<TimeSeriesDTO<BigDecimal>> tdList = null;
    List<TimeSeriesDTO<BigDecimal>> smaList;
    TimeSeriesDTO<BigDecimal> lastMa;
    Set<PriceCheckInfoDTO> pciSet = new HashSet<>();

    tdList = dtDao.findTsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    tdList = (tdList == null) ? null
        : tdList.stream().filter(td -> WDate.beforeOrEquals(td.getDate(), baseDate))
            .collect(Collectors.toList());

    // MA5
    smaList = techIdxSvc.getSMA(tdList, 5);
    lastMa = smaList.stream().filter(ma -> !ma.getDate().before(baseDate)).findFirst().orElse(null);
    if (lastMa == null) {
      WLog.error("SID[" + sid + "]baseDate[" + baseDate + "] getRecentMa5 fail");
    } else {
      pciSet.add(
          new PriceCheckInfoDTO(lastMa.getDate(), PriceCheckPointType.MA5, lastMa.getDecimalValue()));
    }

    // MA20
    smaList = techIdxSvc.getSMA(tdList, 20);
    lastMa = smaList.stream().filter(ma -> !ma.getDate().before(baseDate)).findFirst().orElse(null);
    if (lastMa == null) {
      WLog.error("SID[" + sid + "]baseDate[" + baseDate + "] getRecentMa20 fail");
    } else {
      pciSet.add(
          new PriceCheckInfoDTO(lastMa.getDate(), PriceCheckPointType.MA20, lastMa.getDecimalValue()));
    }

    // MA60
    smaList = techIdxSvc.getSMA(tdList, 60);
    lastMa = smaList.stream().filter(ma -> !ma.getDate().before(baseDate)).findFirst().orElse(null);
    if (lastMa == null) {
      WLog.error("SID[" + sid + "]baseDate[" + baseDate + "] getRecentMa60 fail");
    } else {
      pciSet.add(
          new PriceCheckInfoDTO(lastMa.getDate(), PriceCheckPointType.MA60, lastMa.getDecimalValue()));
    }

    // MA120
    smaList = techIdxSvc.getSMA(tdList, 120);
    if (smaList != null) {
      lastMa = smaList.stream().filter(ma -> !ma.getDate().before(baseDate)).findFirst()
          .orElse(null);
      if (lastMa == null) {
        WLog.error("SID[" + sid + "]baseDate[" + baseDate + "] getRecentMa120 fail");
      } else {
        pciSet.add(new PriceCheckInfoDTO(lastMa.getDate(), PriceCheckPointType.MA120,
            lastMa.getDecimalValue()));
      }
    }

    // MA240
    smaList = techIdxSvc.getSMA(tdList, 240);
    if (smaList != null) {
      lastMa = smaList.stream().filter(ma -> !ma.getDate().before(baseDate)).findFirst()
          .orElse(null);
      if (smaList != null) {
        if (lastMa == null) {
          WLog.error("SID[" + sid + "]baseDate[" + baseDate + "] getRecentMa240 fail");
        } else {
          pciSet.add(
              new PriceCheckInfoDTO(lastMa.getDate(), PriceCheckPointType.MA240,
                  lastMa.getDecimalValue()));
        }
      }
    }
    return pciSet;
  }

  @SvcMethodInfo(methodDesc = "支撐/壓力點(近6周含其他支壓的跳空缺口)", argsDesc = {"", "",
      "關鍵價位種類"}, resultDesc = "void")
  public void getGAPPriceCheckPoints(int sid, Date baseDate, Set<PriceCheckInfoDTO> priceCheckInfoDTO) {
    List<TimeSeriesDTO<OHLCDTO>> ohlcTdList = null;
    try {
      ohlcTdList = dtDao.<OHLCDTO>findRecentTsList(sid, Const._FIELDID_LIST_OHLC, OHLCDTO.class,
          baseDate,
          -30);
    } catch (Exception e) {
      WLog.error(e);
      return;
    }
    // 加入跳空缺口中含支撐壓力線的
    for (int tdIndex = 0; tdIndex < ohlcTdList.size() - 1; tdIndex++) {
      addGAPPriceCheckPoints(tdIndex, ohlcTdList, priceCheckInfoDTO);
    }
    // 加入跳空缺口中突破前方缺口的
    for (int tdIndex = 0; tdIndex < ohlcTdList.size() - 1; tdIndex++) {
      addGAPGAPPriceCheckPoints(tdIndex, ohlcTdList, priceCheckInfoDTO);
    }
  }

  // 加入跳空缺口中含支撐壓力線的
  private void addGAPPriceCheckPoints(int index, List<TimeSeriesDTO<OHLCDTO>> ohlcTdList,
      Set<PriceCheckInfoDTO> priceCheckInfoDTOSet) {
    Set<PriceCheckInfoDTO> tmpPciSet = new HashSet<>();
    BigDecimal lastHigh = ohlcTdList.get(index).getValue().getHigh();
    BigDecimal lastLow = ohlcTdList.get(index).getValue().getLow();
    BigDecimal todayHigh = ohlcTdList.get(index + 1).getValue().getHigh();
    BigDecimal todayLow = ohlcTdList.get(index + 1).getValue().getLow();
    priceCheckInfoDTOSet.stream()
        .filter(pcp -> pcp.getPcpType().getType() != PriceCheckPointTypeType.GAP)
        .forEach(pcp -> {
          if (pcp.getPcpType().getPriceCheckPointGapType() == null) {
            WLog.error(
                "PriceCheckPointType[" + pcp.getPcpType().toString() + "] can't get gap type");
          } else {
            // 跳空缺口突破其他支撐 || 壓力
            if (lastHigh.compareTo(todayLow) < 0) {
              // 向上跳空
              tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index + 1).getDate(),
                  pcp.getPcpType().getPriceCheckPointGapType(), todayLow));
              tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index).getDate(),
                  pcp.getPcpType().getPriceCheckPointGapType(), lastHigh));
            } else if (todayHigh.compareTo(lastLow) < 0) {
              // 今天向下跳空
              tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index + 1).getDate(),
                  pcp.getPcpType().getPriceCheckPointGapType(), todayHigh));
              tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index).getDate(),
                  pcp.getPcpType().getPriceCheckPointGapType(), lastLow));
            }
          }
        });
    priceCheckInfoDTOSet.addAll(tmpPciSet);
  }

  // 加入跳空缺口中突破前方缺口的
  private void addGAPGAPPriceCheckPoints(int index, List<TimeSeriesDTO<OHLCDTO>> ohlcTdList,
      Set<PriceCheckInfoDTO> priceCheckInfoDTOSet) {
    Set<PriceCheckInfoDTO> tmpPciSet = new HashSet<>();
    // 補上跨越跳空缺口的缺口
    // 遞迴缺口
    priceCheckInfoDTOSet.stream()
        .filter(p -> ohlcTdList.get(index).getDate().compareTo(p.getDate()) > 0
            && p.getPcpType().getType() != PriceCheckPointTypeType.GAP)
        .map(p -> p.getPcpType()).collect(Collectors.toSet()).forEach(pcp -> {
          // 同個缺口遞迴日期
          priceCheckInfoDTOSet.stream().filter(p1 -> p1.getPcpType().equals(pcp)).map(p1 -> p1.getDate())
              .collect(Collectors.toSet()).forEach(pcpDate -> {
                List<BigDecimal> priceList = priceCheckInfoDTOSet.stream().filter(
                        pci -> pci.getPcpType().equals(pcp) && pcpDate.compareTo(pci.getDate()) == 0)
                    .map(pci -> pci.getPrice()).collect(Collectors.toList());
                if (priceList.size() == 2) {
                  BigDecimal lastHigh = ohlcTdList.get(index).getValue().getHigh();
                  BigDecimal lastLow = ohlcTdList.get(index).getValue().getLow();
                  BigDecimal todayHigh = ohlcTdList.get(index + 1).getValue().getHigh();
                  BigDecimal todayLow = ohlcTdList.get(index + 1).getValue().getLow();
                  BigDecimal maxPcp = priceList.stream().max(Const._DEFAULT_COMPARATOR_BIGDECIMAL)
                      .orElse(null);
                  BigDecimal minPcp = priceList.stream().min(Const._DEFAULT_COMPARATOR_BIGDECIMAL)
                      .orElse(null);
                  // 跳空缺口突破其他支撐 || 壓力
                  if (lastHigh.compareTo(todayLow) < 0 && todayLow.compareTo(maxPcp) > 0) {
                    // 向上跳空 && 今日低點高於前缺口壓力點
                    tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index + 1).getDate(),
                        pcp.getPriceCheckPointGapType(), todayLow));
                    tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index).getDate(),
                        pcp.getPriceCheckPointGapType(), lastHigh));
                  } else if (todayHigh.compareTo(lastLow) < 0 && todayHigh.compareTo(minPcp) < 0) {
                    // 今天向下跳空 && 今日高點低於前缺口支撐點
                    tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index + 1).getDate(),
                        pcp.getPriceCheckPointGapType(), todayHigh));
                    tmpPciSet.add(new PriceCheckInfoDTO(ohlcTdList.get(index).getDate(),
                        pcp.getPriceCheckPointGapType(), lastLow));
                  }
                }

              });
        });
    priceCheckInfoDTOSet.addAll(tmpPciSet);
  }

  @SvcMethodInfo(methodDesc = "支撐/壓力點(大量交易量價格區間)", resultDesc = "Set<PriceCheckInfo>")
  public Set<PriceCheckInfoDTO> getVolPriceCheckPoints(int sid, Date baseDate) {
    Set<PriceCheckInfoDTO> pciSet = new HashSet<>();
    List<TimeSeriesDTO<OHLCVDTO>> ohlcvList = null;
    try {
      ohlcvList = dtDao.<OHLCVDTO>findRecentTsList(sid, Const._FIELDID_LIST_OHLCV, OHLCVDTO.class,
          baseDate, -40);
    } catch (Exception e) {
      WLog.error(e);
      return null;
    }
    List<TimeSeriesDTO<BigDecimal>> vTsList = ohlcvList.stream()
        .map(ohlc -> new TimeSeriesDTO<BigDecimal>(ohlc.getDate(),
            ohlc.getValue().getTradingShares()))
        .collect(Collectors.toList());

    List<TimeSeriesDTO<BigDecimal>> vMa10List = techIdxSvc.getSMA(vTsList, 10);
    for (int i = 0; i < ohlcvList.size(); i++) {
      if (vMa10List.get(i).getDecimalValue() != null && ohlcvList.get(i).getValue() != null) {
        // vMA10 * 3 < v
        if (vMa10List.get(i).getDecimalValue().multiply(new BigDecimal(3))
            .compareTo(ohlcvList.get(i).getValue().getTradingShares()) < 0) {
          pciSet.add(new PriceCheckInfoDTO(ohlcvList.get(i).getDate(), PriceCheckPointType.GIANT_VOL,
              ohlcvList.get(i).getValue().getClose()));
          pciSet.add(new PriceCheckInfoDTO(ohlcvList.get(i).getDate(), PriceCheckPointType.GIANT_VOL,
              ohlcvList.get(i).getValue().getOpen()));
          continue;
        }
        // vMA10 * 1.5 < v
        if (vMa10List.get(i).getDecimalValue().multiply(BigDecimal.valueOf(1.5))
            .compareTo(ohlcvList.get(i).getValue().getTradingShares()) < 0) {
          pciSet.add(new PriceCheckInfoDTO(ohlcvList.get(i).getDate(), PriceCheckPointType.BIG_VOL,
              ohlcvList.get(i).getValue().getClose()));
          pciSet.add(new PriceCheckInfoDTO(ohlcvList.get(i).getDate(), PriceCheckPointType.BIG_VOL,
              ohlcvList.get(i).getValue().getOpen()));
        }
      }
    }
    return pciSet;
  }

  @SvcMethodInfo(methodDesc = "形態學-N型底出現天數(V2 跌幅 < V1~A1漲幅的1/2，且目前向上突破A1)", argsDesc = {"",
      "最高價List", "最低價List"}, resultDesc = "N型底出現天數")
  public int getNBottomStatus(Date baseDate, List<TimeSeriesDTO<BigDecimal>> highTsList,
      List<TimeSeriesDTO<BigDecimal>> lowTsList) {
    // 擷取資料
    List<TimeSeriesDTO<BigDecimal>> tmpHighTsList = highTsList.stream()
        .filter(ts -> ts.getDate().compareTo(baseDate) < 1).collect(Collectors.toList());
    List<TimeSeriesDTO<BigDecimal>> tmpLowTsList = lowTsList.stream()
        .filter(ts -> ts.getDate().compareTo(baseDate) < 1).collect(Collectors.toList());

    // 取得AV轉折點
    List<List<TimeSeriesDTO<BigDecimal>>> tsTurnPointList = techIdxSvc.getTurningPointList(2,
        tmpHighTsList,
        tmpLowTsList);
    List<TimeSeriesDTO<BigDecimal>> tsATurnList = tsTurnPointList.get(0);
    List<TimeSeriesDTO<BigDecimal>> tsVTurnList = tsTurnPointList.get(1);

    if (CollectionUtils.isNotEmpty(tsATurnList) && CollectionUtils.isNotEmpty(tsVTurnList)) {
      List<TimeSeriesDTO<BigDecimal>> tsVTurnListWithoutNull = tsVTurnList.stream()
          .filter(ts -> ts.getDecimalValue() != null).collect(Collectors.toList());
      for (int i = tsVTurnListWithoutNull.size() - 1; i >= 0; i--) {
        // 第2隻腳
        TimeSeriesDTO<BigDecimal> v2Ts = tsVTurnListWithoutNull.get(i);
        // 第1個頭
        TimeSeriesDTO<BigDecimal> a1Ts = tsATurnList.stream().filter(
                ts -> v2Ts != null && ts.getDecimalValue() != null && ts.getDate()
                    .before(v2Ts.getDate()))
            .max(TimeSeriesDTO.getTsDateComparator()).orElse(null);
        // 第1隻腳
        TimeSeriesDTO<BigDecimal> v1Ts = tsVTurnListWithoutNull.stream()
            .filter(ts -> a1Ts != null && ts.getDate().before(a1Ts.getDate()))
            .max(TimeSeriesDTO.getTsDateComparator()).orElse(null);

        if (v2Ts != null && a1Ts != null && v1Ts != null
            // 第2隻腳 跌幅 < 第1個頭漲幅的1/2
            && a1Ts.getDecimalValue().add(v1Ts.getDecimalValue())
            .compareTo(v2Ts.getDecimalValue().multiply(BigDecimal.valueOf(2))) < 0) {
          // 找出突破A高的點
          TimeSeriesDTO<BigDecimal> breakATs = tmpHighTsList.stream()
              .filter(ts -> ts.getDate().after(v2Ts.getDate())
                  && ts.getDecimalValue().compareTo(a1Ts.getDecimalValue()) > 0)
              .findFirst().orElse(null);
          if (breakATs == null) {
            continue;
          }
          // 找出index並回傳
          for (int j = tmpHighTsList.size() - 1; j >= 0; j--) {
            if (tmpHighTsList.get(j).getDate().compareTo(breakATs.getDate()) == 0) {
              // 目標價為V2+(A1-V1)
              return tmpHighTsList.size() - 1 - j;
            }
          }
        }
      }
    }
    return -1;
  }

  @SvcMethodInfo(methodDesc = "指標與收盤價背離狀態", argsDesc = {"", "",
      "指標List"}, resultDesc = "{>0:股價創新低 && 指標未創新低 , <0:股價創新高 && 指標未創新高}")
  public int getDeviateStatus(int sid, Date date, List<TimeSeriesDTO<BigDecimal>> tsList) {
    int newHighOrLowDeviateStatus = 0;
    List<TimeSeriesDTO<BigDecimal>> closeTdList = null;
    try {
      // 收盤價
      closeTdList = dtDao.findTsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE,
          tsList.get(0).getDate(),
          tsList.get(tsList.size() - 1).getDate());

      List<List<TimeSeriesDTO<BigDecimal>>> tsTurnPointList = techIdxSvc.getTurningPointList(2, tsList,
          tsList);
      List<TimeSeriesDTO<BigDecimal>> tsATurnList = tsTurnPointList.get(0);
      List<TimeSeriesDTO<BigDecimal>> tsVTurnList = tsTurnPointList.get(1);

      // 向前檢驗40個，此參數為檢驗末端Index
      int checkPeriod = 40, deviateMaxPeriod = 10;
      int firstCheckedIndex = checkPeriod;
      if (tsList.size() < checkPeriod) {
        WLog.error("tsList.size()[" + tsList.size() + "] is smaller then checkPeriod[" + checkPeriod
            + "] sid["
            + sid + "]tsList.get(0).getDate()[" + tsList.get(0).getDate()
            + "]tsList.get(tsList.size() - 1).getDate()[" + tsList.get(tsList.size() - 1).getDate()
            + "]");
        return 0;
      }
      for (int i = tsList.size() - 1; i >= 0; i--) {
        Date tsDate = tsList.get(i).getDate();
        // 股價破新低，
        if (// 抓好前置量
            i >= firstCheckedIndex && tsList.get(i).getDecimalValue() != null
                // 早於判斷時間
                && WDate.beforeOrEquals(tsList.get(i).getDate(), date)) {
          newHighOrLowDeviateStatus++;
          // 第一次進入判斷式決定要檢驗到哪一個Index(避免往前判斷抓值時index<0)
          if (firstCheckedIndex == checkPeriod) {
            firstCheckedIndex = Math.max(i - checkPeriod, checkPeriod);
          }

          // 取得tsDate前10天日期，避免背離期過長
          Date firstDeviateDate = tsList.get(i - deviateMaxPeriod).getDate();
          TimeSeriesDTO<BigDecimal> lastVTurnTs = null, lastATurnTs = null;

          // ----------------------------------
          // 股價創新低 && 指標未創新低 => 準備向上
          // 前一個V轉
          lastVTurnTs = tsVTurnList.stream()
              .filter(ts -> ts != null && ts.getDate().before(tsDate)
                  && ts.getDate().after(firstDeviateDate) && ts.getDecimalValue() != null)
              .max(new Comparator<TimeSeriesDTO<BigDecimal>>() {
                @Override
                public int compare(TimeSeriesDTO<BigDecimal> o1, TimeSeriesDTO<BigDecimal> o2) {
                  return o1.getDate().compareTo(o2.getDate());
                }
              }).orElse(null);

          // 前一個A轉
          if (lastVTurnTs != null && lastVTurnTs.getDecimalValue() != null) {
            Date lastVTurnDate = lastVTurnTs.getDate();
            lastATurnTs = tsATurnList.stream()
                .filter(ts -> ts != null && ts.getDate() != null && ts.getDecimalValue() != null
                    && ts.getDate().after(lastVTurnDate) && ts.getDate().before(tsDate))
                .max(new Comparator<TimeSeriesDTO<BigDecimal>>() {
                  @Override
                  public int compare(TimeSeriesDTO<BigDecimal> o1, TimeSeriesDTO<BigDecimal> o2) {
                    return o1.getDate().compareTo(o2.getDate());
                  }
                }).orElse(null);
            BigDecimal last40MinClose = closeTdList.subList(i - checkPeriod, i).stream()
                .map(closeTs -> closeTs.getDecimalValue()).min(Const._DEFAULT_COMPARATOR_BIGDECIMAL)
                .orElse(null);

            // 4736 20210511
            // 股價創新低 && 指標未創新低 => 準備向上
            if (closeTdList != null && closeTdList.get(i).getDecimalValue() != null
                && lastATurnTs != null
                && last40MinClose != null && lastATurnTs.getDecimalValue() != null
                // 股價創新低
                && closeTdList.get(i).getDecimalValue().compareTo(last40MinClose) < 0
                // t/t-1V > 2 底底高成長率>2倍
                && lastVTurnTs.getDecimalValue().multiply(new BigDecimal(2))
                .compareTo(tsList.get(i).getDecimalValue()) < 0
                && // tA/tV*2 < tA/t-1V 高點跌幅<漲幅*0.5
                tsList.get(i).getDecimalValue().multiply(new BigDecimal(2)).compareTo(
                    lastVTurnTs.getDecimalValue().add(lastATurnTs.getDecimalValue())) > 0) {
              return newHighOrLowDeviateStatus;
            }
          }

          // ----------------------------
          // 股價創新高 && 指標未創新高 => 準備向下
          lastATurnTs = null;
          lastVTurnTs = null;
          // 前一個A轉
          lastATurnTs = tsATurnList.stream()
              .filter(ts -> ts != null && ts.getDate() != null && ts.getDecimalValue() != null
                  && ts.getDate().before(tsDate) && ts.getDate().after(firstDeviateDate))
              .max(new Comparator<TimeSeriesDTO<BigDecimal>>() {
                @Override
                public int compare(TimeSeriesDTO<BigDecimal> o1, TimeSeriesDTO<BigDecimal> o2) {
                  return o1.getDate().compareTo(o2.getDate());
                }
              }).orElse(null);
          if (lastATurnTs != null && lastATurnTs.getDate() != null
              && lastATurnTs.getDecimalValue() != null) {
            // 前一個V轉
            Date lastATurnTsDate = lastATurnTs.getDate();
            lastVTurnTs = tsVTurnList.stream().filter(
                    ts -> ts != null && ts.getDate().before(tsDate) && ts.getDate()
                        .after(lastATurnTsDate))
                .max(new Comparator<TimeSeriesDTO<BigDecimal>>() {
                  @Override
                  public int compare(TimeSeriesDTO<BigDecimal> o1, TimeSeriesDTO<BigDecimal> o2) {
                    return o1.getDate().compareTo(o2.getDate());
                  }
                }).orElse(null);

            BigDecimal last40MaxClose = closeTdList.subList(i - checkPeriod, i).stream()
                .map(closeTs -> closeTs.getDecimalValue()).max(Const._DEFAULT_COMPARATOR_BIGDECIMAL)
                .orElse(null);

            // 股價創新高 && 指標未創新高 => 準備向下
            if (closeTdList != null && closeTdList.get(i).getDecimalValue() != null
                && last40MaxClose != null && lastVTurnTs != null
                && lastVTurnTs.getDecimalValue() != null
                // 股價創新高
                && closeTdList.get(i).getDecimalValue().compareTo(last40MaxClose) > 0
                // (100-t)/(100-(t-1A)) > 2 頭頭低，頂部差>2倍
                && lastATurnTs.getDecimalValue().multiply(new BigDecimal(2))
                .subtract(tsList.get(i).getDecimalValue()).compareTo(new BigDecimal(100)) > 0
                && // 跌勢反彈<跌勢1/2
                tsList.get(i).getDecimalValue().multiply(new BigDecimal(2))
                    .subtract(lastVTurnTs.getDecimalValue())
                    .compareTo(lastATurnTs.getDecimalValue()) < 0) {
              return -newHighOrLowDeviateStatus;
            }
          }
        }
      }
    } catch (Exception e) {
      WLog.error(e);
      return Const.ERROR_INT_RC;
    }
    return 0;
  }

  @SvcMethodInfo(methodDesc = "計算每日MA最大震幅", resultDesc = "MA最大震幅 TS List")
  public List<TimeSeriesDTO<BigDecimal>> getMaxMAAmplitude(int sid, int... maDays)
      throws ParseException {
    List<TimeSeriesDTO<BigDecimal>> closeTsList = dtDao.findTsList(sid,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    List<List<TimeSeriesDTO<BigDecimal>>> maList = new ArrayList<>();
    for (int maDay : maDays) {
      maList.add(techIdxSvc.getSMA(closeTsList, maDay));
    }
    return staticSvc.getMaxAmplitude(maList);
  }

  @SvcMethodInfo(methodDesc = "近40天K棒狀態", resultDesc = "{K棒狀態, 發生天數} ex.N天前發生長紅吞噬")
  public HashMap<KBar, Integer> getKBarStatus(int sid, Date baseDate) {
    HashMap<KBar, Integer> result = new HashMap<>();
    try {
      List<TimeSeriesDTO<OHLCVDTO>> ohlcTsList = dtDao.findRecentTsList(sid,
          Const._FIELDID_LIST_OHLCV,
          OHLCVDTO.class, baseDate, -40);

      // 取得變盤線Index
      float rbPercent = 0.03f;
      float rbSolidPercent = 0.6f;
      OHLCVDTO ohlcv, lastOhlcv;
      int changeTrendBarIndex = 0;
      for (changeTrendBarIndex = ohlcTsList.size() - 1; changeTrendBarIndex > 0;
          changeTrendBarIndex--) {
        ohlcv = ohlcTsList.get(changeTrendBarIndex).getValue();
        lastOhlcv = ohlcTsList.get(changeTrendBarIndex - 1).getValue();
        // 變盤線 => 實體棒*3<(H-L)
        if (ohlcv.getHigh()
            .subtract(ohlcv.getClose().max(ohlcv.getOpen())
                .subtract(ohlcv.getClose().min(ohlcv.getOpen()))
                .multiply(new BigDecimal(3)))
            .compareTo(ohlcv.getLow()) > 0 && !result.containsKey(KBar.CHANGE)) {
          // 變盤線出現後第幾天
          int changeKBarDays = (ohlcTsList.size() - 1) - changeTrendBarIndex;
          if (changeKBarDays != (ohlcTsList.size() - 1)) {
            // 今天就是變盤線
            if (changeKBarDays == 0) {
              result.put(KBar.CHANGE, 0);
            } else {
              // 變盤線隔天開高轉多，開低轉空
              result.put(KBar.CHANGE, changeKBarDays);
            }
          }
        }

        // 長紅吞噬
        BigDecimal solidGrowth = ohlcv.getClose().divide(ohlcv.getOpen(), 4, RoundingMode.HALF_UP);
        if (// 長紅棒>3%
            solidGrowth.compareTo(BigDecimal.valueOf(1 + rbPercent)) > 0
                && ohlcv.getClose().subtract(ohlcv.getOpen())
                .divide(ohlcv.getHigh().subtract(ohlcv.getClose().min(ohlcv.getOpen())))
                .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
                // 紅棒收盤高於昨日高點
                && ohlcv.getClose().compareTo(lastOhlcv.getClose()) > 0
//						//成交量>昨天
//						&& ohlcv.getTradingShares().compareTo(lastOhlcv.getTradingShares())>0
                // 昨日為長黑K
                && lastOhlcv.getClose().divide(lastOhlcv.getOpen(), 4, RoundingMode.HALF_UP)
                .compareTo(BigDecimal.valueOf(1 - rbPercent)) < 0
                && lastOhlcv.getOpen().subtract(lastOhlcv.getClose())
                .divide(lastOhlcv.getClose().max(lastOhlcv.getOpen()).subtract(lastOhlcv.getLow()),
                    2,
                    RoundingMode.HALF_UP)
                .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
                && !result.containsKey(KBar.RED_EAT_BLACK)
          // 昨日為大量
        ) {
          result.put(KBar.RED_EAT_BLACK, (ohlcTsList.size() - 1) - changeTrendBarIndex);
        }
        // 長黑吞噬
        if (// 長黑棒>3%
            solidGrowth.compareTo(BigDecimal.valueOf(1 - rbPercent)) < 0
                && ohlcv.getOpen().subtract(ohlcv.getClose()).abs()
                .divide(ohlcv.getOpen().max(ohlcv.getClose()).subtract(ohlcv.getLow()), 4,
                    RoundingMode.HALF_UP)
                .compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
                // 黑棒收盤低於昨日低點
                && ohlcv.getClose().compareTo(lastOhlcv.getClose()) < 0
//						//成交量>昨天
//						&& ohlcv.getTradingShares().compareTo(lastOhlcv.getTradingShares())>0
                // 昨日為長紅K
                && lastOhlcv.getClose().divide(lastOhlcv.getOpen(), 4, RoundingMode.HALF_UP)
                .compareTo(BigDecimal.valueOf(1 + rbPercent)) > 0
                && lastOhlcv.getClose().subtract(lastOhlcv.getOpen())
                .divide(lastOhlcv.getHigh().subtract(lastOhlcv.getClose().min(lastOhlcv.getOpen())))
                .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
                && !result.containsKey(KBar.BLACK_EAT_RED)
          // 昨日為大量
        ) {
          result.put(KBar.BLACK_EAT_RED, (ohlcTsList.size() - 1) - changeTrendBarIndex);
        }
        // 長黑後長下影線
        if (ohlcv.getOpen().min(ohlcv.getClose()).subtract(ohlcv.getLow())
            .compareTo(Const._BIGDECIMAL_0) > 0
            // 實體棒<3% 且 長下影線>實體4倍
            &&
            solidGrowth.subtract(Const._BIGDECIMAL_1).abs().compareTo(BigDecimal.valueOf(rbPercent))
                < 0
            && ohlcv.getOpen().subtract(ohlcv.getClose()).abs()
            .divide(ohlcv.getOpen().min(ohlcv.getClose()).subtract(ohlcv.getLow()), 4,
                RoundingMode.HALF_UP)
            .compareTo(BigDecimal.valueOf(0.25)) < 0
            // 下影棒收盤高於昨日收盤
            && ohlcv.getClose().compareTo(lastOhlcv.getClose()) > 0
//						//成交量>昨天
//						&& ohlcv.getTradingShares().compareTo(lastOhlcv.getTradingShares())>0
            // 昨日為長黑K
            && lastOhlcv.getClose().divide(lastOhlcv.getOpen(), 4, RoundingMode.HALF_UP)
            .compareTo(BigDecimal.valueOf(1 - rbPercent)) < 0
            // 黑實體
            && lastOhlcv.getClose().subtract(lastOhlcv.getOpen()).abs()
            .divide(lastOhlcv.getClose().max(lastOhlcv.getOpen()).subtract(lastOhlcv.getLow()))
            .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
            && !result.containsKey(KBar.BLACK_BUT_TRED)
          // 昨日為大量
        ) {
          result.put(KBar.BLACK_BUT_TRED, (ohlcTsList.size() - 1) - changeTrendBarIndex);
        }

        // 長紅後長上影線
        if (ohlcv.getHigh().subtract(ohlcv.getOpen().max(ohlcv.getClose()))
            .compareTo(Const._BIGDECIMAL_0) > 0
            && lastOhlcv.getHigh().subtract(lastOhlcv.getLow()).compareTo(Const._BIGDECIMAL_0) > 0
            // 實體棒<3% 且 長上影線>實體4倍
            &&
            solidGrowth.subtract(Const._BIGDECIMAL_1).abs().compareTo(BigDecimal.valueOf(rbPercent))
                < 0
            && ohlcv.getOpen().subtract(ohlcv.getClose()).abs()
            .divide(ohlcv.getHigh().subtract(ohlcv.getOpen().max(ohlcv.getClose())), 4,
                RoundingMode.HALF_UP)
            .compareTo(BigDecimal.valueOf(0.25)) < 0
            // 上影棒收盤低於昨日收盤
            && ohlcv.getClose().compareTo(lastOhlcv.getClose()) < 0
//						//成交量>昨天
//						&& ohlcv.getTradingShares().compareTo(lastOhlcv.getTradingShares())>0
            // 昨日為長紅K
            && lastOhlcv.getClose().divide(lastOhlcv.getOpen(), 4, RoundingMode.HALF_UP)
            .compareTo(BigDecimal.valueOf(1 + rbPercent)) > 0
            // 紅實體
            && lastOhlcv.getClose().subtract(lastOhlcv.getOpen()).abs()
            .divide(lastOhlcv.getHigh().subtract(lastOhlcv.getClose().min(lastOhlcv.getOpen())))
            .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
            && !result.containsKey(KBar.RED_BUT_RTBLACK)
          // 昨日為大量
        ) {
          result.put(KBar.RED_BUT_RTBLACK, (ohlcTsList.size() - 1) - changeTrendBarIndex);
        }

        // 長紅+長黑，2跟開收都差不多
        if (lastOhlcv.getHigh().subtract(lastOhlcv.getLow()).compareTo(Const._BIGDECIMAL_0) > 0
            // 2天開盤即收盤價差值<1%
            && ohlcv.getOpen().max(ohlcv.getClose())
            .divide(lastOhlcv.getOpen().max(lastOhlcv.getClose()), 4, RoundingMode.HALF_UP)
            .setScale(4, RoundingMode.HALF_UP)
            .add(ohlcv.getOpen().min(ohlcv.getClose())
                .divide(lastOhlcv.getOpen().min(lastOhlcv.getClose()), 4, RoundingMode.HALF_UP))
            .compareTo(BigDecimal.valueOf(0.01)) < 0
            // 實體
            && lastOhlcv.getClose().subtract(lastOhlcv.getOpen()).abs()
            .divide(lastOhlcv.getHigh().subtract(lastOhlcv.getLow()))
            .setScale(4, RoundingMode.HALF_UP).compareTo(BigDecimal.valueOf(rbSolidPercent)) > 0
            // 長實體
            && lastOhlcv.getClose().divide(lastOhlcv.getOpen(), 4, RoundingMode.HALF_UP)
            .subtract(Const._BIGDECIMAL_1).abs().compareTo(BigDecimal.valueOf(rbPercent)) > 0
            && (!result.containsKey(KBar.RED_AND_BLACK) || !result.containsKey(
            KBar.BLACK_AND_RED))) {
          // 紅黑
          if (ohlcv.getClose().compareTo(ohlcv.getOpen()) < 0 && !result.containsKey(
              KBar.RED_AND_BLACK)) {
            result.put(KBar.RED_AND_BLACK, (ohlcTsList.size() - 1) - changeTrendBarIndex);
          } else if (!result.containsKey(KBar.BLACK_AND_RED)) {
            result.put(KBar.BLACK_AND_RED, (ohlcTsList.size() - 1) - changeTrendBarIndex);
          }
        }
      }
      return result;
    } catch (Exception e) {
      WLog.error(e);
      return null;
    }
  }

  public Integer closeCompareToMA(int sid, Date date, int maDay) {
    List<TimeSeriesDTO<BigDecimal>> close = dtDao.findTsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE, date, date);
    List<TimeSeriesDTO<BigDecimal>> maTsList = techIdxSvc.getSMA(sid, maDay, Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    if(CollectionUtils.isEmpty(close)){
      return null;
    }
    TimeSeriesDTO<BigDecimal> ma = maTsList.stream().filter(maTs -> maTs.getDate().compareTo(close.get(0).getDate()) == 0).findFirst().orElse(null);
    if(ma != null && ma.getValue() != null) {
      return close.get(0).getDecimalValue().compareTo(ma.getDecimalValue());
    } else {
      return null;
    }
  }

  public Integer getMaTrend(int sid, Date date, int maDay){
    List<TimeSeriesDTO<BigDecimal>> maTsList = techIdxSvc.getSMA(sid, maDay, Const._FIELDID_PRICE_DAILY_TW_CLOSE).stream().filter(maTs -> WDate.beforeOrEquals(maTs.getDate(), date)).collect(
        Collectors.toList());
    if(maTsList.size() > 1 && maTsList.get(maTsList.size()-1).getValue() != null && maTsList.get(maTsList.size()-2).getValue() != null) {
      return maTsList.get(maTsList.size()-1).getValue().compareTo(maTsList.get(maTsList.size()-2).getValue());
    } else {
      WLog.debug("can't get Ma Trend, sid["+sid+"]date["+date+"]maDay["+maDay+"]");
      return null;
    }
  }
}
