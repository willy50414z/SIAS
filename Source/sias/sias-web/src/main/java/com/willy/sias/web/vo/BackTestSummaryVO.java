 package com.willy.sias.web.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class BackTestSummaryVO {
	@BackTestResultColumn(desc = "交易標的", order = 0)
	private String target;
	@BackTestResultColumn(desc = "策略代號", order = 1)
	private String strategyCode;
	@BackTestResultColumn(desc = "勝率", order = 2)
	private String profitRatio;
	@BackTestResultColumn(desc = "盈虧比", order = 3)
	private String profitAmtRatio;
	@BackTestResultColumn(desc = "期望值", order = 4)
	private String expectValue;
	@BackTestResultColumn(desc = "交易次數", order = 6)
	private Integer tradeCount;
	private String testIdD;
}
