package com.willy.sias.backtest.dto;

import java.math.BigDecimal;

import com.willy.sias.db.po.DividendTWPO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DividendFillCDivDTO {
	private DividendTWPO divPo;
	private Integer fillDays;
	private BigDecimal maxLossRatio;
}
