package com.willy.sias.batch.processor;

import com.willy.file.txt.WFile_txt;
import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Setter
@ConfigurationProperties("sias.batch.stockprice-tw-twse")
public class STOCK_PRICE_TW_TWSE extends BaseBatchProcessor {
	@Autowired
	private WFile_txt txtFile;
	@Autowired
	private PriceDailyTwRepository spRepo;
	@Autowired
	private WJDBC jdbc;
	private Set<PriceDailyTwPO> spSet;
	
	@Transactional
	public void exec() throws Exception {
		spSet = new HashSet<>();// temp股價資料
		Set<CompanyCategoryPO> ccSet = new HashSet<>();// 股票分類資料
		
		for (String dataType : Const._MOPS_INDUSTRY_TYPE_CODE_1) {
			this.putMapParam("dataType", dataType);

			// 下載股價資料
			filePath = FileUtil.download(this.parseMapParamToString(urlTempl), this.parseMapParamToString(csvFileNameTempl));

			// 讀出準備插入SID對應物件List
			String[][] spAr = txtFile.readToAr(filePath, "證券代號", "備註:",true);
			
			// dataType="01" 而且 沒有資料 表示當天不是交易日，直接return
			if (spAr.length == 0 && dataType.equals("01")) {
				if(dataDate.equals(WType.strToDate(WDate.getNowDate()))) {
					WLog.info("當天資料尚未公告");
				}else {
					WLog.info(this.dataDate + "當天非交易日");
				}
				return;
			}
			
			this.mergeSid(spAr);
			
			//蒐集Price資訊，全部一次匯入，減少連線次數
			this.spSet.addAll(this.getSpList(spAr));

			//蒐集股票分類資訊，全部一次匯入，減少連線次數
			ccSet.addAll(this.getCompanyCategorySet(spAr, dataType));
		}

		jdbc.batchInsert(filtSpSet(spSet));
		
		ccSet = filtCCList(ccSet);

		jdbc.batchInsert(ccSet);

		fixNoPriceData();
	}
	
	private Set<PriceDailyTwPO> filtSpSet(Set<PriceDailyTwPO> spSet){
		Set<PriceDailyTwPO> filtedSpSet = new HashSet<> ();
		Set<Integer> sidSet = spRepo.findSidByDataDate(dataDate);
		for(PriceDailyTwPO sp : spSet) {
			if(!sidSet.contains(sp.getSid())) {
				filtedSpSet.add(sp);
			}
		}
		return filtedSpSet;
	}

	private Set<PriceDailyTwPO> getSpList(String[][] spAr) {
		PriceDailyTwPO sp;
		Set<PriceDailyTwPO> spList = new HashSet<>();
		for (String[] spRow : spAr) {
			WLog.debug(WString.toString(spRow));
			sp = new PriceDailyTwPO();
			sp.setSid(sidMap.get(getStrFromArray(spRow, 0)));
			sp.setDataDate(dataDate);
			sp.setOpen(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 5))));
			sp.setHigh(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 6))));
			sp.setLow(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 7))));
			sp.setClose(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 8))));
			sp.setTradingShares(Long.valueOf(StringUtil.cleanDigitData(getStrFromArray(spRow, 2))));
			sp.setTradingNum(Integer.valueOf(StringUtil.cleanDigitData(getStrFromArray(spRow, 3))));
			sp.setTurnOver(Long.valueOf(StringUtil.cleanDigitData(getStrFromArray(spRow, 4))));
			sp.setLastBuyPrice(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 11))));
			sp.setLastBuyVol(Integer.valueOf(StringUtil.cleanDigitData(getStrFromArray(spRow, 12))));
			sp.setLastSoldPrice(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 13))));
			sp.setLastSoldVol(Integer.valueOf(StringUtil.cleanDigitData(getStrFromArray(spRow, 14))));
			spList.add(sp);
		}
		return spList;
	}
	
	private Set<CompanyCategoryPO> getCompanyCategorySet(String[][] spAr,String dataType) {
		SysConfigPO ccType = scRepo.findByParentIdIsAndCfgValueIs(ParentKey.Category_TWSE.getKey(), dataType);
		// 塞值組出List
		Integer sid;
		CompanyCategoryPO cc;
		Set<CompanyCategoryPO> companyCategorySet = new HashSet<>();
		for (String[] sp : spAr) {
			sid = sidMap.get(sp[0]);
			
			//產業別
			cc = new CompanyCategoryPO();
			cc.setSid(sid);
			cc.setDataDate(dataDate);
			cc.setCategoryId(ccType.getCfgId());
			companyCategorySet.add(cc);
			
			//市場別
			cc = new CompanyCategoryPO();
			cc.setSid(sid);
			cc.setDataDate(dataDate);
			cc.setCategoryId(Const._CFG_ID_MARKET_TYPE_TWSE);
			companyCategorySet.add(cc);
		}
		return companyCategorySet;
	}
	
	@Override
	public void cleanHisData() throws Exception {
		spRepo.deleteByDateAndCategoryId(dataDate, Const._CFG_ID_MARKET_TYPE_TWSE);
	}

	private void fixNoPriceData() {
		while (true) {
			String[][] result = jdbc.queryForAr(
					"select top 50 [DATA_DATE], [SID]  from PRICE_DAILY_TW p where p.[open] = -1 order by [DATA_DATE]");
			if (result.length == 0) {
				break;
			}
			for (String[] strings : result) {
				String[][] price = jdbc.queryForAr(
						"select top 1 [SID], [close] from PRICE_DAILY_TW  where [SID] = '"
								+ strings[1] + "' and [DATA_DATE] < '" + strings[0]
								+ "' order by [DATA_DATE] desc");
				if (price.length > 0) {
					jdbc.getJdbcTemplate().execute(
							"update PRICE_DAILY_TW set [open]='" + price[0][1] + "', [high]='" + price[0][1]
									+ "', [low]='" + price[0][1] + "', [close]='" + price[0][1] + "' where [sid]='"
									+ strings[1] + "' and [DATA_DATE]='" + strings[0] + "'");
				} else {
					jdbc.getJdbcTemplate().execute(
							"delete PRICE_DAILY_TW where [sid]='" + strings[1] + "' and [DATA_DATE]='"
									+ strings[0] + "'");
				}
			}
		}
	}
}