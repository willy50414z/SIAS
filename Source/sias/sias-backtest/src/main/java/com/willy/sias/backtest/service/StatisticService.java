package com.willy.sias.backtest.service;

import com.willy.r.dao.WRStatics;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.config.Const;
import com.willy.util.calculator.WCalculator;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StatisticService {

  @Autowired
  private DataTableDao dtDao;
  @Autowired
  ProfitDTOService ps;
  @Value("${sias.trade.nonRiskReturnRatio}")
  String nonRiskReturnRatio;
  @Autowired
  public WRStatics wr;

  /**
   * 貝他值 (大盤連動性) 相關係數correlation(指數報酬數列 , 策略報酬數列) × ( 策略報酬標準差 ÷ 指數報酬標準差)
   *
   * @param profitDTO
   * @return
   */
  public BigDecimal getBeltaValue(ProfitDTO profitDTO) {
    if (profitDTO.getTradeDetailList().size() == 0) {
      return Const._BIGDECIMAL_m1;
    }
    List<Date> tradeDateList = profitDTO.getTradeDetailList().stream().map(td -> td.getTradeDate())
        .sorted(Const._DEFAULT_COMPARATOR_DATE).collect(Collectors.toList());

    // 加權指數報酬率
    List<TimeSeriesDTO<BigDecimal>> weightIndexTsList = dtDao.findTsList(
        Const._CFG_ID_MARKET_WEIGHTED_INDEX,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE, tradeDateList.get(0),
        tradeDateList.get(tradeDateList.size() - 1));
    List<TimeSeriesDTO<BigDecimal>> weightIndexGrowthList = this.getGrowth(weightIndexTsList, 1);

    // 投資組合報酬率
    List<TimeSeriesDTO<BigDecimal>> netValueTsList = profitDTO.getNetValueTsList();
    List<TimeSeriesDTO<BigDecimal>> investStrategyGrowthList = this.getGrowth(netValueTsList, 1);

    // 相關係數
    BigDecimal correlation = this.getCorrelation(
        weightIndexGrowthList.stream().map(g -> g.getValue()).collect(Collectors.toList()),
        investStrategyGrowthList.stream().map(g -> g.getValue()).collect(Collectors.toList()));

    // 標準差
    BigDecimal sd = this.getSd(
            investStrategyGrowthList.stream().map(g -> g.getValue()).collect(Collectors.toList()))
        .divide(this.getSd(
                weightIndexGrowthList.stream().map(g -> g.getValue()).collect(Collectors.toList())), 4,
            RoundingMode.HALF_UP);
    BigDecimal betaValue = correlation.multiply(sd);
    WLog.info("correlation[" + correlation + "]sd[" + sd + "]betaValue[" + betaValue + "]");
    return betaValue;
  }

  /**
   * 阿法值 (超額報酬) α = R –  R f –β（R m -R f） R：代表投資組合報酬 R f：表示無風險報酬率 β：代表投資組合的系統風險 R m：代表根據基準的市場報酬
   *
   * @param profitDTO
   * @return
   */
  public BigDecimal getAlphaValue(ProfitDTO profitDTO) {
    if (profitDTO.getTradeDetailList().size() == 0) {
      return Const._BIGDECIMAL_m1;
    }
    List<Date> tradeDateList = profitDTO.getTradeDetailList().stream().map(td -> td.getTradeDate())
        .sorted(Const._DEFAULT_COMPARATOR_DATE).collect(Collectors.toList());

    // 策略報酬率 R
    BigDecimal strategyReturnRatio = ps.getGrossProfitRatio(profitDTO,
        tradeDateList.get(tradeDateList.size() - 1));

    // 基準市場報酬R f
    List<TimeSeriesDTO<BigDecimal>> weightIndexTsList = dtDao.findTsList(
        Const._CFG_ID_MARKET_WEIGHTED_INDEX,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE, tradeDateList.get(0),
        tradeDateList.get(tradeDateList.size() - 1));
    BigDecimal baseReturnRatio = weightIndexTsList.get(weightIndexTsList.size() - 1)
        .getDecimalValue()
        .divide(weightIndexTsList.get(0).getDecimalValue(), 4, RoundingMode.HALF_UP);
    // β
    BigDecimal betaValue = profitDTO.getBetaRatio();
    // 投資組合報酬 - 無風險報酬率 - β(基準的市場報酬 - 無風險報酬率)
    BigDecimal alphaValue = strategyReturnRatio.subtract(new BigDecimal(this.nonRiskReturnRatio))
        .subtract(
            betaValue.multiply(baseReturnRatio.subtract(new BigDecimal(this.nonRiskReturnRatio))));
    WLog.info(
        "strategyReturnRatio[" + strategyReturnRatio + "]nonRiskReturnRatio[" + nonRiskReturnRatio
            + "]betaValue[" + betaValue + "]baseReturnRatio[" + baseReturnRatio + "]alphaValue["
            + alphaValue
            + "]");
    return alphaValue;
  }

  public Object eval(String formula) {
    return WCalculator.eval(formula);
  }

  public BigDecimal getSuccessRatio(List<BigDecimal> numList, String criteria) {
    BigDecimal successCount = new BigDecimal(0);
    for (BigDecimal num : numList) {
      if ((boolean) eval(num.toString() + criteria)) {
        successCount = successCount.add(new BigDecimal(1));
      }
    }
    return successCount.divide(new BigDecimal(numList.size()), 4, RoundingMode.HALF_UP);
  }

  // 單一值型別函數(平均數,標準差...)

  /**
   * 取得平均數
   *
   * @param bigDecimalList
   * @return
   */
  public BigDecimal getMean(List<BigDecimal> bigDecimalList) {
    BigDecimal sum = new BigDecimal(0);
    for (BigDecimal num : bigDecimalList) {
      sum = sum.add(num);
    }
    return sum.divide(new BigDecimal(bigDecimalList.size()), 4, RoundingMode.HALF_UP);
  }

  /**
   * 取得相關係數
   *
   * @return
   */
  public BigDecimal getCorrelation(List<BigDecimal> p1List, List<BigDecimal> p2List) {
    return new BigDecimal(String.valueOf(wr.getPCC(p1List, p2List))).setScale(7,
        RoundingMode.HALF_UP);
  }

  /**
   * 取得標準差
   *
   * @return
   */
  public BigDecimal getSd(List<BigDecimal> bigDecimalList) {
    return new BigDecimal(wr.getSd(
        bigDecimalList.stream().map(b -> b.toString()).collect(Collectors.toList()))).setScale(7,
        RoundingMode.HALF_UP);
  }

  /**
   * 取得標準差範圍
   *
   * @param numList 資料集
   * @param formula 標準差計算方式 +1,+2,-1,-2
   * @return
   */
  public BigDecimal getSdEdge(List<BigDecimal> numList, int formula) {
    BigDecimal sd = this.getSd(numList);
    BigDecimal mean = this.getMean(numList);
    WLog.debug("標準差: " + sd + "\t平均值: " + mean);
    String sdEdges = "標準差分布:　";
    for (int i = -2; i < 3; i++) {
      sdEdges += ((formula == i) ? "[" : "") + mean.add(sd.multiply(new BigDecimal(i)))
          + ((formula == i) ? "]" : "") + "\t";
    }
    WLog.debug("標準差分布:　" + sdEdges);
    return mean.add(sd.multiply(new BigDecimal(formula)));
  }

  // 值型別Function

  /**
   * interval其成長率
   *
   * @param tsdList  資料集
   * @param interval 計算區間
   * @return
   * @throws Exception
   */
  public List<TimeSeriesDTO<BigDecimal>> getGrowth(List<TimeSeriesDTO<BigDecimal>> tsdList,
      int interval) {
    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<TimeSeriesDTO<BigDecimal>>();
    for (int rowIndex = 0; rowIndex < tsdList.size(); rowIndex++) {
      if (rowIndex < interval) {
        result.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(rowIndex).getDate(), null));
      } else {
        result.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(rowIndex).getDate(),
            tsdList.get(rowIndex - interval).getDecimalValue().compareTo(new BigDecimal(0)) == 0
                ? null
                : (tsdList.get(rowIndex).getDecimalValue().divide(
                        tsdList.get(rowIndex - interval).getDecimalValue(), 20, RoundingMode.HALF_UP)
                    .subtract(new BigDecimal(1)))));
      }
    }
    return result.size() == 0 ? result : result.subList(1, result.size());
  }

  public List<TimeSeriesDTO<BigDecimal>> getRecentHigh(List<TimeSeriesDTO<BigDecimal>> tsdList,
      int interval) {
    BigDecimal maxPrice;
    List<TimeSeriesDTO<BigDecimal>> recentMax = new ArrayList<TimeSeriesDTO<BigDecimal>>();
    for (int i = 0; i < tsdList.size(); i++) {
      maxPrice = new BigDecimal(0);
      if (i < interval) {
        recentMax.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(i).getDate(), null));
      } else {
        for (int j = i - interval; j < i; j++) {
          maxPrice = maxPrice.max(tsdList.get(j).getDecimalValue());
        }
        recentMax.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(i).getDate(), maxPrice));
      }
    }
    return recentMax;
  }

  /**
   * 取得各tdList中的最大振幅 @param tsdList @return @throws
   */
  public List<TimeSeriesDTO<BigDecimal>> getMaxAmplitude(List<TimeSeriesDTO<BigDecimal>>... tsdAr) {
    // 確認各tdList的日期index相同
    List<Date> tdDateList = null;
    boolean isDateIndexMatch = true;
    for (List<TimeSeriesDTO<BigDecimal>> tsdList : tsdAr) {
      if (tdDateList == null) {
        tdDateList = tsdList.stream().map(td -> td.getDate()).collect(Collectors.toList());
      } else {
        if (tsdList == null) {
          return null;
        }
        WLog.debug("Check tsdList size[" + tsdList.size() + "]");
        if (!tsdList.stream().map(td -> td.getDate()).collect(Collectors.toList())
            .equals(tdDateList)) {
          isDateIndexMatch = false;
          break;
        }
      }
    }

    List<TimeSeriesDTO<BigDecimal>> maxAmplitudeTsList = new ArrayList<>();
    if (isDateIndexMatch) {
      for (int i = 0; i < tsdAr[0].size(); i++) {
        BigDecimal max = Const._BIGDECIMAL_0, min = Const._BIGDECIMAL_0, maxAmplitude = null;
        for (List<TimeSeriesDTO<BigDecimal>> tsdList : tsdAr) {
          if (tsdList.get(i).getDecimalValue() == null) {
            max = Const._BIGDECIMAL_0;
            min = Const._BIGDECIMAL_0;
            break;
          }
          max = max.max(tsdList.get(i).getDecimalValue());
          min = (min.equals(Const._BIGDECIMAL_0)) ? min = tsdList.get(i).getDecimalValue()
              : min.min(tsdList.get(i).getDecimalValue());
        }
        if (max.compareTo(Const._BIGDECIMAL_0) > 0 && min.compareTo(Const._BIGDECIMAL_0) > 0) {
          maxAmplitude = max.divide(min, 4, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1);
        }
//				WLog.info("Date["+tsdAr[0].get(i).getDate()+"]max["+max+"]min["+min+"]maxAmplitude["+maxAmplitude+"]");
        maxAmplitudeTsList.add(
            new TimeSeriesDTO<BigDecimal>(tsdAr[0].get(i).getDate(), maxAmplitude));
      }
      return maxAmplitudeTsList;
    } else {
      return null;
    }
  }

  public List<TimeSeriesDTO<BigDecimal>> getMaxAmplitude(
      List<List<TimeSeriesDTO<BigDecimal>>> tsdAr) {
    // 確認各tdList的日期index相同
    List<Date> tdDateList = null;
    boolean isDateIndexMatch = true;
    for (List<TimeSeriesDTO<BigDecimal>> tsdList : tsdAr) {
      if (tdDateList == null) {
        tdDateList = tsdList.stream().map(td -> td.getDate()).collect(Collectors.toList());
      } else {
        if (tsdList == null) {
          return null;
        }
        WLog.debug("Check tsdList size[" + tsdList.size() + "]");
        if (!tsdList.stream().map(td -> td.getDate()).collect(Collectors.toList())
            .equals(tdDateList)) {
          isDateIndexMatch = false;
          break;
        }
      }
    }

    List<TimeSeriesDTO<BigDecimal>> maxAmplitudeTsList = new ArrayList<>();
    if (isDateIndexMatch) {
      for (int i = 0; i < tsdAr.get(0).size(); i++) {
        BigDecimal max = Const._BIGDECIMAL_0, min = Const._BIGDECIMAL_0, maxAmplitude = null;
        for (List<TimeSeriesDTO<BigDecimal>> tsdList : tsdAr) {
          if (tsdList.get(i).getDecimalValue() == null) {
            max = Const._BIGDECIMAL_0;
            min = Const._BIGDECIMAL_0;
            break;
          }
          max = max.max(tsdList.get(i).getDecimalValue());
          min = (min.equals(Const._BIGDECIMAL_0)) ? min = tsdList.get(i).getDecimalValue()
              : min.min(tsdList.get(i).getDecimalValue());
        }
        if (max.compareTo(Const._BIGDECIMAL_0) > 0 && min.compareTo(Const._BIGDECIMAL_0) > 0) {
          maxAmplitude = max.divide(min, 4, RoundingMode.HALF_UP).subtract(Const._BIGDECIMAL_1);
        }
        maxAmplitudeTsList.add(
            new TimeSeriesDTO<BigDecimal>(tsdAr.get(0).get(i).getDate(), maxAmplitude));
      }
      return maxAmplitudeTsList;
    } else {
      return null;
    }
  }

  /**
   * 對每一個tsd的value做計算
   *
   * @param tsdList
   * @param operator
   * @return
   * @throws Exception
   */
  public List<TimeSeriesDTO<BigDecimal>> caculateTs(List<TimeSeriesDTO<BigDecimal>> tsdList,
      String operator)
      throws Exception {
    for (int tsListIndex = 0; tsListIndex < tsdList.size(); tsListIndex++) {
      tsdList.get(tsListIndex).setValue(WCalculator.eval(tsdList.get(tsListIndex).getValue() + operator));
    }
    return tsdList;
  }

  /**
   * 兩dts的value做計算
   *
   * @param tsdList1
   * @param tsdList2
   * @param formula
   * @return
   * @throws Exception
   */
  public List<TimeSeriesDTO<BigDecimal>> caculateTses(List<TimeSeriesDTO<BigDecimal>> tsdList1,
      List<TimeSeriesDTO<BigDecimal>> tsdList2, String formula) throws Exception {
    TimeSeriesDTO<BigDecimal> tsd1;
    TimeSeriesDTO<BigDecimal> tsd2;
    if (tsdList1.size() != tsdList2.size()) {
      throw new IllegalArgumentException(
          "兩資料集列述不一,資料集1 : " + tsdList1.size() + "列, 資料集2 : " + tsdList1.size());
    }
    for (int tsdListIndex = 0; tsdListIndex < tsdList1.size(); tsdListIndex++) {
      tsd1 = tsdList1.get(tsdListIndex);
      tsd2 = tsdList1.get(tsdListIndex);
      tsd1.setValue(
          new BigDecimal(WCalculator.eval(formula.replace("$tsdList1$", tsd1.getValue().toString())
              .replace("$tsdList2$", tsd2.getValue().toString())).toString()));
    }
    return tsdList1;
  }

  public List<Date> getIntersectDateList(List<Date> dateList1, List<Date> dateList2) {
    List<Date> dateList = new ArrayList<Date>();
    for (Date date : dateList1) {
      if (dateList2.contains(date)) {
        dateList.add(date);
      }
    }
    return dateList;
  }
}
