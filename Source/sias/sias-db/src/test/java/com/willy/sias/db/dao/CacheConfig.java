package com.willy.sias.db.dao;

import org.springframework.cache.CacheManager;
import org.springframework.cache.support.NoOpCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
public class CacheConfig {
  @Bean
  @Profile("test")
  @Primary
  public CacheManager getNoOpCacheManager() {
    return new NoOpCacheManager();
  }
}
