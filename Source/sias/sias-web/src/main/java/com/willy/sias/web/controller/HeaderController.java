package com.willy.sias.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/header")
public class HeaderController {
	@RequestMapping("/")
	public ModelAndView visit() {
		return new ModelAndView("header");
	}
}
