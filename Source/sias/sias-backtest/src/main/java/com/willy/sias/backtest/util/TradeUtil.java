package com.willy.sias.backtest.util;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradeDetailDTO;
import com.willy.sias.backtest.service.ProfitDTOService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.util.BigDecimalUtil;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class TradeUtil {

  @Value("${sias.trade.defaultHandlingFeeDiscount}")
  private BigDecimal defaultHandlingFeeDiscount;// 交易手續費率折扣
  @Value("${sias.trade.handlingFeeRatio}")
  private BigDecimal handlingFeeRatio;// 交易手續費率
  @Value("${sias.trade.txnTaxRatio}")
  private BigDecimal txnTaxRatio;// 交易稅率
  @Value("${sias.trade.shortingSellFeeRatio}")
  private BigDecimal shortingSellFeeRatio;// 交易稅率
  @Value("${sias.trade.shortingSellGuaranteeRatio}")
  private BigDecimal shortingSellGuaranteeRatio;// 融券保證金率
  @Value("${sias.trade.defaultMinHandlingFee}")
  private BigDecimal defaultMinHandlingFee;// 單筆最低手續費
  @Autowired
  private DataTableDao dtDao;
  @Autowired
  private PriceDailyTwRepository priceRepo;
  @Autowired
  private CalendarRepository calendarRepo;
  @Autowired
  private ProfitDTOService ps;
  public final Comparator<TradeDetailDTO> _COMPARATOR_TRADEDETAILPO_TRADEPRICE = Comparator.comparing(
      TradeDetailDTO::getTradePrice);


  public int trade(Integer sid, Date tradeDate, int units, TradeType tradeType,
      ProfitDTO profitDTO) {
    return trade(sid, tradeDate, units, tradeType, PriceType.CLOSE, profitDTO);
  }

  public int trade(Integer sid, Date tradeDate, int units, TradeType tradeType, PriceType pt,
      ProfitDTO profitDTO) {
    BigDecimal tradePrice = getPrice(sid, pt, tradeDate);
    return trade(sid, tradeDate, units, tradeType, tradePrice, profitDTO);
  }

  private int buy(String tradeId, int sid, Date tradeDate, ProfitDTO profitDTO, int tradeUnits,
      BigDecimal tradePrice, Date settleDate) {
    //可沖買存貨
    int shortBuyableCount = ps.getShortBuyableCount(profitDTO, sid, tradeDate).intValue();
    //有可沖買交易直接轉為沖買交易
    if (shortBuyableCount > 0L) {
      WLog.debug("change buy to short buy, shortBuyableCount[" + shortBuyableCount + "]");
      this.trade(sid, tradeDate, Math.min(shortBuyableCount, tradeUnits),
          TradeType.shortBuying, tradePrice, profitDTO);
      //可沖買存貨不足抵銷普買，則繼續進行普買作業
      if (tradeUnits > shortBuyableCount) {
        tradeUnits = tradeUnits - shortBuyableCount;
      } else {
        return Const._TRADE_RESULT_CODE_SUCCESS;
      }
    }
    // 普買作業
    // 確認帳上餘額充足(目前持有資產不足以達成此交易，即減少交易單位)
    BigDecimal acctBalance = ps.getAcctBalance(profitDTO, tradeDate);
    BigDecimal tradeAmt = getTradeAmt(tradeUnits, tradePrice, TradeType.buy);
    if (tradeAmt.compareTo(acctBalance) > 0) {
      return Const._TRADE_RESULT_CODE_BALANCE_NOT_ENOUGH;
    }
    tradeAmt = tradeAmt.multiply(Const._BIGDECIMAL_m1).setScale(0, RoundingMode.HALF_UP);
    // 建立交易紀錄
    TradeDetailDTO tdDto = new TradeDetailDTO();
    tdDto.setTradeId(tradeId);
    tdDto.setTradeType(TradeType.buy);
    tdDto.setSid(sid);
    tdDto.setTradeDate(tradeDate);
    tdDto.setTradeUnit(new BigDecimal(tradeUnits));
    tdDto.setTradePrice(tradePrice);
    tdDto.setSettleDate(settleDate);
    tdDto.setTradeAmt(tradeAmt);
    profitDTO.getTradeDetailList().add(tdDto);
    //調整存貨，看同一天有沒有買入相同標的
    TradeDetailDTO existInv = profitDTO.getInventoryList().stream()
        .filter(
            iv -> iv.getSid().compareTo(sid) == 0 && iv.getTradeDate().compareTo(tradeDate) == 0
                && iv.getTradeType().equals(TradeType.buy)
                && iv.getTradePrice().compareTo(tradePrice) == 0)
        .findFirst().orElse(null);
    if (existInv == null) {
      //沒有，建立存貨
      profitDTO.getInventoryList().add(tdDto.newInstance());
    } else {
      //有 => 存貨+1
      existInv.setTradeUnit(existInv.getTradeUnit().add(new BigDecimal(tradeUnits)));
      existInv.setTradeAmt(existInv.getTradeAmt().add(tradeAmt));
    }
    return Const._TRADE_RESULT_CODE_SUCCESS;
  }

  private int shortBuy(String tradeId, int sid, Date tradeDate, ProfitDTO profitDTO, int tradeUnits,
      BigDecimal tradePrice, Date settleDate) {
    // 計算帳戶餘額是否足以買入金額
    BigDecimal acctBalance = ps.getAcctBalance(profitDTO, tradeDate);
    BigDecimal tradeAmt = getTradeAmt(tradeUnits, tradePrice, TradeType.shortBuying);
    if (tradeAmt.compareTo(acctBalance) > 0) {
      return Const._TRADE_RESULT_CODE_BALANCE_NOT_ENOUGH;
    }

    // 從存貨中取出之前融券的交易
    List<TradeDetailDTO> shortSelledInvList = profitDTO
        .getInventoryList().stream().filter(iv -> iv.getSid().compareTo(sid) == 0
            && iv.getTradeDate().equals(tradeDate) && iv.getTradeType()
            .equals(TradeType.shortSelling))
        .sorted(_COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
    Collections.reverse(shortSelledInvList);//沖掉'賣'的存貨，價高優先
    int shortSelledInvCount = BigDecimalUtil.sum(
            shortSelledInvList.stream().map(TradeDetailDTO::getTradeUnit).collect(Collectors.toList()))
        .intValue();

    //區分平倉數量(tradeUnits)/建倉數量(pureShortBuyUnits)，先處理平倉的
    int pureShortBuyUnits = 0;
    if (shortSelledInvCount > 0) {
      pureShortBuyUnits = Math.max(0, tradeUnits - shortSelledInvCount);
      tradeUnits = Math.min(tradeUnits, shortSelledInvCount);
    }
    if (tradeUnits == 0) {
      return Const._TRADE_RESULT_CODE_SUCCESS;
    }

    tradeAmt = getTradeAmt(tradeUnits, tradePrice, TradeType.shortBuying);
    // 填入買交易資訊
    tradeAmt = tradeAmt.multiply(Const._BIGDECIMAL_m1).setScale(0, RoundingMode.HALF_UP);
    TradeDetailDTO tdDto = new TradeDetailDTO();
    tdDto.setTradeId(tradeId);
    tdDto.setTradeType(TradeType.shortBuying);
    tdDto.setSid(sid);
    tdDto.setTradeDate(tradeDate);
    tdDto.setTradeUnit(new BigDecimal(tradeUnits));
    tdDto.setTradePrice(tradePrice);
    tdDto.setSettleDate(settleDate);
    tdDto.setTradeAmt(tradeAmt);
    profitDTO.getTradeDetailList().add(tdDto);

    // 調整平倉存貨 => 有融券庫存
    if (CollectionUtils.isNotEmpty(shortSelledInvList)) {
      int remainUnit;
      BigDecimal cost = new BigDecimal(0), tmpCost;
      for (TradeDetailDTO inv : shortSelledInvList) {
        //融券庫存>0 && 待交易數量(含平/建倉) >0
        if (shortSelledInvCount > 0 && tradeUnits > 0) {
          //存貨 > 交易單位數
          remainUnit = Math.max(0, inv.getTradeUnit().intValue() - tradeUnits);
          if (remainUnit > 0) {
            //未完銷才需計算金額，否則後面會被remove
            tmpCost = inv.getTradeAmt().multiply(new BigDecimal(tradeUnits))
                .divide(inv.getTradeUnit(), 0,
                    RoundingMode.CEILING);
            inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
            cost = cost.add(tmpCost);
            tradeAmt = tradeAmt.subtract(tmpCost);
          } else {
            cost = cost.add(inv.getTradeAmt());
            tradeAmt = tradeAmt.subtract(inv.getTradeAmt());
          }
          //剩餘庫存(沖賣庫存扣除本次消除庫存)
          shortSelledInvCount -= inv.getTradeUnit().intValue();
          //剩餘待交易數
          tradeUnits -= inv.getTradeUnit().intValue();
          inv.setTradeUnit(new BigDecimal(remainUnit));
          tdDto.addParentTradeId(inv.getTradeId());
        }
      }
      if (cost.compareTo(Const._BIGDECIMAL_0) != 0) {
        tdDto.setGrossProfit(cost.add(tdDto.getTradeAmt()));
        tdDto.setCost(cost);
      }
    }

    //調整單純沖買庫存
    if (tradeUnits > 0) {
      /*
        相同日期/標的的庫存+1, 沒有就建
       */
      TradeDetailDTO existInv = profitDTO.getInventoryList().stream()
          .filter(iv -> iv.getSid().compareTo(sid) == 0
              && iv.getTradeDate().compareTo(tradeDate) == 0
              && iv.getTradeType().equals(TradeType.shortBuying)
              && iv.getTradePrice().compareTo(tradePrice) == 0)
          .findFirst().orElse(null);
      if (existInv == null) {
        //沒有相同日期/標的的庫存 => 建倉
        tdDto = tdDto.newInstance();
        tdDto.setTradeAmt(tradeAmt);
        tdDto.setTradeUnit(new BigDecimal(tradeUnits));
        profitDTO.getInventoryList().add(tdDto);
      } else {
        //相同日期/標的的庫存+1
        existInv.setTradeAmt(existInv.getTradeAmt().add(tradeAmt));
        existInv.setTradeUnit(existInv.getTradeUnit().add(new BigDecimal(tradeUnits)));
      }
    }

    //純沖買的在做一次交易，為了區分平/建倉資料
    if (pureShortBuyUnits > 0) {
      this.trade(sid, tradeDate, pureShortBuyUnits, TradeType.shortBuying, tradePrice, profitDTO);
    }
    return Const._TRADE_RESULT_CODE_SUCCESS;
  }


  //  private int shortBuy(String tradeId, int sid, Date tradeDate, ProfitDTO profitDTO, int tradeUnits, BigDecimal tradePrice, Date settleDate) {
  private int sell(String tradeId, int sid, Date tradeDate, ProfitDTO profitDTO, int tradeUnits,
      BigDecimal tradePrice, Date settleDate) {
    //可沖賣存貨
    int shortSellableInvCount = ps.getShortSellableCount(profitDTO, sid, tradeDate).intValue();
    //可普賣存貨
    List<TradeDetailDTO> sellableInvList = ps.getSellableTdList(sid, tradeDate, profitDTO);
    BigDecimal sellableInvCount = BigDecimalUtil.sum(
        sellableInvList.stream().map(TradeDetailDTO::getTradeUnit).collect(
            Collectors.toList()));
    if (tradeUnits > (shortSellableInvCount + sellableInvCount.intValue())) {
      return Const._TRADE_RESULT_CODE_INVENTORY_NOT_ENOUGH;
    }

    //有可沖賣交易直接轉為沖賣交易
    if (shortSellableInvCount > 0L) {
      WLog.debug("change buy to short sell, shortBuyableCount[" + shortSellableInvCount + "]");
      int shortSellCount = Math.min(shortSellableInvCount, tradeUnits);
      this.trade(sid, tradeDate, shortSellCount,
          TradeType.shortSelling, tradePrice, profitDTO);
      //沖賣存貨不足抵銷普買，則繼續進行普賣作業
      if (shortSellCount == tradeUnits) {
        return Const._TRADE_RESULT_CODE_SUCCESS;
      }
    }
    // 撈出存貨，避免超賣
    tradeUnits -= shortSellableInvCount;

    if (tradeUnits == 0) {
      return Const._TRADE_RESULT_CODE_SUCCESS;
    }

    // 建立交易紀錄
    BigDecimal tradeAmt = getTradeAmt(tradeUnits, tradePrice, TradeType.sell);
    TradeDetailDTO tdDto = new TradeDetailDTO();
    tdDto.setTradeId(tradeId);
    tdDto.setTradeType(TradeType.sell);
    tdDto.setSid(sid);
    tdDto.setTradeDate(tradeDate);
    tdDto.setTradeUnit(new BigDecimal(tradeUnits));
    tdDto.setTradePrice(tradePrice);
    tdDto.setSettleDate(settleDate);
    tdDto.setTradeAmt(tradeAmt);
    profitDTO.getTradeDetailList().add(tdDto);
    //調整存貨
    BigDecimal tmpCost, cost = new BigDecimal(0);
    for (TradeDetailDTO inv : sellableInvList) {
      if (tradeUnits > 0) {
        if (inv.getTradeUnit().intValue() > tradeUnits) {
          tmpCost = inv.getTradeAmt().multiply(new BigDecimal(tradeUnits))
              .divide(inv.getTradeUnit(), 0, RoundingMode.CEILING);
          inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
          inv.setTradeUnit(inv.getTradeUnit().subtract(new BigDecimal(tradeUnits)));
          cost = cost.add(tmpCost);
          tradeUnits = 0;
        } else {
          tradeUnits -= inv.getTradeUnit().intValue();
          inv.setTradeUnit(Const._BIGDECIMAL_0);
          cost = cost.add(inv.getTradeAmt());
        }
        tdDto.addParentTradeId(inv.getTradeId());
      }
    }
    tdDto.setGrossProfit(tdDto.getTradeAmt().add(cost));
    tdDto.setCost(cost);
    return Const._TRADE_RESULT_CODE_SUCCESS;
  }

  private int shortSell(String tradeId, int sid, Date tradeDate, ProfitDTO profitDTO,
      int tradeUnits, BigDecimal tradePrice, Date settleDate) {
    //可平倉invList (沖買/當日普買)
    List<TradeDetailDTO> closeableInvList = profitDTO
        .getInventoryList().stream().filter(iv -> iv.getSid().compareTo(sid) == 0
            && iv.getTradeDate().equals(tradeDate) && (iv.getTradeType()
            .equals(TradeType.shortBuying) || iv.getTradeType()
            .equals(TradeType.buy)))
        .sorted(_COMPARATOR_TRADEDETAILPO_TRADEPRICE).collect(Collectors.toList());
    int closeableInvCount = BigDecimalUtil.sum(
            closeableInvList.stream().map(TradeDetailDTO::getTradeUnit).collect(Collectors.toList()))
        .intValue();
    int pureShortSellCount = 0;
    if (closeableInvCount > 0) {
      pureShortSellCount = Math.max(0, tradeUnits - closeableInvCount);
      tradeUnits = Math.min(tradeUnits, closeableInvCount);
    }

    if (tradeUnits == 0) {
      return Const._TRADE_RESULT_CODE_SUCCESS;
    }

    // 填入買交易資訊
    BigDecimal tradeAmt = getTradeAmt(tradeUnits, tradePrice, TradeType.shortSelling);
    TradeDetailDTO tdDto = new TradeDetailDTO();
    tdDto.setTradeId(tradeId);
    tdDto.setTradeType(TradeType.shortSelling);
    tdDto.setSid(sid);
    tdDto.setTradeDate(tradeDate);
    tdDto.setTradeUnit(new BigDecimal(tradeUnits));
    tdDto.setTradePrice(tradePrice);
    tdDto.setSettleDate(settleDate);
    tdDto.setTradeAmt(tradeAmt);
    profitDTO.getTradeDetailList().add(tdDto);
    // 調整存貨
    //有當日沖買庫存
    if (CollectionUtils.isNotEmpty(closeableInvList)) {
      int remainUnit;
      BigDecimal tmpCost, cost = new BigDecimal(0);
      for (TradeDetailDTO inv : closeableInvList) {
        //可平倉庫存>0 && 待交易數量>0
        if (closeableInvCount > 0 && tradeUnits > 0) {
          remainUnit = Math.max(0, inv.getTradeUnit().intValue() - tradeUnits);
          if (remainUnit > 0) {
            //未完銷才需計算金額，否則後面會被remove
            tmpCost = inv.getTradeAmt().multiply(new BigDecimal(tradeUnits))
                .divide(inv.getTradeUnit(), 0, RoundingMode.CEILING);
            inv.setTradeAmt(inv.getTradeAmt().subtract(tmpCost));
            cost = cost.add(tmpCost);
            tradeAmt = tradeAmt.add(tmpCost);
          } else {
            cost = cost.add(inv.getTradeAmt());
            tradeAmt = tradeAmt.add(inv.getTradeAmt());
          }
          //剩餘庫存(沖賣庫存扣除本次消除庫存)
          closeableInvCount -= inv.getTradeUnit().intValue();
          //剩餘待交易數
          tradeUnits -= inv.getTradeUnit().intValue();
          inv.setTradeUnit(new BigDecimal(remainUnit));
          tdDto.addParentTradeId(inv.getTradeId());
        }
      }
      if (cost.compareTo(Const._BIGDECIMAL_0) != 0) {
        tdDto.setCost(cost);
        tdDto.setGrossProfit(tdDto.getTradeAmt().add(cost));
      }
    }
    if (tradeUnits > 0) {
      TradeDetailDTO existShortSellInv = profitDTO.getInventoryList().stream()
          .filter(iv -> iv.getSid().compareTo(sid) == 0
              && iv.getTradeDate().compareTo(tradeDate) == 0
              && iv.getTradeType().equals(TradeType.shortSelling)
              && iv.getTradePrice().compareTo(tradePrice) == 0)
          .findFirst().orElse(null);
      if (existShortSellInv == null) {
        tdDto = tdDto.newInstance();
        tdDto.setTradeAmt(tradeAmt);
        tdDto.setTradeUnit(new BigDecimal(tradeUnits));
        profitDTO.getInventoryList().add(tdDto);
      } else {
        existShortSellInv.setTradeAmt(existShortSellInv.getTradeAmt().add(tradeAmt));
        existShortSellInv.setTradeUnit(
            existShortSellInv.getTradeUnit().add(new BigDecimal(tradeUnits)));
      }
    }
    if (pureShortSellCount > 0) {
      this.trade(sid, tradeDate, pureShortSellCount, TradeType.shortSelling, tradePrice, profitDTO);
    }
    return Const._TRADE_RESULT_CODE_SUCCESS;
  }

  /**
   * 交易類型1: 買入 2:補券 3:融資 4: 賣出 5:融券 6: 融資賣
   *
   * @param sid sid
   * @param tradeDate tradeDate
   * @param tradeUnits tradeUnits
   * @param tradeType tradeType
   * @param profitDTO profitDTO
   */
  public int trade(Integer sid, Date tradeDate, int tradeUnits, TradeType tradeType,
      BigDecimal tradePrice,
      ProfitDTO profitDTO) {
    int tradeStatus;
    String tradeId = UUID.randomUUID().toString();
    Date settleDate = getSettleDate(tradeDate);
    if (tradePrice == null) {
      throw new NullPointerException(
          "sid[" + sid + "]tradeDate[" + tradeDate + "]TradeType[" + tradeType
              + "] can't find price");
    }
    WLog.debug("start trade: sid[" + sid + "]tradeDate[" + tradeDate + "]tradeType[" + tradeType
        + "]units["
        + tradeUnits + "]");
    switch (tradeType) {
      case buy:
        tradeStatus = this.buy(tradeId, sid, tradeDate, profitDTO, tradeUnits, tradePrice,
            settleDate);
        break;
//			case 2://融資
      case shortBuying:// 補券
        tradeStatus = this.shortBuy(tradeId, sid, tradeDate, profitDTO, tradeUnits, tradePrice,
            settleDate);
        break;
      case sell:
        tradeStatus = this.sell(tradeId, sid, tradeDate, profitDTO, tradeUnits, tradePrice,
            settleDate);
        break;
//			case 5://融資賣
      case shortSelling:// 融券賣
        tradeStatus = this.shortSell(tradeId, sid, tradeDate, profitDTO, tradeUnits, tradePrice,
            settleDate);
        break;
      default:
        return Const._TRADE_RESULT_CODE_TRADE_TYPE_NOT_ALLOW;
    }

    //篩選掉unit == 0 的存貨
    List<TradeDetailDTO> newTdList = new ArrayList<>();
    profitDTO.getInventoryList().stream()
        .filter(td -> td.getTradeUnit().compareTo(Const._BIGDECIMAL_0) > 0)
        .forEach(td -> newTdList.add(td.newInstance()));
    profitDTO.setInventoryList(newTdList);
    return tradeStatus;
  }

  public BigDecimal getPrice(Integer sid, PriceType priceType, Date tradeDate) {
    List<TimeSeriesDTO<BigDecimal>> tdList = dtDao.findTsList(sid, priceType.getKey(), tradeDate,
        tradeDate);
    return (tdList == null || tdList.size() == 0) ? null : tdList.get(0).getDecimalValue();
  }

  public BigDecimal getPrice(Integer sid, Date tradeDate) {
    return getPrice(sid, PriceType.CLOSE, tradeDate);
  }

  /**
   * 取得建倉交易金額
   *
   * @param units units
   * @param unitPrice unitPrice
   * @param tradeType 交易類型 1: 買入 2: 融資 3: 補券 4: 賣出 5:融券 6: 融資賣
   * @return TradeAmt
   */
  public BigDecimal getTradeAmt(int units, BigDecimal unitPrice, TradeType tradeType) {
    if (unitPrice != null) {
      BigDecimal handlingFee = getHandlingFee(units, unitPrice);
      if (units == 0) {
        return new BigDecimal(0);
      }
      switch (tradeType) {
        // 現貨買入
        case buy:
          // 買入金額 = 買入單位*單價*1000(股)+手續費
          return new BigDecimal(units).multiply(unitPrice).add(handlingFee)
              .setScale(0, RoundingMode.HALF_UP);
        // 融券賣 (交割金額為價金9成)，百元以下無條件進位
        case shortSelling:
          return new BigDecimal(units).multiply(unitPrice)
              .multiply(Const._BIGDECIMAL_1.subtract(txnTaxRatio.divide(new BigDecimal(2))))
              .subtract(handlingFee).setScale(0, RoundingMode.CEILING);
        case sell:
          // 賣出金額 = 賣出單位*單價*1000(股)-交易稅-手續費
          return new BigDecimal(units).multiply(unitPrice)
              .multiply(Const._BIGDECIMAL_1.subtract(txnTaxRatio))
              .setScale(0, RoundingMode.CEILING).subtract(handlingFee);
        // 融券賣 (交割金額為價金9成)
        case shortBuying:
          // 券買金額 = 單位*金額+手續費
          return new BigDecimal(units).multiply(unitPrice).add(handlingFee);
        default:
          WLog.error("尚未開啟此交易，tradeType[" + tradeType + "]");
          return null;
      }
    } else {
      WLog.error("tradeType[" + tradeType + "];price[" + unitPrice + "] 交易單位貨單價為null");
      return null;
    }
  }

//	/**
//	 * 計算融券回補，須調整融券賣出價金
//	 * 
//	 * @param tdPo
//	 * @return
//	 */
//	public BigDecimal getShortBuyingAdjSellAmt(List<TradeDetailPO> stTdPoList) {
//		BigDecimal origAmt = stTdPoList.get(0).getSellPrice().multiply(new BigDecimal(stTdPoList.size()));
//		BigDecimal handlingFee = getHandlingFee(stTdPoList.size(), stTdPoList.get(0).getSellPrice());
//		BigDecimal txnFee = txnTaxRatio.multiply(origAmt).setScale(0, RoundingMode.HALF_UP);
//		BigDecimal shortingSellFee = shortingSellFeeRatio.multiply(origAmt);
//		WLog.debug("origAmt[" + origAmt + "]handlingFee[" + handlingFee + "]txnFee[" + txnFee + "]shortingSellFee["
//				+ shortingSellFee + "]");
//		// 券商利息不計
//		return origAmt.subtract(handlingFee).subtract(txnFee).subtract(shortingSellFee);
//	}

  /**
   * 券商手續費 單位數*單價*1000(股)*券商手續費率*券商手續費折扣 (<20以20元計)
   *
   * @param units units
   * @param unitPrice unitPrice
   * @return HandlingFee
   */
  public BigDecimal getHandlingFee(int units, BigDecimal unitPrice) {
    return getHandlingFee(units, unitPrice, defaultMinHandlingFee);
  }

  public BigDecimal getHandlingFee(int units, BigDecimal unitPrice, BigDecimal minHandlingFee) {
    BigDecimal handlingFee;
    handlingFee = new BigDecimal(units)
        .multiply(unitPrice.multiply(handlingFeeRatio.multiply(defaultHandlingFeeDiscount)));
    handlingFee =
        (handlingFee.compareTo(minHandlingFee) > 0) ? handlingFee.setScale(0, RoundingMode.FLOOR)
            : minHandlingFee;
    return handlingFee;
  }

  /**
   * 包含未實現損益的帳戶餘額
   */
//	public BigDecimal getExpectBalance(Date calBaseDate, ProfitPO profitPO) {
//		// 暫存tmpTdList，稍後恢復用
//		List<TradeDetailPO> tmpTdList = profitPO.clone().getTradeDetailList();
//
//		// 把符合日期區間，但未完成交易的也填上交易金額，使完成未實現交易
//		for (Integer sid : profitPO.getSidSet()) {
//			// 賣掉買了沒賣的
//			Long unSellCount = profitPO.getTradeDetailList().stream()
//					.filter(td -> !td.isTradeComplete() && sid == td.getSid() && td.getSellDate() == null
//							&& WDate.beforeOrEquals(td.getBuyDate(), calBaseDate))
//					.count();
//			this.trade(sid, calBaseDate, unSellCount.intValue(), TradeType.sell, profitPO);
//		}
//
//		// 取出帳戶餘額
//		BigDecimal expectBalance = profitPO.getAcctBalance(calBaseDate);
//
//		// 恢復tdList
//		profitPO.setTradeDetailList(tmpTdList);
//
//		return expectBalance;
//	}
//
//	public BigDecimal getMaxExpectLoss(Date calBaseDate, ProfitPO profitPO) {
//		// 取得所有交易日
//		Set<Date> dateSet = new HashSet<>();
//		profitPO.getTradeDetailList().stream().forEach(td -> {
//			dateSet.add(td.getBuyDate());
//			dateSet.add(td.getSellDate());
//		});
//
//		BigDecimal minAcctBalance = null;
//		for (Date date : dateSet) {
//			if (minAcctBalance == null) {
//				minAcctBalance = this.getExpectBalance(date, profitPO);
//			} else {
//				minAcctBalance = this.getExpectBalance(date, profitPO).min(minAcctBalance);
//			}
//		}
//		return minAcctBalance == null ? new BigDecimal(-1)
//				: (minAcctBalance.compareTo(profitPO.getOriginalFunds()) > 0 ? new BigDecimal(0)
//						: profitPO.getOriginalFunds().subtract(minAcctBalance));
//	}
//
//	public BigDecimal getAlfaRatio(Date calBaseDate, ProfitPO sidProfitPo) throws ParseException {
//		ProfitPO marketProfitPo = getMarketBackTest(sidProfitPo);
//		return sidProfitPo.getGrossProfit(calBaseDate)
//				.divide(marketProfitPo.getGrossProfit(calBaseDate), 4, RoundingMode.HALF_UP)
//				.subtract(new BigDecimal(1));
//	}
//
//	public ProfitPO getMarketBackTest(ProfitPO ppo) throws ParseException {
//		ProfitPO profitPO = ppo.clone();
//		// 撈出加權指數收盤
//		List<TimeSeriesDTO<BigDecimal>> marketWeightedIndex = dtDao.findTdsList(Const._CFG_ID_MARKET_WEIGHTED_INDEX,
//				Const._FIELDID_MARKET_WEIGHTED_INDEX_CLOSE);
//
//		// 統計個交易日買賣張數
//		Set<Date> tradeDateSet = new HashSet<>();
//		profitPO.getTradeDetailList().stream().forEach(td -> {
//			tradeDateSet.add(td.getBuyDate());
//			tradeDateSet.add(td.getSellDate());
//		});
//
//		for (Date tradeDate : tradeDateSet) {
//			// 取得當天指數
//			TimeSeriesDTO<BigDecimal> ts = marketWeightedIndex.stream()
//					.filter(mwIndex -> mwIndex.getDate().equals(tradeDate)).findFirst().orElse(null);
//
//			// 取代當天買價
//			profitPO.getTradeDetailList().stream()
//					.filter(td -> td.getBuyDate() != null && td.getBuyDate().equals(tradeDate)).forEach(td -> {
//						td.setBuyPrice(ts.getDecimalValue());
//					});
//
//			// 取代當天賣價
//			profitPO.getTradeDetailList().stream()
//					.filter(td -> td.getSellDate() != null && td.getSellDate().equals(tradeDate)).forEach(td -> {
//						td.setSellPrice(ts.getDecimalValue());
//					});
//		}
//		return profitPO;
//	}
//	private void setMaxAndMinProfitAndLossToTdList(ProfitPO profitPo) {
//		List<TradeDetailPO> tmpTradeDetailList = profitPo.cloneTradeDetailList();
//		// 取出SID
//		Set<Integer> sidSet = new HashSet<>();
//		tmpTradeDetailList.stream().mapToInt(td -> td.getSid()).distinct().forEach(sid -> sidSet.add(sid));
//
//		//
//		for (Integer sid : sidSet) {
//			List<TradeDetailPO> sidTdList = tmpTradeDetailList.stream().filter(td -> td.getSid().compareTo(sid) == 0 && td.isTradeComplete())
//					.collect(Collectors.toList());
//			for (TradeDetailPO td : sidTdList) {
//				Date minTradeDate = null, maxTradeDate = null;
//				//取得買賣區間
//				minTradeDate = new Date(Math.min(td.getBuyDate().getTime(), td.getSellDate().getTime()));
//				maxTradeDate = new Date(Math.max(td.getBuyDate().getTime(), td.getSellDate().getTime()));
//
//				// 取得最高級最低價
//				BigDecimal lowestPrice = null, highestPrice = null;
//				List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(sid, minTradeDate,
//						maxTradeDate);
//				for (PriceDailyTwPO price : priceList) {
//					if (lowestPrice == null || lowestPrice.compareTo(price.getLow()) > 0) {
//						lowestPrice = price.getLow();
//					}
//					if (highestPrice == null || highestPrice.compareTo(price.getHigh()) < 0) {
//						highestPrice = price.getHigh();
//					}
//				}
//				
//				List<TradeDetailPO> singleTradeTdList = sidTdList.stream()
//						.filter(td1 -> td1.getTradeId().equals(td.getTradeId())).collect(Collectors.toList());
//				int tradeCount = singleTradeTdList.size();
//				WLog.info("SID["+sid+"]highestPrice["+highestPrice+"]lowestPrice["+lowestPrice+"]td["+WString.toString(td)+"]");
//				// 先買後賣
//				BigDecimal tradeAmt;
//				if (td.isTradeComplete()
//						&& (td.getBuyDate().before(td.getSellDate()) 
//								|| (td.getBuyDate().equals(td.getSellDate()) && !td.getBuyType().equals(TradeType.shortBuying)))) {
//					//最大獲利
//					tradeAmt = getTradeAmtForEachUnit(tradeCount, highestPrice, td.getSellType());
//					for(TradeDetailPO singleTradeTd : singleTradeTdList) {
//						singleTradeTd.setSellAmt(tradeAmt);
//						singleTradeTd.setSellPrice(highestPrice);
//						singleTradeTd.setMaxProfit(singleTradeTd.getGrossProfit());
//						singleTradeTd.setMaxProfitRatio(singleTradeTd.getGrossProfitRatio());
//					}
//
//					//最大虧損
//					tradeAmt = getTradeAmtForEachUnit(tradeCount, lowestPrice, td.getSellType());
//					for(TradeDetailPO singleTradeTd : singleTradeTdList) {
//						singleTradeTd.setSellAmt(tradeAmt);
//						singleTradeTd.setSellPrice(lowestPrice);
//						singleTradeTd.setMaxLoss(singleTradeTd.getGrossProfit());
//						singleTradeTd.setMaxLossRatio(singleTradeTd.getGrossProfitRatio());
//					}
//				}
//
//				// 先賣後買
//				if (td.isTradeComplete() && td.getBuyDate().after(td.getSellDate()) 
//						|| (td.getBuyDate().equals(td.getSellDate()) && td.getBuyType().equals(TradeType.shortBuying))) {
//					//最大獲利
//					tradeAmt = getTradeAmtForEachUnit(tradeCount, highestPrice, td.getBuyType());
//					for(TradeDetailPO singleTradeTd : singleTradeTdList) {
//						singleTradeTd.setBuyAmt(tradeAmt);
//						singleTradeTd.setBuyPrice(highestPrice);
//						singleTradeTd.setMaxLoss(singleTradeTd.getGrossProfit());
//						singleTradeTd.setMaxLossRatio(singleTradeTd.getGrossProfitRatio());
//					}
//
//					//最大虧損
//					tradeAmt = getTradeAmtForEachUnit(tradeCount, lowestPrice, td.getBuyType());
//					for(TradeDetailPO singleTradeTd : singleTradeTdList) {
//						singleTradeTd.setBuyAmt(tradeAmt);
//						singleTradeTd.setBuyPrice(lowestPrice);
//						singleTradeTd.setMaxProfit(singleTradeTd.getGrossProfit());
//						singleTradeTd.setMaxProfitRatio(singleTradeTd.getGrossProfitRatio());
//					}
//				}
//			}
//
//		}
//		
//		//歸值
//		List<TradeDetailPO> tradeDetailList = profitPo.getTradeDetailList();
//		for (int tdIndex = 0; tdIndex < tmpTradeDetailList.size(); tdIndex++) {
//			tradeDetailList.get(tdIndex).setMaxProfit(tmpTradeDetailList.get(tdIndex).getMaxProfit());
//			tradeDetailList.get(tdIndex).setMaxProfitRatio(tmpTradeDetailList.get(tdIndex).getMaxProfitRatio());
//			tradeDetailList.get(tdIndex).setMaxLoss(tmpTradeDetailList.get(tdIndex).getMaxLoss());
//			tradeDetailList.get(tdIndex).setMaxLossRatio(tmpTradeDetailList.get(tdIndex).getMaxLossRatio());
//		}
//	}
  public BigDecimal getDailyLimitPrice(BigDecimal price, boolean up) {
    BigDecimal limitPrice;
    BigDecimal tick = getTickPrice(price);
    BigDecimal limitPercent = up ? BigDecimal.valueOf(1.1) : BigDecimal.valueOf(0.9);
    if (up) {
      // 漲停價
      limitPrice = price.multiply(limitPercent).setScale(2, RoundingMode.FLOOR);
      while (price.compareTo(limitPrice) > 0) {
        price = price.add(tick);
      }
      price = price.subtract(tick);
    } else {
      // 跌停價
      limitPrice = price.multiply(limitPercent).setScale(2, RoundingMode.CEILING);
      while (price.compareTo(limitPrice) < 0) {
        price = price.subtract(tick);
      }
      price = price.add(tick);
    }
    return price;
  }

  public BigDecimal getTickPrice(BigDecimal price) {
    if (price.compareTo(new BigDecimal(10)) <= 0) {
      return BigDecimal.valueOf(0.01);
    } else if (price.compareTo(new BigDecimal(50)) <= 0) {
      return BigDecimal.valueOf(0.05);
    } else if (price.compareTo(new BigDecimal(100)) <= 0) {
      return BigDecimal.valueOf(0.1);
    } else if (price.compareTo(new BigDecimal(500)) <= 0) {
      return BigDecimal.valueOf(0.5);
    } else if (price.compareTo(new BigDecimal(1000)) <= 0) {
      return BigDecimal.valueOf(1.0);
    } else {
      return BigDecimal.valueOf(5.0);
    }
  }

  public Date getSettleDate(Date txnDate) {
    List<CalendarPO> caList = calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(
        txnDate, Const._CFG_ID_CALENDAR_STOCK_BUS_DATE);
    if (caList.size() == 2 && caList.get(1) != null) {
      return caList.get(1).getDate();
    }
    throw new NullPointerException("get SettleDate by txnDate[" + txnDate + "] fail");
  }
}