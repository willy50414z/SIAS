package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

public class CompanyInfoPOKey implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "\"DATA_DATE\"")
	private Date dataDate;
	@Column(name = "\"SID\"")
	private Integer sid;
}
