package com.willy.sias.batch.processor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.naming.directory.NoSuchAttributeException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.net.web.WJsoup;
import com.willy.sias.db.dao.ConfigDao;
import com.willy.sias.db.po.BatchLogPO;
import com.willy.sias.db.po.FRISTWPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.FRISTWRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

import lombok.Setter;

@Setter
@Service
@ConfigurationProperties("sias.batch.fr-is-tw")
public class FR_IS_TW extends BaseBatchProcessor {
	@Autowired
	private WJsoup jsoup;
	private String qryUrlTempl;
	private String urlTempl;
	private String csvFileNameTempl;
	@Autowired
	private WFile_txt txtFile;
	@Autowired
	private SysConfigRepository sysConfigRepo;
	@Autowired
	private FRISTWRepository frIsRepo;
	@Autowired
	private ConfigDao sysConfigDao;
	private int stockCodeColIndex;// 報表格式不同，以公司代號作為欄位判定標準
	private final String frType = "IS";
	private String year;
	private String season;
	private int ceYear;
	@Autowired
	private WJDBC jdbc;
	
	@Override
	protected void exec() throws Exception {
		Set<FRISTWPO> frIsPoSet = new HashSet<> ();
		
		year = "" + WDate.convertCEYear(WDate.getDateInfo(dataDate, Calendar.YEAR));
		season = "0" + WDate.getQuarter(dataDate);
		ceYear = WDate.getDateInfo(dataDate, Calendar.YEAR);
		
		this.putMapParam("qryYear", year);
		this.putMapParam("qrySeason", season);
		
		// 設定讀取csv相關參數
		txtFile.setEncoding(Const._ENCODING_MS950);
		txtFile.setSeparator(",");

		// 取得市場別
		List<SysConfigPO> marketTypeList = sysConfigRepo.findByParentIdIsOrderByCfgId(ParentKey.MarketType.getKey());

		// 一個個市場別去撈
		for (SysConfigPO marketType : marketTypeList) {
			WLog.info("開始匯入財報marketType[" + marketType.getCfgDesc()+"]");
			int inputNum = 1;
			this.putMapParam("marketType", marketType.getCfgValue());
			// 設定下載csv相關參數
			Elements inputs = this.getInputBtns(marketType.getCfgValue(), year, season);
			for (Element input : inputs) {
				if (input.attr("name").equals("filename")) {
					// 篩選掉重複出現檔案
					if (this.getMapParam("csvTmpName") == null || (this.getMapParam("csvTmpName") != null
							&& !this.getMapParam("csvTmpName").toString().equals(input.val()))) {
						this.putMapParam("inputNum", String.valueOf(inputNum));
						// 下載財報資料為csv
						WLog.debug("下載財報資料");
						this.putMapParam("csvTmpName", input.val());
						String csvFilePath = FileUtil.download(this.parseMapParamToString(urlTempl),
								this.parseMapParamToString(csvFileNameTempl));
						// 讀取
						WLog.debug("讀取財報資料");
						String[][] finItemAr = txtFile.readToAr(csvFilePath);

						// 取得公司代號所在欄位
						stockCodeColIndex = -1;
						getStockCodeColIndex(csvFilePath, finItemAr[0]);

						// 將Title匯入對照表
						WLog.debug("匯入Title對照表");
						this.mergeFRItemName(finItemAr);

						// 將公司匯入對照表
						WLog.debug("匯入SID對照表");
						this.mergeSid(finItemAr, 3, 4);

						// 暫存財報資料(有重複)
						frIsPoSet.addAll(this.compose(finItemAr));
						inputNum++;
					}

				}
			}
		}
		WLog.info("匯入財報資料");
//		frIsRepo.saveAll(frIsPoSet);
		WLog.info("before remove duplicate fr size: " + frIsPoSet.size());
		frIsRepo.findByFrDate(ceYear+season).forEach(fr -> frIsPoSet.remove(fr));
		WLog.info("after remove duplicate fr size: " + frIsPoSet.size());
		jdbc.batchInsert(frIsPoSet);
		
	}

	private Elements getInputBtns(String marketType, String year, String season)
			throws IOException, NoSuchAttributeException, InterruptedException, ExecutionException {
		this.jsoup.putFormData("encodeURIComponent", "1");
		this.jsoup.putFormData("step", "1");
		this.jsoup.putFormData("firstin", "1");
		this.jsoup.putFormData("off", "1");
		this.jsoup.putFormData("isQuery", "Y");
		this.jsoup.putFormData("TYPEK", marketType);
		this.jsoup.putFormData("year", year);
		this.jsoup.putFormData("season", season);
		jsoup.setTimeoutMillionSeconds(120000);
		Document doc = jsoup.getBody(qryUrlTempl);
		return doc.getElementsByTag("input");
	}

	private void getStockCodeColIndex(String csvFilePath, String[] firstRowAr) {
		for (int colIndex = 0; colIndex < firstRowAr.length; colIndex++) {
			if (firstRowAr[colIndex].trim().equals("公司代號")) {
				stockCodeColIndex = colIndex;
				break;
			}
		}
		if (stockCodeColIndex == -1) {
			throw new NotFoundException("未擷取到公司代號所在欄位, csvFilePath["+csvFilePath+"] DATA[" + WString.toString(firstRowAr) + "]");
		}
	}

	private void mergeFRItemName(String[][] frItemAr) {
		SysConfigPO sysConfig;
		HashMap<String, Integer> fieldDescMap = sysConfigDao.findFielddescAndKeyMapByParentkey(ParentKey.ISFRKey.getKey());
		int maxFieldValueIndex = sysConfigDao.findMaxFieldValueIndex(ParentKey.ISFRKey.getKey(), this.frType);
		Set<SysConfigPO> sysConfigList = new HashSet<SysConfigPO>();
		for (int colIndex = (stockCodeColIndex + 2); colIndex < frItemAr[0].length; colIndex++) {
			if (!fieldDescMap.containsKey(frItemAr[0][colIndex])) {
				sysConfig = new SysConfigPO();
				maxFieldValueIndex++;
				sysConfig.setParentId(ParentKey.ISFRKey.getKey());
				sysConfig.setCfgValue(this.frType + WString.fillStrLength(String.valueOf(maxFieldValueIndex), "0", 6));
				sysConfig.setCfgDesc(frItemAr[0][colIndex]);
				sysConfigList.add(sysConfig);
			}
		}
		sysConfigRepo.saveAll(sysConfigList);
	}

	private Set<FRISTWPO> compose(String[][] frItemAr) {
		FRISTWPO frItem;
		String cleanedValue;
		HashMap<String, Integer> sidMap;
		HashMap<String, Integer> finItemMap;
		Set<FRISTWPO> frItemSet = new HashSet<> ();
		try {
			sidMap = sysConfigDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
			finItemMap = sysConfigDao.findFielddescAndKeyMapByParentkey(EnumSet.getFRKey(frType));
			frItemSet = new HashSet<> ();
			// 遍覽公司
			for (int rowIndex = 1; rowIndex < frItemAr.length; rowIndex++) {
				// 遍覽會計科目
				for (int colIndex = (stockCodeColIndex + 2); colIndex < frItemAr[0].length; colIndex++) {
					// 排除無金額會計科目
					if (!frItemAr[rowIndex][colIndex].isEmpty()
							&& StringUtil.cleanDigitData(frItemAr[rowIndex][colIndex]).matches("[0-9]+[.]?[0-9]+")) {
						frItem = new FRISTWPO();
						cleanedValue = StringUtil.cleanDigitData(frItemAr[rowIndex][colIndex]);
						frItem.setSid(sidMap.get(frItemAr[rowIndex][stockCodeColIndex].replace("\"", "")));
						frItem.setFrDate(ceYear+season);
						frItem.setFrKey(finItemMap.get(frItemAr[0][colIndex].replace("\"", "")));
						frItem.setFrValue(new BigDecimal(cleanedValue.equals("") ? "0.00" : cleanedValue));
//						frItem.setFrReleaseDate(null);
						frItemSet.add(frItem);
					}
				}
			}
			return frItemSet;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}