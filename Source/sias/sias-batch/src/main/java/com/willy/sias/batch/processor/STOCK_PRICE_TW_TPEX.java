package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.stockprice-tw-tpex")
public class STOCK_PRICE_TW_TPEX extends BaseBatchProcessor {
	@Autowired
	private SysConfigRepository scRepo;
	@Autowired
	private PriceDailyTwRepository spRepo;
	@Autowired
	private WJDBC jdbc;
	public void exec() throws Exception {
		String[][] spAr = null;
		putMapParam("strDataDate_Slash", WDate.convertCEYear(WDate.getDateInfo(dataDate, Calendar.YEAR))+"/"+WType.dateToStr(dataDate, "MM/dd"));
		List<SysConfigPO> scList = scRepo.findByParentIdIsOrderByCfgId(Const._CFG_ID_TPEX_CATEGORY);
		
		for(SysConfigPO sc : scList) {
			WLog.info("sc["+sc.getCfgValue()+"]");
			//下載資料檔
			putMapParam("dataType", sc.getCfgValue());
			
			filePath = FileUtil.download(parseMapParamToString(urlTempl), parseMapParamToString(csvFileNameTempl));
			
			//讀取資料檔
			spAr = txtFile.readToAr(filePath, "代號", "筆", true);
			
			// 非交易日
			if (spAr.length == 0) {
				if(sc.getCfgValue().equals("02")) {
					WLog.info(this.dataDate + "當天非交易日");
					return;
				} else {
					continue;
				}
			}

			// MergeSID
			this.mergeSid(spAr);
			sidMap = cfgDao.findFieldvalueAndKeyMapByParentkey(ParentKey.SID.getKey());
			
			jdbc.batchInsert(this.getCompanyCategorySet(spAr, sc.getCfgId()));

			spRepo.saveAll(this.getSpSet(spAr));
		}
	}

	private Set<PriceDailyTwPO> getSpSet(String[][] spAr) throws ParseException {
		PriceDailyTwPO sp;
		Set<PriceDailyTwPO> spSet = new HashSet<PriceDailyTwPO>();
		for (String[] spRow : spAr) {
			WLog.debug(WString.toString(spRow));
			sp = new PriceDailyTwPO();
			sp.setSid(sidMap.get(getStrFromArray(spRow, 0)));
			sp.setDataDate(dataDate);
			sp.setOpen(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 4))));
			sp.setHigh(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 5))));
			sp.setLow(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 6))));
			sp.setClose(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 2))));
			
			//成交股數
			String tradeSharedDigit = StringUtil.cleanDigitData(getStrFromArray(spRow, 7));
			sp.setTradingShares(Long.valueOf(tradeSharedDigit.contains(".") ? "-1" : tradeSharedDigit));
			
			//成交筆數
			String tradeNumDigit = StringUtil.cleanDigitData(getStrFromArray(spRow, 9));
			sp.setTradingNum(Integer.valueOf(tradeNumDigit.contains(".") ? "-1" : tradeNumDigit));
			
			//成交金額
			String turnOverDigit = StringUtil.cleanDigitData(getStrFromArray(spRow, 8));
			sp.setTurnOver(Long.valueOf(turnOverDigit.contains(".") ? "-1" : turnOverDigit));
			sp.setLastBuyPrice(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 10))));
			sp.setLastSoldPrice(new BigDecimal(StringUtil.cleanDigitData(getStrFromArray(spRow, 11))));
			spSet.add(sp);
		}
		return spSet;
	}

	private Set<CompanyCategoryPO> getCompanyCategorySet(String[][] spAr, int categoryId) throws ParseException {
		// 塞值組出List
		CompanyCategoryPO cc;
		Set<CompanyCategoryPO> ccSet = new HashSet<CompanyCategoryPO>();
		for (String[] sp : spAr) {
			cc = new CompanyCategoryPO();
			cc.setSid(sidMap.get(sp[0]));
			cc.setDataDate(dataDate);
			cc.setCategoryId(Const._CFG_ID_MARKET_TYPE_TPEX);// 上櫃
			ccSet.add(cc);
			
			cc = new CompanyCategoryPO();
			cc.setSid(sidMap.get(sp[0]));
			cc.setDataDate(dataDate);
			cc.setCategoryId(categoryId);// 上櫃
			ccSet.add(cc);
		}
		if(ccSet.size()>0) {
			ccSet = filtCCList(ccSet);
		}
		return ccSet;
	}

	@Override
	public void cleanHisData() throws Exception {
		// 撈出上櫃類別的SID
		spRepo.deleteByDateAndCategoryId(dataDate, Const._CFG_ID_MARKET_TYPE_TPEX);// 清空當天的上櫃股價資料
	}
}
