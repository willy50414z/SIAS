package com.willy.sias.backtest.dto;

import java.math.BigDecimal;
import java.util.List;

import com.willy.sias.db.dto.TimeSeriesDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeasonDTO {
	private int sid;
	private List<TimeSeriesDTO<BigDecimal>> growth20;
	private List<TradeDetailDTO> bsInfoList;
}
