package com.willy.sias.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.MarketIndexTwPO;
import com.willy.sias.db.po.MarketIndexTwPOKey;

@Repository
public interface MarketIndexTwRepository extends JpaRepository<MarketIndexTwPO, MarketIndexTwPOKey> {

}
