package com.willy.sias.web.service;

import com.willy.r.constant.TaType;
import com.willy.r.dto.TaDataDTO;
import com.willy.sias.backtest.service.StatisticService;
import com.willy.sias.db.dto.OHLCDTO;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.willy.file.serialize.WSerializer;
import com.willy.r.dao.WRChart;
import com.willy.sias.backtest.dto.BackTestInfoDTO;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradeDetailDTO;
import com.willy.sias.backtest.processor.BackTestProcessor;
import com.willy.sias.backtest.service.ProfitDTOService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.OHLCVDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.BackTestLogDPO;
import com.willy.sias.db.po.BackTestLogHPO;
import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.BackTestLogDRepository;
import com.willy.sias.db.repository.BackTestLogHRepository;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.config.Const;
import com.willy.sias.web.vo.BackTestResultVO;
import com.willy.sias.web.vo.BackTestSummaryVO;
import com.willy.sias.web.vo.TradeDetailVO;
import com.willy.sias.web.vo.TradeLog_EachTrade;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

@Service
public class BackTestMainService {
	@Value("${sias.candleChart.dir}")
	private String candleChartDir;
	@Autowired
	private CompanyCategoryRepository ccRepo;
	@Autowired
	private SysConfigRepository configRepo;
	@Autowired
	private BackTestProcessor btProcessor;
	@Autowired
	private BackTestLogDRepository btLogDRepo;
	@Autowired
	private BackTestLogHRepository btLogHRepo;
	@Autowired
	private WSerializer serializer;
	@Autowired
	private WRChart wrChart;
	@Autowired
	private DataTableDao dtDao;
	@Autowired
	private ProfitDTOService ps;

	public Map<Integer, String> getSidAndCompanyDescMapByCompanyCategory(Integer categoryId) {
		List<CompanyCategoryPO> ccList = ccRepo.findByCategoryIdIs(categoryId);

		Set<Integer> sidList = new HashSet<>();
		for (CompanyCategoryPO cc : ccList) {
			sidList.add(cc.getSid());
		}

		List<SysConfigPO> configList = null;
		if (sidList.size() > 2000) {
			Set<Integer> tmpSidList = new HashSet<>();
			for (Integer sid : sidList) {
				tmpSidList.add(sid);
				if (tmpSidList.size() == 2000 && configList == null) {
					configList = configRepo.findByCfgIdInOrderByCfgId(tmpSidList);
					tmpSidList.clear();
				} else if (tmpSidList.size() == 2000) {
					configList.addAll(configRepo.findByCfgIdInOrderByCfgId(tmpSidList));
					tmpSidList.clear();
				}
			}
		} else {
			configList = configRepo.findByCfgIdInOrderByCfgId(sidList);
		}

		Map<Integer, String> sidAndCompanyDescMap = new LinkedHashMap<>();
		for (SysConfigPO config : configList) {
			sidAndCompanyDescMap.put(config.getCfgId(), config.getCfgValue() + " - " + config.getCfgDesc());
		}

		return sidAndCompanyDescMap;

	}

	public Integer backTest(BackTestInfoDTO btInfo) {
		return btProcessor.process(btInfo);
	}

	public BackTestResultVO getBackTestResultVO(Integer testId, String contextPath)
			throws IOException, JSONException, ClassNotFoundException {
		BackTestLogHPO btlH = btLogHRepo.findById(testId).orElse(null);
		if (btlH == null) {
			return null;
		}

		BackTestResultVO btResult = new BackTestResultVO();
		if (StringUtils.isNotEmpty(btlH.getErrMsg())) {
			btResult.setErrMsg(btlH.getErrMsg());
			return btResult;
		}

		List<BackTestLogDPO> btlDList = btLogDRepo.findByTestId(testId);
		if (btlDList == null) {
			return null;
		}

		ProfitDTO profitDTO;
		BigDecimal profitAmtRatio;
		BackTestSummaryVO backTestSummary;
		List<BackTestSummaryVO> backTestSummaryList = new ArrayList<>();
		NumberFormat percentFormatter = new DecimalFormat("###,###,###,###.##");
		for (BackTestLogDPO btlD : btlDList) {
			// 取出ProfitPO
			String tradeDetailStr = WString.unCompress(new String(btlD.getTradeDetail(), StandardCharsets.UTF_8));
			JSONObject tradeDetailJson = new JSONObject(tradeDetailStr);
			tradeDetailStr = tradeDetailJson.getString("ProfitPO");
			// 反序列化並解壓縮，取得ProfitPO物件
			profitDTO = (ProfitDTO) serializer.unCompressAndUnserializeByString(tradeDetailStr);
			backTestSummary = new BackTestSummaryVO();

			// 測試ID
			backTestSummary.setTestIdD(String.valueOf(btlD.getTestIdD()));

			// 交易標的
			StringBuffer tradeTarget = new StringBuffer();
			// 撈出3個
			Set<Integer> querySidSet = new HashSet<>();
			for (int i = 0; i < Math.min(profitDTO.getSidList().size(), 3); i++) {
				querySidSet.add(profitDTO.getSidList().get(i));
			}
			List<SysConfigPO> configList = configRepo.findAllById(querySidSet);
			configList.forEach(config -> tradeTarget.append(config.getCfgValue()).append(" - ")
					.append(config.getCfgDesc()).append("<br/>"));
			if (profitDTO.getSidList().size() > 3) {
				tradeTarget.append("...");
			}
			backTestSummary.setTarget(tradeTarget.toString());

			// 策略名稱
			tradeTarget.setLength(0);
			int charCountPerRow = 25;
			if (!StringUtils.isEmpty(profitDTO.getStrategyDesc())) {
				for (int i = 0; i < profitDTO.getStrategyDesc().length() / charCountPerRow + 1; i++) {
					tradeTarget
							.append(profitDTO.getStrategyDesc().substring(i * charCountPerRow,
									Math.min((i + 1) * charCountPerRow, profitDTO.getStrategyDesc().length())))
							.append("<br/>");
				}
				backTestSummary.setStrategyCode(tradeTarget.toString());
			}

			// 勝率
			if (profitDTO.getProfitRatio() != null) {
				backTestSummary
						.setProfitRatio(percentFormatter.format(profitDTO.getProfitRatio().doubleValue() * 100) + "%");
			} else {
				backTestSummary.setProfitRatio("null");
			}

			// 盈虧金額比
			profitAmtRatio = ps.getProfitAmtRatio(profitDTO);
			if (profitAmtRatio == null) {
				backTestSummary.setProfitAmtRatio("-");
			} else {
				backTestSummary.setProfitAmtRatio(percentFormatter.format(profitAmtRatio.doubleValue()));
			}

			// 平均獲利期望
			backTestSummary.setExpectValue(profitDTO.getExpectValue() == null ? "-"
					: "$ " + percentFormatter.format(profitDTO.getExpectValue()));

			// 交易次數
			backTestSummary.setTradeCount(profitDTO.getTradeCount());

			backTestSummaryList.add(backTestSummary);
		}
		btResult.setBackTestSummaryList(backTestSummaryList);

		// 設定歷史測試資料ID
		btResult.setHistoryTestIdList(getLatest10TestId());

		btResult.setDataTableHtml(testId, contextPath);

		return btResult;
	}

	public List<String> getLatest10TestId() {
		List<String> btidList = new ArrayList<>();
		List<BackTestLogHPO> btlHPoList = btLogHRepo.findTop10ByOrderByTestIdDesc();
		btlHPoList.forEach(btl -> btidList.add(btl.getTestId() + " - " + btl.getTestDesc()));
		return btidList;
	}

	public TradeDetailVO getBackTestTradeDetail(Integer testId) {
		TradeDetailVO tradeDetailVo = new TradeDetailVO();
		try {
			BackTestLogDPO btlD = btLogDRepo.findById(testId).orElse(null);

			String tradeDetailStr = WString.unCompress(new String(btlD.getTradeDetail(), StandardCharsets.UTF_8));
			JSONObject tradeDetailJson = new JSONObject(tradeDetailStr);
			tradeDetailStr = tradeDetailJson.getString("ProfitPO");
			// 反序列化並解壓縮，取得ProfitPO物件
			ProfitDTO profitDTO = (ProfitDTO) serializer.unCompressAndUnserializeByString(tradeDetailStr);

			List<SysConfigPO> scList = configRepo.findByParentIdIsOrderByCfgId(ParentKey.SID.getKey());
			List<TradeLog_EachTrade> tradeLogList = new ArrayList<>();

			// 交易紀錄
			NumberFormat formatter = new DecimalFormat("$###,###,###,###.##");
			profitDTO.getTradeDetailList().stream().filter(td -> td.getGrossProfit() != null).forEach(td -> {
				SysConfigPO sc = scList.stream().filter(scl -> scl.getCfgId().compareTo(td.getSid()) == 0).findFirst()
						.orElse(null);
				List<TradeDetailDTO> buyTradePOList = profitDTO.getTradeDetailList().stream().filter(
						tdd -> td.getParentTradeIdSet() != null && td.getParentTradeIdSet().contains(tdd.getTradeId()))
						.collect(Collectors.toList());

				List<String> buyStrDateList = buyTradePOList.stream().map(tdd -> {
					return WType.dateToStr(tdd.getTradeDate(), WDate.dateFormat_yyyyMMdd_Dash);
				}).collect(Collectors.toList());
				List<String> buyTradeTypeList = buyTradePOList.stream().map(tdd -> tdd.getTradeType().getDesc())
						.collect(Collectors.toList());
				List<String> buyPriceList = buyTradePOList.stream()
						.map(tdd -> formatter.format(tdd.getTradePrice().doubleValue())).collect(Collectors.toList());
				String stockName = sc == null ? "NULL" : sc.getCfgValue() + "-" + sc.getCfgDesc();
				tradeLogList.add(new TradeLog_EachTrade(td.getSid(), stockName, buyStrDateList, buyTradeTypeList,
						buyPriceList, WType.dateToStr(td.getTradeDate(), WDate.dateFormat_yyyyMMdd_Dash),
						td.getTradeType().getDesc(),
						td.getTradePrice() == null ? "" : formatter.format(td.getTradePrice().doubleValue()),
						td.getGrossProfit() == null ? "" : formatter.format(td.getGrossProfit().doubleValue()),
						td.getGrossProfit() == null ? ""
								: td.getGrossProfit()
										.divide(td.getCost().multiply(Const._BIGDECIMAL_m1), 4, RoundingMode.HALF_UP)
										.multiply(Const._BIGDECIMAL_100).setScale(2) + "%"));
			});
			tradeDetailVo.setTradeLogList(tradeLogList);
			collectTradePlanInfo(profitDTO);
			tradeDetailVo.setCustColList(profitDTO.getCustColList());

			// 未實現淨值
			List<TimeSeriesDTO<BigDecimal>> netValueTsList = profitDTO.getNetValueTsList();
			Object[] ja = new Object[CollectionUtils.isEmpty(netValueTsList) ? 0 : netValueTsList.size()];
			for (int i = 0; i < ja.length; i++) {
				Object[] ja1 = new Object[2];
				ja1[0] = netValueTsList.get(i).getDate().getTime();
				ja1[1] = netValueTsList.get(i).getValue();
				ja[i] = ja1;
			}
			tradeDetailVo.setNetValueTsJsonArray(ja);
			// 已實現淨值
			List<TimeSeriesDTO<BigDecimal>> rNetValueTsList = new ArrayList<>();
//			rNetValueTsList.addAll(netValueTsList);//複製未實現淨值資料
			BigDecimal netValue = profitDTO.getOriginalFunds();
			if (netValueTsList != null) {
				// 逐日刷新淨值
				for (TimeSeriesDTO<BigDecimal> ts : netValueTsList) {
					// 撈出當天平倉結算日的紀錄
					List<TradeDetailDTO> tdList = profitDTO.getTradeDetailList().stream().filter(
							td -> td.getGrossProfit() != null && td.getSettleDate().compareTo(ts.getDate()) == 0)
							.collect(Collectors.toList());
					if (tdList.size() > 0) {
						for (TradeDetailDTO td : tdList) {
							netValue = netValue.add(td.getGrossProfit());
						}
					}
					rNetValueTsList.add(new TimeSeriesDTO<BigDecimal>(ts.getDate(), netValue));
				}
				Object[] ja2 = new Object[CollectionUtils.isEmpty(rNetValueTsList) ? 0 : rNetValueTsList.size()];
				for (int i = 0; i < ja2.length; i++) {
					Object[] ja1 = new Object[2];
					ja1[0] = rNetValueTsList.get(i).getDate().getTime();
					ja1[1] = rNetValueTsList.get(i).getValue();
					ja2[i] = ja1;
				}
				tradeDetailVo.setRNetValueTsJsonArray(ja2);
			}
		} catch (Exception e) {
			tradeDetailVo.setErrMsg(WType.exceptionToString(e));
			WLog.error(e);
		}
		return tradeDetailVo;
	}

	@Autowired
	StatisticService statService;

//	@Autowired
//	DataTableDao dtDao;
	public void collectTradePlanInfo(ProfitDTO po) {
//		List<List<String>> tableData = new ArrayList<>();
//		tableData.add(Arrays.asList("MA60>MA20"));
//		po.getTradeDetailList().stream().filter(td -> td.getGrossProfit() != null).forEach(td -> {
//			Set<String> t =  td.getParentTradeIdSet();
//			TradeDetailPO openTd = po.getTradeDetailList().stream().filter(tt -> t.contains(tt.getTradeId())).findFirst().orElse(null);
//			List<String> rowDataList = new ArrayList<>();
//			List<TimeSeriesDTO<BigDecimal>> ma20TsList = statService.getSMA(dtDao.findTdsList(openTd.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 20);
//			List<TimeSeriesDTO<BigDecimal>> ma60TsList = statService.getSMA(dtDao.findTdsList(openTd.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 60);
//			TimeSeriesDTO<BigDecimal> ma60Ts = ma60TsList.stream().filter(ma -> ma.getDate().compareTo(openTd.getTradeDate()) == 0).findFirst().orElse(null);
//			TimeSeriesDTO<BigDecimal> ma20Ts = ma20TsList.stream().filter(ma -> ma.getDate().compareTo(openTd.getTradeDate()) == 0).findFirst().orElse(null);
//			rowDataList.add((ma60Ts == null || ma20Ts == null) ? "NULL" : ma60Ts.getDecimalValue().compareTo(ma20Ts.getDecimalValue()) + "");
//			
//			if(td.getSid() == 349) {
//				System.out.print("");
//			}
//			tableData.add(rowDataList);
//		});
//		po.setCustColList(tableData);
	}

	public String exportCandleChart(int sid, String strDate) {
		// 撈出前後股價資料，共3個月
		try {
			Date date = WType.strToDate(strDate, WDate.dateFormat_yyyyMMdd_Dash);
			// 撈出前後50天資料後排序
			int dataPeriod = 200;
			List<TimeSeriesDTO<OHLCVDTO>> priceList = dtDao.findTsList(sid, Const._FIELDID_LIST_OHLCV, OHLCVDTO.class);
			// 確認目標日INDEX
			// 確認目標日離哪一頭近
			boolean isNearHead = (date.getTime()
					- priceList.get(0).getDate().getTime()) < (priceList.get(priceList.size() - 1).getDate().getTime()
							- date.getTime());
			int targetIndex = 0;
			for (int i = isNearHead ? 0 : priceList.size() - 1; isNearHead ? (i < priceList.size())
					: (i > -1); i += isNearHead ? 1 : -1) {
				if (priceList.get(i).getDate().compareTo(date) == 0) {
					targetIndex = i;
					break;
				}
			}
			Date startDate = priceList.get(Math.max(0, targetIndex - (dataPeriod / 2))).getDate();
			Date endDate = priceList.get(Math.min(priceList.size() - 1, targetIndex + (dataPeriod / 2))).getDate();
			targetIndex = targetIndex - Math.max(0, targetIndex - (dataPeriod / 2)) + 1;

			// 轉換成ArraryList
			List<String[]> dataList = new ArrayList<>();
			priceList.forEach(price -> {
				String[] rowData = new String[6];
				rowData[0] = WType.dateToStr(price.getDate(), WDate.dateFormat_yyyyMMdd_Dash);
				rowData[1] = price.getValue().getOpen() == null ? "0" : price.getValue().getOpen().toString();;
				rowData[2] = price.getValue().getHigh() == null ? "0" : price.getValue().getHigh().toString();
				rowData[3] = price.getValue().getLow() == null ? "0" : price.getValue().getLow().toString();
				rowData[4] = price.getValue().getClose() == null ? "0" : price.getValue().getClose().toString();
				rowData[5] = price.getValue().getTradingShares() == null ? "0" : price.getValue().getTradingShares().toString();
				dataList.add(rowData);
			});

			//投信買賣超
			List<TimeSeriesDTO<BigDecimal>> itNetTsList = dtDao.findTsList(sid, Const._FIELDID_LP_IT_NET, startDate, endDate);
			String itNetStr = convertTsListToTAStrData(
					priceList.stream().filter(price -> WDate.between(price.getDate(), startDate, endDate))
							.collect(
									Collectors.toList()), itNetTsList);

			// 輔助性資料(MA/VOL...)
			final int lineIndex = targetIndex;
			List<TaDataDTO> taDataDtoList = new ArrayList<>();
			taDataDtoList.add(new TaDataDTO(TaType.Vo, null));
			//均價
			taDataDtoList.add(new TaDataDTO(TaType.SMA, new HashMap<String, Object>(){{
				put("n", "60");
				put("col", "'plum1'");
			}}));
			taDataDtoList.add(new TaDataDTO(TaType.SMA, new HashMap<String, Object>(){{
				put("n", "20");
				put("col", "'paleturquoise1'");
			}}));
			taDataDtoList.add(new TaDataDTO(TaType.SMA, new HashMap<String, Object>(){{
				put("n", "10");
				put("col", "'sandybrown'");
			}}));
			taDataDtoList.add(new TaDataDTO(TaType.SMA, new HashMap<String, Object>(){{
				put("n", "5");
				put("col", "'yellow'");
			}}));
			//標示基準日
			taDataDtoList.add(new TaDataDTO(TaType.Lines, new HashMap<String, Object>(){{
				put("v", lineIndex + "");
				put("on", "-1");
				put("col", "'lightgoldenrod3'");
			}}));
			//投信買賣超
			taDataDtoList.add(new TaDataDTO(TaType.TA, new HashMap<String, Object>(){{
				put("data", itNetStr);
				put("col", "'orange'");
				put("type", "'h'");
			}}));

			// 產圖
			SysConfigPO sc = configRepo.findById(sid).orElse(null);
			String filePath = candleChartDir + sid + "_" + strDate + ".jpg";
			String chartName = (sc == null) ? "NULL" : sc.getCfgValue() + " - " + sc.getCfgDesc().trim() + "[" + strDate + "]";
			if(wrChart.genCandleChart(
					chartName,startDate, endDate, filePath, dataList, taDataDtoList)) {
				return filePath;
			} else {
				return null;
			}
		} catch (Exception e) {
			WLog.error("sid[" + sid + "]strDate[" + strDate + "]", e);
			return null;
		}
	}

	private String convertTsListToTAStrData(List<TimeSeriesDTO<OHLCVDTO>> priceList,
			List<TimeSeriesDTO<BigDecimal>> taDataList) {
		StringBuilder taStrData = new StringBuilder();
		//為了讓data照priceList的順序走，並避免缺漏資料，以priceList為主塞資料
		for (int i = 0; i < priceList.size(); i++) {
			if (taDataList.size()>i && priceList.get(i).getDate().compareTo(taDataList.get(i).getDate()) == 0) {
				taStrData.append(taDataList.get(i).getValue().toString()).append(",");
			} else {
				int finalI = i;
				TimeSeriesDTO<BigDecimal> taData = taDataList.stream()
						.filter(data -> data.getDate().compareTo(priceList.get(finalI).getDate()) == 0)
						.findAny().orElse(null);
				if (taData == null) {
					taStrData.append("0").append(",");
				} else {
					taStrData.append(taData.getValue().toString()).append(",");
				}
			}
		}
		taStrData.setLength(taStrData.length() - 1);
		return taStrData.toString();
	}

	public String getStrategySourcePath(Integer testId) {
		BackTestLogHPO btlH = btLogHRepo.findById(testId).orElse(null);
		return btlH.getStrategyFilePath();
	}
}
