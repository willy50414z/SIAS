package com.willy.sias.web.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TradeLog {
	private String tradeDate;
	private String sidName;
	private boolean buy;
	private double tradePrice;
	private int tradeCount;
	private String tradeAmt;
	private String grossProfit;
	private String grossProfitRatio;
}
