package com.willy.sias.db.dao;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.SysConfigRepository;


@Repository(value="ConfigDao")
public class ConfigDao {
	@Autowired
	private SysConfigRepository sysConfigRepo;
	
	public HashMap<Integer, String> findKeyAndFieldvalueMapByParentkey(int parentKey) {
		// TODO Auto-generated method stub
		HashMap<Integer, String> keyAndFieldvalueMap = new HashMap<Integer, String>();
		List<SysConfigPO> fieldMapList = sysConfigRepo.findByParentIdIsOrderByCfgId(parentKey);
		for(SysConfigPO fm :fieldMapList) {
			keyAndFieldvalueMap.put(fm.getCfgId(), fm.getCfgValue());
		}
		return keyAndFieldvalueMap;
	}
	
	public HashMap<String, Integer> findFieldvalueAndKeyMapByParentkey(int parentKey) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> fieldvalueAndKeyMap = new HashMap<String, Integer>();
		List<SysConfigPO> fieldMapList = sysConfigRepo.findByParentIdIsOrderByCfgId(parentKey);
		for(SysConfigPO fm :fieldMapList) {
			fieldvalueAndKeyMap.put(fm.getCfgValue(), fm.getCfgId());
		}
		return fieldvalueAndKeyMap;
	}
	
	public HashMap<String, Integer> findFielddescAndKeyMapByParentkey(int parentKey) {
		// TODO Auto-generated method stub
		HashMap<String, Integer> fielddescAndKeyMap = new HashMap<String, Integer>();
		List<SysConfigPO> fieldMapList = sysConfigRepo.findByParentIdIsOrderByCfgId(parentKey);
		for(SysConfigPO fm :fieldMapList) {
			fielddescAndKeyMap.put(fm.getCfgDesc(), fm.getCfgId());
		}
		return fielddescAndKeyMap;
	}
	
	public int findMaxFieldValueIndex(int parentKey,String keyType) {
		List<SysConfigPO> maxFieldValue = sysConfigRepo.findByParentIdIsAndCfgValueStartingWithOrderByCfgValueDesc(parentKey, keyType);
		return (maxFieldValue == null || maxFieldValue.size() == 0) ? 0 : Integer.valueOf(maxFieldValue.get(0).getCfgValue().substring(maxFieldValue.get(0).getCfgValue().length()-6, maxFieldValue.get(0).getCfgValue().length()));
	}
}
