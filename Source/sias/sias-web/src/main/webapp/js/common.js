function getContextPath() {
	   return window.location.pathname.substring(0, window.location.pathname.indexOf("/",2));
}
function evalOrString(param){
	try{
		return eval(param);
	}catch(exception){
	}
	return param;
}
//json資料轉換成dataTableHtml
function jsonToDataTableHtml(strJson){
	var html = "";
	var json = JSON.parse(strJson);
	var headerTitle = json.dataTableTitle;
	var headerFooter = json.dataTableFooters;
	var dataTableContent = json.dataTableData;
	var isEditable;
	var editable = json.editable;
	//title
	html+="<thead><tr>";
	for(var titleIndex=0; titleIndex<headerTitle.length; titleIndex++){
		html+="<th>"+headerTitle[titleIndex]+"</th>";
	}
	html+="</tr></thead>";
	//Content
	if(dataTableContent == null || dataTableContent.length==0){
		return
	}
	html+="<tbody>";
	for(var contentRowIndex = 0; contentRowIndex<dataTableContent.length; contentRowIndex++){
		html+="<tr>";
		for(var cellIndex = 0; cellIndex<dataTableContent[contentRowIndex].length;cellIndex++){
			//可以編輯?
			isEditable = (editable!=null && editable.length>cellIndex)?editable[cellIndex]:false;
			if(isEditable){
				//Value是否為boolean值
				if(dataTableContent[contentRowIndex][cellIndex] == 'true' || dataTableContent[contentRowIndex][cellIndex] == 'false'){
					html+=("<td><input type='checkbox' name='" + contentRowIndex + "-" + cellIndex + "' " +((dataTableContent[contentRowIndex][cellIndex] == 'true')?"checked":"")+"/></td>");
				}else{
					html+="<td><input type='text' name='" + contentRowIndex + "-" + cellIndex + "' value='"+dataTableContent[contentRowIndex][cellIndex]+"'/></td>";
				}
			}else{
				if(dataTableContent[contentRowIndex][cellIndex] == 'true' || dataTableContent[contentRowIndex][cellIndex] == 'false'){
					html+=("<td><input type='checkbox' name='" + contentRowIndex + "-" + cellIndex + "' "+((dataTableContent[contentRowIndex][cellIndex] == 'true')?"checked":"")+" disable/></td>");
				}else{
					html+="<td>"+evalOrString(dataTableContent[contentRowIndex][cellIndex])+"</td>";
				}
			}
		}
		html+="</tr>";
	}
	html+="</tbody>";
	//title
	if(headerFooter != null){
		html+="<tfoot><tr>";
		for(var footerIndex=0; footerIndex<headerFooter.length; footerIndex++){
			html+="<th>"+headerFooter[footerIndex]+"</th>";
		}
		html+="</tr></tfoot>";
	}
	return html;
}