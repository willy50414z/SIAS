package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.willy.sias.db.po.SharesDispersionPO;
import com.willy.sias.db.po.SharesDispersionPOKey;

@Repository
public interface SharesDispersionRepository extends JpaRepository<SharesDispersionPO, SharesDispersionPOKey> {
	public List<SharesDispersionPO> findByDataDate(Date dataDate);
	@Modifying
	@Transactional
	public void deleteBySidIsAndDataDateLessThan(Integer sid, Date dataDate);
}
