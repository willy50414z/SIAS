package com.willy.sias.backtest.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.willy.sias.util.EnumSet.TradeType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
public class TradePlanDTO {
	private int sid;
	private Date tradeDate;
	private TradeType tradeType;
	private int tradeShares;
	private BigDecimal tradePrice;
}
