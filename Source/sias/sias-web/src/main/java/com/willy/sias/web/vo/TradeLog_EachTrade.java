package com.willy.sias.web.vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TradeLog_EachTrade {
	private Integer sid;
	private String stockName;
	private List<String> buyDateList;
	private List<String> buyTypeList;
	private List<String> buyPriceList;
	private String sellDate;
	private String sellType;
	private String sellPrice;
	private String grossProfit;
	private String grossProfitRatio;
//	private String grossProfitRatio;
//	private String maxProfit;// 最大預期獲利
//	private String maxProfitRatio;// 最大預期獲利
//	private String maxLoss;// 最大預期損失
//	private String maxLossRatio;// 最大預期損失
}
