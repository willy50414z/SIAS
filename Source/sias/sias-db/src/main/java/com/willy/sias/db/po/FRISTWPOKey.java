package com.willy.sias.db.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Table(name="FR_IS_TW")
public class FRISTWPOKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 股票代號
     */
    @Column(name = "SID")
    private int sid;
    
    /**
     * 財報所屬日期
     */
    @Column(name = "FR_DATE")
    private String frDate;
    
    /**
     * 財報項目ID
     */
    @Column(name = "FR_KEY")
    private int frKey;
}
