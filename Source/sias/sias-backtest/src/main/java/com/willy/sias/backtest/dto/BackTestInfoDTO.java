package com.willy.sias.backtest.dto;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.willy.util.type.WType;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BackTestInfoDTO {
	private String strategyFilePath;
	private String strategyName;
	private String strategyArgs;
	private Set<Integer> sidSet = new HashSet<>();
	//初始資金，預設100W
	private BigDecimal funds = new BigDecimal(1000000);
	//回測起始日，預設tpex第一天
	private Date startDate;
	//回測結束日，回測當天
	private Date endDate;
	private String testDesc;
	public BackTestInfoDTO() {
		super();
		startDate = WType.strToDate("20070702");
		endDate = new Date();
	}
	
}
