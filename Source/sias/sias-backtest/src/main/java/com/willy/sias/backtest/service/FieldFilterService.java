package com.willy.sias.backtest.service;


import com.google.gson.Gson;
import com.willy.file.serialize.WSerializer;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.repository.FieldInfoRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.calculator.WCalculator;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WRegex;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class FieldFilterService {

  @Autowired
  private DataTableDao dtDao;
  @Autowired
  private FieldInfoRepository fiRepo;
  @Autowired
  private Jedis jedis;
  @Autowired
  private WSerializer serializer;

  public List<Date> executeProcBlocks(String procBlocks) throws Exception {
    //replace variables
    List<String> varList = WRegex.getMatchedStrList(procBlocks, "[$]{1}[^=]+[=].*");
    for (String var : varList) {
      String[] variableAndValueAr = var.split("=");
      procBlocks = procBlocks.replace(var + "\n", "");
      procBlocks = procBlocks.replace(variableAndValueAr[0], variableAndValueAr[1]);
    }

    //replace block to redisKey and put <redisKey, procBlock> to map
    Map<String, String> redisKeyAndProcBlockMap = new HashMap<>();
    procBlocks = this.replactProcessBlocksToRedisKey(procBlocks, 0,
        redisKeyAndProcBlockMap);

    //exec method and put result to redis
    String uuid = UUID.randomUUID().toString();
    List<Date> result = null;
    try {
      for (String redisKey : redisKeyAndProcBlockMap.keySet()) {
        this.putDateListToRedis(uuid, redisKey, redisKeyAndProcBlockMap);
      }

      //eval redis keys which has dateList in redis, then get result dateList
      result = this.evalRedisKeys(uuid, procBlocks);
    } finally {
      //clean tmp cache
      jedis.del(Const.PROC_BLOCK_HKEY + ":" + uuid);
    }

    return result;
  }

  /**
   * 將procBlock取代成redisKey，方便後續逐個執行並放到redis
   *
   * @param processBlocks           篩選字串
   * @param processBlockKeyIdx      會用遞迴逐層取代()中的block
   * @param redisKeyAndProcBlockMap 空的map，method會將<redisKey, procBlock>put進去供後續用
   */
  public String replactProcessBlocksToRedisKey(String processBlocks, int processBlockKeyIdx,
      Map<String, String> redisKeyAndProcBlockMap) {
    Set<String> processBlockSet = WRegex.getMatchedStrSet(processBlocks,
        WRegex._REGEX_SQUARE_BRACKETS_WRAPED);
    for (String processBlock : processBlockSet) {
      String pbKey = Const.PROC_BLOCK_KEY_PREFFIX + processBlockKeyIdx;
      processBlocks = processBlocks.replace(processBlock,
          "${" + pbKey + "}");
      redisKeyAndProcBlockMap.put(pbKey, processBlock);
      processBlockKeyIdx++;
    }
    if (WRegex.getMatchedStrSet(processBlocks, WRegex._REGEX_SQUARE_BRACKETS_WRAPED).size() == 0) {
      return processBlocks;
    } else {
      return replactProcessBlocksToRedisKey(processBlocks, processBlockKeyIdx,
          redisKeyAndProcBlockMap);
    }
  }

  /**
   * 執行單個procBlock，並將結果put到redis
   *
   * @param uuid                    區隔各執行續的redis資料
   * @param procBlockRedisKey       redisKey
   * @param redisKeyAndProcBlockMap <redisKey, procBlock>
   */
  public void putDateListToRedis(String uuid, String procBlockRedisKey,
      Map<String, String> redisKeyAndProcBlockMap)
      throws InvocationTargetException, IllegalAccessException, IOException, ClassNotFoundException {
    Set<String> subProcBlockRedisKeySet = WRegex.getMatchedStrSet(
        redisKeyAndProcBlockMap.get(procBlockRedisKey), WRegex._REGEX_PARAMETER_WRAPED);
    if (subProcBlockRedisKeySet.size() > 0) {
      for (String subProcBlockRedisKey : subProcBlockRedisKeySet) {
        String procBlockKey = subProcBlockRedisKey.substring(2, subProcBlockRedisKey.length() - 1);
        if(!jedis.hexists(this.getProcBlockRedisKey(uuid), procBlockKey)) {
          putDateListToRedis(uuid, subProcBlockRedisKey.substring(2, subProcBlockRedisKey.length() - 1), redisKeyAndProcBlockMap);
        }
      }
    }

    String procBlockStr = redisKeyAndProcBlockMap.get(procBlockRedisKey);
    List<String> operatorList = WRegex.getMatchedStrList(procBlockStr, "[<>=]{1,2}");
    List<Date> procBlockResult = null;
    List<String> redisProcList = WRegex.getMatchedStrList(procBlockStr, "[$][{]procBlock[0-9]{0,5}[}]");
    List<String> procList = WRegex.getMatchedStrList(procBlockStr, "[{][0-9]{0,5}[}][(][^(^)]*[)]");
    if (operatorList.size() == 0) {
      //func as args of parent func
      List<TimeSeriesDTO<BigDecimal>> procResult = dtDao.findTsListByProcStr(uuid, procBlockStr);
      jedis.hset(getProcBlockRedisKey(uuid), procBlockRedisKey,
          serializer.serializeToString((Serializable) procResult));
      return;
    } else {
      String operator = operatorList.get(0);
      if (procList.size() == 1) {
        //compare proc method value with value
        List<TimeSeriesDTO<BigDecimal>> procResult = dtDao.findTsListByProcStr(uuid, procBlockStr);
        procBlockResult = procResult.stream()
            .filter(ts -> ts.getValue() != null && WCalculator.eval(
                ts.getValue().toString() + procBlockStr.substring(procBlockStr.indexOf(operator),
                    procBlockStr.length() - 1)).intValue() == 1).map(TimeSeriesDTO::getDate)
            .collect(
                Collectors.toList());
      } else if (procList.size() == 2) {
        //compare two proc method
        List<TimeSeriesDTO<BigDecimal>> tsList1 = dtDao.findTsListByProcStr(uuid, procList.get(0));
        List<TimeSeriesDTO<BigDecimal>> tsList2 = dtDao.findTsListByProcStr(uuid, procList.get(1));
        procBlockResult = evalTsList(operator, tsList1, tsList2);
      } else if (redisProcList.size() == 2) {
        //${procBlock0} > ${procBlock1}
        List<TimeSeriesDTO<BigDecimal>> tsList1 = (List<TimeSeriesDTO<BigDecimal>>) serializer.unSerializeByString(jedis.hget(getProcBlockRedisKey(uuid), redisProcList.get(0).substring(2, redisProcList.get(0).length() - 1)));
        List<TimeSeriesDTO<BigDecimal>> tsList2 = (List<TimeSeriesDTO<BigDecimal>>) serializer.unSerializeByString(jedis.hget(getProcBlockRedisKey(uuid), redisProcList.get(1).substring(2, redisProcList.get(1).length() - 1)));
        procBlockResult = evalTsList(operator, tsList1, tsList2);
      }
    }

    if (procBlockResult == null) {
      throw new IllegalArgumentException("can't get result by procBlock[" + procBlockStr + "]");
    } else {
      jedis.hset(Const.PROC_BLOCK_HKEY + ":" + uuid, procBlockRedisKey,
          this.serializeDateListToString(procBlockResult));
    }
  }

  private List<Date> evalTsList(String operator, List<TimeSeriesDTO<BigDecimal>> tsList1, List<TimeSeriesDTO<BigDecimal>> tsList2){
    List<Date> procBlockResult = new ArrayList<>();
    int tmpTs2ListIdx = 0;
    for (TimeSeriesDTO<BigDecimal> ts1 : tsList1) {
      for (int i = tmpTs2ListIdx; i < tsList2.size(); i++) {
        if (ts1.getDate() != null && tsList2.get(tmpTs2ListIdx).getDate() != null
            && ts1.getValue() != null && tsList2.get(tmpTs2ListIdx).getValue() != null
            && ts1.getDate().compareTo(tsList2.get(tmpTs2ListIdx).getDate()) == 0
            && WCalculator.eval(
            ts1.getDecimalValue().compareTo(tsList2.get(tmpTs2ListIdx).getDecimalValue())
                + operator + "0").intValue() == 1) {
          procBlockResult.add(ts1.getDate());
        }
        //if ts2 date after ts1 date, change to next ts1 item
        if (WDate.afterOrEquals(ts1.getDate(), tsList2.get(tmpTs2ListIdx).getDate())) {
          tmpTs2ListIdx += 1;
          break;
        }
        tmpTs2ListIdx = i;
      }
    }
    return procBlockResult;
  }

  /**
   * 執行只有redis key的formula ex. ${a} > ${b}
   *
   * @param uuid       區分不同Thread在redis資料
   * @param procBlocks redis key formula
   */
  public List<Date> evalRedisKeys(String uuid, String procBlocks) {
    int evalResultIdx = 0;

    //eval procBlocks in brackets first
    List<String> procBlocksInBracketList = WRegex.getMatchedStrList(procBlocks,
        WRegex._REGEX_BRACKETS_WRAPED);
    while (procBlocksInBracketList.size() > 0) {
      for (String procBlocksInBracket : procBlocksInBracketList) {
        List<Date> result = evalProcBlock(uuid, procBlocks);
        //put to redis and replace procBlocks
        String redisKey = Const.EVAL_PROC_BLOCK_RESULT_KEY_PREFFIX + evalResultIdx;
        jedis.hset(Const.PROC_BLOCK_HKEY + ":" + uuid, redisKey,
            this.serializeDateListToString(result));
        procBlocks = procBlocks.replace(procBlocksInBracket, "${" + redisKey + "}");
        evalResultIdx++;
      }
      procBlocksInBracketList = WRegex.getMatchedStrList(procBlocks, WRegex._REGEX_BRACKETS_WRAPED);
    }

    //eval procBlocks without brackets
    //get operators
    List<String> operatorList = WRegex.getMatchedStrList(procBlocks, "[&|]+");
    //get procBlockList
    List<String> procBlockList = Arrays.asList(procBlocks.split("[&|]+"));
    //remove comment
    procBlockList = procBlockList.stream()
        .map(procBlock -> procBlock.contains("//") ? procBlock.substring(0, procBlock.indexOf("//"))
            : procBlock).collect(
            Collectors.toList());
    //eval procBlocks
    String firstRedisKey = procBlockList.get(0);
    if (operatorList.size() == 0 && procBlockList.size() == 1) {
      String dateListStr = jedis.hget(Const.PROC_BLOCK_HKEY + ":" + uuid,
          firstRedisKey.substring(2, firstRedisKey.length() - 1));
      return this.unserializeStringToDateList(dateListStr);
    }
    List<Date> result = new ArrayList<>();
    for (int i = 0; i < operatorList.size(); i++) {
      result = evalProcBlock(uuid,
          firstRedisKey + operatorList.get(i) + procBlockList.get(i + 1));

      String redisKey = Const.EVAL_PROC_BLOCK_RESULT_KEY_PREFFIX + evalResultIdx;
      jedis.hset(Const.PROC_BLOCK_HKEY + ":" + uuid, redisKey,
          this.serializeDateListToString(result));
      firstRedisKey = "${" + redisKey + "}";
      evalResultIdx++;
    }
    return result;
  }

  private List<Date> evalProcBlock(String uuid, String procBlocksInBracket) {
    //get operator
    String operator = WRegex.getMatchedStrList(procBlocksInBracket, "[&|]+").get(0);
    if (operator == null) {
      throw new IllegalArgumentException(
          "can't get [& or |] in procBlocksInBracket[" + procBlocksInBracket + "]");
    }

    //get redis key list
    List<String> redisKeyList = WRegex.getMatchedStrList(procBlocksInBracket,
            WRegex._REGEX_PARAMETER_WRAPED).stream().map(key -> key.substring(2, key.length() - 1))
        .collect(
            Collectors.toList());
    if (redisKeyList.size() != 2) {
      throw new IllegalArgumentException("procBlocksInBracket[" + procBlocksInBracket
          + "] should contains two procBlock redis key");
    }
    //get dateLists
    String dateListJs1 = jedis.hget(Const.PROC_BLOCK_HKEY + ":" + uuid, redisKeyList.get(0));
    String dateListJs2 = jedis.hget(Const.PROC_BLOCK_HKEY + ":" + uuid, redisKeyList.get(1));
    List<Date> dateList1 = this.unserializeStringToDateList(dateListJs1);
    List<Date> dateList2 = this.unserializeStringToDateList(dateListJs2);

    //get eval result
    List<Date> result = null;
    dateList1 = dateList1 == null ? new ArrayList<>() : dateList1;
    dateList2 = dateList2 == null ? new ArrayList<>() : dateList2;
    if (operator.equals("&")) {
      result = new ArrayList<>();
      for (Date date1 : dateList1) {
        if (dateList2.contains(date1)) {
          result.add(date1);
        }
      }
    } else if (operator.equals("|")) {
      result = new ArrayList<>(new HashSet<>(dateList1));
    }
    return result;
  }

  private String serializeDateListToString(Collection<Date> dateList) {
    Date[] dateAr = new Date[dateList.size()];
    dateList.toArray(dateAr);
    try {
      return serializer.serializeToString(dateAr);
    } catch (Exception e) {
      WLog.error(e);
      return "";
    }
  }

  private List<Date> unserializeStringToDateList(String serStr) {
    try {
      Date[] dateAr = (Date[]) serializer.unSerializeByString(serStr);
      return Arrays.asList(dateAr);
    } catch (Exception e) {
      WLog.error(e);
      return null;
    }
  }

  private String getProcBlockRedisKey(String uuid){
    return Const.PROC_BLOCK_HKEY + ":" + uuid;
  }
}
