package com.willy.sias.db.dto;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.willy.util.string.WString;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StrategyCheckerLogDTO{
	private Gson gson = new Gson();
	//檢核名稱
	private String checkerName;
	private String checkerDesc;
	private Map<String, Object> checkerParams = new HashMap<> ();
	private Object checkerResult;
	private Class<?> checkerResultType;
	
	public void setCheckerResult(Object checkerResult) throws IOException {
		this.checkerResultType = checkerResult.getClass();
		this.checkerResult = checkerResult;
	}
	//加入檢核所使用的參數
	public void addCheckerParams(String key, Object paramValue) throws IOException {
		checkerParams.put(key, paramValue);
	}
	@Override
	public String toString() {
		return "StrategyCheckerLogDTO [checkerName=" + checkerName + ", checkerDesc=" + checkerDesc
				+ ", checkerParams=" + WString.toString(checkerParams) + ", checkerResult=" + checkerResult + ", checkerResultType="
				+ checkerResultType + "]";
	}
}
