package com.willy.sias.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.LPFutureTWPO;
import com.willy.sias.db.po.LPFutureTwPOKey;


@Repository
public interface LP_FUTURE_TWRepository extends JpaRepository<LPFutureTWPO, LPFutureTwPOKey> {
}