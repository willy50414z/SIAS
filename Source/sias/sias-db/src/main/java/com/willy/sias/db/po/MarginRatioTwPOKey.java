package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "PRICE_DAILY_TW")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MarginRatioTwPOKey implements Serializable {

  /**
   * 標的代號
   */
  @Column(name = "SID")
  private Integer sid;

  /**
   * 資料日期
   */
  @Column(name = "DATA_DATE")
  private Date dataDate;
}
