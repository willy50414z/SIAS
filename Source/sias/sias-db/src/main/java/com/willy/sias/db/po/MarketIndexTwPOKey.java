package com.willy.sias.db.po;

import java.io.Serializable;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

@Table(name = "MARKET_INDEX_TW")
public class MarketIndexTwPOKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 標的代號
	 */
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 資料日期
	 */
	@Column(name = "DATA_DATE")
	private Date dataDate;
}
