package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * LPTransLog_TW_TWSE
 *
 * @author
 */
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LPTranslog_TW_TWSE")
public class LPTransLogTwPOKey implements Serializable {

  /**
   * 標的代號
   */
  @Column(name = "SID")
  private Integer sid;

  /**
   * 資料日期
   */
  @Column(name = "TRANS_DATE")
  private Date transDate;


  private static final long serialVersionUID = 1L;
}