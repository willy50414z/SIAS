package com.willy.sias.batch.processor;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.codehaus.plexus.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.net.web.WSelenium;
import com.willy.net.web.WSelenium.WebDriverType;
import com.willy.sias.db.po.SharesDispersionPO;
import com.willy.sias.db.repository.SharesDispersionRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.shares-dispersion")
public class SHARES_DISPERSION extends BaseBatchProcessor {
	@Autowired
	private SharesDispersionRepository sdRepo;
	@Autowired
	WSelenium su;
	@Autowired
	WFile_txt txtFile;

	private WebDriver browser = null;

	private String urlTempl;
	private String csvFileNameTempl;

	@Value("${selenium.driver.dir}")
	private String webDriverDir;
	
	private String stockCode;

	@Override
	public void cleanHisData() throws Exception {
		stockCode = this.getMapParam("stockCode");
		if (StringUtils.isBlank(stockCode)) {
			throw new IllegalArgumentException("stockCode is blank[" + stockCode + "]");
		}
		if(this.sidMap.get(this.stockCode) == null) {
			throw new IllegalArgumentException("stockCode can[" + stockCode + "]");
		} else {
			sdRepo.deleteBySidIsAndDataDateLessThan(this.sidMap.get(this.stockCode), dataDate);
		}
	}

	@Override
	public void exec() throws Exception {
		SharesDispersionPO sdPo;
		List<SharesDispersionPO> sdPoList = new ArrayList<>();
		txtFile.setEncoding(Const._ENCODING_MS950);

		try {
			String stockCode = this.getMapParam("stockCode");
			if (StringUtils.isBlank(stockCode)) {
				throw new IllegalArgumentException("stockCode is blank[" + stockCode + "]");
			}
			// 一次做1000筆SID，避免DB不能傳太多參數
			WLog.info("Download Shares Dispersion Table SIDCode[" + stockCode + "]");
			String filePath = downloadSharesDispersionTable(stockCode);
			WLog.info("Read Shares Dispersion Table FilePath[" + filePath + "]");
			String[][] sharesDispersion = txtFile.readToAr(filePath);

			for (String[] rowData : sharesDispersion) {
				// 第2欄為數字(資料日期)
				if (rowData != null && rowData.length > 3 && StringUtils.isNumeric(rowData[2])) {
					sdPo = new SharesDispersionPO();
					sdPo.setSid(this.sidMap.get(stockCode));
					sdPo.setDataDate(WType.strToDate(rowData[2]));
					sdPo.setBatchDate(dataDate);
					sdPo.setSharesCount(new BigDecimal(WString.cleanDigitData(rowData[3])));
					sdPo.setShareholderCount(new BigDecimal(WString.cleanDigitData(rowData[4])));
					sdPo.setAvgSharesEachPerson(new BigDecimal(WString.cleanDigitData(rowData[5])));
					sdPo.setHoldingCount1(new BigDecimal(WString.cleanDigitData(rowData[6])));
					sdPo.setPersonCount1(new BigDecimal(WString.cleanDigitData(rowData[8])));
					sdPo.setPersonCount2(new BigDecimal(WString.cleanDigitData(rowData[9])));
					sdPo.setPersonCount3(new BigDecimal(WString.cleanDigitData(rowData[10])));
					sdPo.setPersonCount4(new BigDecimal(WString.cleanDigitData(rowData[11])));
					sdPo.setPersonCount5(new BigDecimal(WString.cleanDigitData(rowData[12])));
					sdPoList.add(sdPo);
				}
			}
			sdRepo.saveAll(sdPoList);
		} catch (Exception e) {
			throw e;
		} finally {
			if (browser != null) {
				browser.close();
			}
		}
	}

	private String downloadSharesDispersionTable(String sidCode)
			throws InterruptedException, ExecutionException, IOException {
		// 下載
		int retryTimes = 10;
		String filePath = null;
		putMapParam("sidCode", sidCode);

		filePath = this.fileDownloadDir + parseMapParamToString(csvFileNameTempl);
		File f = new File(filePath);
		String url = parseMapParamToString(urlTempl);
		if (!f.exists()) {
			if (this.browser == null) {
				su.setWebDriverDir(webDriverDir);
				this.browser = su.getBrowser(WebDriverType.PhantomJS);
			}
			// 開始爬資料
			this.browser.get(url);
			WLog.info("Start Download Shares Dispersion Table URL[" + url + "] Path[" + filePath + "]");
			Document doc = Jsoup.parse(browser.getPageSource());
			// 等待網頁反應
			while (doc.getElementById("Details") == null) {
				this.browser.get(url);
				Thread.sleep(5000);
				doc = Jsoup.parse(browser.getPageSource());
				if (--retryTimes == 0 || browser.getPageSource().contains("查詢無此證券代號資料")) {
					retryTimes = 10;
					txtFile.writeByStr(filePath, "");
					return "";
				}
			}
			// 每次查詢間格5秒，避免過度頻繁
			Thread.sleep(5000);
			StringBuffer sb = new StringBuffer();
			doc.getElementById("Details").getElementsByTag("tr").forEach(tr -> {
				tr.getElementsByTag("td").forEach(td -> {
					sb.append("\"").append(WString.rmStr(td.text(), ",")).append("\",");
				});
				// 去除最後一欄空欄位
				sb.setLength(sb.length() - 3);
				sb.append("\r\n");
			});
			txtFile.writeByStr(filePath, sb.toString());
		}

		WLog.info("Complete Download Shares Dispersion Table URL[" + url + "] Path[" + filePath + "]");
		return filePath;
	}

}