package com.willy.sias.backtest.service;

import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ChipIdxServiceTest {

  @Autowired
  private ChipIdxService ciSvc;
  @MockBean
  private DataTableDao dtDao;

  @Before
  public void init() throws ParseException {
    List<TimeSeriesDTO<BigDecimal>> lpTxnLogTsListBuy = Arrays.asList(
        //連買5天,空2天後繼續買
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160331"), new BigDecimal("10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160401"), new BigDecimal("20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160406"), new BigDecimal("30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160407"), new BigDecimal("40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160408"), new BigDecimal("50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160411"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160412"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160413"), new BigDecimal("60000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160414"), new BigDecimal("70000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160415"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160418"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160419"), new BigDecimal("0")),
        //連買6天後遇到賣超
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160420"), new BigDecimal("10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160421"), new BigDecimal("20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160422"), new BigDecimal("30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160425"), new BigDecimal("40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160426"), new BigDecimal("50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160427"), new BigDecimal("60000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160428"), new BigDecimal("-10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160429"), new BigDecimal("70000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160503"), new BigDecimal("80000")),
        //連買4天，空2天後買超
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160504"), new BigDecimal("-10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160505"), new BigDecimal("10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160506"), new BigDecimal("20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160509"), new BigDecimal("30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160510"), new BigDecimal("40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160511"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160512"), new BigDecimal("50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160516"), new BigDecimal("60000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160517"), new BigDecimal("-10000")),
        //連買5天 空1天，後面就沒資料了
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160623"), new BigDecimal("10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160624"), new BigDecimal("20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160627"), new BigDecimal("30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160628"), new BigDecimal("40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160629"), new BigDecimal("50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160630"), new BigDecimal("0"))
    );



    List<TimeSeriesDTO<BigDecimal>> lpTxnLogTsListSell = Arrays.asList(
        //連賣5天,空2天後繼續賣
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160517"), new BigDecimal("-10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160518"), new BigDecimal("-20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160519"), new BigDecimal("-30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160520"), new BigDecimal("-40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160523"), new BigDecimal("-50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160524"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160525"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160526"), new BigDecimal("-60000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160527"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160530"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160531"), new BigDecimal("0")),

        //連賣6天後遇到買超
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160601"), new BigDecimal("-10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160602"), new BigDecimal("-20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160603"), new BigDecimal("-30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160604"), new BigDecimal("-40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160606"), new BigDecimal("-50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160607"), new BigDecimal("-60000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160608"), new BigDecimal("300")),

        //連賣4天，空2天后賣超
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160613"), new BigDecimal("-10000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160614"), new BigDecimal("-20000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160615"), new BigDecimal("-30000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160616"), new BigDecimal("-40000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160617"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160620"), new BigDecimal("0")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160621"), new BigDecimal("-50000")),
        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160622"), new BigDecimal("1000"))

//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160701"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160704"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160705"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160706"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160707"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160711"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160712"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160713"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160714"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160715"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160718"), new BigDecimal("")),
//        new TimeSeriesDTO<BigDecimal>(WType.strToDate("20160719"), new BigDecimal("")),

    );

    Mockito.when(dtDao.<BigDecimal>findTsList(66208, Const._FIELDID_LP_IT_NET))
        .thenReturn(lpTxnLogTsListBuy);
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET))
        .thenReturn(lpTxnLogTsListSell);
    Mockito.when(dtDao.<BigDecimal>findTsList(66208, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160401")))
        .thenReturn(lpTxnLogTsListBuy.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160401")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66208, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160414")))
        .thenReturn(lpTxnLogTsListBuy.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160414")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66208, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160418")))
        .thenReturn(lpTxnLogTsListBuy.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160418")))
            .collect(
                Collectors.toList()));

    Mockito.when(dtDao.<BigDecimal>findTsList(66208, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160419")))
        .thenReturn(lpTxnLogTsListBuy.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160419")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160519")))
        .thenReturn(lpTxnLogTsListSell.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160519")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160525")))
        .thenReturn(lpTxnLogTsListSell.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160525")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160527")))
        .thenReturn(lpTxnLogTsListSell.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160527")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160530")))
        .thenReturn(lpTxnLogTsListSell.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160530")))
            .collect(
                Collectors.toList()));
    Mockito.when(dtDao.<BigDecimal>findTsList(66209, Const._FIELDID_LP_IT_NET, null,
            WType.strToDate("20160531")))
        .thenReturn(lpTxnLogTsListSell.stream()
            .filter(lpTs -> WDate.beforeOrEquals(lpTs.getDate(), WType.strToDate("20160531")))
            .collect(
                Collectors.toList()));

    List<TimeSeriesDTO<BigDecimal>> reverseLpTxnLogTsListBuy = lpTxnLogTsListBuy.stream().collect(
        Collectors.toList());
    Collections.reverse(reverseLpTxnLogTsListBuy);
    Mockito.when(dtDao.<BigDecimal>findRecentTsList(66208, Const._FIELDID_LP_IT_NET,
            WType.strToDate("20160630"), -120))
        .thenReturn(reverseLpTxnLogTsListBuy);
  }

  @Test
  public void testGetITContinueDaysDateListWithContinueBuy5DaysShouldSuccess() {
    List<Date> expectedResult = Arrays.asList(WType.strToDate("20160408"),
        WType.strToDate("20160411"),
        WType.strToDate("20160412"),
        WType.strToDate("20160413"),
        WType.strToDate("20160414"),
        WType.strToDate("20160426"),
        WType.strToDate("20160427"),
        WType.strToDate("20160511"),
        WType.strToDate("20160512"),
        WType.strToDate("20160516"),
        WType.strToDate("20160629")
    );

    List<Date> result = ciSvc.getITContinueDaysDateList(66208, 5);

    result.forEach(x -> WLog.info(WType.dateToStr(x)));

    Assert.assertTrue(result.equals(expectedResult));
  }

  @Test
  public void getITContinueDaysTest() {
    //Test Sell
    List<Date> expectedResult = Arrays.asList(WType.strToDate("20160523"),
        WType.strToDate("20160524"),
        WType.strToDate("20160525"),
        WType.strToDate("20160526"),
        WType.strToDate("20160606"),
        WType.strToDate("20160607"),
        WType.strToDate("20160617"),
        WType.strToDate("20160620"),
        WType.strToDate("20160621")
    );

    List<Date> result = ciSvc.getITContinueDaysDateList(66209, -5);

    result.forEach(x -> WLog.info(WType.dateToStr(x)));

    Assert.assertTrue(result.equals(expectedResult));
  }

  @Test
  public void testGetITContinueDaysWithBuyShouldSuccess() {
    int result = ciSvc.getITContinueDays(66208, WType.strToDate("20160401"));
    Assert.assertEquals(result, 2);

    result = ciSvc.getITContinueDays(66208, WType.strToDate("20160414"));
    Assert.assertEquals(result, 9);

    result = ciSvc.getITContinueDays(66208, WType.strToDate("20160418"));
    Assert.assertEquals(result, 11);

    result = ciSvc.getITContinueDays(66208, WType.strToDate("20160419"));
    Assert.assertEquals(result, 0);
  }

  @Test
  public void testGetITContinueDaysWithSellShouldSuccess() {
    int result = ciSvc.getITContinueDays(66209, WType.strToDate("20160519"));
    Assert.assertEquals(result, -3);

    result = ciSvc.getITContinueDays(66209, WType.strToDate("20160525"));
    Assert.assertEquals(result, -7);

    result = ciSvc.getITContinueDays(66209, WType.strToDate("20160527"));
    Assert.assertEquals(result, -9);

    result = ciSvc.getITContinueDays(66209, WType.strToDate("20160530"));
    Assert.assertEquals(result, -10);

    result = ciSvc.getITContinueDays(66209, WType.strToDate("20160531"));
    Assert.assertEquals(result, 0);
  }

  @Test
  public void testGetRecentDaysItNetSumWithSampleDaysShouldSuccess() throws ParseException {
    Map<Integer, BigDecimal> result = ciSvc.getRecentDaysItNetSum(66208, WType.strToDate("20160630"));
    Map<Integer, BigDecimal> expextedResult = new LinkedHashMap<>();
    expextedResult.put(2, new BigDecimal("50000"));
    expextedResult.put(3, new BigDecimal("90000"));
    expextedResult.put(5, new BigDecimal("140000"));
    expextedResult.put(10, new BigDecimal("250000"));
    expextedResult.put(20, new BigDecimal("590000"));
    Assert.assertEquals(result, expextedResult);
  }
}
