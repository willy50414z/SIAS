package com.willy.sias.backtest.service;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.util.TradeUtil;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.util.type.WType;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.util.TradeUtil;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@ComponentScan("com.willy")
@RunWith(SpringRunner.class)
public class ProfitDTOServiceTest {
  @Autowired
  TradeUtil tradeUtil;

  @MockBean
  DataTableDao dtDao;
  @MockBean
  CalendarRepository calendarRepo;
  @Autowired
  ProfitDTOService profitSvc;

  @Before
  public void init() {
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210419"),
        WType.strToDate("20210419"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210419"), new BigDecimal("26.30"))));
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210421"),
        WType.strToDate("20210421"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210421"), new BigDecimal("28.35"))));
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210426"),
        WType.strToDate("20210426"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210426"), new BigDecimal("28.80"))));

    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210419"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210420"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210421"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210420"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210421"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210422"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210421"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210422"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210423"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210422"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210423"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210423"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210424"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210425"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210426"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210428"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210427"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(
        Arrays.asList(
            new CalendarPO(WType.strToDate("20210428"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
            new CalendarPO(WType.strToDate("20210429"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE))
    );
  }

  @Test
  public void profitPoStatisticFieldsTest() throws ParseException {
    ProfitDTO profitDTO = new ProfitDTO();
    profitDTO.setOriginalFunds(new BigDecimal(1000000));

    tradeUtil.trade(122, WType.strToDate("20210419"), 3000, TradeType.buy, new BigDecimal("25.5"),
        profitDTO);//76565
    tradeUtil.trade(122, WType.strToDate("20210420"), 3000, TradeType.buy, new BigDecimal("27.5"),
        profitDTO);//82570
    tradeUtil.trade(122, WType.strToDate("20210421"), 3000, TradeType.sell, new BigDecimal("22.5"),
        profitDTO);//67241
    tradeUtil.trade(122, WType.strToDate("20210422"), 3000, TradeType.sell, new BigDecimal("30.5"),
        profitDTO);//91148
    tradeUtil.trade(122, WType.strToDate("20210423"), 3000, TradeType.buy, new BigDecimal("21.5"),
        profitDTO);//64555
    tradeUtil.trade(122, WType.strToDate("20210423"), 3000, TradeType.sell, new BigDecimal("23.5"),
        profitDTO);//70335
    tradeUtil.trade(122, WType.strToDate("20210424"), 3000, TradeType.shortSelling,
        new BigDecimal("29.5"),
        profitDTO);//88293
    tradeUtil.trade(122, WType.strToDate("20210424"), 3000, TradeType.buy, new BigDecimal("35.5"),
        profitDTO);//106591
    tradeUtil.trade(122, WType.strToDate("20210426"), 3000, TradeType.buy, new BigDecimal("28.5"),
        profitDTO);//85573
    tradeUtil.trade(122, WType.strToDate("20210427"), 3000, TradeType.sell, new BigDecimal("23.5"),
        profitDTO);//70229

    profitDTO.getTradeDetailList().forEach(x -> WLog.info(x.toString()));

    assertTrue(profitSvc.getGrossProfit(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("-28608")) == 0);
    assertTrue(profitSvc.getGrossProfitRatio(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("-0.07")) == 0);
    assertTrue(profitSvc.getProfitRatio(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("0.40")) == 0);
    assertTrue(profitSvc.getProfitFactor(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("0.33")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210419"))
        .compareTo(new BigDecimal("1002150")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210421"))
        .compareTo(new BigDecimal("992829")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210423"))
        .compareTo(new BigDecimal("1005034")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210425"))
        .compareTo(new BigDecimal("986736")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210426"))
        .compareTo(new BigDecimal("987361")) == 0);
    assertTrue(profitSvc.getNetValue(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("971392")) == 0);
    assertTrue(profitSvc.getAcctBalance(profitDTO, WType.strToDate("20210419"))
        .compareTo(new BigDecimal("1000000")) == 0);

    assertTrue(profitSvc.getAcctBalance(profitDTO, WType.strToDate("20210422"))
        .compareTo(new BigDecimal("840865")) == 0);
    assertTrue(profitSvc.getAcctBalance(profitDTO, WType.strToDate("20210423"))
        .compareTo(new BigDecimal("908106")) == 0);
    assertTrue(profitSvc.getAcctBalance(profitDTO, WType.strToDate("20210426"))
        .compareTo(new BigDecimal("999254")) == 0);

    //勝率
    assertTrue(profitSvc.getProfitRatio(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("0.4")) == 0);
    //期望值
    assertTrue(profitSvc.getExceptedValue(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("-20036.40")) == 0);
    //盈虧金額比
    assertTrue(profitSvc.getProfitAmtRatio(profitDTO, WType.strToDate("20210427"))
        .compareTo(new BigDecimal("-0.3342")) == 0);
  }
}
