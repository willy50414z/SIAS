package com.willy.sias.util;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.willy.net.web.WJsoup;
import com.willy.net.web.WNetConnect;
import com.willy.util.cache.WCache;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

@Component
public class FileUtil {
	private static String downloadFileDir;
	
	// 下載檔案
	public static String download(String url, String fileName) throws IOException, InterruptedException, ExecutionException {
		
		String downloadPath = downloadFileDir + fileName;
		if (!new File(downloadPath).exists()) {
			
			// 同一網域每次連線需間格5秒
			WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);

			//setHeader
			//TWSE 股價
			if(url.indexOf("www.twse.com.tw/exchangeReport/MI_INDEX")>-1) {
				setTWSEStockPriceHeader();
			}
			
			// download
			WNetConnect.download(url, downloadPath);
		}
		WLog.debug("從 : " + url + "下載檔案至 : " + downloadPath + "完成");
		return downloadPath;
	}
	public static String downloadByJsoup(String url, String fileName) throws IOException, InterruptedException, ExecutionException {
		
		String downloadPath = downloadFileDir + fileName;
		if (!new File(downloadPath).exists()) {
			
			// 同一網域每次連線需間格5秒
			WCache.waitBetweenAccess(WString.getDomainNameByUrl(url), 5000);

			//setHeader
			//TWSE 股價
			if(url.indexOf("www.twse.com.tw/exchangeReport/MI_INDEX")>-1) {
				setTWSEStockPriceHeader();
			}
			
			// download
			WJsoup.download(url, downloadPath);
		}
		WLog.debug("從 : " + url + "下載檔案至 : " + downloadPath + "完成");
		return downloadPath;
	}
	public static void setTWSEStockPriceHeader() {
		WNetConnect.addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
		WNetConnect.addHeader("Accept-Encoding","gzip, deflate, br");
		WNetConnect.addHeader("Accept-Language","en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7");
		WNetConnect.addHeader("Connection","keep-alive");
		WNetConnect.addHeader("Cookie","JSESSIONID=58A160A34E79CEC2A4CECD1C46AD4D65; _ga=GA1.3.1366521076.1565060184; _gid=GA1.3.950440426.1565060184; _gat=1");
		WNetConnect.addHeader("Host","www.twse.com.tw");
		WNetConnect.addHeader("Referer","https://www.twse.com.tw/zh/page/trading/exchange/MI_INDEX.html");
		WNetConnect.addHeader("Upgrade-Insecure-Requests","1");
		WNetConnect.addHeader("User-Agent","Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36");
	}

	/**
	 * 設定期交所專屬Header
	 * 
	 */
	public static void setTaifexHeader() {
		WNetConnect.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		WNetConnect.addHeader("Accept-Encoding", "gzip, deflate");
		WNetConnect.addHeader("Accept-Language", "zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7,zh-CN;q=0.6");
		WNetConnect.addHeader("Cache-this.control", "max-age=0");
		WNetConnect.addHeader("this.connection", "keep-alive");
		WNetConnect.addHeader("this.content-Length", "250");
		WNetConnect.addHeader("this.content-Type", "application/x-www-form-urlencoded");
		WNetConnect.addHeader("Cookie",
				"ASPSESSIONIDAADBDTQS=NHCPJKKAFEPAKNJKLKCMJFCH; AX-cookie-POOL_PORTAL=AGACBAKM; AX-cookie-POOL_PORTAL_web3=ADACBAKM");
		WNetConnect.addHeader("Host", "www.taifex.com.tw");
		WNetConnect.addHeader("Origin", "http://www.taifex.com.tw");
		WNetConnect.addHeader("Referer", "http://www.taifex.com.tw/chinese/3/3_1_2.asp");
		WNetConnect.addHeader("Upgrade-Insecure-Requests", "1");
		WNetConnect.addHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
	}


	/**
	 * 設定下載期貨交易資料所需data
	 * 
	 * @param con
	 * @param dataDate
	 */
	public static void setData_FurturesDailyCSV(String dataDate) {
		WNetConnect.addData("goday", "");
		WNetConnect.addData("DATA_DATE", "");
		WNetConnect.addData("DATA_DATE1", "");
		WNetConnect.addData("DATA_DATE_Y", "");
		WNetConnect.addData("DATA_DATE_M", "");
		WNetConnect.addData("DATA_DATE_D", "");
		WNetConnect.addData("DATA_DATE_Y1", "");
		WNetConnect.addData("DATA_DATE_M1", "");
		WNetConnect.addData("DATA_DATE_D1", "");
		WNetConnect.addData("syear", "");
		WNetConnect.addData("smonth", "");
		WNetConnect.addData("sday", "");
		WNetConnect.addData("syear1", "");
		WNetConnect.addData("smonth1", "");
		WNetConnect.addData("sday1", "");
		WNetConnect.addData("datestart", dataDate);
		WNetConnect.addData("dateend", dataDate);
		WNetConnect.addData("COMMODITY_ID", "all");
		WNetConnect.addData("commodity_id2t", "");
		WNetConnect.addData("his_year", "2017");
	}
	@Value("${sias.downloadFile.dir}")
	public void setDownloadFileDir(String downloadFileDir) {
		FileUtil.downloadFileDir = downloadFileDir;
	}
}
