package com.willy.sias.backtest;

import com.willy.r.util.WRCmd;
import com.willy.sias.backtest.dto.DividendFillCDivDTO;
import com.willy.sias.backtest.dto.MethodInfoDTO;
import com.willy.sias.backtest.processor.CurrentChipStatusProcessor;
import com.willy.sias.backtest.service.ChipIdxService;
import com.willy.sias.db.repository.SysConfigRepository;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.willy.net.utils.StringUtils;
import com.willy.sias.anno.SvcInfo;
import com.willy.sias.anno.SvcMethodInfo;
import com.willy.sias.backtest.dto.BackTestInfoDTO;
import com.willy.sias.backtest.processor.BackTestProcessor;
import com.willy.sias.backtest.processor.CurrentTechStatusProcessor;
import com.willy.sias.backtest.service.BasicService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.CheckExeUtil;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.type.WType;

@SpringBootApplication
@ComponentScan("com.willy")
@EnableTransactionManagement(proxyTargetClass = true)
@Profile("!test")
@EnableCaching
public class StartBackTestService implements CommandLineRunner {

  public static void main(String[] args)
      throws IOException, InterruptedException, ExecutionException, JSONException {
    SpringApplication.run(StartBackTestService.class, args);
  }

  BackTestInfoDTO btInfo = new BackTestInfoDTO();
  @Autowired
  private BackTestProcessor btProcessor;
  @Autowired
  private CurrentTechStatusProcessor curTechStatusProcessor;
  @Autowired
  private CompanyCategoryRepository ccRepo;
  @Autowired
  private DataTableDao dtDao;
  @Autowired
  private ChipIdxService ciSvc;
  @Autowired
  private CurrentChipStatusProcessor cdSvc;
  @Autowired
  private SysConfigRepository scRepo;
  @Autowired
  private WRCmd cmd;

  @Override
  public void run(String... args) throws Exception {
    if (!CheckExeUtil.isExecute(new Exception())) {
      return;
    }
    //回測
    String strategyCode = "SIAS_6";
    runDynamicStrategy(
        "{\"strategyFilePath\":\"D:/Code/Git/SIAS/Source/sias/sias-backtest/src/main/java/com/willy/sias/backtest/strategy/"
            + strategyCode + ".java\"" + ",\"strategyName\":\"" + strategyCode
            + "\",\"sidSet\":[-1],\"funds\":5000000"
            + ",\"startDate\":\"20190101\",\"endDate\":\"20220531\""
            + ",\"testDesc\":\"籌碼測試\"}");
    //技術面狀態
//		exeCurTechStatusProcessor(494, WType.strToDate("20220101"));

    //籌碼面狀態
//    cdSvc.exec();

    //列出指定方法的描述
//    printMethodInfo();

    System.exit(0);
  }

  @Bean
  public CurrentChipStatusProcessor getCurrentChipStatusProcessor() {
    return new CurrentChipStatusProcessor(494, WType.strToDate("20210902"));
  }

  /**
   * 動態上傳策略進行回測
   *
   * @param args
   * @throws Exception
   */
  private void runDynamicStrategy(String... args) throws Exception {
    // 取出交易策略檔案路徑
    if (args != null && args.length > 0 && StringUtils.isNotEmpty(args[0])) {
      // Convert to BackTestInfo
      Gson gs = new Gson();
      BackTestInfoDTO btInfo = null;
      try {
        //parse backtest parameter
        btInfo = gs.fromJson(args[0], BackTestInfoDTO.class);
        if (btInfo.getSidSet() != null && btInfo.getSidSet().iterator().next().compareTo(-1) == 0) {
          btInfo.getSidSet().addAll(ccRepo.findSidWithoutDerivative());
        }

        // remove exclude sid
        List<Integer> excludesidList = btInfo.getSidSet().stream()
            .filter(sid -> sid.compareTo(0) < 0)
            .collect(Collectors.toList());
        excludesidList.addAll(
            excludesidList.stream().map(sid -> sid * -1).collect(Collectors.toList()));
        btInfo.getSidSet().removeAll(excludesidList);
      } catch (JsonSyntaxException e) {
        WLog.error(e);
        WLog.error(
            "Please enter the json args, {\"strategyFilePath\":\"D:/S4.java\",\"strategyName\":\"S4\",\"sidSet\":[1,2,3],\"funds\":9999,\"startDate\":\"20200101\",\"endDate\":\"20210723\",\"testDesc\":\"TETT\"}");
      }

      // 移動策略檔並執行回測
      File strategyFile = new File(btInfo.getStrategyFilePath());
      if (strategyFile.exists()) {
        btInfo.setStrategyFilePath(btInfo.getStrategyFilePath());
        // 執行回測
        btProcessor.process(btInfo);
      } else {
        WLog.error("strategyFilePath[" + strategyFile.getAbsolutePath() + "] is not exist");
      }
    } else {
      WLog.error(
          "Please enter the json args, {StrategyFilePath:, Funds:, {\"strategyFilePath\":\"D:/S4.java\",\"strategyName\":\"S4\",\"sidSet\":[1,2,3],\"funds\":9999,\"startDate\":\"20200101\",\"endDate\":\"20210723\",\"testDesc\":\"\"}");
    }
  }

  /**
   * 分析目前技術型態
   *
   * @param sid
   * @param date
   * @throws Exception
   */
  private void exeCurTechStatusProcessor(int sid, Date date) throws Exception {
    curTechStatusProcessor.setSid(sid);
    curTechStatusProcessor.setAnalyzedate(date);
    curTechStatusProcessor.process();
  }

  private void printMethodInfo() {
    List<MethodInfoDTO> miDto = new ArrayList<>();
    Map<String, Object> svcClassMap = BeanFactory.context.getBeansWithAnnotation(SvcInfo.class);
    svcClassMap.forEach((beanName, classObj) -> {
      SvcInfo svcInfo = classObj.getClass().getAnnotationsByType(SvcInfo.class)[0];
//			System.out.println(svcInfo.value());
      WReflect.getFieldsWithAnnotation(classObj, SvcMethodInfo.class).forEach(method -> {
        SvcMethodInfo svcMethodInfo = method.getAnnotationsByType(SvcMethodInfo.class)[0];
//				System.out.print("\t" + svcMethodInfo.methodDesc() + "\t" + svcMethodInfo.resultDesc() + "\t");

        StringBuffer sb = new StringBuffer("(");
        Arrays.asList(method.getParameters()).forEach(x -> {
          Parameter par = (Parameter) x;
          sb.append(x.toString()).append(", ");
        });
        sb.setLength(sb.length() - 2);
        sb.append(")");
        miDto.add(new MethodInfoDTO(svcInfo.value(), svcMethodInfo.methodDesc(),
            svcMethodInfo.resultDesc(), classObj.getClass().getSimpleName(),
            method.getName() + sb));
      });
    });

    miDto.stream().sorted(new Comparator<MethodInfoDTO>() {
      @Override
      public int compare(MethodInfoDTO o1, MethodInfoDTO o2) {
        return o1.getMethodDesc()
            .compareTo(o2.getMethodDesc());
      }
    }).sorted(new Comparator<MethodInfoDTO>() {
      @Override
      public int compare(MethodInfoDTO o1, MethodInfoDTO o2) {
        return o1.getSvcMethodDesc()
            .compareTo(o2.getSvcMethodDesc());
      }
    }).sorted(new Comparator<MethodInfoDTO>() {
      @Override
      public int compare(MethodInfoDTO o1, MethodInfoDTO o2) {
        return o1.getSvcType()
            .compareTo(o2.getSvcType());
      }
    }).forEach(mi -> System.out.println(
        mi.getSvcType() + "\t" + mi.getSvcMethodDesc() + "\t" + mi.getResultDesc() + "\t"
            + mi.getClassName() + "\t" + mi.getMethodDesc()));
  }

  @Autowired
  BasicService bs;

  private void test() throws Exception {
    List<DividendFillCDivDTO> divDtoList = bs.getDaysOfFillCashDiv(737, -1).stream()
        .filter(div -> div.getDivPo().getCDiv() != null && div.getDivPo().getExCDivDate() != null)
        .collect(Collectors.toList());

    // 至少要有5年的股利發放紀錄
    System.out.println("股利發放紀錄年數["
        + divDtoList.stream()
        .map(div -> WDate.getDateInfo(div.getDivPo().getDataDate(), Calendar.YEAR))
        .collect(Collectors.toSet()).size()
        + "]");

    for (DividendFillCDivDTO div : divDtoList) {
      System.out.println(
          "" + div.getDivPo().getDivBelongs() + " - " + div.getFillDays() + " - " + div.getDivPo()
              .getCDiv());
    }

    List<TimeSeriesDTO<BigDecimal>> epsList = dtDao.findTsList(804, 79);
    epsList.stream().filter(eps -> eps.getDate() != null && eps.getValue() != null).forEach(eps -> {
      WLog.info("date[" + WType.dateToStr(eps.getDate()) + "]eps[" + eps.getValue() + "]");
    });
  }

  private void exeStockTechStatus(int sid, Date date) throws Exception {
    // KD
    curTechStatusProcessor.setSid(sid);
    curTechStatusProcessor.setAnalyzedate(date);
    curTechStatusProcessor.process();
  }

}
