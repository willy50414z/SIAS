package com.willy.sias.web.service;

import com.willy.sias.db.dao.ConfigDao;
import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import com.willy.util.string.WRegex;
import com.willy.util.type.WType;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.codehaus.plexus.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryEditorService {

  @Autowired
  private SysConfigRepository scRepo;
  @Autowired
  private CompanyCategoryRepository ccRepo;
  @Autowired
  private ConfigDao cfgDao;

  public String getFormattedCateString(String unFormattedString) {
    StringBuilder sb = new StringBuilder();
    try {
      JSONObject js = new JSONObject(unFormattedString);
      Iterator keyIt = js.keys();
      List<String> sidCodeList = new ArrayList<>();
      while (keyIt.hasNext()) {
        String sidStr = keyIt.next().toString();
        JSONArray catJa = js.getJSONArray(sidStr);
        sb.append(sidStr).append(" : [");
        //key is sid code
        if (WRegex.match(sidStr, "[\\w]+")) {
          sidCodeList.add(sidStr);
        }
        for (int i = 0; i < catJa.length(); i++) {
          sb.append(" ").append(catJa.getString(i)).append(",");
          //value is sid code
          if (!WRegex.match(sidStr, "[\\w]+")) {
            sidCodeList.add(catJa.getString(i));
          }
        }
        sb.setLength(sb.length() - 1);
        sb.append("]\r\n");
      }
      sb.setLength(sb.length() - 2);

      //check sid is exist
      List<SysConfigPO> scList = scRepo.findByParentIdIsAndCfgValueIn(ParentKey.SID.getKey(),
          sidCodeList);
      if (scList.size() != sidCodeList.size()) {
        scList.forEach(sc -> sidCodeList.remove(sc.getCfgValue()));
        return "part of sid codes are not exist " + scList;
      }
    } catch (Exception e) {
      WLog.error(e);
      sb.append("\r\nremains data convert failed");
    }
    return sb.toString();
  }

  public String saveCatToDb(String unFormattedString) {
    try {
      //customize cat date is 19700101
      List<CompanyCategoryPO> ccList = new ArrayList<>();
      JSONObject js = new JSONObject(unFormattedString);
      Iterator keyIt = js.keys();
      while (keyIt.hasNext()) {
        String sidStr = keyIt.next().toString();
        JSONArray catJa = js.getJSONArray(sidStr);
        if (WRegex.match(sidStr, "[\\w]+")) {
          //merge cat to SysConfig
          List<Integer> cfgIdList = mergeCatToSysConfig(catJa);
          //assemble CompanyCatrgory List for saving
          assembleCompanyCategory(Const.CUSTOMIZE_CATEGORY_DATA_DATE,
              scRepo.findByParentIdIsAndCfgValueIs(ParentKey.SID.getKey(), sidStr).getCfgId(),
              cfgIdList, ccList);
        } else {
          JSONArray ja = new JSONArray();
          ja.put(sidStr);
          List<Integer> cfgIdList = mergeCatToSysConfig(ja);
          //catName:sidList
          for (int i = 0; i < catJa.length(); i++) {
            CompanyCategoryPO cc = new CompanyCategoryPO();
            cc.setDataDate(Const.CUSTOMIZE_CATEGORY_DATA_DATE);
            cc.setSid(
                scRepo.findByParentIdIsAndCfgValueIs(ParentKey.SID.getKey(), catJa.getString(i))
                    .getCfgId());
            cc.setCategoryId(cfgIdList.get(0));
            ccList.add(cc);
          }
        }
      }
      Set<Integer> mergePoSet = ccList.stream().map(CompanyCategoryPO::getSid)
          .collect(Collectors.toSet());
      if (!CollectionUtils.isEmpty(ccList)) {
        ccRepo.deleteByDataDateAndSidIn(Const.CUSTOMIZE_CATEGORY_DATA_DATE, mergePoSet);
        ccRepo.saveAll(ccList);
      }
      return "success";
    } catch (Exception e) {
      WLog.error(e);
      return "save failed";
    }
  }

  private void assembleCompanyCategory(Date date, Integer sid, List<Integer> cfgIdList,
      List<CompanyCategoryPO> ccList) {
    cfgIdList.forEach(cfgId -> {
      CompanyCategoryPO cc = new CompanyCategoryPO();
      cc.setDataDate(date);
      cc.setSid(sid);
      cc.setCategoryId(cfgId);
      ccList.add(cc);
    });

  }

  private List<Integer> mergeCatToSysConfig(JSONArray catNameJa) {
    //convert cat name JSONArray to List
    List<String> catNameList = new ArrayList<>();
    for (int i = 0; i < catNameJa.length(); i++) {
      try {
        catNameList.add(catNameJa.getString(i));
      } catch (JSONException e) {
      }
    }

    //remove duplicate cat name
    List<SysConfigPO> sysCfgList = scRepo.findByParentIdIsAndCfgDescIn(
        ParentKey.CustomerizedCompanyCategory.getKey(), catNameList);
    sysCfgList.forEach(sysCfg -> catNameList.remove(sysCfg.getCfgDesc()));

    //assemble PO List
    if (catNameList.size() > 0) {
      List<SysConfigPO> scList = new ArrayList<>();
      catNameList.forEach(catName -> {
        SysConfigPO sc = new SysConfigPO();
        sc.setCfgDesc(catName);
        sc.setParentId(ParentKey.CustomerizedCompanyCategory.getKey());
        scList.add(sc);
      });
      scRepo.saveAll(scList);
      sysCfgList.addAll(
          scRepo.findByParentIdIsAndCfgDescIn(ParentKey.CustomerizedCompanyCategory.getKey(),
              catNameList));
    }

    //return cfgIdList
    return sysCfgList.stream().map(SysConfigPO::getCfgId).collect(
        Collectors.toList());
  }

  public String queryCompanyCatListByQryStrAndQryType(String qryStr, String qryType)
      throws JSONException {
    if (StringUtils.isBlank(qryStr) || StringUtils.isBlank(qryType)) {
      return "qryStr[" + qryStr + "] or qryType[" + qryType + "] is blank";
    } else {
      JSONObject result = new JSONObject();
      if (Const.CATEGORY_QRY_TYPE_SID.equals(qryType)) {
        SysConfigPO sc = scRepo.findByParentIdIsAndCfgValueIs(
            ParentKey.SID.getKey(), qryStr);
        if (sc == null) {
          return "can't find sid by qryStr[" + qryStr + "]";
        }
        List<CompanyCategoryPO> ccList = ccRepo.findByDataDateIsAndSidIs(
            Const.CUSTOMIZE_CATEGORY_DATA_DATE, sc.getCfgId());
        JSONArray ja = new JSONArray();
        scRepo.findAllById(
                ccList.stream().map(CompanyCategoryPO::getCategoryId).collect(Collectors.toList()))
            .stream().map(SysConfigPO::getCfgDesc).forEach(ja::put);
        result.put(sc.getCfgValue(), ja);
        return result.toString();
      } else if (Const.CATEGORY_QRY_TYPE_CAT.equals(qryType)) {
        //query cats
        List<SysConfigPO> catScList = scRepo.findByParentIdIsAndCfgDescLike(
            ParentKey.CustomerizedCompanyCategory.getKey(), "%" + qryStr + "%");
        for (SysConfigPO catSc : catScList) {
          //query sid
          JSONArray ja = new JSONArray();
          Set<CompanyCategoryPO> ccSet = ccRepo.findByDataDateAndCategoryIdEquals(
              Const.CUSTOMIZE_CATEGORY_DATA_DATE, catSc.getCfgId());
          scRepo.findAllById(
                  ccSet.stream().map(CompanyCategoryPO::getSid).collect(Collectors.toList()))
              .forEach(sc -> ja.put(sc.getCfgValue() + " - " + sc.getCfgDesc()));
          result.put(catSc.getCfgDesc(), ja);
        }
        return result.toString();
      } else {
        return "qryType[" + qryType + "] is not illegal";
      }
    }
  }
}
