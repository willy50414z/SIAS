package com.willy.sias.backtest.processor;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.willy.file.serialize.WSerializer;
import com.willy.sias.backtest.dto.BackTestInfoDTO;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.service.ProfitAnalyzeService;
import com.willy.sias.backtest.service.ProfitDTOService;
import com.willy.sias.backtest.strategy.StrategyArg;
import com.willy.sias.backtest.strategy.TradeStrategy;
import com.willy.sias.db.po.BackTestLogDPO;
import com.willy.sias.db.po.BackTestLogHPO;
import com.willy.sias.db.repository.BackTestLogDRepository;
import com.willy.sias.db.repository.BackTestLogHRepository;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

@Service
public class BackTestProcessor {
	@Autowired
	private ProfitAnalyzeService paService;
	@Autowired
	private BackTestLogHRepository backTestHRepo;
	@Autowired
	private BackTestLogDRepository backTestDRepo;
	@Autowired
	private WSerializer serializer;
	@Autowired
	private CaculStaticsFields csf;
	protected TradeStrategy strategy;
	private String msg = "";
	protected BackTestInfoDTO backTestInfoDTO;
	private ProfitDTO profitAnalyzeResult;
	@Autowired
	private ProfitDTOService profitService;
	private Integer testId;

	public BackTestProcessor() {
		super();
	}

	public Integer process(BackTestInfoDTO backTestInfoDTO) {
		this.backTestInfoDTO = backTestInfoDTO;
		WLog.info("Back test start");
		if (this.testId == null && this.strategy == null) {
			setTestId();
			WLog.info("TestID[" + this.testId + "]");
		}

		// 共用驗證
		WLog.info("Common Validate");
		msg = commonValidate();
		if (!StringUtils.isEmpty(msg)) {
			saveTradeLog(msg);
			return this.testId;
		}

		// 設定策略所需參數
		WLog.info("Set Strategy Args");
		msg = setStrategyArgs();
		if (!StringUtils.isEmpty(msg)) {
			saveTradeLog(msg);
			return this.testId;
		}

		// 回測並計算績效統計資料
		WLog.info("Profit Analyze");
		msg = profitAnalyze();
		if (!StringUtils.isEmpty(msg)) {
			saveTradeLog(msg);
			return this.testId;
		}

		// 儲存回測LOG
		WLog.info("saveTradeLog");
		msg = saveTradeLog();
		if (!StringUtils.isEmpty(msg)) {
			saveTradeLog(msg);
			return this.testId;
		}

		WLog.info("run async caculate statistics batch");
		msg = runStatisticsBatch();
		if (!StringUtils.isEmpty(msg)) {
			saveTradeLog(msg);
			return this.testId;
		}

		WLog.info("Back test end");

		return this.testId;
	}

	private String commonValidate() {
		return "";
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private String setStrategyArgs() {
		try {
			String strategyArgs = this.backTestInfoDTO.getStrategyArgs();
			if (strategyArgs == null) {
				return "";
			}
			JSONObject jsonArgs = new JSONObject(strategyArgs);
			this.strategy.getClass().getDeclaredFields();
			Field[] fieldList = this.strategy.getClass().getDeclaredFields();
			for (Field field : fieldList) {
				// 篩選出策略需要的參數欄位
				if (field.getDeclaredAnnotation(StrategyArg.class) != null) {
					if (field.getType().getSimpleName().equals("List")) {
						// 建立參數物件
						String typeName = field.getGenericType().getTypeName();
						String genericClassPath = typeName.substring(typeName.indexOf("<") + 1, typeName.length() - 1);
						Object argObj;

						// 建立List
						List argsList = new ArrayList<>();
						JSONObject arg;
						JSONArray argsObjAr = jsonArgs.getJSONArray(field.getName());
						for (int argIndex = 0; argIndex < argsObjAr.length(); argIndex++) {
							arg = argsObjAr.getJSONObject(argIndex);
							argObj = Class.forName(genericClassPath).newInstance();
							Iterator<String> keyIt = arg.keys();
							while (keyIt.hasNext()) {
								String key = keyIt.next();
								WReflect.setFieldValue(argObj, key, arg.get(key));
							}
							argsList.add(argObj);
						}

						// 塞入Strategy
						WReflect.setFieldValue(this.strategy, field.getName(), argsList);
					} else {
						WReflect.setFieldValue(this.strategy, field.getName(),
								WType.objToSpecificType(jsonArgs.get(field.getName()), field.getType()));
					}
				}
			}
		} catch (Exception e) {
			WLog.error(e);
			return "Strategy args set failed. Args[" + this.backTestInfoDTO.getStrategyArgs() + "]";
		}
		return this.strategy.checkArgs();
	}

	private String profitAnalyze() {
		try {
			// 執行回測
			profitAnalyzeResult = paService.getStrategyTradeResult(this.backTestInfoDTO);
			// 先計算一些簡單的統計資訊
			if (profitAnalyzeResult.getTradeDetailList().size() > 0) {
				profitService.setSimpleStatisticFields(profitAnalyzeResult);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			profitAnalyzeResult = null;
			WLog.error(e);
			return "Profit analyze fail\r\n" + WType.exceptionToString(e);
		}
		return "";
	}

	@Transactional
	public String saveTradeLog() {
		// 將結果存入DB
		try {
			BackTestLogHPO backTestLogHPo = new BackTestLogHPO();
			backTestLogHPo.setTestId(this.testId);
			backTestLogHPo.setTestDesc(backTestInfoDTO.getTestDesc());
			backTestLogHPo.setStrategyFilePath(new File(backTestInfoDTO.getStrategyFilePath()).getAbsolutePath());
			backTestHRepo.save(backTestLogHPo);

			BackTestLogDPO backTestLogDPo = new BackTestLogDPO();
			backTestLogDPo.setTestId(testId);
			JSONObject detailJSON = new JSONObject();
			detailJSON.put("ProfitPO", serializer.serializeAndCompressToString(profitAnalyzeResult));
			backTestLogDPo.setTradeDetail(WString.compress(detailJSON.toString()).getBytes(StandardCharsets.UTF_8));
			backTestDRepo.save(backTestLogDPo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "error on save back test log to db\r\n" + WType.exceptionToString(e);
		}
		return "";
	}

	// 回測過程發生Exception，只存回測Header
	@Transactional
	public String saveTradeLog(String errMsg) {
		// 將結果存入DB
		try {
			WLog.error(errMsg);
			BackTestLogHPO backTestLogHPo = new BackTestLogHPO();
			backTestLogHPo.setTestId(this.testId);
			backTestLogHPo.setTestDesc(backTestInfoDTO.getTestDesc());
			backTestLogHPo.setErrMsg(errMsg.substring(0, Math.min(errMsg.length(), 500)));
			backTestHRepo.save(backTestLogHPo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			WLog.error("error on save back test log to db", e);
		}
		return "";
	}

	private String runStatisticsBatch() {
		csf.setTestId(this.testId);
		try {
			FutureTask<String> future = new FutureTask<String>(csf);
			new Thread(future).start();
			while (!future.isDone()) {
				Thread.sleep(10000);
			}
			return future.get();
		} catch (Exception e) {
			return WType.exceptionToString(e);
		}
	}

	private void setTestId() {
		this.testId = backTestHRepo.findNextTestId();
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}
}

@Component
class CaculStaticsFields implements Callable<String> {
	@Autowired
	private BackTestLogDRepository btLogDRepo;
	@Autowired
	private WSerializer serializer;
	@Autowired
	private ProfitDTOService profitService;
	private Integer testId;

	public String call() throws Exception {
		List<BackTestLogDPO> btlDList = btLogDRepo.findByTestId(testId);
		BackTestLogDPO btlD = btlDList.get(0);
		String tradeDetailStr;
		try {
			tradeDetailStr = WString.unCompress(new String(btlD.getTradeDetail(), StandardCharsets.UTF_8));
			JSONObject tradeDetailJson = new JSONObject(tradeDetailStr);
			tradeDetailStr = tradeDetailJson.getString("ProfitPO");
			// 反序列化並解壓縮，取得ProfitPO物件
			ProfitDTO profitDTO = (ProfitDTO) serializer.unCompressAndUnserializeByString(tradeDetailStr);
			// 計算績效統計資料
			if (profitDTO.getTradeDetailList().size() > 0) {
				profitService.setComplexStatisticFields(profitDTO);
			}
			JSONObject detailJSON = new JSONObject();
			detailJSON.put("ProfitPO", serializer.serializeAndCompressToString(profitDTO));
			btlD.setTradeDetail(WString.compress(detailJSON.toString()).getBytes(StandardCharsets.UTF_8));
			btLogDRepo.save(btlD);
		} catch (Exception e) {
			e.printStackTrace();
			return "Error occur while caculate statics fields - " + WType.exceptionToString(e);
		}
		return "";
	}

	public Integer getTestId() {
		return testId;
	}

	public void setTestId(Integer testId) {
		this.testId = testId;
	}

}