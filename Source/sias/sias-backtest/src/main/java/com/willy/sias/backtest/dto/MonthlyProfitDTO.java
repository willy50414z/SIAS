package com.willy.sias.backtest.dto;

import com.willy.sias.backtest.service.StatisticService;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.willy.util.log.WLog;

import lombok.Data;

@Data
@Component
@Scope("prototype")
public class MonthlyProfitDTO {
	private int month;
	private BigDecimal mean;
	private BigDecimal sd;
	private BigDecimal growRatio;
	private List<BigDecimal> growthList = new ArrayList<> ();
	@Autowired
	private StatisticService statisticService;
	public void setGrowthList(List<BigDecimal> growthList) {
		this.growthList = growthList;
		if(CollectionUtils.isNotEmpty(this.growthList) && this.growthList.size()>10) {
			this.mean = statisticService.getMean(this.growthList);
			this.sd = statisticService.getSd(this.growthList);
			this.growRatio = statisticService.getSuccessRatio(this.growthList, ">0");
		}else {
			this.mean = null;
			this.sd = null;
			this.growRatio = null;
			WLog.info(this.month+"月成長率資料數量過少，無法進行統計分析，資料數:"+(CollectionUtils.isEmpty(this.growthList)?"0":String.valueOf(this.growthList.size())));
		}
	}
	@Override
	public String toString() {
		return "月份[" + month + "], 平均收益率[" + mean + "], 標準差[" + sd + "], 勝率[" + growRatio + "]";
	}
	
}