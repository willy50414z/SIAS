package com.willy.sias.db.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class KDJStatusDTO {
	List<TimeSeriesDTO<KDJDTO>> kdjTsList;
	private final BigDecimal highKDValue = new BigDecimal(80);
	private final BigDecimal lowKDValue = new BigDecimal(20);
	
	/**
	 * 	確認KD位階
	 * @param date
	 * @return -1: 低檔鈍化, 0:沒有鈍化, 1: 高檔鈍化 
	 */
	public int getKDOutDays(Date date) {
		List<TimeSeriesDTO<KDJDTO>> partkdjTsList = kdjTsList.stream()
				.filter(ts -> WDate.beforeOrEquals(ts.getDate(), date)).collect(Collectors.toList());
		if(partkdjTsList == null) {
			WLog.error("Can't get partkdjTsList for Date["+date+"]");
			return Const.ERROR_INT_RC;
		}
		if(partkdjTsList.size()<3) {
			WLog.error("Can't get enough partkdjTsList for Date["+date+"]partkdjTsList.size()["+partkdjTsList.size()+"]");
			return Const.ERROR_INT_RC;
		}
		
		int bluntDays = 0;
		for (int i = partkdjTsList.size() - 1; i > -1; i--) {
			if (partkdjTsList.get(i).getValue().getK().compareTo(highKDValue) >= 0
					|| partkdjTsList.get(i).getValue().getD().compareTo(highKDValue) >= 0) {
				if (bluntDays < 0) {
					break;
				}
				bluntDays++;
			}
			if (partkdjTsList.get(i).getValue().getK().compareTo(lowKDValue) <= 0
					|| partkdjTsList.get(i).getValue().getD().compareTo(lowKDValue) <= 0) {
				if (bluntDays > 0) {
					break;
				}
				bluntDays--;
			}
			if(bluntDays == 0) {
				break;
			}
		}
		return bluntDays;
	}
	
	/**
	 * 	計算已經交叉幾個交易日了
	 * @param date
	 * @return
	 */
	public int getCrossDays(Date date) {
		int crossDays = 0;
		for (int i = kdjTsList.size() - 1; i > -1; i--) {
			if(WDate.beforeOrEquals(kdjTsList.get(i).getDate(), date)) {
				if(kdjTsList.get(i).getValue().getK().compareTo(kdjTsList.get(i).getValue().getD())>0) {
					if(crossDays < 0) {
						break;
					}
					crossDays++;
				} else if(kdjTsList.get(i).getValue().getK().compareTo(kdjTsList.get(i).getValue().getD())<0){
					if(crossDays > 0) {
						break;
					}
					crossDays--;
				}
			}
		}
		return crossDays;
	}
	
	public int getKDSlope(Date date) {
		List<TimeSeriesDTO<KDJDTO>> kdjList = kdjTsList.stream().filter(kdj -> WDate.beforeOrEquals(kdj.getDate(), date)).collect(Collectors.toList());
		if(kdjList.size()<2) {
			WLog.error("kdjList size < 2, can't get kdj slope");
			return 0;
		}
		KDJDTO lastKDJ = kdjList.get(kdjList.size() - 1).getValue();
		KDJDTO last2KDJ = kdjList.get(kdjList.size() - 2).getValue();
		return lastKDJ.getK().compareTo(last2KDJ.getK()) + lastKDJ.getD().compareTo(last2KDJ.getD());
	}
}
