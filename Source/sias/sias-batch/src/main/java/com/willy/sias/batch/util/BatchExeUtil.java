package com.willy.sias.batch.util;

import com.willy.sias.batch.processor.BaseBatchProcessor;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.EnumSet.BatchName;
import com.willy.sias.util.config.Const;
import com.willy.util.type.WType;
import java.util.Date;
import java.util.Map;
import org.springframework.stereotype.Service;

@Service
public class BatchExeUtil {
	public void exec(BatchName batchName, String date, Map<String, String> paramsMap) throws Exception {
		Class<?> batchPorcessorClass = Class.forName(Const.batchProcessorPackage + batchName);
		BaseBatchProcessor batchPorcessor = (BaseBatchProcessor) BeanFactory.getBean(batchPorcessorClass);
		batchPorcessor.setStrDataDate(date);
		if(paramsMap != null && paramsMap.size()>0) {
			paramsMap.keySet().forEach(key -> batchPorcessor.putMapParam(key, paramsMap.get(key)));
		}
		batchPorcessor.process();
	}
	public void exec(BatchName batchName, Date date, Map<String, String> paramsMap) throws Exception {
			exec(batchName, WType.dateToStr(date), paramsMap);
	}
}
