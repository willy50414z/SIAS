package com.willy.sias.util.config;

import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Const {
	@Value("${sias.strategy.tmpFileDir}")
	public String strategyTmpFileDir;
	
	public final static String _ENCODING_MS950 = "MS950";
	public final static String _ENCODING_UTF8 = "UTF-8";
	
	public final static BigDecimal _BIGDECIMAL_0 = new BigDecimal(0);
	public final static BigDecimal _BIGDECIMAL_1 = new BigDecimal(1);
	public final static BigDecimal _BIGDECIMAL_m1 = new BigDecimal(-1);
	public final static BigDecimal _BIGDECIMAL_100 = new BigDecimal(100);
	
	public final static String _SID = "SID";

	public final static String _STRATEGY_TEST_COMMON_VALIDATE_SUCCESS = "策略測試分析資料成功";
	public final static String _STRATEGY_TEST_INIT_DATA_SUCCESS = "策略測試初始資料成功";
	public final static String _STRATEGY_TEST_CUSTOMERIZE_VALIDATE_SUCCESS = "策略測試篩選資料成功";
	public final static String _STRATEGY_TEST_FILTER_DATA_SUCCESS = "策略測試篩選資料成功";
	public final static String _STRATEGY_TEST_ANALYZE_DATA_SUCCESS = "策略測試分析資料成功";
	public final static String _STRATEGY_TEST_SUCCESS = "策略測試成功";
	public final static String _STRATEGY_TEST_UNEXCEPT_ERROR = "策略測試發生未預期錯誤";
	public final static String _STRATEGY_TEST_PARAM_VALID_ERROR = "策略測試參數驗證失敗";
	public final static String _STRATEGY_TEST_TARGET_DATA_NOT_ENOUGH_ERROR = "投資目標資料量不足";

	public final static String _STRATEGY_TEST_MONTHLY_PROFIT_LOG_FILE_NAME = "月獲利分析結果";
	public final static String _STRATEGY_TEST_WEEKLY_PROFIT_LOG_FILE_NAME = "周獲利分析結果";
	public final static Integer _STRATEGY_TEST_PROFIT_ANALYZE_START_YEAR = 2007;

	public final static String _STRATEGY_PACKAGE_NAME = "com.willy.sias.backtest.strategy";
	public final static String _DB_DAO_PACKAGE_NAME = "com.willy.sias.db.dao";

	public final static String _STRATEGY_CLASS_SUFFIX = "Strategy";
	public final static String _STRATEGY_ARGS_CLASS_SUFFIX = "StrategyArgs";

	public final static String _STRATEGY_NAME_SEASON = "Season";

	public final static Integer _FIELDID_LP_FI_NET = 114;
	public final static Integer _FIELDID_LP_IT_NET = 116;
	public final static Integer _FIELDID_PRICE_DAILY_TW_OPEN = 5;
	public final static Integer _FIELDID_PRICE_DAILY_TW_HIGH = 6;
	public final static Integer _FIELDID_PRICE_DAILY_TW_LOW = 7;
	public final static Integer _FIELDID_PRICE_DAILY_TW_CLOSE = 8;
	public final static Integer _FIELDID_PRICE_DAILY_TW_TRADING_NUM = 9;
	public final static Integer _FIELDID_PRICE_DAILY_TW_TRADING_SHARES = 10;
	public final static Integer _FIELDID_PRICE_DAILY_TW_TURN_OVER = 11;
	public final static Integer _FIELDID_MARKET_WEIGHTED_INDEX_CLOSE = 45;
	public final static Integer _FIELDID_LP_IT_HOLD = 118;
	public final static List<Integer> _FIELDID_LIST_OHLC = Arrays.asList(_FIELDID_PRICE_DAILY_TW_OPEN, _FIELDID_PRICE_DAILY_TW_HIGH, _FIELDID_PRICE_DAILY_TW_LOW, _FIELDID_PRICE_DAILY_TW_CLOSE, _FIELDID_PRICE_DAILY_TW_LOW);
	public final static List<Integer> _FIELDID_LIST_OHLCV = Arrays.asList(_FIELDID_PRICE_DAILY_TW_OPEN, _FIELDID_PRICE_DAILY_TW_HIGH, _FIELDID_PRICE_DAILY_TW_LOW, _FIELDID_PRICE_DAILY_TW_CLOSE, _FIELDID_PRICE_DAILY_TW_TRADING_SHARES);
	
	//股權分散 - 集保總張數
	public final static Integer _FIELDID_SHARES_DISPERSION_TOTAL_SHARE_COUNT = 100;
	//股權分散 - 總股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT = 101;
	//股權分散 - 平均張數/人
	public final static Integer _FIELDID_SHARES_DISPERSION_AVG_SHARES_COUNT = 102;
	//股權分散 - >400張大股東持有張數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHARE_COUNT_WITH_400UP = 103;
	//	股權分散 - >400張大股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT_WITH_400UP = 104;
	//	股權分散 - 400-600張大股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT_WITH_400TO600 = 105;
	//	股權分散 - 600-800張大股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT_WITH_600TO800 = 106;
	//	股權分散 - 800-1000張大股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT_WITH_800TO1000 = 107;
	//	股權分散 - >1000張大股東人數
	public final static Integer _FIELDID_SHARES_DISPERSION_SHAREHOLDER_COUNT_WITH_1000UP = 108;
	// 從永豐API撈取每分鐘股價
	public final static Integer _FIELDID_KBAR = 400;
	// 從永豐API撈取逐筆成交資訊
	public final static Integer _FIELDID_TICKS = 401;
	//	現金股利
	public final static Integer _FIELDID_CASH_DIVIDEND = 204;
	//	股票股利
	public final static Integer _FIELDID_STOCK_DIVIDEND = 205;
	//	現金股利殖利率
	public final static Integer _FIELDID_CASH_DIVIDEND_YIELD = 206;
	//	股票股利殖利率
	public final static Integer _FIELDID_STOCK_DIVIDEND_YIELD = 207;
	// EPS
	public final static Integer _FIELDID_EPS = 201;
	//月營收YOY
	public final static Integer _FIELDID_MONTH_REVENUE_YOY = 203;
	
	public final static Integer _TRADE_TYPE_BUY = 1;
	public final static Integer _TRADE_TYPE_SHORT_BUY = 2;
	public final static Integer _TRADE_TYPE_MARGIN_BUY = 3;
	public final static Integer _TRADE_TYPE_SELL = 4;
	public final static Integer _TRADE_TYPE_SHORT_SELL = 5;
	public final static Integer _TRADE_TYPE_MARGIN_SELL = 6;

	public final static Integer _BATCH_STATUS_SUCESS = 1;
	public final static Integer _BATCH_STATUS_FAIL = 0;
	
	public final static Integer _CFG_PARENT_ID_STOCK_CATEGORY_TWSE = 1;// TWSE分類
	public final static Integer _CFG_PARENT_ID_MARKET_TYPE = 2;// 市場別
	public final static Integer _CFG_PARENT_ID_SID = 3;// 股票代碼
	public final static Integer _CFG_PARENT_ID_BS_FR_KEY = 4;// 資產負債表會計科目代碼
	public final static Integer _CFG_PARENT_ID_IS_FR_KEY = 5;// 損益表會計科目代碼
	public final static Integer _CFG_PARENT_ID_CF_FR_KEY = 6;// 現金流量表會計科目代碼
	public final static Integer _CFG_PARENT_ID_MARKET_INDEX = 7;// 市場指數
	public final static Integer _CFG_PARENT_ID_KV_TABLE_NAME = 8;// KV對應TableName
	public final static Integer _CFG_PARENT_ID_STOCK_CATEGORY_TPEX = 48;// TPEX分類
	public final static Integer _CFG_PARENT_ID_FURURE_TYPE = 86;// 期貨分類
	public final static Integer _CFG_PARENT_ID_OPTION_TYPE = 87;// 選擇權分類
	public final static Integer _CFG_PARENT_ID_CALENDAR_TYPE = 119861;// 日曆類別
	public final static Integer _CFG_PARENT_ID_MARGIN_REMARK = 123072;// 信用交易註記
	public final static Integer _CFG_ID_MARKET_WEIGHTED_INDEX = 84898;
	public final static Integer _CFG_ID_CALENDAR_STOCK_BUS_DATE = 119862;
	public final static Integer _CFG_ID_FIN_RATIO_TYPE = 118522;
	public final static Integer _CFG_ID_MARKET_TYPE_TWSE = 44;
	public final static Integer _CFG_ID_MARKET_TYPE_TPEX = 45;
	
	//財報
	//EPS
	public final static Integer _CFG_ID_FR_IS_EPS = 30930;

	public final static Integer _CFG_ID_FR_IS_NET_PROFIT_AFTER_TAX = 30952;
	
	public final static Integer _CFG_ID_TPEX_CATEGORY = 48;
	public final static Integer _CFG_ID_VOL_WEIGHT_INDEX = 84898;

	public final static Integer _CFG_ID_USD_INDEX = 122029;

	public final static Integer _CFG_ID_VIX_INDEX = 124072;
	
	public final static String _MARKET_TYPE_TWSE = "TWSE";
	public final static String _MARKET_TYPE_TPEX = "TPEX";
	
	public final static String batchProcessorPackage = "com.willy.sias.batch.processor.";
	
	public final static int _TRADE_RESULT_CODE_SUCCESS = 4000;
	public final static int _TRADE_RESULT_CODE_BALANCE_NOT_ENOUGH = 9000;
	public final static int _TRADE_RESULT_CODE_INVENTORY_NOT_ENOUGH = 9001;
	public final static int _TRADE_RESULT_CODE_TRADE_TYPE_NOT_ALLOW = 9002;
	
	public final static String _MAIL_TEMPLATE_ID_BATCH_FAIL = "B001";
	
	public final static String[] _MOPS_INDUSTRY_TYPE_CODE_ALL = { "MS"// 大盤統計資訊
			, "MS2"// 委託及成交統計資訊
			, "0049"// 封閉式基金
			, "019919T"// 受益證券
			, "0999"// 認購權證(不含牛證)
			, "0999P"// 認售權證(不含熊證)
			, "0999C"// 牛證(不含可展延牛證)
			, "0999B"// 熊證(不含可展延熊證)
			, "0999X"// 可展延牛證
			, "0999Y"// 可展延熊證
			, "0999GA"// 附認股權特別股
			, "0999GD"// 附認股權公司債
			, "0999G9"// 認股權憑證
			, "CB"// 可轉換公司債
			, "01"// 水泥工業
			, "0099P"// ETF
			, "02"// 食品工業
			, "03"// 塑膠工業
			, "04"// 紡織纖維
			, "05"// 電機機械
			, "06"// 電器電纜
			, "07"// 化學生技醫療
			, "21"// 化學工業
			, "22"// 生技醫療業
			, "08"// 玻璃陶瓷
			, "09"// 造紙工業
			, "10"// 鋼鐵工業
			, "11"// 橡膠工業
			, "12"// 汽車工業
			, "13"// 電子工業
			, "24"// 半導體業
			, "25"// 電腦及週邊設備業
			, "26"// 光電業
			, "27"// 通信網路業
			, "28"// 電子零組件業
			, "29"// 電子通路業
			, "30"// 資訊服務業
			, "31"// 其他電子業
			, "14"// 建材營造
			, "15"// 航運業
			, "16"// 觀光事業
			, "17"// 金融保險
			, "18"// 貿易百貨
			, "9299"// 存託憑證
			, "23"// 油電燃氣業
			, "19"// 綜合
			, "20"// 其他
	};
	public final static String[] _MOPS_INDUSTRY_TYPE_CODE_1 = { "01"// 水泥工業
			, "0099P"// ETF
			, "02"// 食品工業
			, "03"// 塑膠工業
			, "04"// 紡織纖維
			, "05"// 電機機械
			, "06"// 電器電纜
			, "07"// 化學生技醫療
			, "21"// 化學工業
			, "22"// 生技醫療業
			, "08"// 玻璃陶瓷
			, "09"// 造紙工業
			, "10"// 鋼鐵工業
			, "11"// 橡膠工業
			, "12"// 汽車工業
			, "13"// 電子工業
			, "24"// 半導體業
			, "25"// 電腦及週邊設備業
			, "26"// 光電業
			, "27"// 通信網路業
			, "28"// 電子零組件業
			, "29"// 電子通路業
			, "30"// 資訊服務業
			, "31"// 其他電子業
			, "14"// 建材營造
			, "15"// 航運業
			, "16"// 觀光事業
			, "17"// 金融保險
			, "18"// 貿易百貨
			, "23"// 油電燃氣業
			, "19"// 綜合
			, "20"// 其他
	};

	public final static Set<Integer> _COMPANY_CATEGORY_ID_SET_WITHOUT_ETF = new HashSet<Integer>(
			Arrays.asList(12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
					36, 37, 38, 39, 40, 41, 42, 43));
	
	public final static int ERROR_INT_RC = 999;
	
	public final static Comparator<BigDecimal> _DEFAULT_COMPARATOR_BIGDECIMAL = new Comparator<BigDecimal>() {
		@Override
		public int compare(BigDecimal o1, BigDecimal o2) {
			// TODO Auto-generated method stub
			return o1.compareTo(o2);
		}
	};
	// TODO Auto-generated method stub
	public final static Comparator<Date> _DEFAULT_COMPARATOR_DATE = Date::compareTo;
	public final static Comparator<Date> _COMPARATOR_DATE_DESC = (date1, date2) -> date1.compareTo(date2) * -1;
	public final static Comparator<String> _DEFAULT_COMPARATOR_STRING = Comparator.naturalOrder();
	public final static Comparator<Integer> _DEFAULT_COMPARATOR_INTEGER = Comparator.naturalOrder();
	public final static List<String> _FIN_REPORT_PUBLISH_MMDD_DATE_LIST = Arrays.asList("0331", "0515", "0814", "1114");
	public final static int _3H_2BL_CHECK_DAYS = 15;

	public final static int _AV_TURN_CHECK_TICKS = 3;

	public final static String FIELD_CAT_SPLIT_STR = ":";

	public final static String PROC_BLOCK_HKEY = "sias:processBlocks";

	public final static String PROC_BLOCK_KEY_PREFFIX = "procBlock";

	public final static String EVAL_PROC_BLOCK_RESULT_KEY_PREFFIX = "evalProcBlockResult";

	public final static String CATEGORY_QRY_TYPE_SID = "sid";
	public final static String CATEGORY_QRY_TYPE_CAT = "cat";

	public final static Date CUSTOMIZE_CATEGORY_DATA_DATE = WType.strToDate("19700101");
}
