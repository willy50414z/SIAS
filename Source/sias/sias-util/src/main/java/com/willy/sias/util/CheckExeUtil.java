package com.willy.sias.util;

import com.willy.util.log.WLog;

public class CheckExeUtil {
	public static boolean isExecute(Exception e) {
		StackTraceElement[] stackTraceAr = e.getStackTrace();
		int startClassCount = 0;
		for(int i=0;i<stackTraceAr.length;i++) {
			if(stackTraceAr[0].getClassName().equals(stackTraceAr[i].getClassName())) {
				if(startClassCount==1) {
					return true;
				} else {
					startClassCount++;
				}
			}
		}
		WLog.error("start class is not " + stackTraceAr[0].getClassName()
				+ " or not \"org.springframework.boot.loader.JarLauncher\"");
		return false;
	}
}
