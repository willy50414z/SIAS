package com.willy.sias.db.repository;

import com.willy.sias.db.po.IdxDailyUsPO;
import com.willy.sias.db.po.IdxDailyUsPOKey;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdxDailyUsRepository extends JpaRepository<IdxDailyUsPO, IdxDailyUsPOKey> {

}
