//package com.willy.sias.backtest.strategy;
//
//import java.math.BigDecimal;
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.backtest.util.StatisticUtil;
//import com.willy.sias.backtest.util.TradeUtil;
//import com.willy.sias.db.dto.TimeSeriesDTO;
//import com.willy.sias.util.config.Const;
//import com.willy.util.log.WLog;
//import com.willy.util.type.WType;
//
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.Setter;
//
///**
// * 對沖交易策略 兩支標的近1,3,6個月收盤價計算相關係數，以權重[3,2,1]加總 取出|相關係數|最高的6支標的 當2標的收盤價乖離率>2倍布林，進場
// * 
// * @author Willy
// *
// */
//@Component
//@Getter
//public class HedgeTxnStrategy extends TradeStrategy {
//	@Autowired
//	private TradeUtil tradeUtil;
//	@Autowired
//	private StatisticUtil statisticService;
//
//	@Override
//	public void setTradeDetail() {
//		// 取出標的
//		List<Integer> sidList = profitPo.getSidList();
//
//		// 相關係數-sid1-sid2
//		BigDecimal[] weightIndexAr = new BigDecimal[] { new BigDecimal(3), new BigDecimal(2), new BigDecimal(1) };
//		Integer[] calBaseDateIndexAr = new Integer[] { 20, 60, 120 };
//		List<SidCCDTO> sidCCDTOList = new ArrayList<>();
//		// 檢驗過的SID
//		List<String> validedSidList = new ArrayList<>();
//		// 逐個SID取出做配對，相同的或比對過的跳過
//		sidList.forEach(sid1 -> {
//			sidList.forEach(sid2 -> {
//				if (sid1.compareTo(sid2) != 0 && !(validedSidList.contains(sid1 + "-" + sid2)
//						|| validedSidList.contains(sid2 + "-" + sid1))) {
//
//					WLog.info("Start Check SID1[" + sid1 + "]SID2[" + sid2 + "]");
//					// 取得近120交易日
//					List<Date> recent120BusDateList = dtDao.findBusDate(tradeDate, -120);
//					if (recent120BusDateList.size() == 120) {
//						// 取得股價List
//						List<TimeSeriesDTO<BigDecimal>> sid1PriceList = dtDao.findTdsList(sid1,
//								Const._FIELDID_PRICE_DAILY_TW_CLOSE, recent120BusDateList.get(0),
//								recent120BusDateList.get(recent120BusDateList.size() - 1));
//						if (sid1PriceList.size() == 120) {
//							List<TimeSeriesDTO<BigDecimal>> sid2PriceList = dtDao.findTdsList(sid2,
//									Const._FIELDID_PRICE_DAILY_TW_CLOSE, recent120BusDateList.get(0),
//									recent120BusDateList.get(recent120BusDateList.size() - 1));
//							if (sid2PriceList.size() == 120) {
//								BigDecimal ccTotal = new BigDecimal(0);
//								for (int i = 0; i < calBaseDateIndexAr.length; i++) {
//									// 逐個區間取出來做計算
//									List<TimeSeriesDTO<BigDecimal>> subSid1TsList = sid1PriceList.subList(0,
//											calBaseDateIndexAr[i] - 1);
//									List<TimeSeriesDTO<BigDecimal>> subSid2TsList = sid2PriceList.subList(0,
//											calBaseDateIndexAr[i] - 1);
//									// 取得相關係數
//									List<BigDecimal> subSid1PriceList = new ArrayList<>();
//									List<BigDecimal> subSid2PriceList = new ArrayList<>();
//									subSid1TsList
//											.forEach(subSid1Ts -> subSid1PriceList.add(subSid1Ts.getDecimalValue()));
//									subSid2TsList
//											.forEach(subSid2Ts -> subSid2PriceList.add(subSid2Ts.getDecimalValue()));
//									BigDecimal cc = statisticService.getCorrelation(subSid1PriceList, subSid2PriceList);
//									WLog.info("SID1[" + sid1 + "]SID2[" + sid2 + "]CC[" + cc + "]Interval["
//											+ calBaseDateIndexAr[i] + "]");
//									ccTotal = ccTotal.add(cc.multiply(weightIndexAr[i]));
//								}
//								// 加入List
//								sidCCDTOList.add(new SidCCDTO(sid1, sid2, ccTotal));
//							} else {
//								WLog.info("Get SID2[" + sid2 + "] 120 price before[" + tradeDate + "]，result only ["
//										+ sid2PriceList.size() + "] rows");
//							}
//						} else {
//							WLog.info("Get SID1[" + sid1 + "] 120 price before[" + tradeDate + "]，result only ["
//									+ sid1PriceList.size() + "] rows");
//						}
//					} else {
//						WLog.info("Get 120 trade date before[" + tradeDate + "]，result only ["
//								+ recent120BusDateList.size() + "] days");
//					}
//				}
//			});
//		});
//
//		// 依相關係數排序
//		sidCCDTOList.sort(new Comparator<SidCCDTO>() {
//			@Override
//			public int compare(SidCCDTO o1, SidCCDTO o2) {
//				return o1.getCc().compareTo(o2.getCc());
//			}
//		});
//
//		SidCCDTO sidCCDto;
//		try {
//			WLog.info("檢驗日期:" + WType.dateToStr(tradeDate));
//		} catch (ParseException e) {
//		}
//		WLog.info("相關係數最高前3名");
//		for (int i = 0; i < 3; i++) {
//			sidCCDto = sidCCDTOList.get(i);
//			WLog.info("\tSID1[" + sidCCDto.getSid1() + "]SID2[" + sidCCDto.getSid2() + "]CC[" + sidCCDto.getCc() + "]");
//		}
//		WLog.info("相關係數最低前3名");
//		for (int i = 0; i < 3; i++) {
//			sidCCDto = sidCCDTOList.get(sidCCDTOList.size() - (i + 1));
//			WLog.info("\tSID1[" + sidCCDto.getSid1() + "]SID2[" + sidCCDto.getSid2() + "]CC[" + sidCCDto.getCc() + "]");
//		}
//
//	}
//
//	@Override
//	public String getStrategyDesc() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String checkArgs() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void init() {
//	}
//
//	@Override
//	public Integer call() throws Exception {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
////	private SidCCDTO createSidCCDto(Integer sid1, Integer sid2, List<TimeSeriesDTO<BigDecimal>> sid1PriceList,
////			List<TimeSeriesDTO<BigDecimal>> sid2PriceList, Integer startInterval, Integer endInterval) {
////		return new SidCCDTO(sid1, sid2, );
////	}
//}
//
//@Setter
//@Getter
//@AllArgsConstructor
//class SidCCDTO {
//	private Integer sid1;
//	private Integer sid2;
//	private BigDecimal cc;
//}
