//package com.willy.sias.backtest.strategy;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.backtest.service.StatisticService;
//import com.willy.sias.db.dto.TimeSeriesDTO;
//import com.willy.sias.db.po.PriceDailyTwPO;
//import com.willy.sias.db.po.PriceDailyTwPOKey;
//import com.willy.sias.db.po.SysConfigPO;
//import com.willy.sias.db.repository.PriceDailyTwRepository;
//import com.willy.sias.db.repository.SysConfigRepository;
//import com.willy.sias.util.EnumSet.TradeType;
//import com.willy.sias.util.config.Const;
//import com.willy.util.log.WLog;
//import com.willy.util.type.WType;
//
///**
// * 連續10天+-2布林乖(20天)離0.93~1.07 後5天漲幅>5%
// * 
// * @author Willy
// *
// */
//@Component
//public class BreakBooleanDownStrategy extends TradeStrategy {
//
//	
//	@Autowired
//	private StatisticService statisticService;
//	@Autowired
//	private SysConfigRepository scRepo;
//	//布林糾纏日數
//	private final int booleanCaculateDate = 20;
//	//布林糾纏日數
//	private final int getReadyInterval = 20;
//	//持有日數
//	private final int holdingDays = 10;
//	//上下布林誤差
//	private final double maxBooleanBias = 1.03;
//	//突破布林
//	private final double breakRatio = 1.05;
//	@Autowired
//	private PriceDailyTwRepository priceRepo;
//
//	@Override
//	public void setTradeDetail(Date tradeDate, ProfitPO profitPo) {
//		// 撈出前35天股價
//		List<Integer> sidList = profitPo.getSidList();
//		List<Date> recent35BusDateList = dtDao.findBusDate(tradeDate, -(getReadyInterval + 25));
//		if (recent35BusDateList.size() != getReadyInterval + 25) {
//			return;
//		}
//		for (int sid : sidList) {
//			try {
//				//持有10天融券買回
//				profitPo.getTradeDetailList().stream().filter(td -> {
//					List<Date> sellDateList = dtDao.findBusDate(td.getSellDate(), holdingDays);
//					return tradeDate.equals(sellDateList.get(sellDateList.size()-1)) && !td.isTradeComplete() && td.getSid().compareTo(sid) == 0;
//				}).collect(Collectors.toList()).forEach( td -> {
//					SysConfigPO po = scRepo.findById(td.getSid()).orElse(null);
//					WLog.info("SIDCode[" + po.getCfgValue() + "]SIDName["+po.getCfgDesc()+"]tradeDate["+tradeDate+"] SELL");
//					tu.trade(sid, tradeDate, 1, TradeType.shortBuying, profitPo);
//					});
//				
//				List<TimeSeriesDTO<BigDecimal>> priceTsList = dtDao.findTdsList(sid,
//						Const._FIELDID_PRICE_DAILY_TW_CLOSE, recent35BusDateList.get(0),
//						recent35BusDateList.get(recent35BusDateList.size() - 1));
//				if (priceTsList.size() != (getReadyInterval + 25)) {
//					continue;
//				}
//				// 20-30天做+-2布林，取最大誤差<0.07
//				StringBuffer sb = new StringBuffer();
//				BigDecimal maxBias = new BigDecimal(0);
//				List<BigDecimal> priceList = new ArrayList<>();
//
//				for (int dayIndex = 20; dayIndex < (getReadyInterval + 20); dayIndex++) {
//					priceTsList.subList(dayIndex - 20, dayIndex).forEach(ts -> priceList.add(ts.getDecimalValue()));
//					BigDecimal mean = statisticService.getMean(priceList);
//					BigDecimal sd = statisticService.getSd(priceList);
//					BigDecimal bias = mean.add(sd.multiply(new BigDecimal(2))).divide(
//							mean.subtract(sd.multiply(new BigDecimal(2))), 4, RoundingMode.HALF_UP);
//					sb.append(WType.dateToStr(priceTsList.get(dayIndex - 20).getDate())).append("到")
//							.append(WType.dateToStr(priceTsList.get(dayIndex).getDate()))
//							.append("布林["
//									+ mean.add(sd.multiply(new BigDecimal(2))).setScale(4, RoundingMode.HALF_UP)
//									+ "/" + mean + "/" + mean.subtract(sd.multiply(new BigDecimal(2))).setScale(4, RoundingMode.HALF_UP)
//									+ "]");
//					if (maxBias.compareTo(bias) < 0) {
//						maxBias = bias;
//					}
//				}
//				if (maxBias.compareTo(new BigDecimal(maxBooleanBias)) > 0) {
//					continue;
//				}
//
//				// 30-35最大收盤漲幅>30日5% & 成交量>2000張 --> 融券賣出 --> 印出SID及時間
//				PriceDailyTwPOKey pKey = new PriceDailyTwPOKey();
//				pKey.setSid(sid);
//				pKey.setDataDate(tradeDate);
//				PriceDailyTwPO pricePo = priceRepo.findById(pKey).orElse(null);
//				for (TimeSeriesDTO<BigDecimal> ts : priceTsList.subList((getReadyInterval + 20) - 1,
//						(getReadyInterval + 20) + 4)) {
//					if (ts.getDecimalValue().compareTo(priceTsList.get((getReadyInterval + 20) - 2).getDecimalValue()
//							.multiply(new BigDecimal(breakRatio))) > 0 && pricePo != null && pricePo.getTradingShares()>2000000L) {
//						SysConfigPO po = scRepo.findById(sid).orElse(null);
//						if (po != null) {
//								sb.append("交易日["+WType.dateToStr(ts.getDate())+"]收盤["+ts.getDecimalValue()+"]");
//								WLog.info("SIDCode[" + po.getCfgValue() + "]SIDName["+po.getCfgDesc()+"]Date[" + WType.dateToStr(tradeDate) + "]MaxBias["
//										+ maxBias.setScale(4, RoundingMode.HALF_UP).toString() + "]"+sb.toString());
//								tu.trade(sid, tradeDate, 1, TradeType.shortSelling, profitPo);
//								break;
//						}
//					}
//				}
//				
//			} catch (ParseException e) {
//				WLog.error(e);
//			}
//		}
//		;
//	}
//
//	@Override
//	public String getStrategyDesc() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String checkArgs() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void init() {
//	}
//
//}
