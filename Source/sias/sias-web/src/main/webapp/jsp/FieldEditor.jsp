<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SIAS</title>
<%@ include file="/jsp/Header.jsp"%>
</head>
<script>
  var catName = "";

  function generateSubCatSelect(result){
    subSelectIndex = catName.split(":").length;
    subSelectId = "subCatSelect"+subSelectIndex;

    //generate subCat select
    if(result.subCatFieldNameSet != null) {
      $("#subCatSelect").append("<select id='"+subSelectId+"' subSelectIndex='"+subSelectIndex+"'><option selected></option>");
      $.each(result.subCatFieldNameSet, function(index, value) {
          $("#"+subSelectId).append(
              "<option value=\"" + value + "\">" + value +
              "</option>");
      })
    }


    //subCat event
    $("#"+subSelectId).selectmenu({
       change: function(event, data) {
          if($(this).val().trim() == "") {
            return;
          }

          //delete child cat select
          thisSelectIndex = parseInt($(this).attr("subSelectIndex"))
          for(var i=(thisSelectIndex + 1);i<10;i++) {
            var selectDom = $("#subCatSelect"+i);
            if(selectDom == null) {
              break;
            } else {
              selectDom.remove();
            }
          }

          //remove required arg input
          $("#fieldInfoRequiredField>table").remove();

          //remove textarea text
          $("#fieldInfoDetail").text("")

          //rebuild child cat select
          catNameAr = catName.split(":");
          if(catNameAr.length < thisSelectIndex) {
            //選取最後一層select
            catName += ":" + $(this).val()
          } else {
            catName = "";
            for(var i=0;i<thisSelectIndex;i++) {
              catName += catNameAr[i] + ":"
            }
            catName += $(this).val().trim();
          }

          $.ajax({
          type: "POST",
          url: getContextPath() +
              "/fieldQuery/getSubFieldInfo?catName="+catName,
          success: function(result) {
            if(result.fieldInfoJs != null) {
              //選到欄位，產生欄位訊息及待填欄位
              //欄位訊息
              $("#fieldInfoDetail").text(result.fieldInfoJs)
              //待填欄位
              if(result.processArgList != null && result.processArgList.length > 0) {
                $("#fieldInfoRequiredField>table").remove();
                var tableHtml = "<table>";
                $.each(result.processArgList, function(index, value) {
                  tableHtml += "<tr><td>"+ value +" : </td><td><input type='text'/></td></tr>";
                });
                tableHtml += "</table>";
                $("#fieldInfoRequiredField").append(tableHtml);
              }
            } else {
              //選擇有子分類欄位，產生子分類select
              generateSubCatSelect(result);
            }
          }
      });
        }
     });
  }

  $(function() {
    $("#fieldCatSelect").selectmenu({
         change: function(event, data) {
            catName = $(this).val();
            if(catName == "") {
              return;
            }

            //remove subCatSelects
            for(var i=1;i<10;i++) {
              var selectDom = $("#subCatSelect"+i);
              if(selectDom == null) {
                break;
              } else {
                selectDom.remove();
              }
            }

            //remove required arg input
            $("#fieldInfoRequiredField>table").remove();

            //remove textarea text
            $("#fieldInfoDetail").text("")

            //generate sunCat Select
            $.ajax({
                type: "POST",
                url: getContextPath() +
                    "/fieldQuery/getSubFieldInfo?catName="+catName,
                success: function(result) {
                    generateSubCatSelect(result);
                }
            });
        }
    });

    $("#fieldFilterEditor").on('change' ,function(){
      fieldFilterBlock = this.val
      $.ajax({
      type: "POST",
      url: getContextPath() +
          "/fieldQuery/getFieldBlockDesc",
      data: {
       "fieldBlock": $("#fieldFilterEditor").val()
      },
      success: function(result) {
        $("#fieldFilterDesc").text(result);
      },
      error: function(e) {
        $("#fieldFilterDesc").text("error");
      }
      })
    })

    $("#exec").on("click", function(){
      $.ajax({
        type: "POST",
        url: getContextPath() +
            "/fieldQuery/execFieldFilter",
        data: {
         "fieldFilter": $("#fieldFilterEditor").val()
        },
        success: function(result) {
          $("#result").text(result);
        },
        error: function(e) {
          $("#result").text("error");
        }
      })
    })
  })
</script>
<body>
<fieldset>
<legend>欄位查詢</legend>
  <table>
    <tr>
      <select id="fieldCatSelect">
        <option></option>
        <c:forEach var="fieldInfoCatName" items="${fieldInfoCatNameList}">
          <option value="${fieldInfoCatName}">
            <c:out value="${fieldInfoCatName}" />
          </option>
        </c:forEach>
      </select>
    </tr>
    <tr>
      <div id="subCatSelect"/>
    </tr>
    <tr>
      <textarea id="fieldInfoDetail" style='width: 1184px; height: 93px;'></textarea>
    </tr>
    <tr>
      <div id="fieldInfoRequiredField"/>
    </tr>
    </table>
    </fieldset>
    <fieldset>
      <legend>欄位編輯</legend>
      <table>
      <tr>
        <textarea id="fieldFilterEditor" style='width: 1184px; height: 93px;'></textarea>
      </tr>
      <tr>
          <textarea id="fieldFilterDesc" style='width: 1184px; height: 93px;' readonly></textarea>
      </tr>
      <tr>
          <button id="exec" class="btn btn-primary">執行</button>
          執行結果: <textarea id="result" style='width: 1184px; height: 93px;' readonly></textarea>
      </tr>
    </table>
  </fieldset>
</body>
</html>