package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * LP_FUTURE_TW
 * @author 
 */
@Table(name="LP_FUTURE_TW")
@Entity
@Data
@IdClass(LPFutureTwPOKey.class)
public class LPFutureTWPO implements Serializable {
    /**
     * 資料日期
     */
    @Id
    @Column(name = "DATA_DATE")
    private Date dataDate;
    
    /**
     * 期貨類別
     */
    @Id
    @Column(name = "FUTURE_TYPE")
    private Integer futureType;

    /**
     * 外資多方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "FI_LONG_TXN_LOT")
    private BigDecimal fiLongTxnLot;

    /**
     * 投信多方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "IT_LONG_TXN_LOT")
    private BigDecimal itLongTxnLot;

    /**
     * 自營商多方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "DL_LONG_TXN_LOT")
    private BigDecimal dlLongTxnLot;

    /**
     * 外資空方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "FI_SHORT_TXN_LOT")
    private BigDecimal fiShortTxnLot;

    /**
     * 投信空方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "IT_SHORT_TXN_LOT")
    private BigDecimal itShortTxnLot;

    /**
     * 自營商空方交易口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "DL_SHORT_TXN_LOT")
    private BigDecimal dlShortTxnLot;

    /**
     * 淨交易口數
     */
    @Formula("(FI_LONG_BAL_LOT + IT_LONG_BAL_LOT + DL_LONG_BAL_LOT) - (FI_SHORT_BAL_LOT + IT_SHORT_BAL_LOT + DL_SHORT_BAL_LOT)")
    @EqualsAndHashCode.Exclude
    @Column(name = "NET_TXN_LOT")
    private BigDecimal netTxnLot;

    /**
     * 外資多方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "FI_LONG_BAL_LOT")
    private BigDecimal fiLongBalLot;

    /**
     * 投信多方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "IT_LONG_BAL_LOT")
    private BigDecimal itLongBalLot;

    /**
     * 自營商多方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "DL_LONG_BAL_LOT")
    private BigDecimal dlLongBalLot;

    /**
     * 外資空方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "FI_SHORT_BAL_LOT")
    private BigDecimal fiShortBalLot;

    /**
     * 投信空方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "IT_SHORT_BAL_LOT")
    private BigDecimal itShortBalLot;

    /**
     * 自營商空方未平倉口數
     */
    @EqualsAndHashCode.Exclude
    @Column(name = "DL_SHORT_BAL_LOT")
    private BigDecimal dlShortBalLot;

    /**
     * 淨未平倉口數
     */
    @Formula("(FI_LONG_BAL_LOT + IT_LONG_BAL_LOT + DL_LONG_BAL_LOT) - (FI_SHORT_BAL_LOT + IT_SHORT_BAL_LOT + DL_SHORT_BAL_LOT)")
    @EqualsAndHashCode.Exclude
    @Column(name = "NET_BAL_LOT")
    private BigDecimal netBalLot;
    
	private static final long serialVersionUID = 1L;
}