USE [SIAS_SIT_PRICE]
CREATE TABLE [EXCHANGE] (
	[SID] INT,
	[DATA_DATE] DATETIME,
	[OPEN] DECIMAL(5,3),
	[HIGH] DECIMAL(5,3),
	[LOW] DECIMAL(5,3),
	[CLOSE] DECIMAL(5,3),
	[VOL] DECIMAL(18,0)
)
USE [SIAS_SIT]
CREATE SYNONYM [dbo].[EXCHANGE] FOR [SIAS_SIT_PRICE].[dbo].[EXCHANGE]
SET IDENTITY_INSERT [dbo].[SYS_CONFIG] ON
INSERT INTO SYS_CONFIG ("CFG_ID", "PARENT_ID", "CFG_VALUE", "CFG_DESC") VALUES (123052, 0, 'EXCHANGE TWD/USD', '台幣/美元 匯率');
INSERT INTO SYS_CONFIG ("CFG_ID", "PARENT_ID", "CFG_VALUE", "CFG_DESC") VALUES (123053, 123052, 'EXCHANGE TWD/USD', '台幣/美元 匯率');
SET IDENTITY_INSERT [dbo].[SYS_CONFIG] OFF 
INSERT INTO field_info VALUES(83,'匯率 - 台幣/美元','[EXCHANGE]',NULL,NULL,'[CLOSE]','[DATA_DATE]',NULL,null)