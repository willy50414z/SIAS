package com.willy.sias.batch.processor;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.ExchangePO;
import com.willy.sias.db.repository.ExchangeRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.Setter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;


@Service
@Setter
@ConfigurationProperties(prefix = "sias.batch.exchange-usd-twd")
public class EXCHANGE_USD_TWD extends BaseBatchProcessor
{
	@Autowired
	private WJsoup js;
	@Autowired
	private ExchangeRepository exchangeRepo;

	@Override
	public void cleanHisData() throws Exception
	{

	}

	@Override
	protected void exec() throws Exception
	{
		long time = WDate.addDay(this.dataDate, -1).getTime() / 1000;
		this.putMapParam("startTime", time + "");
		this.putMapParam("endTime", time + "");
		js.setActionMode(Connection.Method.GET);
		String dataJs = js.get(this.parseMapParamToString(this.urlTempl), Const._ENCODING_UTF8).getElementsByTag("body").get(0).text();
		JSONObject js = new JSONObject(dataJs).getJSONObject("data");
		JSONArray timeAr = js.getJSONArray("t");
		JSONArray openAr = js.getJSONArray("o");
		JSONArray highAr = js.getJSONArray("h");
		JSONArray lowAr = js.getJSONArray("l");
		JSONArray closeAr = js.getJSONArray("c");
		JSONArray volAr = js.getJSONArray("v");

		List<ExchangePO> dataList = new ArrayList<>();
		for (int i = 0; i < timeAr.length(); i++)
		{
			Date dataDate = WType.strToDate(WType.dateToStr(new Date(Long.valueOf(timeAr.get(i).toString() + "000")), "yyyyMMdd"));
			dataList.add(new ExchangePO(123053, dataDate, new BigDecimal(openAr.get(i).toString())
					, new BigDecimal(highAr.get(i).toString())
					, new BigDecimal(lowAr.get(i).toString())
					, new BigDecimal(closeAr.get(i).toString())
					, (volAr.get(i) == null || volAr.get(i).toString().equals("null")) ? null : new BigDecimal(volAr.get(i).toString())));
		}
		exchangeRepo.saveAll(dataList);
	}
}
