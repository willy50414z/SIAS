package com.willy.sias.db.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TicksDTO {
	private BigDecimal askVolume;
	private BigDecimal askPrice;
	private BigDecimal close;
	private BigDecimal bidPrice;
	private BigDecimal bidVolume;
	private BigDecimal volume;
}
