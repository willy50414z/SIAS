package com.willy.sias.batch.processor;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.marketindex-tw-twse-txn-info")
public class MARKET_INDEX_TW_TWSE_TXN_INFO extends BaseBatchProcessor {

	@Autowired
	private WFile_txt txtFile;
	@Autowired
	private PriceDailyTwRepository priceRepo;
	private String urlTempl;
	private String csvFileNameTempl;
	
	@Override
	public void cleanHisData() throws Exception {
	}

	@Override
	public void exec() throws Exception {
		// 蒐集[股價]、[分類]資料
		String filePath;
		txtFile.setEncoding("MS950");
		
		// 下載股價資料
		WLog.info("下載股價資料");
		filePath = FileUtil.download(parseMapParamToString(urlTempl), parseMapParamToString(csvFileNameTempl));
		
		// 取得指數資料
		WLog.info("讀取指數成交資訊");
		String[][] dataAr =  txtFile.readToAr(filePath, "日期", "說明:",true);
		
		if(dataAr.length == 0) {
			WLog.info("當天非交易日");
		}

		// 匯入指數資料
		WLog.info("匯入指數資料");
		priceRepo.saveAll(getSpList(dataAr));
	}
	
	private List<PriceDailyTwPO> getSpList(String[][] dataAr) throws ParseException{
		Date startDate = WType.strToDate(WType.dateToStr(this.dataDate, "yyyyMM") + "01");
		Date endDate = WDate.addDate(startDate, Calendar.MONTH, 1);
		List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(Const._CFG_ID_VOL_WEIGHT_INDEX, startDate, endDate);
		for(String[] row : dataAr) {
			PriceDailyTwPO p = priceList.stream().filter(price -> {
					return price.getDataDate()
							.compareTo(WType.strToDate(WDate.convertCEYear(row[0].substring(0, row[0].indexOf("/")))
									+ row[0].substring(row[0].indexOf("/")), "yyyy/MM/dd")) == 0;
			}).findFirst().orElse(null);
			if(p != null) {
				p.setTurnOver(Long.valueOf(WString.cleanIntegerData(row[2])));
				p.setTradingShares(Long.valueOf(WString.cleanIntegerData(row[1])));
				p.setTradingNum(Integer.valueOf(WString.cleanIntegerData(row[3])));
			}
		}
		return priceList;
	}
}
