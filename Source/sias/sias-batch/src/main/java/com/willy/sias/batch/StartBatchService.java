package com.willy.sias.batch;

import com.willy.net.mail.MsgType;
import com.willy.net.mail.WMail;
import com.willy.sias.batch.util.BatchExeUtil;
import com.willy.sias.db.po.BatchLogPO;
import com.willy.sias.db.repository.BatchLogRepository;
import com.willy.sias.util.CheckExeUtil;
import com.willy.sias.util.EnumSet.BatchName;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan("com.willy")
@EnableTransactionManagement(proxyTargetClass = true)
public class StartBatchService implements CommandLineRunner {

  public static void main(String[] args) throws Exception {
    SpringApplication.run(StartBatchService.class, args);
  }

  @Autowired
  private BatchExeUtil beUtil;
  @Autowired
  public JdbcTemplate jdbcTemplate;
  @Autowired
  public BatchLogRepository blRepo;
  @Autowired
  public WMail mail;
  @Value("${spring.mail.receiver.batch.b001}")
  private String batchFailMailReceivers;

  @Override
  public void run(String... args) throws Exception {
    if (!CheckExeUtil.isExecute(new Exception())) {
      return;
    }

    // 重跑之前失敗的
    reRunFailBatch();

    // 逐批次執行
    initBatch(WType.dateToStr(
        blRepo.findFirstByDataDateLessThanOrderByDataDateDesc(new Date()).getDataDate()));

    // 發送失敗批次mail
    sendBatchFailInfo();

    System.exit(0);
  }

  public void reRunFailBatch() throws Exception {
    List<BatchLogPO> blList = blRepo.findByStatusIs(Const._BATCH_STATUS_FAIL);
    for (BatchLogPO bl : blList) {
      beUtil.exec(BatchName.valueOf(bl.getBatchName()), bl.getDataDate(), null);
    }
  }

  public void initBatch(String date) throws Exception {
    Date batchDate = WType.strToDate((date.length() > 0) ? date : "20150901");
    while (batchDate.before(new Date())) {
      for (BatchName bn : BatchName.values()) {
        // 每日的
        if (bn.getFrequence() == 'D') {
          beUtil.exec(bn, batchDate, null);
        }
        // 每月批次15號執行
        if (bn.getFrequence() == 'M' && WDate.getDateInfo(batchDate, Calendar.DAY_OF_MONTH) == 15) {
          beUtil.exec(bn, batchDate, null);
        }
        // 每季批次1/4/7/10月15號執行
        if (bn.getFrequence() == 'Q' && WDate.getDateInfo(batchDate, Calendar.MONTH) % 3 == 1
            && WDate.getDateInfo(batchDate, Calendar.DAY_OF_MONTH) == 15) {
          beUtil.exec(bn, batchDate, null);
        }
        // 專門給DIVIDEND_TW，跑完要改回D
        if (bn.getFrequence() == 'Y' && WDate.getDateInfo(batchDate, Calendar.MONTH) == 1
            && WDate.getDateInfo(batchDate, Calendar.DAY_OF_MONTH) == 1) {
          beUtil.exec(bn, batchDate, null);
        }
      }
      batchDate = WDate.addDate(batchDate, Calendar.DAY_OF_MONTH, 1);
    }
  }

  public void sendBatchFailInfo() throws Exception {
    List<String> data;
    List<List<String>> dataList = new ArrayList<>();

    List<BatchLogPO> blList = blRepo.findByStatusIs(Const._BATCH_STATUS_FAIL);

    for (BatchLogPO bl : blList) {
      data = new ArrayList<>();
      data.add(WType.dateToStr(bl.getDataDate()));
      data.add(bl.getBatchName());
      data.add(bl.getErrMsg());
      dataList.add(data);
    }

    mail.setContent(MsgType.Text, "");

    if (!CollectionUtils.isEmpty(dataList)) {
      mail.setHtmlListContent(Const._MAIL_TEMPLATE_ID_BATCH_FAIL, dataList, null);
    }
    mail.send("SIAS排成執行完成通知，失敗" + dataList.size() + "筆 - " + WDate.getNowTime("MM/dd"),
        Arrays.asList(batchFailMailReceivers.split(",")));
  }
}
