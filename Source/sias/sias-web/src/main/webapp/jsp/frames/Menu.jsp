<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
	<ul>
		<li><a target="main" href="<%=request.getContextPath() %>/BackTestMain/viewBackTestMain">回測結果</a></li>
		<li><a target="main" href="<%=request.getContextPath() %>/BatchSummary/viewBatchSummary">批次總覽</a></li>
		<li><a target="main" href="<%=request.getContextPath() %>/fieldQuery/">欄位編輯器</a></li>
		<li><a target="main" href="<%=request.getContextPath() %>/categoryEditor/">股票類別編輯器</a></li>
	</ul>
</body>
</html>