package com.willy.sias.web.service;

import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.willy.file.serialize.WSerializer;
import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.service.ProfitDTOService;
import com.willy.sias.db.po.BackTestLogDPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.BackTestLogDRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.web.vo.BackTestResultVO;
import com.willy.sias.web.vo.BackTestSummaryVO;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

@Service
public class BackTestSummaryService {
	@Autowired
	private BackTestLogDRepository btlDRepo;
	@Autowired
	private SysConfigRepository configRepo;
	@Autowired
	private WSerializer serializer;
	@Autowired
	private ProfitDTOService ps;

	public List<BackTestSummaryVO> getBackTestSummary(Integer testId, String contextPath) throws ParseException {
		// 撈出H
		List<BackTestLogDPO> btHList = btlDRepo.findByTestId(testId);

		List<BackTestLogDPO> btdList = null;
		if (btHList != null && btHList.size() > 0) {
			btdList = btlDRepo.findByTestId(btHList.get(0).getTestId());
		} else {
			WLog.info("There has no any back test data");
			return null;
		}

		List<BackTestSummaryVO> btsVoList = new ArrayList<>();
		NumberFormat formatter = new DecimalFormat("$###,###,###,###.##");
		btdList.forEach(btd -> {
			ProfitDTO profitDTO = null;
			profitDTO = getBackTestTradeDetail(btd.getTradeDetail());

			// 取出標的代碼
			StringBuffer stockCode = new StringBuffer();
			List<SysConfigPO> configList = configRepo.findAllById(profitDTO.getSidSet());
			configList.forEach(conf -> stockCode.append(conf.getCfgValue()).append(","));
			stockCode.setLength(stockCode.length() - 1);
			
//			btsVoList.add(new BackTestSummaryVO(stockCode.toString(), profitPo.getStrategyDesc(),
//					formatter.format(ps.getProfitRatio(profitPo).doubleValue()),
//					formatter.format(ps.getProfitAmtRatio(profitPo).doubleValue()),
//					formatter.format(ps.getAvgProfitAmt(profitPo).doubleValue()),
//					formatter.format(ps.getAvgLossAmt(profitPo).doubleValue()),
//					profitPo.getTradeCount(),
//					String.valueOf(btd.getTestId())));
		});
		
		BackTestResultVO btResultVo = new BackTestResultVO();
		btResultVo.setBackTestSummaryList(btsVoList);
		btResultVo.setDataTableHtml(contextPath);
		
		return btsVoList;
	}

	public ProfitDTO getBackTestTradeDetail(byte[] tradeDetail){
		ProfitDTO profitDTO = null;
		try {
			// 解壓縮並撈出ProfitPO
			String tradeDetailStr = WString.unCompress(new String(tradeDetail, StandardCharsets.UTF_8));
			JSONObject tradeDetailJson = new JSONObject(tradeDetailStr);
			tradeDetailStr = tradeDetailJson.getString("ProfitPO");

			// 反序列化並解壓縮，取得ProfitPO物件
			profitDTO = (ProfitDTO) serializer.unCompressAndUnserializeByString(tradeDetailStr);
		} catch (Exception e) {
			WLog.error("Get Trade Detail Data Fail", e);
		}
		return profitDTO;
	}
}
