package com.willy.sias.backtest.strategy;

import com.willy.sias.backtest.dto.TradePlanDTO;
import com.willy.sias.backtest.service.ChipIdxService;
import com.willy.sias.backtest.service.TechDmsService;
import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.willy.sias.backtest.dto.ProfitDTO;

@Service
@Scope("prototype")
public class SIAS_6 extends TradeStrategy {

  @Autowired
  private ChipIdxService chipIdxSvc;
  @Autowired
  private TechDmsService techDmsSvc;
  @Autowired
  private TechIdxService techIdxSvc;
  @Autowired
  private PriceDailyTwRepository priceRepo;

  @Override
  public void collectTradePlanList() {
    //確認標的當日有交易
    PriceDailyTwPO price = priceRepo.findFirstBySidOrderByDataDate(this.getSid());
    List<Date> tradeDateList = this.getTradeDateList().stream()
        .filter(date -> WDate.afterOrEquals(date, price.getDataDate())).collect(
            Collectors.toList());
    for (Date transDate : tradeDateList) {

      if(transDate.equals(tradeDateList.get(tradeDateList.size()-1))){
        continue;
      }

      //近N日投信皆為買超
      Map<Integer, BigDecimal> itNetMap = chipIdxSvc.getRecentDaysItNetSum(this.getSid(),
          transDate);
      boolean isRecentAllBuyable =
          itNetMap.values().stream().filter(v -> v.compareTo(BigDecimal.ZERO) <= 0).count() == 0;

      //收盤在MA20上且MA20朝上
      Integer closeCompareToMA = techDmsSvc.closeCompareToMA(this.getSid(), transDate, 20);
      Integer maTrend = techDmsSvc.getMaTrend(this.getSid(), transDate, 20);
      boolean isMaUpTrend =
          closeCompareToMA != null && closeCompareToMA.compareTo(1) == 0 && maTrend != null
              && maTrend.compareTo(1) == 0;

      if (isRecentAllBuyable && isMaUpTrend) {
        this.addTradePlanList(new TradePlanDTO(this.getSid(), transDate, TradeType.buy, 1000,
            tu.getPrice(this.getSid(), transDate)));
        this.addTradePlanList(new TradePlanDTO(this.getSid(), transDate, TradeType.sell, 1000,
            tu.getPrice(this.getSid(), transDate)));
      }

    }


  }

  @Override
  public void collectTradePlanInfo(ProfitDTO po) {

  }

}
