package com.willy.sias.backtest.processor;

import com.willy.sias.backtest.service.ChipDmsService;
import com.willy.sias.backtest.service.ChipIdxService;
import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.po.LPTransLogTwPOKey;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.sias.db.repository.LP_FUTURE_TWRepository;
import com.willy.sias.db.repository.LP_OPTION_TWRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.FrReleaseDate;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public class CurrentChipStatusProcessor extends CurrentStatusProcessor {

  @Autowired
  private LPTransLog_TW_TWSERepository lpRepo;
  @Autowired
  private TechIdxService techIdxSvc;
  @Autowired
  private ChipIdxService ciSvc;
  @Autowired
  private ChipDmsService cdSvc;
  @Autowired
  private SysConfigRepository scRepo;
  @Autowired
  private LP_FUTURE_TWRepository futureRepo;
  @Autowired
  private LP_OPTION_TWRepository optionRepo;

  public CurrentChipStatusProcessor(Integer sid, Date analyzedate) {
    super();
    this.sid = sid;
    this.analyzedate = analyzedate;
  }

  public CurrentChipStatusProcessor() {
    super();
  }

  @Override
  public void exec() {
    //基本資訊
    SysConfigPO sc = scRepo.findById(this.sid).orElse(null);

    // 財報發放
    List<Date> frTimeSlot = FrReleaseDate.getFrTimeSlot(this.analyzedate);
    Date frCalEndDate = frTimeSlot.get(0);
    Date nextFrRelDate = frTimeSlot.get(1);

    //投信買入
    LPTransLogTwPO lpPo = lpRepo.findById(new LPTransLogTwPOKey(this.sid, this.analyzedate))
        .orElse(null);

    //投信持有
    BigDecimal itHoldRatio = cdSvc.getItHoldRatio(this.sid, this.analyzedate, this.analyzedate).get(0).getDecimalValue();

    //月線
    List<TimeSeriesDTO<BigDecimal>> ma20TsList = techIdxSvc.getSMA(this.sid, 20,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    //股本
    ciSvc.getIssuedSharesList(this.sid, null, null);
    //外資期貨
//    futureRepo

    //外資選擇權
//    optionRepo

    WLog.info(sc.getCfgValue() + " - " + sc.getCfgDesc());
    WLog.info("上季結束日["+frCalEndDate+"]上季財報發布日["+nextFrRelDate+"]");
    WLog.info("***三大法人***");
    WLog.info("投信持有比率[" + itHoldRatio.multiply(new BigDecimal(100)) + " %]");
//    WLog.info("投信買入比率[" + itHoldRatio.multiply(new BigDecimal(100)) + " %]");
  }

  @Override
  String getProcessorName() {
    // TODO Auto-generated method stub
    return "籌碼面";
  }
}
