package com.willy.sias.util;

import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class TypeUtil {

  /**
   * WType can't get cl declaredFields
   *
   * @param resultSet
   * @param cl
   * @param <T>
   * @return
   * @throws SQLException
   * @throws InstantiationException
   * @throws IllegalAccessException
   */
  public <T> T convertResultSetToBean(ResultSet resultSet, Class<T> cl)
      throws Exception {
// 取得rs相關資料
    int columnCount;
    ResultSetMetaData rsmd = resultSet.getMetaData();
    columnCount = rsmd.getColumnCount();
    // 塞進List
    T bean;
    List<String> lowerCaseFieldNameList = Arrays.asList(cl.getDeclaredFields()).stream()
        .map(f -> f.getName().toLowerCase()).collect(
            Collectors.toList());
    bean = cl.newInstance();
    for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
      String colName = WString.rmStr(rsmd.getColumnName(columnIndex), "_");
      if (lowerCaseFieldNameList.contains(colName.toLowerCase())) {
        setFieldValue(bean, colName, resultSet.getString(columnIndex));
      }
    }
    return bean;
  }

  public Object setFieldValue(Object obj, String fieldName, Object value) throws Exception {
    Object result = null;
    try {
      Field field = BeanFactory.getBean(this.getClass()).getFieldsMap(obj.getClass())
          .get(fieldName.toLowerCase());
      field.setAccessible(true);
      field.set(obj, WType.objToSpecificType(value, field.getType()));
    } catch (Exception e) {
      throw e;
    }
    return result;
  }

  public HashMap<String, Field> getFieldsMap(Class clazz) throws ExecutionException {
    HashMap<String, Field> fieldsMap = new HashMap();
    for (Class cl = clazz; cl != null; cl = cl.getSuperclass()) {
      Field[] fieldAr = cl.getDeclaredFields();

      for (int i = 0; i < fieldAr.length; i++) {
        Field f = fieldAr[i];
        fieldsMap.put(f.getName().toLowerCase(), f);
      }
    }
    return fieldsMap;
  }
}
