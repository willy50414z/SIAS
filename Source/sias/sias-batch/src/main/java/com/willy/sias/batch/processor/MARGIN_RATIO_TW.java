package com.willy.sias.batch.processor;

import com.willy.net.web.WSelenium;
import com.willy.net.web.WSelenium.WebDriverType;
import com.willy.sias.db.po.MarginRatioTwPO;
import com.willy.sias.db.repository.MarginRatioTwRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Setter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.MoveTargetOutOfBoundsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@Service
@Setter
@ConfigurationProperties(prefix = "sias.batch.margin-ratio-tw")
public class MARGIN_RATIO_TW extends BaseBatchProcessor {

  /**
   * 沒辦法直接抓到資料 所以要用recordMarginRatio()開啟瀏覽器 用滑鼠在圖上滑動，錄製html的資料 錄完再用debug把keepRecord改成false，存進DB
   */

  @Autowired
  private WSelenium su;
  @Autowired
  private MarginRatioTwRepository mrRepo;

  @Override
  public void cleanHisData() throws Exception {

  }

  public void exec() throws FileNotFoundException, InterruptedException {
    HashMap<String, String> marginRatioMap = this.recordMarginRatio();
    this.saveDataToDB(marginRatioMap);
  }

  public HashMap<String, String> recordMarginRatio()
      throws FileNotFoundException, InterruptedException {
    ChromeDriver wb = (ChromeDriver) su.getBrowser(WebDriverType.Chrome);
    //開啟網頁
    wb.get(this.urlTempl);
    //抓到滑鼠移動基準點
    WebElement mouseMoveStartElement = wb.findElementById("c2-53117").findElements(By.tagName("text")).stream().filter(text -> text.getText().equals("Percent")).findAny().get();
    Actions action = new Actions(wb);


    int moveXIndex = 0;
    boolean keepRecord = true;
    HashMap<String, String> dataList = new HashMap<>();
    //FIXME 移到共用邏輯，供撈此網站排程使用
    while (keepRecord) {
      //move mouse to trigger data display
      //滑鼠移到網頁上才會跳出資料
      try{
        action.moveToElement(mouseMoveStartElement, moveXIndex++, 0).perform();
      } catch (MoveTargetOutOfBoundsException e) {
        //移到超出畫面會拋exception即可結束
        keepRecord = false;
        break;
      }

      //retrieve data
      List<String> elementList = Jsoup.parse(wb.getPageSource()).getElementById("ccApp")
          .getElementsByTag("tspan")
          .stream().map(
              Element::text).collect(
              Collectors.toList());
      try {
        dataList.put(elementList.get(0), elementList.get(3));
      } catch (Exception e) {
      }
    }
    wb.close();
    return dataList;
  }

  public void saveDataToDB(HashMap<String, String> mapData) {
    HashMap<String, String> monthMap = new HashMap<String, String>() {{
      put("Jan", "01");
      put("Feb", "02");
      put("Mar", "03");
      put("Apr", "04");
      put("May", "05");
      put("Jun", "06");
      put("Jul", "07");
      put("Aug", "08");
      put("Sep", "09");
      put("Oct", "10");
      put("Nov", "11");
      put("Dec", "12");
    }};
    List<MarginRatioTwPO> mrList = new ArrayList<>();
    mapData.forEach((k, v) -> {
      k = k.substring(k.indexOf(",") + 1).trim();
      String month = monthMap.get(k.substring(0, 3));
      int commonIdx = k.indexOf(",");
      String day = WString.fillStrLength(k.substring(commonIdx - 2, commonIdx).trim(), "0", -2);
      String year = k.substring(k.length() - 4);
      Date date = WType.strToDate(year + month + day);
      if (v.trim().length() < 7) {
        mrList.add(
            new MarginRatioTwPO(Const._CFG_ID_MARKET_WEIGHTED_INDEX, date, new BigDecimal(v)));
      }
    });
    mrRepo.saveAll(mrList);
  }
}
