package com.willy.sias.batch.vo;

import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter @Setter
public class BatchStatusListVO {
	private String batchServiceName;
	private String batchName;
	private String lastExeTime;
	private String dataDate;
	private String failLog;
}
