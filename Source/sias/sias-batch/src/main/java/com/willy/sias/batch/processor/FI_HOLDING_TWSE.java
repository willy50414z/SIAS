package com.willy.sias.batch.processor;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.util.string.WRegex;
import com.willy.util.string.WString;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.fi-holding-twse")
public class FI_HOLDING_TWSE extends BaseBatchProcessor {
	@Autowired
	private WJsoup js;
	@Autowired
	private WFile_txt fileTxt;
	@Autowired
	private LPTransLog_TW_TWSERepository lpRepo;
	
	private String urlTempl;
	private String csvFileNameTempl;
	
	@Override
	public void cleanHisData() throws Exception {
		
	}

	@Override
	protected void exec() throws Exception {
		//下載資料檔
		String filePath = downloadFIHoldingTwseTable();
		
		//讀取資料檔
		String[][] fiHoldingAr = fileTxt.readToAr(filePath);
		
		mergeSid(fiHoldingAr);
		
		//撈出當天的lp資料更新進去
		List<LPTransLogTwPO> lpList = lpRepo.findByTransDateIs(this.dataDate);
		
		for(String[] fiHolding : fiHoldingAr) {
			LPTransLogTwPO lpPo = lpList.stream().filter(lp -> lp.getSid().compareTo(sidMap.get(fiHolding[0]))==0).findFirst().orElse(null);
			if(lpPo==null) {
				continue;
			}
			lpPo.setPublishCount(new BigDecimal(WString.cleanIntegerData(fiHolding[3])));
			lpPo.setFiHolding(new BigDecimal(WString.cleanIntegerData(fiHolding[5])));
			lpPo.setFiBuyable(new BigDecimal(WString.cleanIntegerData(fiHolding[4])));
			if(StringUtils.isNotEmpty(fiHolding[10])) {
				List<String> matchStrList = WRegex.getMatchedStrList(fiHolding[10], ">.{1}<");
				if(matchStrList.size()>0) {
					lpPo.setFiBuyableMdfReason(matchStrList.get(0).substring(1, 2));
				}
			}
		}
		lpRepo.saveAll(lpList);
	}
	private String downloadFIHoldingTwseTable() throws IOException, InterruptedException, ExecutionException, JSONException {
		String filePath = fileDownloadDir + parseMapParamToString(csvFileNameTempl);
		if(new File(filePath).exists()) {
			return filePath;
		}
		String data = js.getString(parseMapParamToString(urlTempl), "UTF-8");
		JSONObject json;
		json = new JSONObject(data);
		JSONArray dataAr = json.getJSONArray("data");
		StringBuffer sb = new StringBuffer();
		for(int i=0;i<dataAr.length();i++) {
			if(!StringUtils.isEmpty(dataAr.get(i).toString())) {
				sb.append(cleanDate(dataAr.get(i).toString())).append("\r\n");
			}
		}
		fileTxt.writeByStr(filePath, sb.toString());
		return filePath;
	}
	private String cleanDate(String rawStr) {
		rawStr = WString.rmStr(rawStr, "[", "]");
		StringBuffer sb = new StringBuffer();
		String[] strAr = rawStr.split(",");
		
		String tmpStr = "";
		for(String s : strAr) {
			s = WString.rmStr(s, "\\", "/");
			if((s.contains("\"") || !"".equals(tmpStr)) && !WRegex.match(s, "\".*\"")) {
				tmpStr += s;
				if(WRegex.match(tmpStr, "\".*\"")) {
					sb.append(WString.rmStr(tmpStr, ",")).append(",");
					tmpStr = "";
				}
			}else {
				if(WRegex.match(s, "\".*\"")) {
					sb.append(s).append(",");
				} else {
					sb.append("\"").append(s).append("\",");
				}
			}
		}
		sb.setLength(sb.length()-1);
		return sb.toString();
	}
}
