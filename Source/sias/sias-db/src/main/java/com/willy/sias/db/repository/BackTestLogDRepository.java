package com.willy.sias.db.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.BackTestLogDPO;

@Repository
public interface BackTestLogDRepository extends JpaRepository<BackTestLogDPO, Integer> {
	public List<BackTestLogDPO> findByTestId(Integer testId);
}
