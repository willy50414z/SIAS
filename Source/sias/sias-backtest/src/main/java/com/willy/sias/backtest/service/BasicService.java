package com.willy.sias.backtest.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.willy.sias.backtest.dto.DividendFillCDivDTO;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.OHLCVDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.DividendTWPO;
import com.willy.sias.db.repository.DividendTWRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;

/**
 * 基本分析
 * 
 * @author Willy
 *
 */
@Service
public class BasicService {
	@Autowired
	private DividendTWRepository dividendRepo;
	@Autowired
	private DataTableDao dtDao;

	// 填息日數
	public List<DividendFillCDivDTO> getDaysOfFillCashDiv(int sid, int year) throws Exception {
		return getDaysOfFillCashDiv(Arrays.asList(sid), year);
	}
	public List<DividendFillCDivDTO> getDaysOfFillCashDiv(Set<Integer> sidSet, int year) throws Exception {
		ArrayList<Integer> list = new ArrayList<>();
		list.addAll(sidSet);
		return getDaysOfFillCashDiv(list, year);
	}
	public List<DividendFillCDivDTO> getDaysOfFillCashDiv(List<Integer> sidList, int year) throws Exception {
		List<DividendFillCDivDTO> divFillDayList = new ArrayList<>();
		// year為-1撈出所有資料
		List<DividendTWPO> divList = (year == -1) ? dividendRepo.findBySidIn(sidList)
				: dividendRepo.findBySidInAndExSDivDateOrExCDivDateEqualsYear(sidList, year);
		for (int sid : sidList) {
			List<TimeSeriesDTO<OHLCVDTO>> ohlcTsList;
			synchronized(dtDao) {
				ohlcTsList = dtDao.findTsList(sid, Const._FIELDID_LIST_OHLCV,
						OHLCVDTO.class);
			}
			for (DividendTWPO div : divList.stream().filter(div -> div.getSid().compareTo(sid) == 0)
					.collect(Collectors.toList())) {
				// 有現金股利或股票股利
				if ((div.getExCDivDate() != null && div.getCDiv() != null)) {
					// 取出發放日期(不論股票或現金)
					Date tmpDate = div.getExCDivDate() == null ? div.getExSDivDate() : div.getExCDivDate();
					// 取出除權息當天價格
					TimeSeriesDTO<OHLCVDTO> exOHLC = ohlcTsList.stream()
							.filter(oh -> oh.getDate().compareTo(tmpDate) == 0).findFirst().orElse(null);
					if (exOHLC == null || exOHLC.getValue() == null || exOHLC.getValue().getOpen() == null) {
						continue;
					}
					// 除權息後最高價>填息價Ts
					TimeSeriesDTO<OHLCVDTO> fillDivOhlc = ohlcTsList.stream()
							.filter(oh -> WDate.afterOrEquals(oh.getDate(), tmpDate) && oh.getValue().getHigh()
									.compareTo(exOHLC.getValue().getOpen().add(div.getCDiv())) > 0)
							.findFirst().orElse(null);
					WLog.debug("SID[" + sid + "]除權息日[" + WType.dateToStr(tmpDate) + "]填權息日["
							+ ((fillDivOhlc == null) ? "" : fillDivOhlc.getDate()) + "]股票股利[" + div.getSDiv() + "]現金股利["
							+ div.getCDiv() + "]");
					if (exOHLC != null && fillDivOhlc != null) {
						// 有除權息前一天股價及填權息日 => add 填權息天數
						Long fillDays = ohlcTsList.stream().filter(oh -> oh.getDate().after(tmpDate)
								&& WDate.beforeOrEquals(oh.getDate(), fillDivOhlc.getDate())).count();
						// 填權息前最低收盤價
						BigDecimal minCloseAfterEx = ohlcTsList.stream()
								.filter(oh -> WDate.afterOrEquals(oh.getDate(), tmpDate)
										&& WDate.beforeOrEquals(oh.getDate(), fillDivOhlc.getDate()))
								.map(oh -> oh.getValue().getClose()).min(Const._DEFAULT_COMPARATOR_BIGDECIMAL)
								.orElse(null);

						divFillDayList.add(new DividendFillCDivDTO(div, fillDays.intValue(),
								minCloseAfterEx.divide(exOHLC.getValue().getOpen(), 4, RoundingMode.HALF_UP)
										.subtract(Const._BIGDECIMAL_1)));
					} else if (exOHLC != null && fillDivOhlc == null) {
						// 有除權息前一天股價但沒有填權息 => add -1表示未被填權息
						divFillDayList.add(new DividendFillCDivDTO(div, -1, Const._BIGDECIMAL_m1));
					}
				}
			}
		}
		return divFillDayList;
	}
}
