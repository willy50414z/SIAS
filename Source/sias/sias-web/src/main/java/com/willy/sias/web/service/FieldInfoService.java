package com.willy.sias.web.service;

import com.google.gson.Gson;
import com.willy.sias.db.po.FieldInfoPO;
import com.willy.sias.db.repository.FieldInfoRepository;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.sias.web.vo.FieldInfoVO;
import com.willy.util.string.WRegex;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldInfoService {

  @Autowired
  private Gson gs;

  @Autowired
  private FieldInfoRepository fiRepo;

  private JSONObject result;

  public FieldInfoVO getFieldInfoVo(FieldInfoPO fieldInfoPO) {
    FieldInfoVO fiVo = new FieldInfoVO();

    //FieldInfoJs
    fiVo.setFieldInfoJs(gs.toJson(fieldInfoPO));

    //arguments
    LinkedList<String> argNameList = new LinkedList<>();
    if (fieldInfoPO.getProcessInfo() != null) {
      String[] processAr = fieldInfoPO.getProcessInfo().split("#");
      if (processAr.length > 2) {
        Collections.addAll(argNameList, processAr[2].split(","));
      }
      argNameList.addAll(StringUtil.retrieveUniqueParamKeyList(fieldInfoPO.getProcessInfo()));
    }
    fiVo.setProcessArgList(argNameList);
    return fiVo;
  }

  public FieldInfoVO getSubCatOrFieldNameList(String catName, List<FieldInfoPO> fiList) {
    FieldInfoVO result = new FieldInfoVO();

    result.setSubCatFieldNameSet(fiList.stream()
        .map(fi -> {
          String subCat = fi.getFieldName().substring(catName.length() + 1);
          if (subCat.contains(Const.FIELD_CAT_SPLIT_STR)) {
            return subCat.split(Const.FIELD_CAT_SPLIT_STR)[0];
          } else {
            return subCat;
          }
        }).sorted(Comparator.naturalOrder()).collect(
            Collectors.toCollection(LinkedHashSet::new)));
    return result;
  }

  public String getFieldDesc(String fieldBlockStr) throws JSONException {
    //replace varable in blockStr
    final String tmpBlockStr = fieldBlockStr;

    List<String> varList = WRegex.getMatchedStrList(fieldBlockStr, "[$]{1}[^=]+[=].*");
    for(String var : varList) {
      String[] variableAndValueAr = var.split("=");
      fieldBlockStr = fieldBlockStr.replace(var + "\n", "");
      fieldBlockStr = fieldBlockStr.replace(variableAndValueAr[0], variableAndValueAr[1]);
    }

    int fieldBlockIdx = 0;
    Set<String> fieldBlockSet = null;
    final String fieldBlockDescKeyPrefix = "blockDescKey";
    Map<String, String> fieldBlockDescMap = new LinkedHashMap<>();
    while (fieldBlockSet == null || fieldBlockSet.size() > 0) {
      fieldBlockSet = WRegex.getMatchedStrSet(fieldBlockStr, WRegex._REGEX_SQUARE_BRACKETS_WRAPED);

      for (String fieldBlock : fieldBlockSet) {

        //get field Id set
        Set<Integer> fieldIdSet = WRegex.getMatchedStrSet(fieldBlock, "[{][^}{][0-9]{0,3}[}]").stream()
            .map(str -> Integer.parseInt(str.substring(1, str.indexOf("}"))))
            .collect(Collectors.toSet());

        //query fieldInfos
        List<FieldInfoPO> fieldInfoList = fiRepo.findAllById(fieldIdSet);

        //put field desc to map
        for (FieldInfoPO fieldInfoPO : fieldInfoList) {
          FieldInfoVO fieldInfoVo = this.getFieldInfoVo(fieldInfoPO);
          Set<String> methodSet = WRegex.getMatchedStrSet(fieldBlock,
              "[\\{][" + fieldInfoPO.getFieldId() + "]{1,}[\\}][(][^()]*[)]");
          for (String method : methodSet) {
            String fieldDesc =
                fieldInfoPO.getFieldName()
                    .substring(fieldInfoPO.getFieldName().lastIndexOf(":") + 1)
                    + "(";
            List<String> argNameList = fieldInfoVo.getProcessArgList();
            if (argNameList !=null && argNameList.size() > 0) {
              String[] args = method.substring(method.indexOf("(") + 1, method.indexOf(")"))
                  .split(",");
              for (int i = 0; i < argNameList.size(); i++) {
                fieldDesc += argNameList.get(i) + ": " + ((args.length > i) ? args[i] : "") + ", ";
              }
              fieldDesc = fieldDesc.substring(0, fieldDesc.length() - 2);
            }
            fieldDesc += ")";

            String blockDescKey = fieldBlockDescKeyPrefix + fieldBlockIdx;
            fieldBlockStr = fieldBlockStr.replace(fieldBlock, "${" + blockDescKey + "}");
            fieldBlock = fieldBlock.replace(method, fieldDesc);
            fieldBlockDescMap.put(blockDescKey, fieldBlock);
            fieldBlockIdx++;
          }
        }
      }
    }

    //replace descKey in descMap and remove map data which no need
    Set<String> redundantKey = new HashSet<>();
    fieldBlockDescMap.forEach((k, v) -> {
      Set<String> keySet = WRegex.getMatchedStrSet(v, WRegex._REGEX_PARAMETER_WRAPED).stream()
          .map(key -> key.substring(2, key.length() - 1)).collect(
              Collectors.toSet());
      if (keySet.size() > 0) {
        for (String key : keySet) {
          redundantKey.add(key);
          fieldBlockDescMap.put(k, v.replace("${" + key + "}", fieldBlockDescMap.get(key)));
        }
      }
    });
    redundantKey.forEach(fieldBlockDescMap::remove);

    //replace map key to value
    for (String key : fieldBlockDescMap.keySet()) {
      fieldBlockStr = fieldBlockStr.replace("${" + key + "}", fieldBlockDescMap.get(key));
    }
    return fieldBlockStr;
  }
}
