//package com.willy.sias.backtest.strategy;
//
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.db.dto.OHLCVDTO;
//import com.willy.sias.db.dto.TimeSeriesDTO;
//import com.willy.sias.db.po.SysConfigPO;
//import com.willy.sias.db.repository.SysConfigRepository;
//import com.willy.sias.util.config.Const;
//import com.willy.util.date.WDate;
//import com.willy.util.log.WLog;
//import com.willy.util.string.WString;
//
//@Service
//public class SaveToDBStrategy extends TradeStrategy {
//	@Autowired
//	private SysConfigRepository scRepo;
//	private String tableName = "";
//	
//	@Override
//	public void setTradeDetail(Integer sid, List<Date> tradeDateList, ProfitPO profitPo) {
//		/**
//		 * 背離第一天買，直到
//		 */
//		List<TimeSeriesDTO<OHLCVDTO>> ohlcTsList;
//		List<TimeSeriesDTO<BigDecimal>> closeTsList = null, volTsList = null, highTsList = null
//				, ma5 = null, vma5 = null, ma10 = null, ma20 = null, ma60 = null, ma120 = null
//				, growth40 = null, max40 = null, growth5 = null, maxMaAmplitude = null;
//		
//		
//		try {
//			ohlcTsList = dtDao.<OHLCVDTO>findTdsList(sid, Const._FIELDID_LIST_OHLCV, OHLCVDTO.class);
//			
//			volTsList = new ArrayList<> ();
//			closeTsList = new ArrayList<> ();
//			highTsList = new ArrayList<> ();
//			for(TimeSeriesDTO<OHLCVDTO> ohlcTs : ohlcTsList) {
//				closeTsList.add(new TimeSeriesDTO<BigDecimal>(ohlcTs.getDate(), ohlcTs.getValue().getClose()));
//				volTsList.add(new TimeSeriesDTO<BigDecimal>(ohlcTs.getDate(), ohlcTs.getValue().getTradingShares()));
//				highTsList.add(new TimeSeriesDTO<BigDecimal>(ohlcTs.getDate(), ohlcTs.getValue().getHigh()));
//			}
//			ma5 = statService.getSMA(closeTsList, 5);
//			ma10 = statService.getSMA(closeTsList, 10);
//			ma20 = statService.getSMA(closeTsList, 20);
//			ma60 = statService.getSMA(closeTsList, 60);
//			ma120 = statService.getSMA(closeTsList, 120);
//			vma5 = statService.getSMA(volTsList, 5);
//			growth40 = statService.getGrowth(closeTsList, 40);
//			max40 = statService.getRecentHigh(highTsList, 40);
//			growth5 = statService.getGrowth(closeTsList, 5);
//			maxMaAmplitude = statService.getMaxAmplitude(ma5, ma10, ma20, ma60);
//			export(sid, tradeDateList, new String[] {"ma5", "ma10", "ma20", "ma60", "ma120", "vma5", "max40", "growth5", "growth40", "maxAmplitude"}, ohlcTsList, ma5, ma10, ma20, ma60, ma120, vma5, max40, growth5, growth40, maxMaAmplitude);
//			
//			
////			if(closeTsList==null || ma5 == null || ma20 == null || ma120==null) {
////				WLog.error("sid["+sid+"] can't get enough data, "+(closeTsList==null?"closeTsList is null ":"")+(ma5==null?"ma5 is null ":"")+(ma20==null?"ma20 is null ":""));
////				return;
////			}
////			if(closeTsList.size() != ma5.size() || closeTsList.size() != ma20.size()) {
////				WLog.error("sid["+sid+"] can't get match size data, closeTsList.size()["+closeTsList.size()+"]ma5.size()["+ma5.size()+"]ma20.size()["+ma20.size()+"]");
////				return;
////			}
////			
//			//取出MA5與MA20近40交易日交叉>3次日期的Index
////			int isMa20LtMa5 = 0, wasMa20LtMa5 = 5;
////			List<Integer> crossIndexList = new ArrayList<> ();
////			Set<Integer> crossCriterMeetIndexSet = new HashSet<> ();
////			for(int i=0;i<closeTsList.size();i++) {
////				if(!tradeDateList.contains(closeTsList.get(i).getDate())) {
////					continue;
////				}
////				if (closeTsList.get(i)!= null && ma20.get(i) != null && ma20.get(i).getValue() != null && ma5.get(i) != null && ma5.get(i).getValue() != null) {
////					isMa20LtMa5 = ma20.get(i).getDecimalValue().compareTo(ma5.get(i).getDecimalValue());
////					wasMa20LtMa5 = (wasMa20LtMa5==5) ? isMa20LtMa5 : wasMa20LtMa5;//第一次進入此行時wasMa20LtMa5=isMa20LtMa5
////					if(wasMa20LtMa5 != isMa20LtMa5) {
////						crossIndexList.add(i);
////						wasMa20LtMa5 = isMa20LtMa5;
////						if (crossIndexList.size() > 2) {
////							int firstCrossIndex = crossIndexList.get(crossIndexList.size()-3);
////							int lastCrossIndex = crossIndexList.get(crossIndexList.size()-1);
////							for (int j = lastCrossIndex; j < Math.min((firstCrossIndex + 39), closeTsList.size()-1); j++) {
////								crossCriterMeetIndexSet.add(j);
////							}
////						}
////					}
////				}
////			}
//			
//			//T日為圖破盤整隔天
//			//取出40天內漲跌幅<3%的
////			for(int crossCriterMeetIndex = 41; crossCriterMeetIndex<volTsList.size()-2;crossCriterMeetIndex++) {
////				if(crossCriterMeetIndex > 40) {
////					//40天振福
////					BigDecimal growth40 = null;
////					try {
////						growth40 = closeTsList.get(crossCriterMeetIndex-2).getDecimalValue().divide(closeTsList.get(crossCriterMeetIndex-41).getDecimalValue(), 4, RoundingMode.HALF_UP);
////					}catch(Exception e) {
////						growth40 = null;
////						continue;
////					}
////					
////					//40天最高價
////					BigDecimal max40 = new BigDecimal(0);
////					for(int i=crossCriterMeetIndex-41;i<crossCriterMeetIndex-1;i++) {
////						max40 = max40.max(ohlcTsList.get(i).getValue().getHigh());
////					}
////					//成交量
////	//				Date closeDate = closeTsList.get(crossCriterMeetIndex-1).getDate();
////	//				TimeSeriesDTO<BigDecimal> openTs = openTsList.stream().filter(op -> op.getDate().equals(closeDate)).findFirst().orElse(null);
////	//				if(openTs != null) {
////	//					open = openTs.getDecimalValue();
////	//				}
////	//				if(open == null) {
////	//					WLog.error("can't get open by date["+closeTsList.get(crossCriterMeetIndex-1).getDate()+"]");
////	//					continue;
////	//				}
////					
////					BigDecimal open = ohlcTsList.get(crossCriterMeetIndex).getValue().getOpen();
////					BigDecimal tGrowth = closeTsList.get(crossCriterMeetIndex-1).getDecimalValue().divide(closeTsList.get(crossCriterMeetIndex-2).getDecimalValue(), 4, RoundingMode.HALF_UP);
////					BigDecimal solidGrowth = closeTsList.get(crossCriterMeetIndex-1).getDecimalValue().divide(ohlcTsList.get(crossCriterMeetIndex-1).getValue().getOpen(), 4, RoundingMode.HALF_UP);
//////					WLog.info("sid["+sid+"]date["+closeTsList.get(crossCriterMeetIndex).getDate()+"]growth40["+growth40+"]max40["+max40+"]tGrowth["+tGrowth+"]solidGrowth["+solidGrowth+"]-2close["+closeTsList.get(crossCriterMeetIndex-2).getDecimalValue()+"]");
//////					WLog.info("ma120Date["+(ma120.get(crossCriterMeetIndex-1)==null?"null":ma120.get(crossCriterMeetIndex-1).getDate())+"]price["+(ma120.get(crossCriterMeetIndex-1)==null?"null":ma120.get(crossCriterMeetIndex-1).getDecimalValue())+"]vma5.get(crossCriterMeetIndex-1).getDecimalValue()["+vma5.get(crossCriterMeetIndex-1).getDecimalValue()+"]volTsList.get(crossCriterMeetIndex-1).getDecimalValue()["+volTsList.get(crossCriterMeetIndex-1).getDecimalValue()+"]");
////					if(growth40.subtract(new BigDecimal(1)).abs().compareTo(new BigDecimal(0.03))<0
////							//當天漲幅>5%
////							&& tGrowth.compareTo(new BigDecimal(1.05))>0
////							//破2個月新高 
////							&& max40.compareTo(closeTsList.get(crossCriterMeetIndex-1).getDecimalValue())<0
//////							&& max40.compareTo(ma120.get(crossCriterMeetIndex-1).getDecimalValue())<0
////							//當天紅K>3%
////							&& solidGrowth.compareTo(new BigDecimal(1.03))>0
////							//多頭排列
////							&& ma60.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(ma20.get(crossCriterMeetIndex-1).getDecimalValue())<0
////							&& ma20.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(ma5.get(crossCriterMeetIndex-1).getDecimalValue())<0
////							&& ma120.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(ma60.get(crossCriterMeetIndex-1).getDecimalValue())<0
////							//成交量MA5>2000
////							&& vma5.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(new BigDecimal(2000000))>0
////							//成交量>成交量MA5 * 2
////							&& volTsList.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(vma5.get(crossCriterMeetIndex-1).getDecimalValue().multiply(new BigDecimal(2)))>0
////							//MA120>所有均線
//////							&& (ma120.get(crossCriterMeetIndex-1) != null && ma120.get(crossCriterMeetIndex-1).getValue() != null
//////							&& ma120.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(ma60.get(crossCriterMeetIndex-1).getDecimalValue())>0
//////							&& ma120.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(ma5.get(crossCriterMeetIndex-1).getDecimalValue().max(ma10.get(crossCriterMeetIndex-1).getDecimalValue().max(ma20.get(crossCriterMeetIndex-1).getDecimalValue().max(ma60.get(crossCriterMeetIndex-1).getDecimalValue()))))>0)
//////							&& ma120.get(crossCriterMeetIndex-1).getDecimalValue().compareTo(closeTsList.get(crossCriterMeetIndex-1).getDecimalValue())<0
//////							//近5天漲幅<5%
//////							&& closeTsList.get(crossCriterMeetIndex-2).getDecimalValue().divide(closeTsList.get(crossCriterMeetIndex-7).getDecimalValue(), 4, RoundingMode.HALF_UP).compareTo(new BigDecimal(1.05))<0
//////							//隔天開盤未漲停
//////							&& open.compareTo(tu.getDailyLimitPrice(ohlcTsList.get(crossCriterMeetIndex-1).getValue().getClose(), false)) < 0
////							) {
//////						tu.trade(sid, closeTsList.get(crossCriterMeetIndex-1).getDate(), 1, TradeType.buy, ohlcTsList.get(crossCriterMeetIndex-1).getValue().getClose(), profitPo);
//////						tu.trade(sid, closeTsList.get(crossCriterMeetIndex+2).getDate(), 1, TradeType.sell, ohlcTsList.get(crossCriterMeetIndex+2).getValue().getClose(), profitPo);
//////						//每上漲2%放空一張
//////						BigDecimal sellPrice = open;
//////						BigDecimal high = ohlcTsList.get(crossCriterMeetIndex).getValue().getHigh();
//////						int buyCount = 0;
//////						while(sellPrice.compareTo(high)<=0) {
//////							
//////							sellPrice = sellPrice.multiply(new BigDecimal(1.02), new MathContext(2,RoundingMode.CEILING));
//////							buyCount+=1;
//////						}
////						
//////						if(ohlcTsList.get(crossCriterMeetIndex+1).getValue().getClose().divide(ohlcTsList.get(crossCriterMeetIndex+1).getValue().getOpen(), 4, RoundingMode.HALF_UP).compareTo(new BigDecimal(1))>0
//////								&& ohlcTsList.get(crossCriterMeetIndex+2).getValue().getClose().divide(ohlcTsList.get(crossCriterMeetIndex+2).getValue().getOpen(), 4, RoundingMode.HALF_UP).compareTo(new BigDecimal(1))>0) {
//////							tu.trade(sid, closeTsList.get(crossCriterMeetIndex+2).getDate(), buyCount, TradeType.shortBuying, PriceType.CLOSE, profitPo);
//////						} else {
//////							tu.trade(sid, closeTsList.get(crossCriterMeetIndex+3).getDate(), buyCount, TradeType.shortBuying, PriceType.CLOSE, profitPo);
//////						}
////						
//////						tu.trade(sid, closeTsList.get(crossCriterMeetIndex).getDate(), buyCount, TradeType.shortBuying, PriceType.CLOSE, profitPo);
////						
////	//					tu.trade(sid, closeTsList.get(crossCriterMeetIndex).getDate(), 1, TradeType.shortSelling, PriceType.OPEN, profitPo);
////	//					if(openTsList.get(crossCriterMeetIndex).getDecimalValue().compareTo(closeTsList.get(crossCriterMeetIndex-1).getDecimalValue())<)
////	//					tu.trade(sid, closeTsList.get(crossCriterMeetIndex).getDate(), 1, TradeType.shortSelling, PriceType.OPEN, profitPo);
////					}
////				}
////			}
//		}catch(Exception e) {
//			WLog.error(e);
//		}
////		for(Date tradeDate : tradeDateList) {
////			if()
////			//篩選出MA5與MA20近40交易日交叉>3次的日期
////			int isMa20LtMa5 = ma20.get(i).getDecimalValue().compareTo(ma5.get(i).getDecimalValue());
////			wasMa20LtMa5 = (i==ma20.size()-1) ? isMa20LtMa5 : wasMa20LtMa5;
//////			for(int i=ma20.size()-1;i>19;i--) {
//////				if(ma20.get(i).getDecimalValue()!=null && ma5.get(i).getDecimalValue()!=null) {
//////					int isMa20LtMa5 = ma20.get(i).getDecimalValue().compareTo(ma5.get(i).getDecimalValue());
//////					wasMa20LtMa5 = (i==ma20.size()-1) ? isMa20LtMa5 : wasMa20LtMa5;
////////					System.out.println("wasMa20LtMa5["+wasMa20LtMa5+"]isMa20LtMa5["+isMa20LtMa5+"]tradeDate["+tradeDate+"]ma20Date["+ma20.get(i).getDate()+"]ma20["+ma20.get(i).getDecimalValue()+"]ma5Date["+ma5.get(i).getDate()+"]ma5["+ma5.get(i).getDecimalValue()+"]");
//////					if(wasMa20LtMa5 != isMa20LtMa5) {
//////						crossTimes++;
//////						wasMa20LtMa5 = isMa20LtMa5;
//////					}
//////				}
//////			}
////		}
//	}
//	private void export(Integer sid, List<Date> tradeDateList, String[] title, List<TimeSeriesDTO<OHLCVDTO>> ohlcTsList, List<TimeSeriesDTO<BigDecimal>>... bdTsList) {
//		SysConfigPO sc = scRepo.findById(sid).orElse(null);
//		if(ohlcTsList == null) {
//			throw new NullPointerException("ohlcTsList is null");
//		}
//		if(bdTsList == null) {
//			throw new NullPointerException("bdTsList is null");
//		}
//		for(int i=0;i<bdTsList.length;i++) {
//			if(bdTsList[i] == null) {
//				WLog.error("bdTsList["+i+"]title["+title[i]+"] is null");
//				return;
//			}
////			WLog.info("bdTsList["+i+"].size()["+bdTsList[i].size()+"]");
//		}
//		if(title.length != bdTsList.length) {
//			throw new IllegalArgumentException("title.length["+title.length+"] != bdTsList.length["+bdTsList.length+"]");
//		}
//		
//		//沒有建立過Table走CREATE Table流程
//		if(tableName.equals("")) {
//			dropExpiredTable(10);
//			tableName = "BACKTEST_" + WDate.getNowTime("yyyyMMddhhmmss");
//			StringBuffer createTableSql = new StringBuffer("CREATE TABLE ").append(tableName).append(
//					"([SID] INT,[SIDCODE] NVARCHAR(50),[SIDNAME] NVARCHAR(50),[DATE] DATETIME,[OPEN] DECIMAL(9,2),[HIGH] DECIMAL(9,2),[LOW] DECIMAL(9,2),[CLOSE] DECIMAL(9,2),[TRADINGSHARES] INT");
//			for (int i = 0; i < title.length; i++) {
//				createTableSql.append(", [").append(title[i].toUpperCase()).append("] DECIMAL(18,6)");
//			}
//			createTableSql.append(", CONSTRAINT [PK_").append(tableName).append("] PRIMARY KEY CLUSTERED ") 
//			.append("([SID] ASC,[DATE] ASC))");
//			WLog.info("create table sql="+createTableSql.toString());
//			jdbc.jdbcTemplate.execute(createTableSql.toString());
//		}
//		
//		
//		StringBuffer insertSql = new StringBuffer("insert into ").append(tableName).append(" values(?");
//		for(int i=0;i<9+bdTsList.length-1;i++) {
//			insertSql.append(",?");
//		}
//		insertSql.append(")");
//		
//		List<Object[]> batchArgs = new ArrayList<> ();
//		for (int i = 0; i < ohlcTsList.size(); i++) {
//			if(!tradeDateList.contains(ohlcTsList.get(i).getDate())) {
//				continue;
//			}
//			Object[] objAr = new Object[9+bdTsList.length];
//			objAr[0] = sid;objAr[1] = sc.getCfgValue();objAr[2] = sc.getCfgDesc().trim();objAr[3] = ohlcTsList.get(i).getDate();objAr[4] = ohlcTsList.get(i).getValue().getOpen();
//			objAr[5] = ohlcTsList.get(i).getValue().getHigh();objAr[6] = ohlcTsList.get(i).getValue().getLow();objAr[7] = ohlcTsList.get(i).getValue().getClose();objAr[8] = ohlcTsList.get(i).getValue().getTradingShares();
//			for(int j=0;j<bdTsList.length;j++) {
//				objAr[j+9] = bdTsList[j].get(i).getDecimalValue();
//			}
//			batchArgs.add(objAr);
//		}
//		jdbc.jdbcTemplate.batchUpdate(insertSql.toString(), batchArgs);
//	}
//	
//	
//}
