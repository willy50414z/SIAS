package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import lombok.Data;

/**
 * Dividend
 * 
 * @author
 */
@Table(name = "Dividend_TW")
@Entity
@IdClass(DividendTWPOKey.class)
@Data
public class DividendTWPO implements Serializable {
	/**
	 * 公告時間
	 */
	@Column(name = "DATA_DATE")
	private Date dataDate;
	/**
	 * 股票股利_盈餘轉增資配股(Revenue)
	 */
	@Column(name = "S_DIV_REV")
	private BigDecimal sDivRev;

	/**
	 * 股票股利_法定盈餘公積、資本公積轉增資(Legalreser
	 */
	@Column(name = "S_DIV_LEG_RES")
	private BigDecimal sDivLegres;
	
	/**
	 * 現金股利_盈餘分配之股東現金股
	 */
	@Column(name = "C_DIV_REV")
	private BigDecimal cDivRev;

	/**
	 * 現金股利_法定盈餘公積、資本公積發放之現金
	 */
	@Column(name = "C_DIV_LEG_RES")
	private BigDecimal cDivLegres;
	
	/**
	 * 除權加總
	 */
	@Formula("S_DIV_REV + S_DIV_LEG_RES")
	@Column(name = "S_DIV")
	private BigDecimal sDiv;
	
	/**
	 * 除息加總
	 */
	@Formula("C_DIV_REV + C_DIV_LEG_RES")
	@Column(name = "C_DIV")
	private BigDecimal cDiv;
	
	/**
	 * 除權交易日
	 */
	@Column(name = "EX_S_DIV_DATE")
	private Date exSDivDate;
	
	/**
	 * 除息交易日
	 */
	@Column(name = "EX_C_DIV_DATE")
	private Date exCDivDate;

	/**
	 * 除息發放日
	 */
	@Column(name = "PU_C_DIV_DATE")
	private Date puCDivDate;


	private static final long serialVersionUID = 1L;
	
	/**
	 * 股票代號
	 */
	@Id
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 股利所屬年度
	 */
	@Id
	@Column(name = "DIV_BELONGS")
	private String divBelongs;
	
	/**
	 * 權利分派基準日
	 */
	@Id
	@Column(name = "PU_RIGHT_BASE_DATE")
	private Date puRightBaseDate;
}