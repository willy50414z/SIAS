package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * StockPrice_TW
 * @author 
 */
@Table(name="PRICE_DAILY_TW")
@Entity
@Data
@IdClass(PriceDailyTwPOKey.class)
@NoArgsConstructor
public class PriceDailyTwPO implements Serializable {
	 /**
     * 標的代號
     */
    @Id
    @Column(name = "SID")
    private Integer sid;

    /**
     * 資料日期
     */
    @Id
    @Column(name = "DATA_DATE")
    private Date dataDate;
    /**
     * 開盤價
     */
    @Column(name = "\"OPEN\"")
    private BigDecimal open;

    /**
     * 最高價
     */
    @Column(name = "HIGH")
    private BigDecimal high;

    /**
     * 最低價
     */
    @Column(name = "LOW")
    private BigDecimal low;

    /**
     * 收盤價
     */
    @Column(name = "\"CLOSE\"")
    private BigDecimal close;

    /**
     * 成交金額
     */
    @Column(name = "TURN_OVER")
    private Long turnOver;

    /**
     * 成交股數
     */
    @Column(name = "TRADING_SHARES")
    private Long tradingShares;

    /**
     * 成交筆數
     */
    @Column(name = "TRADING_NUM")
    private Integer tradingNum;

    /**
     * 最後買價
     */
    @Column(name = "LAST_BUY_PRICE")
    private BigDecimal lastBuyPrice;

    /**
     * 最後買量
     */
    @Column(name = "LAST_BUY_VOL")
    private Integer lastBuyVol;

    /**
     * 最後賣價
     */
    @Column(name = "LAST_SOLD_PRICE")
    private BigDecimal lastSoldPrice;

    /**
     * 最後賣量
     */
    @Column(name = "LAST_SOLD_VOL")
    private Integer lastSoldVol;

    private static final long serialVersionUID = 1L;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PriceDailyTwPO other = (PriceDailyTwPO) obj;
		if (dataDate == null) {
			if (other.dataDate != null)
				return false;
		} else if (!dataDate.equals(other.dataDate))
			return false;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataDate == null) ? 0 : dataDate.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		return result;
	}

	public PriceDailyTwPO(Integer sid, Date dataDate, BigDecimal open, BigDecimal high, BigDecimal low,
			BigDecimal close) {
		super();
		this.sid = sid;
		this.dataDate = dataDate;
		this.open = open;
		this.high = high;
		this.low = low;
		this.close = close;
	}
    
}