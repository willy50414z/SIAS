package com.willy.sias.batch.processor;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.lptranslog-tw-tpex")
public class LP_TXN_LOG_TW_TPEX extends BaseBatchProcessor {
	@Autowired
	private LPTransLog_TW_TWSERepository lptRepo;
	@Value("${sias.downloadFile.dir}")
	private String downloadFileDir;
	private String urlTempl1;
	@Autowired
	private WJsoup jsoup;
	@Transactional
	public void exec() throws Exception {
		//沒檔案上線查
		String data = getRawData();
		if(data.length()==0) {
			WLog.error("RawData獲取失敗");
		}
		JSONObject js = new JSONObject(data);
		JSONArray jsa = js.getJSONArray("aaData");
		if(jsa == null || jsa.length() == 0) {
			WLog.info(this.dataDate + "當天非交易日");
			return;
		}
		//mergeSid
		this.mergeSid(jsa, 0, 1);
		
		//組BeanList
		List<LPTransLogTwPO> lpList = getLpList(jsa);
//		LPTransLogTwPO lp = new LPTransLogTwPO();
//		lp.setSid(117498);
//		lp.setTransDate(WType.strToDate("20150504"));
//		lp.setFiBuy(123);
//		lp.setFiSell(123);
//		lp.setItBuy(45000);
//		lp.setItSell(0);
//		lptRepo.save(lp);
//		List<LPTransLogTwPO> lpList = new ArrayList<>();
//		lpList.add(lp);
		lptRepo.saveAll(lpList);
	}
	
	private String getRawData() throws IOException, InterruptedException, ExecutionException, ParseException {
		String data = "";
		String filePath = downloadFileDir + this.parseMapParamToString(csvFileNameTempl);
		//讀檔
		if(new File(filePath).exists()) {
			data = txtFile.readToStr(filePath);
			return data;
		} else {
			String year = WDate.convertCEYear(WDate.getDateInfo(this.dataDate, Calendar.YEAR) + "");
			String urlMMdd = WType.dateToStr(this.dataDate, "MM/dd");
			this.putMapParam("urlyyyMMdd", year + "/" + urlMMdd);
			String url = this.dataDate.before(WType.strToDate("20141130")) ? this.urlTempl1 : this.urlTempl;
			if(this.dataDate.before(WType.strToDate("20141201")) && this.dataDate.after(WType.strToDate("20070420"))) {
				url = this.urlTempl1;
			} else if (this.dataDate.after(WType.strToDate("20141130"))) {
				url = this.urlTempl;
			}
			data = jsoup.get(this.parseMapParamToString(url), Const._ENCODING_UTF8).getElementsByTag("body").get(0).text().toString();
			data = WString.unicodeToString(data);
			txtFile.writeByStr(filePath, data);
			return data;
		}
	}
	
	private List<LPTransLogTwPO> getLpList(JSONArray jsa) throws JSONException, ParseException{
		
		List<LPTransLogTwPO> lpList = new ArrayList<>();
		LPTransLogTwPO lp;
		int colAdj = this.dataDate.before(WType.strToDate("20180115")) ? -6 : 0;  
		for(int i=0;i<jsa.length();i++) {
			JSONArray jsa1 = jsa.getJSONArray(i);
			if(StringUtils.isBlank(jsa1.getString(0))) {
				continue;
			}
			lp = new LPTransLogTwPO();
			lp.setSid(sidMap.get(jsa1.getString(0)));//代號
			lp.setTransDate(this.dataDate);
			if(lp.getSid().compareTo(117498) == 0) {
				lp.setTransDate(this.dataDate);
			}
			if (this.dataDate.after(WType.strToDate("20141130"))) {
				if (colAdj == 0) {
					lp.setFiNsBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(2))));// 外資及陸資(不含外資自營商)-買進股數
					lp.setFiNsSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(3))));// 外資及陸資(不含外資自營商)-賣出股數
					lp.setFiSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(5))));// 外資自營商-買進股數
					lp.setFiSelfSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(6))));// 外資自營商-賣出股數
				}
				lp.setFiBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(8 + colAdj))));// 外資及陸資-買進股數
				lp.setFiSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(9 + colAdj))));// 外資及陸資-賣出股數
				lp.setItBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(11 + colAdj))));// 投信-買進股數
				lp.setItSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(12 + colAdj))));// 投信-賣出股數
				lp.setDlSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(14 + colAdj))));// 自營商(自行買賣)-買進股數
				lp.setDlSelfSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(15 + colAdj))));// 自營商(自行買賣)-賣出股數
				lp.setDlHedgingBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(17 + colAdj))));// 自營商(避險)-買進股數
				lp.setDlHedgingSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(18 + colAdj))));// 自營商(避險)-賣出股數
				lp.setDlBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(20 + colAdj))));// 自營商-買進股數
				lp.setDlSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(21 + colAdj))));// 自營商-賣出股數
			} else if (WDate.afterOrEquals(this.dataDate, WType.strToDate("20070421"))
					&& WDate.beforeOrEquals(this.dataDate, WType.strToDate("20141130"))) {
				lp.setFiBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(2))));// 外資及陸資-買進股數
				lp.setFiSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(3))));// 外資及陸資-賣出股數
				lp.setItBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(5))));// 投信-買進股數
				lp.setItSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(6))));// 投信-賣出股數
				lp.setDlBuy(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(8))));// 自營商-買進股數
				lp.setDlSell(Integer.valueOf(StringUtil.cleanDigitData(jsa1.getString(9))));// 自營商-賣出股數
			}
			lpList.add(lp);
		}
		return lpList;
	}
	
	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
//		lptRepo.deleteByTransDateEquals(dataDate);
	}
}
