package com.willy.sias.batch.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.sias.db.dao.ShiaojiDao;
import com.willy.sias.db.dto.KBarDTO;
import com.willy.sias.db.po.CompanyCategoryPO;
import com.willy.sias.db.po.PriceSecondTwPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.PriceSecondTwRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.config.Const;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.yf-price-second")
public class YF_PRICE_SECOND extends BaseBatchProcessor {
	@Autowired
	private SysConfigRepository scRepo;
	@Autowired
	private PriceSecondTwRepository spRepo;
	@Autowired
	private CompanyCategoryRepository ccRepo;
	@Autowired
	private ShiaojiDao shiaojiUtil;

	public void exec() throws Exception {
		// 取出sidCode
		List<Integer> sidList = ccRepo.findSidWithoutDerivative();
		List<PriceSecondTwPO> psList = new ArrayList<>();
		for (int sid : sidList) {
			SysConfigPO sc = scRepo.findById(sid).orElse(null);
//			List<KBarDTO> kbList = shiaojiUtil.getKBarList(sc.getCfgValue().replace(".TW", ""), this.dataDate,
//					this.dataDate);
//			psList.addAll(kBarDTOToPriceSecondTwPO(sid, kbList));
//			if (psList.size() > 10000) {
//				spRepo.saveAll(psList);
//				psList.clear();
//			}
//			Thread.sleep(1500);
		}
//		spRepo.saveAll(psList);
	}

	private List<PriceSecondTwPO> convertKbarDtoToPriceSecondTwPO(int sid, List<KBarDTO> kbList) {
		PriceSecondTwPO ps;
		List<PriceSecondTwPO> psList = new ArrayList<>();
		for (KBarDTO kb : kbList) {
			ps = new PriceSecondTwPO();
			ps.setOpen(kb.getOpen());
			ps.setHigh(kb.getHigh());
			ps.setLow(kb.getLow());
			ps.setClose(kb.getClose());
			ps.setVolume(kb.getVolume());
//			ps.setDataDate(kb.getDate());
			ps.setSid(sid);
			psList.add(ps);
		}
		return psList;
	}

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub

	}
}
