package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * StockPrice_TW
 * @author 
 */
@Table(name="IDX_DAILY_US")
@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class IdxDailyUsPOKey implements Serializable {
    /**
     * 標的代號
     */
    @Column(name = "SID")
    private Integer sid;

    /**
     * 資料日期
     */
    @Column(name = "DATA_DATE")
    private Date dataDate;

    private static final long serialVersionUID = 1L;
}