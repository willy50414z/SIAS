<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>歷史回測</title>
<%@ include file="/jsp/Header.jsp"%>
<script>
	$(function() {
		$("#batchCodeSelect").selectmenu();
		$("#batchDatePicker").datepicker({"dateFormat":"yy/mm/dd"});
		$("#exeBatchBtn").on("click", function(){
			if($("#batchCodeSelect").val() == '' || $("#batchDatePicker").val() == ''){
				alert("Batch名稱及日期皆不得為空");
				return;
			}
			$.ajax({
				type : "POST",
				url : getContextPath() + "/BatchSummary/exeBatch",
				data : {
					batchCode : $("#batchCodeSelect").val()
					,strDate : $("#batchDatePicker").val()
				}
			});
			alert("批次開始執行，完成後將發送Mail通知");
		});
	});
</script>
<style>
.sidItem {
	border: 0.5px gray solid;
	margin: 2px;
	border-radius: 3px;
	text-align: center
}

.sidItem:hover {
	background-color: #E0E0E0;
}

.selectedSidItem {
	background-color: #E0E0E0;
}

.sidSelectedRegion {
	border: 1px black solid;
	border-radius: 3px;
	width: 40%;
}

.chooseSidBtn {
	width: 20px;
	heigth: 10px;
	size: 10px;
}
</style>
</head>
<body>
	<div id="batchDiv">
		<fieldset>
			<legend>執行單筆排程</legend>
			<table>
				<tr>
					<td>批次:</td>
					<td><select id="batchCodeSelect">
							<option></option>
							<c:forEach var="batchSummary" items="${batchSummaryList}">
								<option value="${batchSummary.batchCode}">
									<c:out value="${batchSummary.batchCode} - ${batchSummary.batchName}" />
								</option>
							</c:forEach>
					</select></td>
					<td width="10px"></td>
					<td>日期:</td>
					<td><input type="text" id="batchDatePicker"></td>
					<td width="10px"></td>
					<td><button type="button" class="btn btn-primary"
							id="exeBatchBtn">執行</button></td>
				</tr>
			</table>
			<table>
				<div id="testResultTable"></div>
			</table>
		</fieldset>
	</div>
	<div id="batchDetail">
		<div id="tradeDetailContent"></div>
	</div>
</body>
</html>