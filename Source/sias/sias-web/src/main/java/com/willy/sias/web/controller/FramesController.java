package com.willy.sias.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/frames")
public class FramesController {
	@RequestMapping("/Top")
	public String visitTop() {
		return "/frames/Top";
	}
	@RequestMapping("/Menu")
	public String visitMenu() {
		return "/frames/Menu";
	}
}
