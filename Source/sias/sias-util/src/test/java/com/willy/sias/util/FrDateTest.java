package com.willy.sias.util;

import com.willy.sias.util.EnumSet.FrReleaseDate;
import com.willy.util.type.WType;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FrDateTest {
  @Test
  public void testGetNextFrReleaseDate(){
    //Q4
    Assert.assertTrue(FrReleaseDate.getNextFrReleaseDate(WType.strToDate("20200101")).equals(WType.strToDate("20200331")));

    //Q1
    Assert.assertTrue(FrReleaseDate.getNextFrReleaseDate(WType.strToDate("20200401")).equals(WType.strToDate("20200515")));

    //Q2
    Assert.assertTrue(FrReleaseDate.getNextFrReleaseDate(WType.strToDate("20200701")).equals(WType.strToDate("20200814")));

    //Q3
    Assert.assertTrue(FrReleaseDate.getNextFrReleaseDate(WType.strToDate("20201001")).equals(WType.strToDate("20201114")));

    //Q4
    Assert.assertTrue(FrReleaseDate.getNextFrReleaseDate(WType.strToDate("20201115")).equals(WType.strToDate("20210331")));
  }

  @Test
  public void testGetNextFrReleaseSlot() {
    //Q4
    Assert.assertTrue(FrReleaseDate.getFrTimeSlot(WType.strToDate("20200101"))
        .equals(Arrays.asList(WType.strToDate("20191231"), WType.strToDate("20200331"))));

    //Q1
    Assert.assertTrue(FrReleaseDate.getFrTimeSlot(WType.strToDate("20200401"))
        .equals(Arrays.asList(WType.strToDate("20200331"), WType.strToDate("20200515"))));

    //Q2
    Assert.assertTrue(FrReleaseDate.getFrTimeSlot(WType.strToDate("20200701"))
        .equals(Arrays.asList(WType.strToDate("20200630"), WType.strToDate("20200814"))));

    //Q3
    Assert.assertTrue(FrReleaseDate.getFrTimeSlot(WType.strToDate("20201001"))
        .equals(Arrays.asList(WType.strToDate("20200930"), WType.strToDate("20201114"))));

    //Q4
    Assert.assertTrue(FrReleaseDate.getFrTimeSlot(WType.strToDate("20201115"))
        .equals(Arrays.asList(WType.strToDate("20201231"), WType.strToDate("20210331"))));
  }
}
