package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * BatchLog
 * 
 * @author
 */
@Table(name = "BATCH_LOG")
@Entity
@Data
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
public class BatchLogPO implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 批次LOG ID
	 */
	@Id
	@Column(name = "\"BATCH_LOG_ID\"")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int batchLogId;
	
	/**
	 * 資料日期
	 */
	@NonNull
	@Column(name = "\"DATA_DATE\"")
	private Date dataDate;
	
	/**
	 * 批次名稱
	 */
	@NonNull
	@Column(name = "\"BATCH_NAME\"")
	private String batchName;
	
	/**
	 * 	批次狀態(0:正常, 1:異常)
	 */
	@NonNull
	@Column(name = "\"STATUS\"")
	private Integer status;
	
	/**
	 * 	錯誤信息
	 */
	@Column(name = "\"ERR_MSG\"")
	private String errMsg;
	
	/**
	 * 建立日期
	 */
	@NonNull
	@Column(name = "\"CREATE_DATE\"")
	private Date createDate;

}