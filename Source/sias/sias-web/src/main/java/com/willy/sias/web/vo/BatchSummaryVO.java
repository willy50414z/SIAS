package com.willy.sias.web.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BatchSummaryVO {
	private String batchCode;
	private String batchName;
	private String batchFrequence;
}
