package com.willy.sias.db.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;

import com.willy.util.log.WLog;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class TimeSeriesDTO<T> implements Serializable{
	private static final long serialVersionUID = 1L;
	Date date;
	T value;
	public BigDecimal getDecimalValue() {
		if(value == null || StringUtils.isEmpty(value.toString())) {
			return null;
		}
		try{
			return new BigDecimal(value.toString());
		}catch(Exception e) {
			WLog.error("value["+value.toString()+"]", e);
			throw e;
		}
		
	}
	public static <T> Comparator<TimeSeriesDTO<T>> getTsDateComparator(){
		return new Comparator<TimeSeriesDTO<T>>() {
			@Override
			public int compare(TimeSeriesDTO<T> o1, TimeSeriesDTO<T> o2) {
				// TODO Auto-generated method stub
				return o1.getDate().compareTo(o2.getDate());
			}
		};
	}
}
