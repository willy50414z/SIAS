package com.willy.sias.backtest.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.willy.sias.db.dto.TimeSeriesDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProfitDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Set<Integer> sidSet;
	private BigDecimal originalFunds;// 初始資金
	private Date firstTradeDate;// 測試起日
	private Date lastTradeDate;// 測試迄日
	private BigDecimal acctBalance;// 帳戶結餘
	private BigDecimal creditAmt;// 信用額度(信用交易限額)
	private String strategyDesc;
	private List<TradeDetailDTO> tradeDetailList = new ArrayList<>(); // 交易明細
	private List<TradeDetailDTO> inventoryList = new ArrayList<>(); // 交易明細
	private Set<String> tradeIdSet = new HashSet<>();
	private List<List<String>> custColList;//客製化欄位值

	// 統計數據
	private BigDecimal grossProfit;// 毛利
	private BigDecimal grossProfitRatio;// 毛利%
	private List<TimeSeriesDTO<BigDecimal>> netValueTsList;
//	private BigDecimal maxProfitAmt;// 最大獲利金額
//	private BigDecimal maxProfitAmtRatio;// 最大獲利金額%
//	private BigDecimal maxLossAmt;// 最大損失金額
//	private BigDecimal maxLossAmtRatio;// 最大損失金額%
//	private Integer maxConLossCount;// 最大連續損失次數
//	private BigDecimal maxConLossAmt;// 最大連續損失金額
//	private BigDecimal maxConLossRatio;// 最大連續損失金額比率%
//	private BigDecimal maxExpectLoss;// 最大預期損失
//	private BigDecimal maxExpectLossRatio;// 最大預期損失%
//	private BigDecimal riskRatio;// 風險比(總獲利/最大虧損[MDD])
	private BigDecimal profitFactor;// 獲利因子(總獲利金額/ 總虧損金額) >1.5
	private BigDecimal alfaRatio;// 阿爾法比率(標的獲利/大盤獲利-1) - 總風險扣除掉系統性風險以外，額外創造的超額報酬
	private BigDecimal betaRatio;// 貝他比率(標的獲利/大盤獲利-1) - 與大盤連動性
	private BigDecimal profitRatio;//勝率
	private BigDecimal expectValue;//期望值
	private BigDecimal profitAmtRatio;//盈虧金額比
	
	
	
	
//
//	/**
//	 * 已實現損益
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getRealizedProfit(Date calBaseDate) {
//		//交易損益
//		BigDecimal realizedProfit = getTxnBalance(calBaseDate);
//		//排除未實現損益
//		for(TradeDetailPO inventory : inventoryList) {
//			realizedProfit.subtract(inventory.getTradeAmt());
//		}
//		return realizedProfit;
//	}
//
//	/**
//	 * 已實現損益報酬率
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getRealizedProfitRatio(Date calBaseDate) {
//		BigDecimal txnBalance = getRealizedProfit(calBaseDate);
//		//交易成本 (只算普買/券買)
//		List<BigDecimal> tradeAmtList = tradeDetailList.stream()
//				.filter(tradeDetail -> tradeDetail.getTradeDate().compareTo(calBaseDate) <= 0
//				&& (tradeDetail.getTradeType() == TradeType.buy || tradeDetail.getTradeType() == TradeType.shortBuying))
//				.map(tradeDetail -> tradeDetail.getTradeAmt()).collect(Collectors.toList());
//		BigDecimal buyingCost = new BigDecimal(0);
//		for(BigDecimal tradeAmt : tradeAmtList) {
//			buyingCost = buyingCost.add(tradeAmt.abs());
//		}
//		//去除存貨
//		for(TradeDetailPO inventory : inventoryList) {
//			buyingCost = buyingCost.add(inventory.getTradeAmt());
//		}
//		return txnBalance.divide(buyingCost).setScale(2, RoundingMode.HALF_UP);
//	}
//	
//	/**
//	 * 盈虧比/獲利金額比率(總獲利/總虧損)
//	 * @return
//	 */
//	public BigDecimal getProfitAmtRatio(Date calBaseDate) {
//		Set<Integer> sidList = tradeDetailList.stream()
//				.filter(tradeDetail -> tradeDetail.getTradeDate().compareTo(calBaseDate) <= 0)
//				.map(tradeDetail -> tradeDetail.getSid()).collect(Collectors.toSet());
//		
//		BigDecimal profitAmt = new BigDecimal(0);
//		BigDecimal lossAmt = new BigDecimal(0);
//		for (int sid : sidList) {
//			BigDecimal realizedProfit = new BigDecimal(0);
//			//總損益
//			List<BigDecimal> tradeAmtList = tradeDetailList.stream()
//					.filter(tradeDetail -> tradeDetail.getTradeDate().compareTo(calBaseDate) <= 0
//							&& tradeDetail.getSid().compareTo(sid) == 0)
//					.map(tradeDetail -> tradeDetail.getTradeAmt()).collect(Collectors.toList());
//			for(BigDecimal tradeAmt : tradeAmtList) {
//				realizedProfit = realizedProfit.add(tradeAmt);
//			}
//			//減掉未實現損益
//			tradeAmtList = inventoryList.stream().filter(inventory -> inventory.getSid().compareTo(sid) == 0).map(inventory -> inventory.getTradeAmt()).collect(Collectors.toList());
//			for(BigDecimal tradeAmt : tradeAmtList) {
//				realizedProfit = realizedProfit.subtract(tradeAmt);
//			}
//			//損益>0?加入profitAmt:加入lossAmt
//			if(realizedProfit.compareTo(Const._BIGDECIMAL_0)>=0) {
//				profitAmt = profitAmt.add(realizedProfit);
//			} else {
//				lossAmt = lossAmt.subtract(realizedProfit);
//			}
//		}
//		return profitAmt.divide(lossAmt).setScale(2, RoundingMode.HALF_UP);
//	}
//	
	

//
//	/**
//	 * 取得至某日為止的獲利比率
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMax(Date calBaseDate) {
//		BigDecimal profitCount = new BigDecimal(0);
//		BigDecimal investCount = new BigDecimal(0);
//		BigDecimal addCount = new BigDecimal(1);
//		for (TradeDetailPO td : tradeDetailList) {
//			if (td.isTradeComplete() && WDate.beforeOrEquals(td.getBuyDate(), calBaseDate)
//					&& WDate.beforeOrEquals(td.getSellDate(), calBaseDate)) {
//				if (td.getGrossProfit().compareTo(new BigDecimal(0)) > 1) {
//					profitCount = profitCount.add(addCount);
//				}
//				investCount = investCount.add(addCount);
//			}
//		}
//		return profitCount.divide(investCount, 4, RoundingMode.HALF_UP);
//	}
//
//	/**
//	 * 最大損失金額(整體毛利最少者)
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxLossAmt(Date calBaseDate) {
//		BigDecimal grossProfit, maxLossAmt = new BigDecimal(0);
//
//		// 取得目前有交易日的日期
//		Set<Date> tradeDateSet = getAllTradeDateSet().stream()
//				.filter(tradeDate -> WDate.beforeOrEquals(tradeDate, calBaseDate)).collect(Collectors.toSet());
//
//		for (Date tradeDate : tradeDateSet) {
//			grossProfit = this.getGrossProfit(tradeDate);
//			if (maxLossAmt.equals(new BigDecimal(0))) {
//				maxLossAmt = grossProfit;
//			}
//			if (grossProfit.compareTo(maxLossAmt) < 0) {
//				maxLossAmt = grossProfit;
//			}
//		}
//		return (maxLossAmt.compareTo(new BigDecimal(0)) > 0) ? new BigDecimal(0) : maxLossAmt.abs();
//	}
//
//	/**
//	 * 最大獲利金額(整體毛利最多者)
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxProfitAmt(Date calBaseDate) {
//		BigDecimal grossProfit, maxProfitAmt = new BigDecimal(0);
//
//		// 取得目前有交易日的日期
//		Set<Date> tradeDateSet = getAllTradeDateSet().stream()
//				.filter(tradeDate -> WDate.beforeOrEquals(tradeDate, calBaseDate)).collect(Collectors.toSet());
//
//		for (Date tradeDate : tradeDateSet) {
//			grossProfit = this.getGrossProfit(tradeDate);
//			if (maxProfitAmt.equals(new BigDecimal(0))) {
//				maxProfitAmt = grossProfit;
//			}
//			if (grossProfit.compareTo(maxProfitAmt) > 0) {
//				maxProfitAmt = grossProfit;
//			}
//		}
//		return (maxProfitAmt.compareTo(new BigDecimal(0)) < 0) ? new BigDecimal(0) : maxProfitAmt;
//	}
//
//	/**
//	 * 最大損失金額%
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxLossRatio(Date calBaseDate) {
//		if (this.originalFunds.compareTo(new BigDecimal(0)) <= 0) {
//			return new BigDecimal(-1);
//		} else {
//			return this.getMaxLossAmt(calBaseDate).divide(this.originalFunds, 4, RoundingMode.HALF_UP);
//		}
//	}
//
//	/**
//	 * 最大獲利金額%
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxProfitRatio(Date calBaseDate) {
//		if (this.originalFunds.compareTo(new BigDecimal(0)) <= 0) {
//			return new BigDecimal(-1);
//		} else {
//			return this.getMaxProfitAmt(calBaseDate).divide(this.originalFunds, 4, RoundingMode.HALF_UP);
//		}
//	}
//
//	/**
//	 * 最大連續損失次數
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public Integer getMaxConLossCount(Date calBaseDate) {
//		Integer maxConLossCount = 0, conLossCount = 0;
//		String tradeId = "";
//		tradeIdSet.clear();
//		for (TradeDetailPO td : tradeDetailList) {
//			// 交易次數為單位
//			if (td.isTradeComplete() && WDate.beforeOrEquals(td.getBuyDate(), calBaseDate)
//					&& WDate.beforeOrEquals(td.getSellDate(), calBaseDate) && !tradeIdSet.contains(td.getTradeId())) {
//				tradeIdSet.add(td.getTradeId());
//				// 記錄虧錢次數
//				if (td.getGrossProfit().compareTo(new BigDecimal(0)) < 0) {
//					if (!td.getTradeId().equals(tradeId)) {
//						tradeId = td.getTradeId();
//						conLossCount++;
//					}
//				}
//				// 沒虧錢時重置虧錢次數，並與最大虧錢次數比較
//				else if (td.getGrossProfit().compareTo(new BigDecimal(0)) >= 0) {
//					if (conLossCount > maxConLossCount)
//						maxConLossCount = conLossCount;
//					conLossCount = 0;
//				}
//			}
//		}
//		return maxConLossCount;
//	}
//
//	/**
//	 * 最大連續損失金額
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxConLossAmt(Date calBaseDate) {
//		BigDecimal lossAmt = new BigDecimal(0), maxConLossAmt = new BigDecimal(0);
//
//		for (TradeDetailPO td : tradeDetailList) {
//			// 買賣日都在計算日前，加總毛利(只取已完成交易的)
//			if (td.isTradeComplete() && WDate.beforeOrEquals(td.getBuyDate(), calBaseDate)
//					&& WDate.beforeOrEquals(td.getSellDate(), calBaseDate)) {
//				if (td.getGrossProfit().compareTo(new BigDecimal(0)) < 0) {
//					// 虧損
//					lossAmt = lossAmt.add(td.getGrossProfit());
//				} else {
//					// 獲利
//					if (lossAmt.compareTo(maxConLossAmt) < 0)
//						maxConLossAmt = lossAmt;
//					lossAmt = new BigDecimal(0);
//				}
//			}
//		}
//		return maxConLossAmt;
//	}
//
//	/**
//	 * 最大連續損失金額%
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getMaxConLossAmtRatio(Date calBaseDate) {
//		if (this.originalFunds.compareTo(new BigDecimal(0)) <= 0) {
//			return new BigDecimal(-1);
//		} else {
//			return getMaxConLossAmt(calBaseDate).divide(this.originalFunds, 4, RoundingMode.HALF_UP);
//		}
//	}
//
//	/**
//	 * 風險比(最大獲利/最大損失)
//	 * 
//	 * @param calBaseDate
//	 * @return
//	 */
//	public BigDecimal getRiskRatio(Date calBaseDate) {
//		BigDecimal maxLossAmt = this.getMaxLossAmt(calBaseDate);
//		BigDecimal maxProfitAmt = getMaxProfitAmt(calBaseDate);
//		if (maxLossAmt.compareTo(new BigDecimal(0)) == 0 || maxProfitAmt.compareTo(new BigDecimal(0)) == 0) {
//			WLog.warn("getRiskRatio fail, maxLossAmt [" + maxLossAmt + "] or maxProfitAmt[" + maxProfitAmt + "] is 0");
//			return new BigDecimal(-1);
//		} else {
//			return this.getMaxProfitAmt(calBaseDate).divide(maxLossAmt.abs(), 4, RoundingMode.HALF_UP);
//		}
//	}
//


	public ProfitDTO clone() {
		ProfitDTO profitDTO = new ProfitDTO();
		profitDTO.setAcctBalance(this.acctBalance);
		profitDTO.setAlfaRatio(alfaRatio);
		profitDTO.setSidSet(this.sidSet);
		profitDTO.setOriginalFunds(this.originalFunds);
		profitDTO.setLastTradeDate(this.lastTradeDate);
		profitDTO.setFirstTradeDate(this.firstTradeDate);
		profitDTO.setTradeDetailList(this.cloneTradeDetailList());
		profitDTO.setInventoryList(this.cloneInventoryList());
		return profitDTO;
	}
	
	public List<TradeDetailDTO> cloneTradeDetailList(){
		TradeDetailDTO tdPo;
		List<TradeDetailDTO> tdList = new ArrayList<> ();
		for (TradeDetailDTO td : this.getTradeDetailList()) {
			tdPo = new TradeDetailDTO();
			tdPo.setTradeId(td.getTradeId());
			tdPo.setTradeType(td.getTradeType());
			tdPo.setSid(td.getSid());
			tdPo.setTradeDate(td.getTradeDate());
			tdPo.setTradeUnit(td.getTradeUnit());
			tdPo.setTradePrice(td.getTradePrice());
			tdPo.setTradeAmt(td.getTradeAmt());
			tdPo.setGrossProfit(td.getGrossProfit());
			tdPo.setSettleDate(td.getSettleDate());
			tdList.add(tdPo);
		}
		return tdList;
	}
	public List<TradeDetailDTO> cloneInventoryList(){
		TradeDetailDTO tdPo;
		List<TradeDetailDTO> tdList = new ArrayList<> ();
		for (TradeDetailDTO td : this.getInventoryList()) {
			tdPo = new TradeDetailDTO();
			tdPo.setTradeId(td.getTradeId());
			tdPo.setTradeType(td.getTradeType());
			tdPo.setSid(td.getSid());
			tdPo.setTradeDate(td.getTradeDate());
			tdPo.setTradeUnit(td.getTradeUnit());
			tdPo.setTradePrice(td.getTradePrice());
			tdPo.setTradeAmt(td.getTradeAmt());
			tdPo.setGrossProfit(td.getGrossProfit());
			tdPo.setSettleDate(td.getSettleDate());
			tdList.add(tdPo);
		}
		return tdList;
	}
//	
//	/**
//	 * 平均獲利金額(總獲利/獲利次數)
//	 * @return
//	 */
//	public BigDecimal getAvgProfitAmt() {
//		String tradeId = "";
//		BigDecimal profitAmt= new BigDecimal(0);
//		BigDecimal profitCount= new BigDecimal(0);
//		for(TradeDetailPO td : tradeDetailList){
//			if(td.getGrossProfit() != null) {
//				if(td.getGrossProfit().compareTo(new BigDecimal(0)) > 0) {
//					profitAmt = profitAmt.add(td.getGrossProfit());
//					if(!tradeId.equals(td.getTradeId())) {
//						tradeId = td.getTradeId();
//						profitCount = profitCount.add(new BigDecimal(1));
//					}
//					
//				}
//			}
//		};
//		return profitCount.compareTo(new BigDecimal(0)) > 0 ? profitAmt.divide(profitCount).setScale(4, RoundingMode.HALF_UP) : null;
//	}
//	
//	/**
//	 * 平均虧損金額(總虧損/虧損次數)
//	 * @return
//	 */
//	public BigDecimal getAvgLossAmt() {
//		String tradeId = "";
//		BigDecimal lossAmt= new BigDecimal(0);
//		BigDecimal lossCount= new BigDecimal(0);
//		for(TradeDetailPO td : tradeDetailList){
//			BigDecimal grossProfit = td.getGrossProfit();
//			if(grossProfit != null) {
//				if(grossProfit.compareTo(new BigDecimal(0)) < 0) {
//					lossAmt = lossAmt.add(td.getGrossProfit());
//					if(!tradeId.equals(td.getTradeId())) {
//						tradeId = td.getTradeId();
//						lossCount = lossCount.add(new BigDecimal(1));
//					}
//				}
//			}
//		};
//		return lossCount.compareTo(new BigDecimal(0)) > 0 ? lossAmt.divide(lossCount).setScale(4, RoundingMode.HALF_UP) : null;
//	}
	public Integer getTradeCount() {
		HashSet<String> tradeIdSet = new HashSet<>();
		this.getTradeDetailList().stream().forEach(td -> tradeIdSet.add(td.getTradeId()));
		return tradeIdSet.size();
	}
	
	public List<Integer> getSidList(){
		List<Integer> sidList = new ArrayList<> ();
		if(this.sidSet == null || this.sidSet.size() == 0) {
			return sidList;
		}
		sidList.addAll(this.sidSet);
		return sidList;
	}
}
