package com.willy.sias.db.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class MACDDTO {
	private BigDecimal macd;
	private BigDecimal dif;
	private BigDecimal osc;
}
