package com.willy.sias.db.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class MAStatusDTO {
	private List<TimeSeriesDTO<BigDecimal>>  closeList;
	
}
