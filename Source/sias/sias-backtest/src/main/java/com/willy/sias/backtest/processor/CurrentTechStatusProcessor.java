package com.willy.sias.backtest.processor;

import com.willy.sias.backtest.dto.PriceCheckInfoDTO;
import com.willy.sias.backtest.service.TechDmsService;
import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.KDJDTO;
import com.willy.sias.db.dto.KDJStatusDTO;
import com.willy.sias.db.dto.MACDDTO;
import com.willy.sias.db.dto.MACDStatusDTO;
import com.willy.sias.db.dto.OHLCVDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.PriceDailyTwRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.KBar;
import com.willy.sias.util.EnumSet.TrendType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrentTechStatusProcessor extends CurrentStatusProcessor {
	@Autowired
	private TechIdxService tiSvc;
	@Autowired
	private TechDmsService tdSvc;

	/**
	 *	趨勢
	 * 		MA 5, 10, 20多空頭排列
	 *		上升/下降軌道、箱型整理 (近60日股價判斷)  *短期趨勢: 將判定當日視為一個壓力or支撐點，與前3個壓支畫出趨勢
	 *
	 *	型態
	 *		N字底 (V2 跌幅 < V1~A1漲幅的1/2，且目前向上突破A1)
	 *
	 *	K棒狀態
	 *		變盤線/長黑吞噬...
	 *	
	 *	支撐壓力
	 *		MA/跳空缺口(30)/大量交易(40)
	 */
	
	public CurrentTechStatusProcessor(Integer sid, Date analyzedate) {
		super();
		this.sid = sid;
		this.analyzedate = analyzedate;
		List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(this.sid, this.analyzedate,
				this.analyzedate);
		if(priceList != null && priceList.size()>0) {
			todayPrice = priceList.get(0);
		}
	}

	public CurrentTechStatusProcessor() {
		super();
	}

	@Override
	public void exec() {
		try {
			SysConfigPO sc = scRepo.findById(this.sid).orElse(null);
			if(sc ==null) {
				WLog.error("未設定此SID["+sid+"]");
				return;
			}
			PriceDailyTwPO pricePO = priceRepo.findFirstBySidIsAndDataDateLessThanEqualOrderByDataDateDesc(this.sid, this.analyzedate);
			
			if(pricePO.getDataDate().compareTo(this.analyzedate)==0) {
				System.out.println("分析標的: " + sc.getCfgValue() + " " + sc.getCfgDesc() + "\t日期: " + WType.dateToStr(this.analyzedate));
			}else {
				System.out.println("分析標的: " + sc.getCfgValue() + " " + sc.getCfgDesc() + "\t原定分析日期: " + WType.dateToStr(this.analyzedate) + "\t實際分析日期: " + WType.dateToStr(pricePO.getDataDate()));
				this.analyzedate = pricePO.getDataDate();
			}
			// 趨勢
			System.out.println("-----趨勢-----");
			trendAnalyzer();
			
			// 型態
			System.out.println("-----型態-----");
			patternAnalyzer();
			
			//K棒
			System.out.println("-----K棒狀態-----");
			kBarAnalyzer();
			
			// 支撐壓力
			System.out.println("-----支撐壓力-----");
			psAnalyzer();
			
			//位置
			
			//均線
			
			//成交量
			
			// KD均線空頭排
			kdAnalyzer();
			// MACD
			macdAnalyzer();

			// 頭頭高底底高
			// 型態

			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void kdAnalyzer() throws Exception {
		List<TimeSeriesDTO<KDJDTO>> kdjTsList = tiSvc.getKD(sid);
		if (kdjTsList == null) {
			WLog.error("SID[" + sid + "] get KD list fail");
			return;
		}
		KDJStatusDTO kdjDto = new KDJStatusDTO(kdjTsList);

		// KD是否低檔鈍化
		int crossDays = kdjDto.getCrossDays(analyzedate);
		int kDOutDays = kdjDto.getKDOutDays(analyzedate);
		int kDSlope = kdjDto.getKDSlope(analyzedate);
//		System.out.println("kdjDto.getCrossDays(analyzedate)[" + crossDays + "]");
//		System.out.println("kdjDto.getKDOutDays(date)[" + kDOutDays + "]");
//		System.out.println("kdjDto.getKDSlope(date)[" + kDSlope + "]");

//		TimeSeriesDTO<KDJDTO> kdjTs = kdjTsList.stream().filter(ts -> WDate.beforeOrEquals(ts.getDate(), analyzedate)).findFirst().orElse(null);
//		System.out.println("K["+kdjTs.getValue().getK()+"]D["+kdjTs.getValue().getD()+"]J["+kdjTs.getValue().getJ()+"]");

		System.out.println("多頭依KD做多，空頭依KD做空，盤整不看KD");
		if (kDOutDays != 0) {
			if (Math.abs(kDOutDays) > 3) {
				System.out.println("KD指標處於" + ((kDOutDays > 0) ? "高" : "低") + "檔鈍化" + kDOutDays + "天，建議依均線作為進出場判斷");
			} else {
				System.out.println("KD指標處於超" + ((kDOutDays > 0) ? "買" : "賣") + "區" + Math.abs(kDOutDays) + "天");
			}
		}
		if (crossDays > 0 && kDSlope  == 2) {
			System.out.println("KD指標處於黃金交叉第" + Math.abs(crossDays) + "天，進入多頭市場");
		} else if (crossDays < 0 && kDSlope == -2) {
			System.out.println("KD指標處於死亡交叉第" + Math.abs(crossDays) + "天，進入空頭市場");
		}

		List<TimeSeriesDTO<BigDecimal>> kTsList = new ArrayList<>();
		List<TimeSeriesDTO<KDJDTO>> kdTsList = tiSvc.getKD(this.sid);
		kdTsList.stream().forEach(ts -> {
			if (ts.getValue().getK() != null) {
				kTsList.add(new TimeSeriesDTO<BigDecimal>(ts.getDate(), ts.getValue().getK()));
			}
		});
		int deviateStatus = tdSvc.getDeviateStatus(this.sid, this.analyzedate, kTsList);
		if (deviateStatus != 0) {
			System.out.println("KD指標背離，短期趨勢走" + (deviateStatus > 0 ? "多" : "空") + "第" + Math.abs(deviateStatus) + "天");
		}
	}

	private void macdAnalyzer() throws Exception {
		List<TimeSeriesDTO<MACDDTO>> macdTsList = tiSvc.getMACD(sid);
		if (macdTsList == null) {
			WLog.error("SID[" + sid + "] get MACD list fail");
			return;
		}
		MACDStatusDTO macdDto = new MACDStatusDTO(macdTsList);
		int oscStatusDays = macdDto.getOSCStatus(analyzedate);
		int difMacdDays = macdDto.getDIFMACDStatus(analyzedate);

		// dif&macd皆在0軸上
		// 綠柱逐漸縮短>3天
		if (difMacdDays > 0) {
			System.out.println("DIF及MACD線>0軸，目前呈多頭走勢第[" + difMacdDays + "]天");
		} else if (difMacdDays < 0) {
			System.out.println("DIF及MACD線<0軸，目前呈空頭走勢第[" + (-difMacdDays) + "]天");
		}

		TimeSeriesDTO<MACDDTO> macdTs = macdTsList.stream().filter(ts -> ts.getDate().compareTo(analyzedate) == 0).findFirst()
				.orElse(null);
		boolean oscGtZero = macdTs.getValue().getOsc().compareTo(Const._BIGDECIMAL_0) > 0;
		if (oscStatusDays > 2) {
			System.out.println("目前為" + (oscGtZero ? "紅" : "綠") + "柱OSC已連續" + (oscGtZero ? "增長" : "縮短") + "["
					+ oscStatusDays + "]天，呈多頭走勢");
		} else if (oscStatusDays < -2) {
			System.out.println("目前為" + (oscGtZero ? "紅" : "綠") + "柱OSC已連續" + (oscGtZero ? "縮短" : "增長") + "["
					+ (-oscStatusDays) + "]天，呈多頭走勢");
		}

		List<TimeSeriesDTO<BigDecimal>> oscTsList = new ArrayList<>();
		macdTsList = tiSvc.getMACD(this.sid);
		macdTsList.forEach(ts -> {
			if (ts.getValue().getOsc() != null) {
				oscTsList.add(new TimeSeriesDTO<BigDecimal>(ts.getDate(), ts.getValue().getOsc()));
			}
		});
		int deviateStatus = tdSvc.getDeviateStatus(this.sid, this.analyzedate, oscTsList);
		if (deviateStatus != 0) {
			System.out.println("MACD指標背離，中期趨勢走" + (deviateStatus > 0 ? "多" : "空") + "第" + Math.abs(deviateStatus) + "天");
		}
	}

	private void trendAnalyzer() throws Exception {
		int maArgmtDay = tdSvc.getMAArgmtDays(this.sid, this.analyzedate);
		if (maArgmtDay > 0) {
			System.out.println("均線多頭排列第[" + maArgmtDay + "]天");
		} else if (maArgmtDay < 0) {
			System.out.println("均線空頭排列第[" + (-maArgmtDay) + "]天");
		}

		System.out.println("---長期趨勢---");
		TrendType tt = tdSvc.getTrendType(this.sid, this.analyzedate, true);
		if (tt.getBreakUdTrackStatus() == 0) {
			System.out.println("目前趨勢呈現" + tt.getDesc());
		} else {
			System.out.println("股價已向" + (tt.getBreakUdTrackStatus() > 0 ? "上" : "下") + "突破" + tt.getDesc()
					+ Math.abs(tt.getBreakUdTrackStatus()) + "天");
		}
		
		//將當天加入壓力/支撐點重新判斷
		System.out.println("---短期趨勢---");
		tt = tdSvc.getTrendType(this.sid, this.analyzedate, false);
		if (tt.getBreakUdTrackStatus() == 0) {
			System.out.println("目前趨勢呈現" + tt.getDesc());
		} else {
			System.out.println("股價已向" + (tt.getBreakUdTrackStatus() > 0 ? "上" : "下") + "突破" + tt.getDesc()
					+ Math.abs(tt.getBreakUdTrackStatus()) + "天");
		}
	}
	
	
	
	private void kBarAnalyzer() throws Exception {
		//變盤線(影線>實體3倍)看隔日開高開低決定短期趨勢走向
		HashMap<KBar, Integer> kBarMap = tdSvc.getKBarStatus(this.sid, this.analyzedate);
		if(kBarMap == null) {
			return;
		}
		for(KBar kbarType : KBar.values()) {
			if(kBarMap.containsKey(kbarType)) {
				System.out.println(kBarMap.get(kbarType) + kbarType.getTrendDesc());
			}
		}
	}
	
	private void psAnalyzer() {
		List<PriceDailyTwPO> priceList = priceRepo.findBySidIsAndDataDateBetween(this.sid, this.analyzedate,
				this.analyzedate);
		if(CollectionUtils.isEmpty(priceList)) {
			return;
		}
		PriceDailyTwPO pricePo = priceList.get(0);
		Set<PriceCheckInfoDTO> priceCheckPoints = tdSvc.getPriceCheckPoints(this.sid, this.analyzedate);
		//列出上下3檔壓力/支撐
		List<PriceCheckInfoDTO> pressureList = new ArrayList<> (), sopportList = new ArrayList<> ();
		for(PriceCheckInfoDTO pcInfo : priceCheckPoints) {
			if(pcInfo.getPrice().compareTo(pricePo.getClose())>0){
				//加入最低的壓力，排除最高的
				//加入後排除最大的
				pressureList.add(pcInfo);
				if (pressureList.size()==4) {
					//降冪
					pressureList.sort(getPriceCheckInfoComparator());
					pressureList.remove(0);
				}
			} else if(pcInfo.getPrice().compareTo(pricePo.getClose())<0) {
				//加入最低的支撐，排除最高的
				//加入後排除最小的
				sopportList.add(pcInfo);
				if (sopportList.size()==4) {
					//降冪
					sopportList.sort(getPriceCheckInfoComparator());
					sopportList.remove(3);
				}
			}
		}
		pressureList.forEach(p -> {
			System.out.println("日期["+WType.dateToStr(p.getDate(), WDate.dateFormat_yyyyMMdd)+"]壓力種類["+p.getPcpType().getDesc()+"]價格["+p.getPrice()+"]");
		});
		System.out.println("今日收盤價["+pricePo.getClose()+"]");
		sopportList.forEach(p -> {
			System.out.println("日期["+WType.dateToStr(p.getDate(), WDate.dateFormat_yyyyMMdd)+"]支撐種類["+p.getPcpType().getDesc()+"]價格["+p.getPrice()+"]");
		});
//		priceCheckPoints.forEach(x -> System.out.println(x.getPcpType().getDesc() + " - " + x.getDate() + " - " + x.getPrice()));
	}
	
	private Comparator<PriceCheckInfoDTO> getPriceCheckInfoComparator() {
		return new Comparator<PriceCheckInfoDTO>() {
			@Override
			public int compare(PriceCheckInfoDTO o1, PriceCheckInfoDTO o2) {
				return o1.getPrice().compareTo(o2.getPrice()) * -1;
			}
		};
	}
	
	private void patternAnalyzer() {
		try {
			List<TimeSeriesDTO<OHLCVDTO>> ohlcvList = dtDao.findRecentTsList(this.sid,
					Const._FIELDID_LIST_OHLCV, OHLCVDTO.class, this.analyzedate, -30);
			
			//N字底
			//3711 20200407
			//8069 20200422
			int nBottomStatus = tdSvc.getNBottomStatus(this.analyzedate,
					ohlcvList.stream()
							.map(ohlcv -> new TimeSeriesDTO<BigDecimal>(ohlcv.getDate(), ohlcv.getValue().getHigh()))
							.collect(Collectors.toList()),
					ohlcvList.stream()
							.map(ohlcv -> new TimeSeriesDTO<BigDecimal>(ohlcv.getDate(), ohlcv.getValue().getLow()))
							.collect(Collectors.toList()));
			if (nBottomStatus > -1) {
				System.out.println(nBottomStatus + "日前N字底成形，只出現在底部，成型機率高，可在第二隻腳後紅K試單，目標價為V2+(A1-V1)，跌破第二隻腳停損");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	String getProcessorName() {
		// TODO Auto-generated method stub
		return "技術面";
	}
}
