//package com.willy.sias.batch.processor;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.stereotype.Service;
//
//import com.willy.sias.db.dao.ShiaojiDao;
//import com.willy.sias.db.dto.TicksDTO;
//import com.willy.sias.db.po.SysConfigPO;
//import com.willy.sias.db.po.TicksTwPO;
//import com.willy.sias.db.repository.CompanyCategoryRepository;
//import com.willy.sias.db.repository.SysConfigRepository;
//import com.willy.sias.db.repository.TicksTwRepository;
//
//import lombok.Setter;
//
//@Service
//@Setter
//@ConfigurationProperties("sias.batch.yf-ticks")
//public class YF_TICKS extends BaseBatchProcessor {
//	@Autowired
//	private SysConfigRepository scRepo;
//	@Autowired
//	private TicksTwRepository ttRepo;
//	@Autowired
//	private CompanyCategoryRepository ccRepo;
//	@Autowired
//	private ShiaojiDao shiaojiUtil;
//
//	public void exec() throws Exception {
//		// 取出sidCode
//		List<Integer> sidList = ccRepo.findSidWithoutDerivative();
//		List<TicksTwPO> psList = new ArrayList<>();
//		for (int sid : sidList) {
//			SysConfigPO sc = scRepo.findById(sid).orElse(null);
//			List<TicksDTO> kbList = shiaojiUtil.getTicksList(sc.getCfgValue().replace(".TW", ""), this.dataDate);
//			psList.addAll(ticksDTOToPriceSecondTwPO(sid, kbList));
//			if (psList.size() > 10000) {
//				ttRepo.saveAll(psList);
//				psList.clear();
//			}
//			Thread.sleep(1500);
//		}
//		ttRepo.saveAll(psList);
//	}
//
//	private List<TicksTwPO> ticksDTOToPriceSecondTwPO(int sid, List<TicksDTO> kbList) {
//		TicksTwPO ticks;
//		List<TicksTwPO> ticksList = new ArrayList<>();
//		for (TicksDTO kb : kbList) {
//			ticks = new TicksTwPO();
//			ticks.setSid(sid);
//			ticks.setDataDate(this.dataDate);
//			ticks.setTxnTime(kb.getTxnTime());
//			ticks.setAskPrice(kb.getAskPrice());
//			ticks.setAskVolume(kb.getAskVolume());
//			ticks.setClose(kb.getClose());
//			ticks.setBidPrice(kb.getBidPrice());
//			ticks.setBidVolume(kb.getBidVolume());
//			ticks.setVolume(kb.getVolume());
//			ticksList.add(ticks);
//		}
//		return ticksList;
//	}
//
//	@Override
//	public void cleanHisData() throws Exception {
//		// TODO Auto-generated method stub
//
//	}
//}
