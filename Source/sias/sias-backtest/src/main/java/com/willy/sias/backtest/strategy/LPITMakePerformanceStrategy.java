//package com.willy.sias.backtest.strategy;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//import java.util.stream.Collectors;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.db.dto.TimeSeriesDTO;
//import com.willy.sias.db.po.LPTransLogTwPO;
//import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
//import com.willy.sias.util.EnumSet.TradeType;
//import com.willy.sias.util.config.Const;
//import com.willy.util.date.WDate;
//import com.willy.util.log.WLog;
//
///**
// * 投信季底作帳行情(季報發布日: 5/15,8/14,11/14,3/31)
// * 
// * @author Willy 1. 時機: 各季底(尤其6.12月) 2. 假設進場時機為6月 3. 條件1: 4.5月皆為買超 4. 條件2:
// *         股價站在月線上 5. 條件3:
// */
//public class LPITMakePerformanceStrategy extends TradeStrategy {
//	private List<String> testedMonth = new ArrayList<>();
//	@Autowired
//	private LPTransLog_TW_TWSERepository lpRepo;
//
//	@Override
//	public void setTradeDetail(Date tradeDate, ProfitPO profitPo) {
//		// 取得月份
//		Integer year = WDate.getDateInfo(tradeDate, Calendar.YEAR);
//		Integer month = WDate.getDateInfo(tradeDate, Calendar.MONTH);
//		// 確認是年中或年底且排除測試過的
//		if (month.compareTo(1) == 0 || month.compareTo(7) == 0 || testedMonth.contains(year + "" + month)) {
//			return;
//		}
//		testedMonth.add(year + "" + month);
//
//		// 取得前一個月最後交易日
//		List<Date> pastTradeDateList = dtDao.findBusDate(tradeDate, -1);
//		if (pastTradeDateList == null || pastTradeDateList.size() == 0 || pastTradeDateList.get(0) == null) {
//			WLog.error("Can't get last BusDate tradeDate[" + tradeDate + "]");
//			return;
//		}
//
//		for (Integer sid : profitPo.getSidSet()) {
//			// 投資20天後賣掉
//			profitPo.getTradeDetailList().stream().filter(td -> {
//				try {
//					return !td.isTradeComplete() && WDate.calDate(td.getBuyDate(), Calendar.DATE, 20).before(tradeDate);
//				} catch (ParseException e) {
//					return false;
//				}
//			}).collect(Collectors.toList())
//					.forEach(td -> tu.trade(sid, tradeDate, 1, TradeType.sell, profitPo));
//			
//			List<Date> last40BusDate = dtDao.findBusDate(tradeDate, -40);
//			if (last40BusDate == null || last40BusDate.size() < 40) {
//				continue;
//			}
//
//			List<LPTransLogTwPO> lpPo = lpRepo.findBySidIsAndTransDateBetween(sid, last40BusDate.get(0),
//					last40BusDate.get(last40BusDate.size() - 1));
//			if (lpPo == null || lpPo.size() < 40) {
//				continue;
//			}
//
//			// 確認前2個月皆為買超狀態
//			BigDecimal last4To2MonthITNetBuy = new BigDecimal(0);
//			BigDecimal last2To0MonthITNetBuy = new BigDecimal(0);
//			for (int i = 0; i < 40; i++) {
//				if (i < 20) {
//					last4To2MonthITNetBuy = last4To2MonthITNetBuy.add(new BigDecimal(lpPo.get(i).getItNet()));
//				} else {
//					last2To0MonthITNetBuy = last2To0MonthITNetBuy.add(new BigDecimal(lpPo.get(i).getItNet()));
//				}
//			}
//			if (last4To2MonthITNetBuy.compareTo(new BigDecimal(0)) > 0
//					&& last2To0MonthITNetBuy.compareTo(new BigDecimal(0)) > 0) {
//				continue;
//			}
//
//			// 近5天股價站在月線上
//			List<TimeSeriesDTO<BigDecimal>> priceTsList = dtDao.findTdsList(sid, Const._FIELDID_PRICE_DAILY_TW_CLOSE,
//					last40BusDate.get(0), last40BusDate.get(last40BusDate.size() - 1));
//			if (priceTsList == null || priceTsList.size() < 40) {
//				continue;
//			}
//			// 近5天ma20都<收盤價
//			for (int i = last40BusDate.size() - 5; i < last40BusDate.size(); i++) {
//				// 取得MA20
//				BigDecimal last20CloseSum = new BigDecimal(0);
//				for (int j = i - 20; j < i; j++) {
//					last20CloseSum = priceTsList.get(j).getDecimalValue();
//				}
//				// ma20 < 收盤價
//				if (last20CloseSum.divide(new BigDecimal(20), 4, RoundingMode.HALF_UP)
//						.compareTo(priceTsList.get(i).getDecimalValue()) < 0) {
//					continue;
//				}
//			}
//			tu.trade(sid, tradeDate, 1, TradeType.buy, profitPo);
//		}
//	}
//
//	@Override
//	public String getStrategyDesc() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public String checkArgs() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void init() {
//	}
//
//}
