package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

/**
 * LP_FUTURE_TW
 * @author 
 */
@Table(name="FR_IS_TW")
@Entity
@Data
@IdClass(FRISTWPOKey.class)
public class FRISTWPO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 股票代號
     */
    @Id
    @Column(name = "SID")
    private int sid;
    
    /**
     * 財報所屬日期
     */
    @Id
    @Column(name = "FR_DATE")
    private String frDate;
    
    /**
     * 財報項目ID
     */
    @Id
    @Column(name = "FR_KEY")
    private int frKey;
    
    /**
     * 財報項目值
     */
    @Column(name = "FR_VALUE")
    private BigDecimal frValue;
    
    /**
     * 財報公布日
     */
    @Column(name = "FR_RELEASE_DATE")
    private String frReleaseDate;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FRISTWPO other = (FRISTWPO) obj;
		if (frDate == null) {
			if (other.frDate != null)
				return false;
		} else if (!frDate.equals(other.frDate))
			return false;
		if (frKey != other.frKey)
			return false;
		if (sid != other.sid)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((frDate == null) ? 0 : frDate.hashCode());
		result = prime * result + frKey;
		result = prime * result + sid;
		return result;
	}
    
}
