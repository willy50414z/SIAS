package com.willy.sias.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.PriceSecondTwPO;
import com.willy.sias.db.po.PriceSecondTwPOKey;

@Repository
public interface PriceSecondTwRepository extends JpaRepository<PriceSecondTwPO, PriceSecondTwPOKey> {
}