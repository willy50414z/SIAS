package com.willy.sias.db.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.BackTestLogHPO;

@Repository
public interface BackTestLogHRepository extends JpaRepository<BackTestLogHPO, Integer> {
	public List<BackTestLogHPO> findTop10ByOrderByTestIdDesc();
	@Query(value = "SELECT NEXT VALUE FOR  BACK_TEST_SEQ", nativeQuery  =true)
	public Integer findNextTestId();
}
