package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.po.CalendarPOKey;

@Repository
public interface CalendarRepository extends JpaRepository<CalendarPO, CalendarPOKey> {
	public List<CalendarPO> findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(Date date, int dataType);
	public List<CalendarPO> findByDateTypeIsOrderByDate(int dateType);
	public List<CalendarPO> findByDateTypeIsAndDateBetween(int dateType, Date startDate, Date endDate);
	public List<CalendarPO> findByDateTypeIsAndDateGreaterThanEqual(int dateType, Date startDate);
	public List<CalendarPO> findByDateTypeIsAndDateLessThanEqual(int dateType, Date endDate);
	
}
