package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.file.txt.WFile_txt;
import com.willy.sias.db.po.MonthlyRevenueTWPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.MonthlyRevenueTWRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.FileUtil;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

import lombok.Setter;

@Setter
@Service
@ConfigurationProperties("sias.batch.monthly-revenue-tw")
public class MONTHLY_REVENUE_TW extends BaseBatchProcessor {
	private String urlTempl;
	private String csvFileNameTempl;
	@Autowired
	private WFile_txt txtFile;
	@Autowired
	private SysConfigRepository sysConfigRepo;
	@Autowired
	private MonthlyRevenueTWRepository mrRepo;
	private String year;
	private String month;

	@Override
	protected void exec() throws Exception {
		Set<MonthlyRevenueTWPO> revenuePoSet = new HashSet<> ();
		
		year = "" + WDate.convertCEYear(WDate.getDateInfo(dataDate, Calendar.YEAR));
		month = String.valueOf(WDate.getDateInfo(dataDate, Calendar.MONTH));
		this.putMapParam("year", year);
		this.putMapParam("season", month);
		
		// 設定讀取csv相關參數
		txtFile.setEncoding("UTF-8");
		txtFile.setSeparator(",");

		// 取得市場別
		List<SysConfigPO> marketTypeList = sysConfigRepo.findByParentIdIsOrderByCfgId(ParentKey.MarketType.getKey());

		// 一個個市場別去撈
		for (SysConfigPO marketType : marketTypeList) {
			WLog.info("開始匯入每月營收marketType[" + marketType.getCfgDesc()+"]");
			this.putMapParam("marketType", marketType.getCfgValue());
			String csvFilePath = FileUtil.download(this.parseMapParamToString(urlTempl),
					this.parseMapParamToString(csvFileNameTempl));

			// 讀取
			WLog.debug("讀取營收資料");
			String[][] revenueItemAr = txtFile.readToAr(csvFilePath, "出表日期", null, true);

			// 將公司匯入對照表
			WLog.debug("匯入SID對照表");
			mergeSid(revenueItemAr, 2, 3);

			// 暫存財報資料(有重複)
			revenuePoSet.addAll(this.compose(revenueItemAr));
		}
		WLog.info("匯入營收資料");
		mrRepo.saveAll(revenuePoSet);
	}

	private Set<MonthlyRevenueTWPO> compose(String[][] revenueAr) {
		MonthlyRevenueTWPO revenuePo;
		Set<MonthlyRevenueTWPO> revenueSet = new HashSet<> ();
		int ceYear = WDate.getDateInfo(dataDate, Calendar.YEAR);
		String revenueDate = ceYear + WString.fillStrLength(month, "0", -2);
		try {
			for(String[] rowData : revenueAr) {
				if(rowData.length > 10) {
					revenuePo = new MonthlyRevenueTWPO();
					revenuePo.setSid(sidMap.get(rowData[2]));
					revenuePo.setRevenueDate(revenueDate);
					revenuePo.setRevenueLastMonth(new BigDecimal(WString.cleanIntegerData(rowData[6])));
					revenuePo.setRevenueLastYear(new BigDecimal(WString.cleanIntegerData(rowData[7])));
					revenuePo.setSumRevenue(new BigDecimal(WString.cleanIntegerData(rowData[10])));
					revenuePo.setSumRevenueLastYear(new BigDecimal(WString.cleanIntegerData(rowData[11])));
					revenuePo.setRevenue(new BigDecimal(WString.cleanIntegerData(rowData[5])));
					revenueSet.add(revenuePo);
				}
			}
			return revenueSet;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
		
	}
}