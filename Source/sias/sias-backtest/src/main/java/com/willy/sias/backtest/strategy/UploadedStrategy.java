//package com.willy.sias.backtest.strategy;
//
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.willy.sias.backtest.po.ProfitPO;
//import com.willy.sias.backtest.service.TechnicalService;
//import com.willy.sias.backtest.util.StatisticUtil;
//import com.willy.sias.db.repository.SysConfigRepository;
//import com.willy.sias.util.EnumSet.TradeType;
//import com.willy.sias.util.EnumSet.TrendType;
//import com.willy.util.log.WLog;
//
//@Service
//public class UploadedStrategy extends TradeStrategy {
//	@Autowired
//	private SysConfigRepository scRepo;
//	private String tableName = "";
//	@Autowired
//	private SysConfigRepository sysConfigRepo;
//	@Autowired
//	private StatisticUtil staticService;
//	@Autowired
//	private TechnicalService techService;
//
//	@Override
//	public void setTradeDetail(Integer sid, List<Date> tradeDateList, ProfitPO profitPo) {
//		try {
//			for (Date date : tradeDateList) {
//				// 均線多排
//				int maArgmtDay = techService.getMAArgmtDays(sid, date);
//				if (maArgmtDay < 5) {
//					continue;
//				}
//				// 長期趨勢為上升趨勢
//				TrendType tt = techService.getTrendType(sid, date, true);
//				if (tt != TrendType.UP_WARD) {
//					continue;
//				}
//				// 短期趨勢為上升趨勢
//				tt = techService.getTrendType(sid, date, false);
//				if (tt != TrendType.UP_WARD || tt.getBreakUdTrackStatus() < 1) {
//					continue;
//				}
//				tu.trade(sid, date, 1, TradeType.buy, profitPo);
//			}
//
////			List<TimeSeriesDTO<OHLCVDTO>> ohlcTsList = dtDao.<OHLCVDTO>findTdsList(sid, Const._FIELDID_LIST_OHLCV, OHLCVDTO.class);
////			List<TimeSeriesDTO<BigDecimal>> tradeShareMa5List = statService.getSMA(ohlcTsList.stream().map(x -> new TimeSeriesDTO<BigDecimal>(x.getDate(), x.getValue().getTradingShares())).collect(Collectors.toList()), 5);
////			for (int i = 4; i < ohlcTsList.size(); i++) {
////				if (ohlcTsList.get(i) != null && ohlcTsList.get(i).getDate() != null && tradeDateList.contains(ohlcTsList.get(i).getDate())) {
////					Date today = ohlcTsList.get(i).getDate();
////					//T-4為長紅K
////					boolean isT4lr = ohlcTsList.get(i - 4).getValue().getClose()
////							.divide(ohlcTsList.get(i - 4).getValue().getOpen(), 4, RoundingMode.HALF_UP)
////							.compareTo(new BigDecimal(1.05)) > 0;
////
////					//T-1~T-3為黑K
////					boolean isT1_3bk = true;
////					BigDecimal t1_3Lowest = new BigDecimal(9999);
////					for(int j=1; j<4;j++) {
////						t1_3Lowest = t1_3Lowest.min(ohlcTsList.get(i - j).getValue().getLow());
////						//若T-1~T-3有一天收盤>=開盤則為false
////						if (ohlcTsList.get(i - j).getValue().getOpen().compareTo(Const._BIGDECIMAL_0) > 0
////								&& ohlcTsList.get(i - j).getValue().getClose()
////										.divide(ohlcTsList.get(i - j).getValue().getOpen(), 4, RoundingMode.HALF_UP)
////										.compareTo(new BigDecimal(1)) >= 0) {
////							isT1_3bk = false;
////							break;
////						}
////					}
////					
////					//is T-1 ~ T-3 low higher than T-4 open
////					boolean isT1_3LowHigherThanT4op = t1_3Lowest.compareTo(ohlcTsList.get(i - 4).getValue().getOpen()) == 1;
////					
////					//is ma tradeshare > 2000000
////					boolean isTradeShareEnough = false;
////					TimeSeriesDTO<BigDecimal> ma5Ts = tradeShareMa5List.stream().filter(ts -> ts.getDate().compareTo(today)==0).findFirst().orElse(null);
////					if(ma5Ts != null && ma5Ts.getDecimalValue() != null && ma5Ts.getDecimalValue().compareTo(new BigDecimal(2000000))>0) {
////						isTradeShareEnough = true;
////					}
////					
////					int maArgmtDay = techService.getMAArgmtDays(sid, today);
////					
////					if(isT4lr && isT1_3bk && isT1_3LowHigherThanT4op && isTradeShareEnough && maArgmtDay>5) {
////						tu.trade(sid, today, 1, TradeType.buy, profitPo);
////					}
////				}
////			}
//		} catch (Exception e) {
//			WLog.error(e);
//		}
//	}
//
//	@Override
//	public void init() {
//		// TODO Auto-generated method stub
//		super.init();
//	}
//
//}
