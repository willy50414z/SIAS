package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="IDX_DAILY_US")
@Entity
@Data
@IdClass(IdxDailyUsPOKey.class)
@NoArgsConstructor
public class IdxDailyUsPO implements Serializable {

  @Id
  @Column(name = "SID")
  private Integer sid;

  /**
   * 資料日期
   */
  @Id
  @Column(name = "DATA_DATE")
  private Date dataDate;
  /**
   * 開盤價
   */
  @Column(name = "\"OPEN\"")
  private BigDecimal open;

  /**
   * 最高價
   */
  @Column(name = "HIGH")
  private BigDecimal high;

  /**
   * 最低價
   */
  @Column(name = "LOW")
  private BigDecimal low;

  /**
   * 收盤價
   */
  @Column(name = "\"CLOSE\"")
  private BigDecimal close;
}
