package com.willy.sias.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.willy.sias.db.po.MonthlyRevenueTWPO;
import com.willy.sias.db.po.MonthlyRevenueTWPOKey;

public interface MonthlyRevenueTWRepository extends JpaRepository<MonthlyRevenueTWPO, MonthlyRevenueTWPOKey> {

}
