package com.willy.sias.web.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import com.willy.util.log.WLog;

@Controller
@RequestMapping("/")
public class LinebotController {
	@Value("${line.bot.userId}")
	private String userId;
	@Autowired
	private LineMessagingClient client;
	
	@ResponseBody
	@RequestMapping("/callback")
	public void callback() throws ParseException {
	}
	
	@ResponseBody
	@RequestMapping("/putTextMessage/{message}")
	public void putTextMessage(@PathVariable("message") String message) throws ParseException {
		pushTextMessage(message);
	}

	public BotApiResponse pushTextMessage(String message) {
		return pushTextMessage(this.userId, message);
	}

	public BotApiResponse pushTextMessage(String userId, String message) {
		TextMessage textMessage = new TextMessage(message);
		PushMessage pushMessage = new PushMessage(userId, textMessage);

		BotApiResponse botApiResponse = null;
		try {
			botApiResponse = client.pushMessage(pushMessage).get();
		} catch (Exception e) {
			WLog.error(e);
		}
		return botApiResponse;
	}
}

@Component
class LineBeanConfig{
	@Bean
	public LineMessagingClient getLineMessagingClient(@Value("${line.bot.channelToken}") String channelToken) {
		return LineMessagingClient.builder(channelToken).build();
	}
}
