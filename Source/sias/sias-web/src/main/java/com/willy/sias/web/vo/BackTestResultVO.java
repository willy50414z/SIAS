package com.willy.sias.web.vo;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.codehaus.plexus.util.StringUtils;

import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BackTestResultVO {
	// 回策錯誤信息
	private String errMsg;
	// 篩選條件Map<欄位名稱,欄位描述>
	private List<String> historyTestIdList;
	// 顯示結果list
	private List<BackTestSummaryVO> backTestSummaryList;
	private String dataTableHtml;

	public void setDataTableHtml(Integer testId, String contextPath) {
		StringBuffer dataTableHtml = new StringBuffer("<table id=\"backTestResultTable\" style=\"width:90%\"><thead><tr>"); 
		BackTestResultColumn brCol;
		List<String> titleList = new ArrayList<>();
		List<String> colOrderList = new ArrayList<>();
		Field[] smyFields = BackTestSummaryVO.class.getDeclaredFields();
		for (Field smyField : smyFields) {
			if (smyField.getDeclaredAnnotation(BackTestResultColumn.class) != null) {
				brCol = smyField.getDeclaredAnnotation(BackTestResultColumn.class);
				titleList.add(brCol.order() + "-" + brCol.desc());
				colOrderList.add(brCol.order()+"-"+smyField.getName());
			}
		}
		//設定Title
		titleList.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		});
		titleList.forEach(title -> dataTableHtml.append("<th>").append(title.substring(title.indexOf("-") + 1)).append("</th>"));
		dataTableHtml.append("<th>").append("交易明細").append("</th>");
		dataTableHtml.append("<th>").append("").append("</th>");
		dataTableHtml.append("</tr></thead><tbody>");
		
		//設定Content
		colOrderList.sort(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				// TODO Auto-generated method stub
				return o1.compareTo(o2);
			}
		});
		Object colObjValue;
		for(BackTestSummaryVO btsmyVo : backTestSummaryList) {
			dataTableHtml.append("<tr>");
			for(int colIndex=0;colIndex<colOrderList.size();colIndex++) {
				try {
					colObjValue = WReflect.getFieldValue(btsmyVo, colOrderList.get(colIndex).substring(colOrderList.get(colIndex).indexOf("-") + 1));
					dataTableHtml.append("<td>").append(StringUtils.defaultString(colObjValue)).append("</td>");
				} catch (Exception e) {
					WLog.error(e);
				}
			}
			dataTableHtml.append("<td><button type=\"button\" class=\"btn btn-primary tradeDetailBtn\" value=\""+btsmyVo.getTestIdD()+"\">交易明細</button></td>");
			dataTableHtml.append("<td><button type=\"button\" onClick=\"downloadStrategySource("+testId+")\"><img src=\"" + contextPath + "/images/download.png\" style=\"width:10px;\"></button></td>");
			dataTableHtml.append("</tr>");
		}
		dataTableHtml.append("</tbody></table>");
		
		this.dataTableHtml = dataTableHtml.toString();
	}
}
