package com.willy.sias.backtest.service;

import com.willy.r.test.WRTechInd;
import com.willy.sias.anno.SvcInfo;
import com.willy.sias.anno.SvcMethodInfo;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.KDJDTO;
import com.willy.sias.db.dto.MACDDTO;
import com.willy.sias.db.dto.OHLCDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * 技術指標相關分析工具
 *
 * @author Willy
 */
@Service
@SvcInfo("技術指標")
public class TechIdxService {

  @Autowired
  public DataTableDao dtDao;
  @Autowired
  private StatisticService staticSvc;
  @Autowired
  private WRTechInd rTechIndex;

  @SvcMethodInfo(methodDesc = "均線", resultDesc = "均線 Ts List")
  @Cacheable(value="sias:TechIdxService", key="{#root.methodName, #sid, #interval, #fieldId}")
  public List<TimeSeriesDTO<BigDecimal>> getSMA(Integer sid, Integer interval, Integer fieldId) {
    List<TimeSeriesDTO<BigDecimal>> tdList = dtDao.findTsList(sid, fieldId);
    return getSMA(tdList, interval);
  }

  @SvcMethodInfo(methodDesc = "均線", argsDesc = {"均線資料tsList",
      ""}, resultDesc = "均線 Ts List")
  public List<TimeSeriesDTO<BigDecimal>> getSMA(List<TimeSeriesDTO<BigDecimal>> tsdList,
      int interval) {
    if (CollectionUtils.isEmpty(tsdList) || tsdList.size() < interval) {
      return null;
    }
    BigDecimal avg = new BigDecimal(0);
    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<TimeSeriesDTO<BigDecimal>>();
    // 為回傳相同長度資料，先塞進部分Null
    for (int i = 0; i < interval - 1; i++) {
      result.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(i).getDate(), null));
    }
    for (int rowIndex = interval - 1; rowIndex < tsdList.size(); rowIndex++) {
      if (rowIndex >= interval) {
        // 加上新增的
        avg = avg.add(tsdList.get(rowIndex).getDecimalValue());
        // 減掉舊的
        avg = avg.subtract(tsdList.get(rowIndex - interval).getDecimalValue());
        result.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(rowIndex).getDate(),
            (avg.divide(new BigDecimal(interval), 4, RoundingMode.HALF_UP))));
      } else {
        // 第一筆資料
        for (int row = 0; row < interval; row++) {
          avg = avg.add(tsdList.get(row).getDecimalValue());
        }
        result.add(new TimeSeriesDTO<BigDecimal>(tsdList.get(rowIndex).getDate(),
            (avg.divide(new BigDecimal(interval), 4, RoundingMode.HALF_UP))));
      }
    }
    return result;
  }

  @SvcMethodInfo(methodDesc = "數值連續上升/下降期數", resultDesc = "連續 (return>0?上升:下降) N期")
  public int getSlopeContinueDays(int sid, Date baseDate, int fieldId) {
    List<TimeSeriesDTO<BigDecimal>> tsdList = dtDao.findTsList(sid, fieldId).stream()
        .filter(ts -> WDate.beforeOrEquals(ts.getDate(), baseDate)).collect(
            Collectors.toList());
    if (tsdList == null) {
      throw new IllegalArgumentException(
          "can't get tdList. sid[" + sid + "]baseDate[" + baseDate + "]fieldId[" + fieldId + "]");
    }
    return getSlopeContinueDays(tsdList);
  }

  @SvcMethodInfo(methodDesc = "數值連續上升/下降期數", resultDesc = "連續 (return>0?上升:下降) N期")
  public int getSlopeContinueDays(List<TimeSeriesDTO<BigDecimal>> tsList) {
    int trend = 0;
    int continueDays = 0;
    for (int i = tsList.size() - 2; i > -1; i--) {
      if(trend == 0) {
        trend = tsList.get(i+1).getDecimalValue().compareTo(tsList.get(i).getDecimalValue());
      }
      if (trend != 0 && (trend == tsList.get(i+1).getDecimalValue().compareTo(tsList.get(i).getDecimalValue()))) {
        continueDays++;
      } else {
        break;
      }
    }
    return continueDays * trend;
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  @SvcMethodInfo(methodDesc = "past 60 days股價3期轉折點", resultDesc = "[0: 股價最高價轉折點, 1: 最低價轉折點]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(int sid, Date baseDate) {
    List[] turningPointListAr = new ArrayList[2];
    for (int checkTicks = 3; checkTicks > 0; checkTicks--) {
      List<List<TimeSeriesDTO<BigDecimal>>> tmpTurningPointList = this.getTurningPointList(sid,
          baseDate, checkTicks);

      if (tmpTurningPointList == null) {
        WLog.warn("SID[" + sid + "]date[" + baseDate + "] can't get past 60 days data");
        return null;
      }

      for (int i = 0; i < 2; i++) {
        if (turningPointListAr[i] == null) {
          turningPointListAr[i] =
              tmpTurningPointList.get(i).stream().filter(tp -> tp.getDecimalValue() != null).count()
                  > 1 ? tmpTurningPointList.get(i) : null;
        }
      }

      if (turningPointListAr[0] != null && turningPointListAr[1] != null) {
        break;
      }
    }
    return Arrays.asList(turningPointListAr);
  }

  @SvcMethodInfo(methodDesc = "past 60 days股價N期轉折點", resultDesc = "[0: 股價最高價轉折點, 1: 最低價轉折點]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(int sid, Date baseDate,
      int checkTicks) {
    return getTurningPointList(sid, baseDate, Const._FIELDID_PRICE_DAILY_TW_HIGH,
        Const._FIELDID_PRICE_DAILY_TW_LOW, checkTicks);
  }

  @SvcMethodInfo(methodDesc = "past 60 days Field 3期轉折點", argsDesc = {"", "", "A轉 Field ID",
      "V轉 Field ID"}, resultDesc = "[0: Field1 A轉點, 1: Field2 V轉點]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(int sid, Date baseDate,
      int highFieldId,
      int lowFieldId) {
    return getTurningPointList(sid, baseDate, highFieldId, lowFieldId, 3);
  }

  @SvcMethodInfo(methodDesc = "past 60 days Field N期轉折點", argsDesc = {"", "", "A轉 Field ID",
      "V轉 Field ID", "轉折判斷期數"}, resultDesc = "[0: Field1 A轉點, 1: Field2 V轉點]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(int sid, Date baseDate,
      int highFieldId,
      int lowFieldId, int checkTicks) {
    // 取出進60交易日日期
    List<TimeSeriesDTO<BigDecimal>> highPriceList = dtDao.findRecentTsList(sid,
        highFieldId,
        baseDate, -60);
    if (highPriceList.size() < 60) {
      return null;
    }
    List<TimeSeriesDTO<BigDecimal>> lowPriceList;
    if (highFieldId == lowFieldId) {
      lowPriceList = new ArrayList<>();
      lowPriceList.addAll(highPriceList);
    } else {
      lowPriceList = dtDao.findRecentTsList(sid, lowFieldId, baseDate, -60);
    }
    if (CollectionUtils.isEmpty(highPriceList) || CollectionUtils.isEmpty(lowPriceList)) {
      throw new IllegalArgumentException(
          "highPrice or lowPrice get fail, sid[" + sid + "]highPrice["
              + (highPriceList == null ? "null" : highPriceList.toString()) + "]lowPrice["
              + (lowPriceList == null ? "null" : lowPriceList.toString()) + "]highPrice.size()["
              + highPriceList.size() + "]lowPrice.size()[" + lowPriceList.size() + "]");
    }
    return getTurningPointList(checkTicks, highPriceList, lowPriceList);
  }

  @SvcMethodInfo(methodDesc = "股價高低轉折點", argsDesc = {"A轉List",
      "V轉List"}, resultDesc = "[0: Field1 A轉點, 1: Field2 V轉點]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(
      List<TimeSeriesDTO<BigDecimal>> highPriceList,
      List<TimeSeriesDTO<BigDecimal>> lowPriceList) {
    return getTurningPointList(3, highPriceList, lowPriceList);
  }

  @SvcMethodInfo(methodDesc = "股價高低轉折點", argsDesc = {"前後N期內最高點", "上轉折資料",
      "下轉折資料"}, resultDesc = "[0: Field1 A轉點, 1: Field2 V轉點], 長度與PriceList相同，非轉折點value=null")
  public List<List<TimeSeriesDTO<BigDecimal>>> getTurningPointList(int checkTicks,
      List<TimeSeriesDTO<BigDecimal>> highPriceList, List<TimeSeriesDTO<BigDecimal>> lowPriceList) {
    // 取出股價轉折點
    List<TimeSeriesDTO<BigDecimal>> vTurnTsList = new ArrayList<>();
    List<TimeSeriesDTO<BigDecimal>> aTurnTsList = new ArrayList<>();
    for (int i = 0; i < checkTicks; i++) {
      aTurnTsList.add(new TimeSeriesDTO<BigDecimal>(highPriceList.get(i).getDate(), null));
      vTurnTsList.add(new TimeSeriesDTO<BigDecimal>(highPriceList.get(i).getDate(), null));
    }
    for (int i = checkTicks; i <= highPriceList.size() - (checkTicks + 1); i++) {
      // 取得近checkTicks期最大(小)值
      BigDecimal max = new BigDecimal(0), min = new BigDecimal(0);
      for (int j = i - checkTicks; j <= i + checkTicks; j++) {
        if (j != i) {
          min =
              min.compareTo(Const._BIGDECIMAL_0) == 0 ? lowPriceList.get(j).getDecimalValue() : min;
          max = max.max(highPriceList.get(j).getDecimalValue());
          min = min.min(lowPriceList.get(j).getDecimalValue());
        }
      }

//			WLog.debug("Date["+highPriceList.get(i).getDate()+"]max["+max+"]min["+min+"]");

      // 當日高價>前後N日最高價
      // || (當日高價==前後N日最高價 && 當天資料尚未被記錄)
      if (highPriceList.get(i).getDecimalValue().compareTo(max) > 0
          || (highPriceList.get(i).getDecimalValue().compareTo(max) == 0 && (aTurnTsList.size() == 0
          || (aTurnTsList.get(aTurnTsList.size() - 1).getDecimalValue() == null
          || (aTurnTsList.get(aTurnTsList.size() - 1).getDecimalValue() != null
          && aTurnTsList.get(aTurnTsList.size() - 1).getDecimalValue()
          .compareTo(highPriceList.get(i).getDecimalValue()) != 0))))) {
        aTurnTsList.add(new TimeSeriesDTO<BigDecimal>(highPriceList.get(i).getDate(),
            highPriceList.get(i).getDecimalValue()));
      } else {
        aTurnTsList.add(new TimeSeriesDTO<BigDecimal>(highPriceList.get(i).getDate(), null));
      }

      if (lowPriceList.get(i).getDecimalValue().compareTo(min) < 0
          || (lowPriceList.get(i).getDecimalValue().compareTo(min) == 0 && (vTurnTsList.size() == 0
          || (vTurnTsList.get(vTurnTsList.size() - 1).getDecimalValue() == null
          || (vTurnTsList.get(vTurnTsList.size() - 1).getDecimalValue() != null
          && vTurnTsList.get(vTurnTsList.size() - 1).getDecimalValue()
          .compareTo(lowPriceList.get(i).getDecimalValue()) != 0))))) {
        vTurnTsList.add(new TimeSeriesDTO<BigDecimal>(lowPriceList.get(i).getDate(),
            lowPriceList.get(i).getDecimalValue()));
      } else {
        vTurnTsList.add(new TimeSeriesDTO<BigDecimal>(lowPriceList.get(i).getDate(), null));
      }
    }
    // 將未判斷到的add進去，以便回傳相同長度List
    for (int i = highPriceList.size() - checkTicks; i < highPriceList.size(); i++) {
      aTurnTsList.add(new TimeSeriesDTO<BigDecimal>(highPriceList.get(i).getDate(), null));
      vTurnTsList.add(new TimeSeriesDTO<BigDecimal>(lowPriceList.get(i).getDate(), null));
    }

    List<List<TimeSeriesDTO<BigDecimal>>> turningPointList = new ArrayList<>();
    turningPointList.add(aTurnTsList);
    turningPointList.add(vTurnTsList);
    return turningPointList;
  }

  @SvcMethodInfo(methodDesc = "KD指標(預設參數9天)", resultDesc = "KDJDTO TS List")
  public List<TimeSeriesDTO<KDJDTO>> getKD(int sid) throws Exception {
    return getKD(sid, 9);
  }

  @SvcMethodInfo(methodDesc = "KD指標", argsDesc = {"", "KD天數"}, resultDesc = "KDJDTO TS List")
  public List<TimeSeriesDTO<KDJDTO>> getKD(int sid, int kdPeriod) throws Exception {
    List<TimeSeriesDTO<KDJDTO>> kdjTsList = new ArrayList<>();
    // 取出股價
    List<TimeSeriesDTO<OHLCDTO>> ohlcList = dtDao.findTsList(sid,
        Const._FIELDID_LIST_OHLC,
        OHLCDTO.class);

    // init KDJ, insert some default kdj
    ArrayList<KDJDTO> kdjList = new ArrayList<KDJDTO>();
    // default previous n-1 kdj are 50
    for (int i = 0; i < kdPeriod - 1; i++) {
      kdjList.add(new KDJDTO(new BigDecimal(50), new BigDecimal(50), new BigDecimal(50)));
    }

    BigDecimal rsv, k, d, j;
    BigDecimal oneThird = new BigDecimal(1).divide(new BigDecimal(3), 20, RoundingMode.HALF_UP);
    BigDecimal twoThird = new BigDecimal(2).divide(new BigDecimal(3), 20, RoundingMode.HALF_UP);
    // 逐天計算
    for (int i = kdPeriod - 1; i < ohlcList.size(); i++) {
      BigDecimal highestPrice = new BigDecimal(0);
      BigDecimal lowestPrice = new BigDecimal(0);
      // 往回算N天最高/低價
      for (int l = 0; l < kdPeriod; l++) {
        highestPrice = highestPrice.max(ohlcList.get(i - l).getValue().getHigh());
        if (lowestPrice.equals(new BigDecimal(0))) {
          lowestPrice = ohlcList.get(i - l).getValue().getLow();
        } else {
          lowestPrice = lowestPrice.min(ohlcList.get(i - l).getValue().getLow());
        }
      }
      if (highestPrice.compareTo(lowestPrice) == 0) {
        rsv = new BigDecimal(0);
      } else {
        rsv = ohlcList.get(i).getValue().getClose().subtract(lowestPrice)
            .divide(highestPrice.subtract(lowestPrice), 4, RoundingMode.HALF_UP)
            .multiply(new BigDecimal(100));
      }
      // 2/3 * 昨日K + 1/3 * 今日rsv
      k = twoThird.multiply(kdjList.get(kdjList.size() - 1).getK())
          .setScale(4, RoundingMode.HALF_UP)
          .add(oneThird.multiply(rsv).setScale(4, RoundingMode.HALF_UP));
      // 2/3 * 昨日D + 1/3 * 今日K
      d = twoThird.multiply(kdjList.get(kdjList.size() - 1).getD())
          .setScale(4, RoundingMode.HALF_UP)
          .add(oneThird.multiply(k).setScale(4, RoundingMode.HALF_UP));
      // 3d-2k
      j = new BigDecimal(3).multiply(d).setScale(4, RoundingMode.HALF_UP)
          .subtract(new BigDecimal(2).multiply(k).setScale(4, RoundingMode.HALF_UP));

      kdjList.add(new KDJDTO(k, d, j));
      kdjTsList.add(
          new TimeSeriesDTO<KDJDTO>(ohlcList.get(i).getDate(), kdjList.get(kdjList.size() - 1)));
    }
    return kdjTsList;
  }

  @SvcMethodInfo(methodDesc = "MACD指標", resultDesc = "MACDDTO TS List")
  public List<TimeSeriesDTO<MACDDTO>> getMACD(int sid) throws Exception {
    // 取出股價
    List<TimeSeriesDTO<BigDecimal>> closeTsList = dtDao.findTsList(sid,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE);

    // 僅取收盤價
    List<String> closeList = closeTsList.stream().map(close -> close.getDecimalValue().toString())
        .collect(Collectors.toList());
    // 計算
    double[][] macd = rTechIndex.getMACD(closeList);
    // assemble
    MACDDTO macdDto;
    List<TimeSeriesDTO<MACDDTO>> macdTsList = new ArrayList<>();
    for (int i = 0; i < closeTsList.size(); i++) {
      macdDto = new MACDDTO();
      if (Double.isNaN(macd[i][0])) {
        macdDto.setMacd(null);
      } else {
        macdDto.setMacd(BigDecimal.valueOf(macd[i][0]));
      }
      if (Double.isNaN(macd[i][1])) {
        macdDto.setDif(null);
      } else {
        macdDto.setDif(BigDecimal.valueOf(macd[i][1]));
      }
      if (Double.isNaN(macd[i][2])) {
        macdDto.setOsc(null);
      } else {
        macdDto.setOsc(BigDecimal.valueOf(macd[i][2]));
      }
      macdTsList.add(new TimeSeriesDTO<MACDDTO>(closeTsList.get(i).getDate(), macdDto));
    }
    return macdTsList;
  }

  @SvcMethodInfo(methodDesc = "布林通道", resultDesc = "[0:上布林tsList, 1: 下布林tsList]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getBoolean(int sid) {
    return getBoolean(sid, 20);
  }

  @SvcMethodInfo(methodDesc = "布林通道", argsDesc = {"",
      "布林MA天數"}, resultDesc = "[0:上布林tsList, 1: 下布林tsList]")
  public List<List<TimeSeriesDTO<BigDecimal>>> getBoolean(int sid, int period) {
    List<List<TimeSeriesDTO<BigDecimal>>> result = new ArrayList<>();
    List<TimeSeriesDTO<BigDecimal>> closeTdList = dtDao.findTsList(sid,
        Const._FIELDID_PRICE_DAILY_TW_CLOSE);
    if (CollectionUtils.isEmpty(closeTdList)) {
      throw new IllegalArgumentException("can't get close td list");
    }
    List<TimeSeriesDTO<BigDecimal>> upBoolean = new ArrayList<>();
    List<TimeSeriesDTO<BigDecimal>> midBoolean = new ArrayList<>();
    List<TimeSeriesDTO<BigDecimal>> dnBoolean = new ArrayList<>();
    if (closeTdList.size() < period) {
      return null;
    }
    // 填補長度
    for (int i = 0; i < period; i++) {
      upBoolean.add(new TimeSeriesDTO<BigDecimal>(closeTdList.get(i).getDate(), null));
      midBoolean.add(new TimeSeriesDTO<BigDecimal>(closeTdList.get(i).getDate(), null));
      dnBoolean.add(new TimeSeriesDTO<BigDecimal>(closeTdList.get(i).getDate(), null));

    }

    BigDecimal mean, sd;
    List<BigDecimal> dataList;
    List<TimeSeriesDTO<BigDecimal>> targetTsList;
    for (int i = period; i <= closeTdList.size(); i++) {
      targetTsList = closeTdList.subList(i - period, i);
      dataList = targetTsList.stream().map(ts -> ts.getDecimalValue()).collect(Collectors.toList());
      mean = staticSvc.getMean(dataList);
      sd = staticSvc.getSd(dataList).multiply(new BigDecimal(2));
      dnBoolean.add(
          new TimeSeriesDTO<BigDecimal>(closeTdList.get(i - 1).getDate(), mean.subtract(sd)));
      midBoolean.add(new TimeSeriesDTO<BigDecimal>(closeTdList.get(i - 1).getDate(), mean));
      upBoolean.add(new TimeSeriesDTO<BigDecimal>(closeTdList.get(i - 1).getDate(), mean.add(sd)));
    }
    result.add(dnBoolean);
    result.add(midBoolean);
    result.add(upBoolean);
    return result;
  }

  public List<TimeSeriesDTO<BigDecimal>> getSmaDeduction(Integer sid, Integer interval,
      Integer fieldId) {
    //get data
    List<TimeSeriesDTO<BigDecimal>> smaTsList = BeanFactory.getBean(this.getClass()).getSMA(sid, interval, fieldId);
    List<TimeSeriesDTO<BigDecimal>> rawDataTsList = dtDao.findTsList(sid, fieldId);

    //convert to deduction list
    int rawDataIdx = 0;
    List<TimeSeriesDTO<BigDecimal>> result = new ArrayList<>();
    for (TimeSeriesDTO<BigDecimal> smaTs : smaTsList) {
      if (smaTs.getValue() == null) {
        continue;
      }
      int keepDeductDays = 0;
      for (int i = rawDataIdx; i < rawDataTsList.size(); i++) {
        if (rawDataTsList.get(i).getDate().compareTo(smaTs.getDate()) == 0) {
          rawDataIdx = i;
          if (i >= (interval - 1)) {
            for (int j = i - (interval - 1); j <= i; j++) {
              int smaCompareToRawData = smaTs.getDecimalValue()
                  .compareTo(rawDataTsList.get(j).getDecimalValue());
              if (keepDeductDays == 0) {
                keepDeductDays = smaCompareToRawData;
              } else if ((smaCompareToRawData > 0) == (keepDeductDays > 0)) {
                keepDeductDays += smaCompareToRawData;
              } else {
                break;
              }
            }
            result.add(new TimeSeriesDTO<>(smaTs.getDate(), new BigDecimal(keepDeductDays)));
          }
        } else if (rawDataTsList.get(i).getDate().compareTo(smaTs.getDate()) > 0) {
          break;
        }
      }
    }
    return result;
  }
}
