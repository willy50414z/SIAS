package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BatchLog
 * 
 * @author
 */
@Table(name = "CALENDAR")
@IdClass(CalendarPOKey.class)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CalendarPO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "\"DATE\"")
    private Date date;
	@Id
    @Column(name = "\"DATE_TYPE\"")
    private Integer dateType;
}