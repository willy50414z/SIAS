package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import lombok.Data;

/**
 * LPTransLog_TW_TWSE
 * @author 
 */
@Table(name="LP_TRANS_LOG_TW")
@Entity
@IdClass(LPTransLogTwPOKey.class)
@Data
public class LPTransLogTwPO implements Serializable {
	/**
     * 資料日期
     */
	@Id
	@Column(name = "TRANS_DATE")
    private Date transDate;
    /**
     * 外資非自營商買進股數
     */
	@Column(name = "FI_NS_BUY")
    private Integer fiNsBuy;

    /**
     * 外資非自營商賣出股數
     */
	@Column(name = "FI_NS_SELL")
    private Integer fiNsSell;

    /**
     * 外資非自營商買賣超
     */
	@Formula("FI_NS_BUY - FI_NS_SELL")
    @Column(name = "FI_NS_NET")
    private Integer fiNsNet;

    /**
     * 外資自營商買進股數
     */
    @Column(name = "FI_SELF_BUY")
    private Integer fiSelfBuy;

    /**
     * 外資自營商賣出股數
     */
    @Column(name = "FI_SELF_SELL")
    private Integer fiSelfSell;

    /**
     *	 外資自營商買賣超
     */
    @Formula("FI_SELF_BUY - FI_SELF_SELL")
    @Column(name = "FI_SELF_NET")
    private Integer fiSelfNet;

    /**
     * 外資買進股數
     */
    @Column(name = "FI_BUY")
    private Integer fiBuy;

    /**
     * 外資賣出股數
     */
    @Column(name = "FI_SELL")
    private Integer fiSell;

    /**
     * 外資買賣超
     */
    @Formula("(FI_NS_BUY - FI_NS_SELL) + (FI_SELF_BUY - FI_SELF_SELL)")
    @Column(name = "FI_NET")
    private Integer fiNet;

    /**
     * 投信買進股數
     */
    @Column(name = "IT_BUY")
    private Integer itBuy;

    /**
     * 投信賣出股數
     */
    @Column(name = "IT_SELL")
    private Integer itSell;

    /**
     * 投信買賣超
     */
    @Formula("IT_BUY - IT_SELL")
    @Column(name = "IT_NET")
    private Integer itNet;

    /**
         *  自營商買進股數(避險)
     */
    @Column(name = "DL_HEDGING_BUY")
    private Integer dlHedgingBuy;

    /**
     * 自營商賣出股數(避險)
     */
    @Column(name = "DL_HEDGING_SELL")
    private Integer dlHedgingSell;

    /**
     * 	投信買賣超(避險)
     */
    @Formula("DL_HEDGING_BUY - DL_HEDGING_SELL")
    @Column(name = "DL_HEDGING_NET")
    private Integer dlHedgingNet;

    /**
     * 	自營商買進股數(自行買賣)
     */
    @Column(name = "DL_SELF_BUY")
    private Integer dlSelfBuy;

    /**
     * 自營商賣出股數(自行買賣)
     */
    @Column(name = "DL_SELF_SELL")
    private Integer dlSelfSell;

    /**
     * 自營商買賣超(自行買賣)
     */
    @Formula("DL_SELF_BUY - DL_SELF_SELL")
    @Column(name = "DL_SELF_NET")
    private Integer dlSelfNet;

    /**
     * 自營商買進股數
     */
    @Column(name = "DL_BUY")
    private Integer dlBuy;

    /**
     * 自營商賣出股數
     */
    @Column(name = "DL_SELL")
    private Integer dlSell;

    /**
     * 自營商買賣超
     */
    @Formula("(DL_HEDGING_BUY - DL_HEDGING_SELL) + (DL_SELF_BUY - DL_SELF_SELL)")
    @Column(name = "DL_NET")
    private Integer dlNet;

    /**
     * 三大法人買進股數
     */
    @Formula("(FI_BUY + IT_BUY + DL_BUY)")
    @Column(name = "LP_BUY")
    private Integer lpBuy;

    /**
     * 三大法人賣出股數
     */
    @Formula("(FI_SELL + IT_SELL + DL_SELL)")
    @Column(name = "LP_SELL")
    private Integer lpSell;

    /**
     * 三大法人買賣超
     */
    @Formula("(FI_BUY + IT_BUY + DL_BUY) - (FI_SELL + IT_SELL + DL_SELL)")
    @Column(name = "LP_NET")
    private Integer lpNet;

    private static final long serialVersionUID = 1L;

    /**
     * 標的代號
     */
    @Id
    private Integer sid;
    @Column(name = "FI_HOLDING")
    private BigDecimal fiHolding;
    @Column(name = "FI_BUYABLE")
    private BigDecimal fiBuyable;
    @Column(name = "FI_BUYABLE_MDF_REASON")
    private String fiBuyableMdfReason;
    @Column(name = "IT_HOLDING")
    private BigDecimal itHolding;
    @Column(name = "PUBLISH_COUNT")
    private BigDecimal publishCount;
}