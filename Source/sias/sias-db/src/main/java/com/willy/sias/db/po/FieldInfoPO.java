package com.willy.sias.db.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "FIELD_INFO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FieldInfoPO {
	@Id
	@Column(name = "FIELD_ID")
	private Integer fieldId;
	@Column(name = "FIELD_NAME")
	private String fieldName;
	@Column(name = "TABLE_NAME")
	private String tableName;
	@Column(name = "FIELD_KEY")
	private Integer fieldKey;
	@Column(name = "KEY_COL_NAME")
	private String keyColName;
	@Column(name = "VAL_COL_NAME")
	private String valColName;
	@Column(name = "DATE_COL_NAME")
	private String dateColName;
	@Column(name = "FIX_FUNC")
	private String fixFunc;
	@Column(name = "PROCESS_INFO")
	private String processInfo;
}
