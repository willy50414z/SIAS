package com.willy.sias.db.dto;

import lombok.Data;

@Data
public class FieldInfoProcessInfoDTO {
	private String daoClassName;
	private String methodName;
	private int argCount;
//	public static void main(String[] args) {
//		FieldInfoProcessInfoDTO f = new FieldInfoProcessInfoDTO();
//		f.setDaoClassName("ShiaojiDao");
//		f.setMethodName("getKBar");
//		f.setArgCount(3);
//		Gson gs = new Gson();
//		System.out.print(gs.toJson(f));
//		Method[] mes = ShiaojiDao.class.getMethods();
//		for(Method me : mes) {
//			System.out.print(gs.toJson(me));
//		}
//	}
}
