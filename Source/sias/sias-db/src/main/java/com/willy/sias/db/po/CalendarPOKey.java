package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * CALENDAR
 * @author 
 */
@Getter @Setter
@Table(name="CALENDAR")
public class CalendarPOKey implements Serializable {
    @Column(name = "\"DATE\"")
    private Date date;

    @Column(name = "\"DATE_TYPE\"")
    private Integer dateType;

    private static final long serialVersionUID = 1L;
}