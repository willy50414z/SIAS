package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.PriceDailyTwPO;
import com.willy.sias.db.po.PriceDailyTwPOKey;

@Repository
public interface PriceDailyTwRepository extends JpaRepository<PriceDailyTwPO, PriceDailyTwPOKey> {
	@Modifying
	@Transactional
	@Query("delete from PriceDailyTwPO p" + 
			" where p.dataDate=?1" + 
			"	and p.sid in (select cc.sid " + 
			"		from CompanyCategoryPO cc " + 
			"		where cc.categoryId=?2 " + 
			"		and cc.dataDate = " + 
			"			(select max(dataDate) from CompanyCategoryPO cc " + 
			"			where cc.dataDate<=?1 and cc.categoryId=?2))")
	public void deleteByDateAndCategoryId(Date date, Integer categoryId);
	@Query("select sid from PriceDailyTwPO where dataDate=?1")
	public Set<Integer> findSidByDataDate(Date date);
	public List<PriceDailyTwPO> findBySidIsAndDataDateBetween(Integer sid, Date startDate, Date endDate);
	public List<PriceDailyTwPO> findBySidIsAndDataDateIn(int sid, List<Date> dataDateList);
	public PriceDailyTwPO findFirstBySidIsAndDataDateLessThanEqualOrderByDataDateDesc(int sid, Date dataDate);
	public PriceDailyTwPO findFirstBySidOrderByDataDate(int sid);
}