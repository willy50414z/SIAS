package com.willy.sias.db.po;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * BatchLog
 * 
 * @author
 */
@Table(name = "BACK_TEST_LOG_H")
@Entity
@Data
public class BackTestLogHPO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "\"TEST_ID\"")
	private Integer testId;
	@Column(name = "\"TEST_DESC\"")
	private String testDesc;
	@Column(name = "\"ERR_MSG\"")
	private String errMsg;
	@Column(name = "\"STRATEGY_FILE_PATH\"")
	private String strategyFilePath;
}