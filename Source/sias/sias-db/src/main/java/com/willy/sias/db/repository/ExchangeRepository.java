package com.willy.sias.db.repository;

import com.willy.sias.db.po.ExchangePO;
import com.willy.sias.db.po.ExchangePOKey;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ExchangeRepository extends JpaRepository<ExchangePO, ExchangePOKey>
{
}
