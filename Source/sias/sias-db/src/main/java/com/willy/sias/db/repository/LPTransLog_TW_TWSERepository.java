package com.willy.sias.db.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.po.LPTransLogTwPOKey;

@Repository
public interface LPTransLog_TW_TWSERepository extends JpaRepository<LPTransLogTwPO, LPTransLogTwPOKey> {
	public List<LPTransLogTwPO> findBySidIsAndTransDateBetween(Integer sid, Date startDate, Date endDate);
	public List<LPTransLogTwPO> findByTransDateIs(Date transDate);
	public List<LPTransLogTwPO> findBySidIsOrderByTransDate(Integer sid);
	public LPTransLogTwPO findFirstBySidIsAndItHoldingNotNullOrderByTransDateDesc(Integer sid);
	@Query("SELECT distinct sid FROM LPTransLogTwPO WHERE itHolding IS NULL")
	public List<Integer> findDistinctSIDByItHoldingIsNull();
}