package com.willy.sias.db.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
@Configuration
public class JDBCDSConfig {
	@Bean
	public JdbcTemplate getJdbcTemplate(@Autowired @Qualifier("JDBCDS") DataSource ds) {
		return new JdbcTemplate(ds);
	}
	@Bean("JDBCTransactionManager")
	public DataSourceTransactionManager transactionManager(@Autowired @Qualifier("JDBCDS") DataSource ds) {
		DataSourceTransactionManager transactionManager = new DataSourceTransactionManager();
		transactionManager.setDataSource(ds);
		return transactionManager;
	}
}
