package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="MARGIN_RATIO_TW")
@Entity
@Data
@IdClass(MarginRatioTwPOKey.class)
@NoArgsConstructor
@AllArgsConstructor
public class MarginRatioTwPO implements Serializable {
  @Id
  @Column(name = "SID")
  private Integer sid;

  @Id
  @Column(name = "DATA_DATE")
  private Date dataDate;

  @Column(name = "MARGIN_RATIO")
  private BigDecimal marginRatio;
}
