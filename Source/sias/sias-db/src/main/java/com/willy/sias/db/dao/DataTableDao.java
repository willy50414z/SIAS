package com.willy.sias.db.dao;

import com.google.gson.Gson;
import com.willy.file.serialize.WSerializer;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.FieldInfoPO;
import com.willy.sias.db.repository.FieldInfoRepository;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.TypeUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.reflect.WReflect;
import com.willy.util.string.WRegex;
import com.willy.util.string.WString;
import com.willy.util.type.WType;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;

@Repository
public class DataTableDao {

  @Autowired
  private FieldInfoRepository fiRepo;
  @Autowired
  private WJDBC jdbcRepo;
  @Autowired
  private TypeUtil typeUtil;
  @Autowired
  private Gson gs;
  @Autowired
  private WSerializer serializer;
  @Autowired
  private Jedis jedis;

  private DataTableDao dtDao;//inject by DataTableDao.getDataTableDao(), for cache


  @Cacheable(value = "sias:DataTableDao", key = "{#root.methodName, #sid, #fieldKeyList, #cl.name}")
  public <T> List<TimeSeriesDTO<T>> findTsList(Integer sid, List<Integer> fieldKeyList, Class<?> cl,
      Object... params) {
    // 撈出要查詢的欄位
    List<FieldInfoPO> fieldInfoList = fiRepo.findAllById(fieldKeyList);
    if (fieldInfoList.size() == 1 && fieldInfoList.get(0).getProcessInfo() != null) {
      Object[] methodParams = null;
      if (sid != null) {
        methodParams = new Object[params.length + 1];
        methodParams[0] = sid;
        for (int i = 0; i < params.length; i++) {
          methodParams[i + 1] = params[i];
        }
      }
      return this.findTsListByProcessInfo(fieldInfoList.get(0).getProcessInfo(),
          methodParams == null ? params : methodParams);
    }

    //while query by funcString, but no processInfo. set first parameter as sid
    if (sid == null && params.length > 0 && params[0] != null) {
      sid = Integer.parseInt(params[0].toString());
    }

    // 確認同屬一個Table
    if (fieldInfoList.stream().map(FieldInfoPO::getTableName).distinct().count() != 1) {
      throw new IllegalArgumentException(
          "Query column saved in different tables sid[" + sid + "]fieldKeyList[" + WString.toString(
              fieldKeyList) + "]");
    }
    FieldInfoPO fieldInfo = fieldInfoList.get(0);
    String dateColumnName = fieldInfo.getDateColName();
    String tableName = fieldInfo.getTableName();
    String keyColumnName = fieldInfo.getKeyColName();
    List<String> valColNameList = fieldInfoList.stream().map(FieldInfoPO::getValColName)
        .collect(Collectors.toList());
    String valColumns = StringUtils.join(valColNameList, ",");

    // 串SQL
    StringBuilder sql = new StringBuilder("select ");
    sql.append(dateColumnName).append(",").append(valColumns).append(" from ").append(tableName)
        .append(" where [SID]=").append(sid);

    if (keyColumnName != null) {
      // KVTable
      sql.append(" and ").append(keyColumnName).append(" in (");
      fieldInfoList.forEach(fi -> sql.append(fi.getFieldKey()).append(","));
      sql.setLength(sql.length() - 1);
      sql.append(")");
    }
    sql.append(" order by 1");
    WLog.debug(sql.toString());
    return jdbcRepo.queryForBeanList(sql.toString(), (resultSet, i) -> {
      try {
        return new TimeSeriesDTO(resultSet.getDate(1),
            fieldKeyList.size() == 1 ? resultSet.getObject(2) == null ? null
                : WType.objToSpecificType(resultSet.getObject(2), cl)
                : typeUtil.convertResultSetToBean(resultSet, cl));
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    });
  }

  public <T> List<TimeSeriesDTO<T>> findTsList(int sid, List<Integer> fieldKeyList, Class<T> cl,
      Date startDate, Date endDate) {
    List<TimeSeriesDTO<T>> tsList = BeanFactory.getBean(this.getClass())
        .findTsList(sid, fieldKeyList, cl);
    return tsList.stream().filter(ts -> WDate.between(ts.getDate(), startDate, endDate))
        .collect(Collectors.toList());
  }

  public <T> List<TimeSeriesDTO<T>> findTsList(Integer sid, Integer fieldKey, Class<T> cl,
      Date startDate, Date endDate) {
    return this.findTsList(sid, Collections.singletonList(fieldKey), cl, startDate, endDate);
  }

  public <T> List<TimeSeriesDTO<T>> findTsList(int sid, List<Integer> fieldKeyList, Class<T> cl,
      Date date) {
    return BeanFactory.getBean(this.getClass()).findTsList(sid, fieldKeyList, cl, date, date);
  }

  public <T> List<TimeSeriesDTO<T>> findTsList(int sid, Integer fieldKey, Class<T> cl, Date date) {
    return BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), cl, date, date);
  }

  public <T> List<TimeSeriesDTO<T>> findTsList(Integer sid, Integer fieldKey, Class<T> cl) {
    return BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), cl);
  }

  public List<TimeSeriesDTO<BigDecimal>> findTsList(Integer sid, Integer fieldKey) {
    return BeanFactory.getBean(this.getClass()).findTsList(sid, fieldKey, BigDecimal.class);
  }

  public List<TimeSeriesDTO<BigDecimal>> findTsList(Integer sid, Integer fieldKey,
      Object... params) {
    return BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), BigDecimal.class, params);
  }

  public List<TimeSeriesDTO<BigDecimal>> findTsList(int sid, int fieldKey, Date startDate,
      Date endDate) {
    return BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), BigDecimal.class, startDate, endDate);
  }

  public List<TimeSeriesDTO<BigDecimal>> findTsList(int sid, int fieldKey, Date date) {
    return BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), BigDecimal.class, date, date);
  }

  public <T> List<TimeSeriesDTO<T>> findRecentTsList(int sid, List<Integer> fieldKeyList,
      Class<T> cl, Date baseDate, int interval) {
    //get whole data
    List<TimeSeriesDTO<T>> tsList = BeanFactory.getBean(this.getClass())
        .findTsList(sid, fieldKeyList, cl);
    return retriveRecentData(tsList, baseDate, interval);
  }

  public <T> List<TimeSeriesDTO<T>> findRecentTsList(int sid, int fieldKey, Class<T> cl,
      Date baseDate, int interval) {
    //get whole data
    List<TimeSeriesDTO<T>> tsList = BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), cl);
    return retriveRecentData(tsList, baseDate, interval);
  }

  public <T> List<TimeSeriesDTO<T>> findRecentTsList(int sid, int fieldKey, Date baseDate,
      int interval) {
    //get whole data
    List<TimeSeriesDTO<T>> tsList = BeanFactory.getBean(this.getClass())
        .findTsList(sid, Collections.singletonList(fieldKey), BigDecimal.class);
    return retriveRecentData(tsList, baseDate, interval);
  }

  private <T> List<TimeSeriesDTO<T>> retriveRecentData(List<TimeSeriesDTO<T>> tsList, Date baseDate,
      int interval) {
    //get data in specific period
    tsList = tsList.stream().filter(ts -> interval > 0 ? WDate.afterOrEquals(ts.getDate(), baseDate)
        : WDate.beforeOrEquals(ts.getDate(), baseDate)).collect(Collectors.toList());
    tsList.sort(
        interval > 0 ? Comparator.comparing((TimeSeriesDTO<T> o) -> o.getDate()) : (o1, o2) -> {
          return o1.getDate().compareTo(o2.getDate()) * -1;//降冪
        });
    return tsList.subList(0, Math.min(tsList.size(), Math.abs(interval)));
  }

  public Set<Date> findDateSetByFieldIdAndSidList(int fieldId, List<Integer> sidList) {
    FieldInfoPO fieldInfo = fiRepo.findById(fieldId).orElse(null);
    if (fieldInfo == null || fieldInfo.getDateColName() == null) {
      throw new NullPointerException("can't find DateColName by fieldId[" + fieldId + "]");
    }

    StringBuffer tmpSidSb = new StringBuffer();
    Set<Date> tradeDateSet = new HashSet<>();
    for (int i = 0; i < (sidList.size() / 1001 + 1); i++) {
      tmpSidSb.setLength(0);
      sidList.subList(1000 * i, Math.min(1000 * (i + 1), sidList.size()))
          .forEach(sid -> tmpSidSb.append(sid).append(","));
      if (tmpSidSb.length() == 0) {
        return new HashSet<>();
      }
      tmpSidSb.setLength(tmpSidSb.length() - 1);
      tradeDateSet.addAll(jdbcRepo.queryForBeanSet(
          "select " + fieldInfo.getDateColName() + " from " + fieldInfo.getTableName()
              + " where sid in (" + tmpSidSb + ") and " + fieldInfo.getValColName() + ">0 group by "
              + fieldInfo.getDateColName() + "  order by " + fieldInfo.getDateColName(),
          (rs, rowNum) -> {
            // TODO Auto-generated method stub
            return rs.getDate(1);
          }));
    }
    return tradeDateSet;
  }

  @Cacheable(value = "sias:DataTableDao", key = "{#root.methodName, #baseDate, #interval}")
  public List<Date> findBusDate(Date baseDate, Integer interval) {
    String sql = "select txDate.DATE from (select top " + Math.abs(interval)
        + " c.DATE from CALENDAR c where c.DATE " + (interval > 0 ? ">=" : "<=")
        + " ? and DATE_TYPE = ? " + " group by c.DATE order by c.DATE " + (interval > 0 ? ""
        : "desc") + ") txDate order by 1";
    return jdbcRepo.queryForBeanList(sql, (rs, rowNum) -> rs.getDate(1), baseDate,
        Const._CFG_ID_CALENDAR_STOCK_BUS_DATE);
  }

  @Cacheable(value = "sias:DataTableDao", key = "{#root.methodName, #sid}")
  public List<Date> findTradeDateListBySidOrderByDate(int sid) {
    return jdbcRepo.queryForBeanList(
        "select DATA_DATE from PRICE_DAILY_TW p where p.SID=? group by DATA_DATE order by 1",
        (rs, rowNum) -> {
          // TODO Auto-generated method stub
          return rs.getDate(1);
        }, sid);
  }

  public List<Date> findTradeDateList(int sid, Date baseDate, Integer interval) {
    List<Date> txnDateAr = BeanFactory.getBean(this.getClass())
        .findTradeDateListBySidOrderByDate(sid);
    txnDateAr = txnDateAr.stream().filter(date -> interval > 0 ? WDate.afterOrEquals(date, baseDate)
        : WDate.beforeOrEquals(date, baseDate)).collect(Collectors.toList());
    txnDateAr.sort(interval > 0 ? Const._DEFAULT_COMPARATOR_DATE : Const._COMPARATOR_DATE_DESC);
    return txnDateAr.subList(0, Math.abs(interval));
  }

  public List<TimeSeriesDTO<BigDecimal>> findItHoldingRatioByDate(int sid, Date startDate,
      Date endDate) {
    return findItHoldingRatioByItHoldingRatioAndDate(sid, null, null, startDate, endDate);
  }

  public List<TimeSeriesDTO<BigDecimal>> findItNetRatioByDate(int sid, Date startDate,
      Date endDate) {
    return findItNetRatioByItHoldingRatioAndDate(sid, null, null, startDate, endDate);
  }

  public List<TimeSeriesDTO<BigDecimal>> findItHoldingRatioByItHoldingRatio(int sid,
      BigDecimal minItHoldingRatio, BigDecimal maxItHoldingRatio) {
    return findItHoldingRatioByItHoldingRatioAndDate(sid, minItHoldingRatio, maxItHoldingRatio,
        null, null);
  }

  public List<TimeSeriesDTO<BigDecimal>> findItNetRatioByItHoldingRatio(int sid,
      BigDecimal minItHoldingRatio, BigDecimal maxItHoldingRatio) {
    return findItNetRatioByItHoldingRatioAndDate(sid, minItHoldingRatio, maxItHoldingRatio, null,
        null);
  }

  public List<TimeSeriesDTO<BigDecimal>> findItHoldingRatioByItHoldingRatioAndDate(int sid,
      BigDecimal minItHoldingRatio, BigDecimal maxItHoldingRatio, Date startDate, Date endDate) {
    return findIssuedSharesRatioByFieldIdAndRatioAndDate(sid, Const._FIELDID_LP_IT_HOLD,
        minItHoldingRatio, maxItHoldingRatio, startDate, endDate);
  }

  public List<TimeSeriesDTO<BigDecimal>> findItNetRatioByItHoldingRatioAndDate(int sid,
      BigDecimal minItHoldingRatio, BigDecimal maxItHoldingRatio, Date startDate, Date endDate) {
    return findIssuedSharesRatioByFieldIdAndRatioAndDate(sid, Const._FIELDID_LP_IT_NET,
        minItHoldingRatio, maxItHoldingRatio, startDate, endDate);
  }

  public List<TimeSeriesDTO<BigDecimal>> findIssuedSharesRatioByFieldIdAndRatioAndDate(int sid,
      int fieldId, BigDecimal minItHoldingRatio, BigDecimal maxItHoldingRatio, Date startDate,
      Date endDate) {
    List<TimeSeriesDTO<BigDecimal>> tsList = findIssuedSharesRatioByFieldIdAndRatioAndDate(sid,
        fieldId);
    return tsList.stream().filter(
            ts -> (minItHoldingRatio != null && ts.getDecimalValue().compareTo(minItHoldingRatio) >= 0)
                && (maxItHoldingRatio != null && ts.getDecimalValue().compareTo(maxItHoldingRatio) >= 0)
                && (startDate != null && WDate.afterOrEquals(ts.getDate(), startDate)) && (
                endDate != null && WDate.afterOrEquals(ts.getDate(), endDate)))
        .collect(Collectors.toList());
  }


  @Cacheable(value = "sias:DataTableDao", key = "{#root.methodName, #sid, #fieldId}")
  public List<TimeSeriesDTO<BigDecimal>> findIssuedSharesRatioByFieldIdAndRatioAndDate(int sid,
      int fieldId) {
    FieldInfoPO fi = fiRepo.findById(fieldId).orElse(null);
    if (fi == null) {
      throw new NullPointerException("Can't get field_info, fieldId[" + fieldId + "]");
    }
    List<Object> parList = new ArrayList<>();
    parList.add(sid);
    String sql = "SELECT *  " + "FROM (SELECT lp." + fi.getDateColName() + " AS trans_date "
        + "        , lp." + fi.getValColName() + "/(SELECT TOP 1 (fr1.fr_Value * 1000) / ( "
        + "                    SELECT fr2.fr_Value " + "                    FROM FR_IS_TW fr2 "
        + "                    WHERE fr2.fr_Key = " + Const._CFG_ID_FR_IS_EPS
        + " AND fr2.fr_Date=fr1.fr_Date AND fr1.sid = fr2.sid) AS shares "
        + "                    FROM FR_IS_TW fr1 " + "                    WHERE fr1.fr_Key = "
        + Const._CFG_ID_FR_IS_NET_PROFIT_AFTER_TAX
        + " AND fr1.sid=lp.sid AND fr1.fr_Date <= CONVERT(VARCHAR, lp.trans_date, 112) "
        + "                    ORDER BY fr1.fr_Date DESC) AS itHoldRatio " + "    FROM "
        + fi.getTableName() + " lp " + "    WHERE lp.sid=? ) ir " + "WHERE itHoldRatio IS NOT null"
        + " order by 1";
    Object[] parAr = new Object[parList.size()];
    parList.toArray(parAr);
    return jdbcRepo.queryForBeanList(sql, (rs, rowNum) -> {
      // TODO Auto-generated method stub
      return new TimeSeriesDTO<>(rs.getDate(1), new BigDecimal(rs.getString(2)));
    }, parAr);
  }

  public List<TimeSeriesDTO<BigDecimal>> findTsListByProcStr(String uuid, String procBlock)
      throws InvocationTargetException, IllegalAccessException, IOException, ClassNotFoundException {
    //prepare data
    Set<String> procIdSet = WRegex.getMatchedStrSet(procBlock, "[{][0-9]{0,5}[}]");
    FieldInfoPO fi;
    if(procIdSet.size() == 1) {
      //get field info
      String fieldId = procIdSet.iterator().next();
      fi = fiRepo.findById(Integer.parseInt(fieldId.substring(1, fieldId.length()-1))).orElse(null);
      if(fi == null) {
        throw new IllegalArgumentException("Can't found FieldInfoPO by FieldId["+fieldId+"]");
      }
    } else {
      throw new IllegalArgumentException("Can't found method id in procBlock["+procBlock+"]");
    }

    if(fi.getProcessInfo() == null) {
      String[] blockStrArgs = procBlock.substring(procBlock.indexOf("(") + 1, procBlock.indexOf(")")).split(",");
      return this.findTsList(Integer.valueOf(blockStrArgs[0]), fi.getFieldId());
    }

    String[] procInfos = fi.getProcessInfo().split("#");
    String daoBeanId = procInfos[0];
    String methodName = procInfos[1];
    String[] blockStrArgs = procBlock.substring(procBlock.indexOf("(") + 1, procBlock.indexOf(")")).split(",");
    Object[] blockArgs = new Object[blockStrArgs.length];
    if(blockArgs.length > 7) {
      throw new IllegalArgumentException("only provide the method less than 7 args, please adjust program for requirement");
    }
    for (int i = 0; i < blockArgs.length; i++) {
      if (WRegex.match(blockStrArgs[i], WRegex._REGEX_PARAMETER_WRAPED)) {
        Object arg = serializer.unSerializeByString(jedis.hget(Const.PROC_BLOCK_HKEY + ":" + uuid,
            blockStrArgs[i].substring(2, blockStrArgs[i].length() - 1)));
        blockArgs[i] = arg;
      } else {
        blockArgs[i] = blockStrArgs[i].trim();
      }
    }
    Object daoObj = BeanFactory.getBean(daoBeanId);
    Method method = Arrays.stream(daoObj.getClass().getMethods()).filter(
        me -> me.getName().equals(methodName)
            && me.getParameterTypes().length == blockArgs.length && (me.getReturnType()
            .equals(List.class))).findAny().orElse(null);
    if (method == null) {
      throw new NullPointerException(
          "can't get method, daoBeanId[" + daoBeanId + "]methodName[" + methodName + "]argsCount["
              + blockArgs.length + "]");
    }

    return (List<TimeSeriesDTO<BigDecimal>>) WReflect.invoke(daoObj, method, blockArgs);
  }

  private <T> List<TimeSeriesDTO<T>> findTsListByProcessBlocks(String processBlockStr) {
    Set<String> blocks = StringUtil.getProccessBlock(processBlockStr);

    List<Object> paramList = new ArrayList<>();

    //loop every block
    for (String block : blocks) {
      //prepare data
      String fieldId = block.substring(1, block.indexOf("("));
      String[] blockParams = block.substring(block.indexOf("(") + 1, block.indexOf(")")).split(",");
      Object[] paramAr = new Object[blockParams.length];

      //assemble parameter of block process
      for (int i = 0; i < blockParams.length; i++) {
        blockParams[i] = blockParams[i].trim();
        if (WRegex.match(blockParams[i], WRegex._REGEX_PARAMETER_WRAPED)) {
          paramAr[i] = paramList.get(
              Integer.parseInt(blockParams[i].substring(2, blockParams[i].indexOf("}"))));
        } else {
          paramAr[i] =
              WString.isNumeric(blockParams[i]) ? Integer.valueOf(blockParams[i]) : blockParams[i];
        }
      }
      paramList.add(
          BeanFactory.getBean(this.getClass()).findTsList(null, Integer.valueOf(fieldId), paramAr));
    }
    return (List<TimeSeriesDTO<T>>) paramList.get(paramList.size() - 1);
  }

  public <T> List<TimeSeriesDTO<T>> findTsListByProcessInfo(String funcStr, Object... params) {
    try {
      if (StringUtil.getProccessBlock(funcStr).size() > 0) {
        //funcStr含${xxx}參數，從params[0]取出去取代掉
        //dtDao.findTsListByProcessInfo("[92([46(${sid})],[93(${sid})])]", "{\"sid\":\"494\"}")
        Set<String> keySet =StringUtil.retrieveParamKeySet(funcStr);
        if(keySet.size() > 0 && params.length > 0 && params[0] != null) {
          funcStr = WString.parseParamsToStrByJson(funcStr, new JSONObject(params[0].toString()));
        }
        return findTsListByProcessBlocks(funcStr);
      }

      //beanId#methodName#arg1,arg2,arg3
      String[] processInfoAr = funcStr.split("#");
      String daoBeanName = processInfoAr[0];
      String methodName = processInfoAr[1];
      String[] processArgsName = processInfoAr[2].split(",");

      //getMethod
      Object daoObj = BeanFactory.getBean(daoBeanName);
      Method method = Arrays.stream(daoObj.getClass().getMethods()).filter(
          me -> me.getName().equals(methodName)
              && me.getParameterTypes().length == processArgsName.length && (me.getReturnType()
              .equals(List.class))).findAny().orElse(null);
      if (method == null) {
        throw new NullPointerException(
            "can't get method, beanId[" + daoBeanName + "]methodName[" + methodName + "]argsCount["
                + processArgsName.length + "]");
      }

      //invoke method
      List<TimeSeriesDTO<T>> result;
      switch (processArgsName.length) {
        case 0:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj);
          break;
        case 1:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0]);
          break;
        case 2:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1]);
          break;
        case 3:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1], params[2]);
          break;
        case 4:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1], params[2],
              params[3]);
          break;
        case 5:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1], params[2],
              params[3], params[4]);
          break;
        case 6:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1], params[2],
              params[3], params[4], params[5]);
          break;
        case 7:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params[0], params[1], params[2],
              params[3], params[4], params[5], params[6]);
          break;
        default:
          result = (List<TimeSeriesDTO<T>>) method.invoke(daoObj, params);
          break;
      }
      return result;
    } catch (Exception e) {
      WLog.error(e);
      return null;
    }
  }
}
