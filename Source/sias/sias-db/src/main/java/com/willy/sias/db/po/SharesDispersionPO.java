package com.willy.sias.db.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

@Table(name="SHARES_DISPERSION")
@Entity
@Data
@IdClass(SharesDispersionPOKey.class)
public class SharesDispersionPO {
	/**
     * 標的代號
     */
    @Id
    @Column(name = "SID")
    private Integer sid;
    /**
     *	 資料發布日期
     */
    @Id
    @Column(name = "DATA_DATE")
    private Date dataDate;
    /**
     * 集保總張數
     */
    @Column(name = "\"SHARES_COUNT\"")
    private BigDecimal sharesCount;

    /**
     * 總股東人數
     */
    @Column(name = "SHAREHOLDER_COUNT")
    private BigDecimal shareholderCount;

    /**
     * 平均張數/人
     */
    @Column(name = "AVG_SHARES_EACH_PERSON")
    private BigDecimal avgSharesEachPerson;

    /**
     * >400張大股東持有張數
     */
    @Column(name = "\"HOLDING_COUNT_1\"")
    private BigDecimal holdingCount1;
    
    /**
     * >400張大股東人數
     */
    @Column(name = "\"PERSON_COUNT_1\"")
    private BigDecimal personCount1;
    
    /**
     * 400~600張人數
     */
    @Column(name = "\"PERSON_COUNT_2\"")
    private BigDecimal personCount2;
    
    /**
     * 600~800張人數
     */
    @Column(name = "\"PERSON_COUNT_3\"")
    private BigDecimal personCount3;
    
    /**
     * 800~1000張人數
     */
    @Column(name = "\"PERSON_COUNT_4\"")
    private BigDecimal personCount4;
    
    /**
     * >1000張人數
     */
    @Column(name = "\"PERSON_COUNT_5\"")
    private BigDecimal personCount5;
    /**
     * 	資料日期
     */
    @Column(name = "BATCH_DATE")
    private Date batchDate;
}
