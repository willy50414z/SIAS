from flask import Flask, request
import shioaji as sj
import pandas as pd

api = None
app = Flask(__name__)

#確認有登入shiaoji
def checkIsLogin():
    if api is None:
        return "Shiaoji is not logined, please request login with person_id & passwd"
    else:
        return ""

#逐筆成交資料
#http://127.0.0.1:5000/ticks?stockCode=2330&date=2021-08-25
@app.route('/ticks', methods=['GET'])
def ticks():
    if bool(checkIsLogin()):
        return checkIsLogin()
    try:
        args = request.args
        ticks = api.ticks(api.Contracts.Stocks[args.get('stockCode')], args.get('date'))
        df = pd.DataFrame({**ticks})
        df.ts = pd.to_datetime(df.ts)
    except Exception:
        return "{\"ts\":{}}"
    return df.to_json()

#每分鐘成交筆數
#http://127.0.0.1:5000/kBar?stockCode=2330&startDate=2021-05-25&endDate=2021-08-08
@app.route('/kBar', methods=['GET'])
def kBar():
    if bool(checkIsLogin()):
        return checkIsLogin()
    try:
        args = request.args
        kbars = api.kbars(api.Contracts.Stocks[args.get('stockCode')], start=args.get('startDate'), end=args.get('endDate'))
        df = pd.DataFrame({**kbars})
        df.ts = pd.to_datetime(df.ts)
    except Exception:
        return "{\"ts\":{}}"
    return df.to_json()

#登入
@app.route('/login', methods=['POST'])
def login():
    #未連線才做連線，已連線直接return
    if bool(checkIsLogin()):
        args = request.values
        person_id=args.get('person_id')
        passwd=args.get('passwd')

        # 建立 Shioaji api 物件
        global api
        api = sj.Shioaji()

        # 登入帳號
        api.login(
            person_id=person_id,
            passwd=passwd,
            contracts_cb=lambda security_type: print(f"{repr(security_type)} fetch done.")
        )
        return "{status: \"200\"}"
    return "{status: \"210\"}"

if __name__ == '__main__':
    app.run(host='127.0.0.1')

