package com.willy.sias.backtest.strategy;

import com.willy.sias.backtest.dto.DividendFillCDivDTO;
import com.willy.sias.backtest.dto.TradePlanDTO;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.db.dto.OHLCDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;

@Service
@Scope("prototype")
public class S4 extends TradeStrategy {
	private final String spliutCjar = ";";
	@Override
	public void collectTradePlanList() {
		List<DividendFillCDivDTO> tradeDivDtoList = new ArrayList<>();
		try {
			// 撈出除息資料
			List<DividendFillCDivDTO> divDtoList = bs.getDaysOfFillCashDiv(this.getSid(), -1).stream()
					.filter(div -> div.getDivPo().getCDiv() != null && div.getDivPo().getExCDivDate() != null)
					.collect(Collectors.toList());
			// 至少要有5年的股利發放紀錄
			if (divDtoList.stream().map(div -> WDate.getDateInfo(div.getDivPo().getDataDate(), Calendar.YEAR))
					.collect(Collectors.toSet()).size() < 5) {
				return;
			}

			//將股利紀錄By年份group
			List<Integer> fastFillDivYearList = new ArrayList<>();
			List<String> divInfoList = new ArrayList<>();
			for (int year : divDtoList.stream()
					.map(ddiv -> WDate.getDateInfo(ddiv.getDivPo().getExCDivDate(), Calendar.YEAR))
					.collect(Collectors.toList())) {
				Stream<DividendFillCDivDTO> thisYearDivStream = divDtoList.stream()
				.filter(div -> WDate.getDateInfo(div.getDivPo().getExCDivDate(), Calendar.YEAR) == year);
				double dd = thisYearDivStream.mapToInt(div -> div.getFillDays()).summaryStatistics().getAverage();
				divInfoList.add(year + spliutCjar + dd);
			}
			for (int i = 4; i < divInfoList.size(); i++) {
				int lessThen30 = 0;
				BigDecimal sumFillDays = new BigDecimal(0);
				for (int j = i; j >= 0; j--) {
					
					try{
						sumFillDays = sumFillDays.add(new BigDecimal(divInfoList.get(i).split(spliutCjar)[1]));
					}catch(Exception e) {
						System.out.println(divInfoList.get(i));
					}
					
					if (new BigDecimal(divInfoList.get(i).split(spliutCjar)[1]).compareTo(new BigDecimal(30)) <= 0) {
						lessThen30++;
					}
				}
				if (sumFillDays.divide(new BigDecimal(5)).compareTo(new BigDecimal(30)) < 0 || lessThen30 > 3) {
					fastFillDivYearList.add(Integer.valueOf(divInfoList.get(i).split(spliutCjar)[0]));
				}
			}
			
			
			// 近5次填息時間<30天機率>80%
			tradeDivDtoList = new ArrayList<>();
			for (int i = 4; i < divDtoList.size(); i++) {
				int less30Count = 0;
				// 統計過去填息天數<30天機率
				for (int j = i; j >= 0; j--) {
					less30Count += divDtoList.get(j).getFillDays().compareTo(100) > 0 ? 0 : 1;
				}
				// 過去填息天數<30天機率 > 80%加入tradeDivDtoList
				if (new BigDecimal(less30Count).divide(new BigDecimal(i + 1), 2, RoundingMode.HALF_UP)
						.compareTo(new BigDecimal(0.8)) > 0) {
					tradeDivDtoList.add(divDtoList.get(i));
				}
			}
			
			//第一季EPS YOY成長
			List<TimeSeriesDTO<BigDecimal>> epsTsList = dtDao.findTsList(this.getSid(), Const._FIELDID_EPS);
			epsTsList = epsTsList.stream().filter( eps -> WType.dateToStr(eps.getDate(), "MMdd").equals("0515")).collect(Collectors.toList());
			//第一季EPS YOY>0年份List
			List<Integer> s1EpsYOYLt0YearList = statSvc.getGrowth(epsTsList, 1).stream()
					.filter(td -> td != null && td.getDecimalValue() != null && td.getDecimalValue().compareTo(Const._BIGDECIMAL_0) > 0)
					.map(td -> WDate.getDateInfo(td.getDate(), Calendar.YEAR)).collect(Collectors.toList());
			
			//五月營收YOY成長
			List<TimeSeriesDTO<BigDecimal>> revYOYTsList = dtDao.findTsList(this.getSid(), Const._FIELDID_MONTH_REVENUE_YOY);
			//五月營收YOY>0年份List
			List<Integer> m5RevYoyLt0YearList = revYOYTsList.stream().filter(rev -> WDate.getDateInfo(rev.getDate(), Calendar.MONTH) == 5
					&& rev.getDecimalValue().compareTo(Const._BIGDECIMAL_0) > 0).map(rev -> WDate.getDateInfo(rev.getDate(), Calendar.YEAR)).collect(Collectors.toList());
			
			List<TimeSeriesDTO<BigDecimal>> ma20TsList = tiSvc.getSMA(dtDao.findTsList(this.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 20);
			List<TimeSeriesDTO<BigDecimal>> ma60TsList = tiSvc.getSMA(dtDao.findTsList(this.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 60);

			//篩選出 除息日 && 測試期間 && 第一季EPS YOY > 0
			List<Date> tradeDateList = this.getTradeDateList().stream()
					.filter(td -> s1EpsYOYLt0YearList
									.contains(WDate.getDateInfo(td, Calendar.YEAR))
							&& m5RevYoyLt0YearList
									.contains(WDate.getDateInfo(td, Calendar.YEAR))
//							&& fastFillDivYearList
//									.contains(WDate.getDateInfo(td.getDivPo().getExCDivDate(), Calendar.YEAR))
									).collect(Collectors.toList());
			
			for (Date tradeDate : tradeDateList) {
				// 將需要做交易的日期做交易紀錄
				DividendFillCDivDTO tradeDivDto = tradeDivDtoList.stream()
						.filter(td -> td.getDivPo().getExCDivDate().compareTo(tradeDate) == 0).findFirst().orElse(null);
				if (tradeDivDto != null) {
					if (tradeDivDto.getDivPo().getCDiv() == null
							|| tradeDivDto.getDivPo().getCDiv().compareTo(Const._BIGDECIMAL_0) == 0) {
						continue;
					}
					BigDecimal openPrice = this.tu.getPrice(tradeDivDto.getDivPo().getSid(), PriceType.OPEN,
							tradeDivDto.getDivPo().getExCDivDate());
					
//					//MA20 > MA60才做買進
//					TimeSeriesDTO<BigDecimal> openMa60 = ma60TsList.stream().filter(ma60 -> ma60.getDate().compareTo(tradeDivDto.getDivPo().getExCDivDate()) == 0).findFirst().orElse(null);
//					TimeSeriesDTO<BigDecimal> openMa20 = ma20TsList.stream().filter(ma20 -> ma20.getDate().compareTo(tradeDivDto.getDivPo().getExCDivDate()) == 0).findFirst().orElse(null);
//					if(openMa60 != null && openMa20 != null && openMa20.getValue().compareTo(openMa60.getValue())<0) {
//						continue;
//					}
					
					// 除權息當天開盤買
					this.addTradePlanList(new TradePlanDTO(this.getSid(), tradeDivDto.getDivPo().getExCDivDate(), TradeType.buy,
							1000, openPrice));

					/**
					 * 往後看90天 漲破填息價賣 跌破開盤價5%賣出
					 **/
 					// 填息價
					BigDecimal fillDivPrice = openPrice.add(tradeDivDto.getDivPo().getCDiv());
					// 未來90天價格
					List<TimeSeriesDTO<OHLCDTO>> tsList = dtDao.findRecentTsList(tradeDivDto.getDivPo().getSid(),
							Const._FIELDID_LIST_OHLC, OHLCDTO.class, tradeDivDto.getDivPo().getExCDivDate(), 90);
					//未來90天MA20/60死亡交叉日
					Date deadCrossDate = null;
					List<TimeSeriesDTO<BigDecimal>> partMa60 = ma60TsList.stream().filter(ma60 -> ma60.getDate().compareTo(tradeDivDto.getDivPo().getExCDivDate())>=0).collect(Collectors.toList());
					List<TimeSeriesDTO<BigDecimal>> partMa20 = ma20TsList.stream().filter(ma20 -> ma20.getDate().compareTo(tradeDivDto.getDivPo().getExCDivDate())>=0).collect(Collectors.toList());
					for(int i=0;i<Math.min(90, Math.min(partMa60.size(), partMa20.size()));i++) {
						if(partMa20.get(i).getValue().compareTo(partMa60.get(i).getValue())<0) {
							deadCrossDate = partMa20.get(i).getDate();
							break;
						}
					}
					
					// 篩選要賣出的TS
//					TimeSeriesDTO<OHLCDTO> sellTs = tsList.stream()
//							.filter(ts -> ts.getValue().getHigh().compareTo(fillDivPrice) > 0 || ts.getValue().getLow()
//									.divide(openPrice, 2, RoundingMode.HALF_UP).compareTo(new BigDecimal("0.95")) < 0)
//							.findFirst().orElse(null);
					Date deadCrossDate1 = deadCrossDate;
					TimeSeriesDTO<OHLCDTO> sellTs = tsList.stream()
							.filter(ts -> ts.getValue().getHigh().compareTo(fillDivPrice) > 0 
//									|| deadCrossDate1 != null && ts.getDate().compareTo(deadCrossDate1)==0
									|| openPrice.multiply(new BigDecimal(0.95)).compareTo(ts.getValue().getClose()) > 0
									)
							.findFirst().orElse(null);
					BigDecimal sellPrice = null;
					if (sellTs == null) {
						// 90天內沒有符合條件的賣出價
						sellPrice = tsList.get(tsList.size() - 1).getValue().getClose();
					} else if (sellTs.getValue().getHigh().compareTo(fillDivPrice) > 0) {
						// 填息價賣出
						sellPrice = fillDivPrice;
					} else {
						sellPrice = sellTs.getValue().getClose();
					}

					if (sellPrice != null) {
						this.addTradePlanList(
								new TradePlanDTO(this.getSid(), sellTs.getDate(), TradeType.sell, 1000, sellPrice));
//						this.tu.trade(tradeDivDto.getDivPo().getSid(), sellTs.getDate(), 1000, TradeType.sell,
//								sellPrice, this.profitPo);
					} else {
						WLog.error("Can't get sell price, sid["+this.getSid()+"]tradeDate["+tradeDate+"]tradeDivDto[" + tradeDivDto + "]");
					}
				}
			}
		} catch (Exception e) {
			WLog.error(e);
		}
	}

	@Override
	public void collectTradePlanInfo(ProfitDTO po) {
//		List<List<String>> tableData = new ArrayList<>();
//		tableData.add(Arrays.asList("MA60>MA20"));
//		po.getTradeDetailList().stream().filter(td -> td.getTradeType().equals(TradeType.buy)).forEach(td -> {
//			List<String> rowDataList = new ArrayList<>();
//			List<TimeSeriesDTO<BigDecimal>> ma20TsList = statService.getSMA(dtDao.findTdsList(td.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 20);
//			List<TimeSeriesDTO<BigDecimal>> ma60TsList = statService.getSMA(dtDao.findTdsList(td.getSid(), Const._FIELDID_PRICE_DAILY_TW_CLOSE), 60);
//			TimeSeriesDTO<BigDecimal> ma60Ts = ma60TsList.stream().filter(ma -> ma.getDate().equals(td.getTradeDate())).findFirst().orElse(null);
//			TimeSeriesDTO<BigDecimal> ma20Ts = ma20TsList.stream().filter(ma -> ma.getDate().equals(td.getTradeDate())).findFirst().orElse(null);
//			rowDataList.add((ma60Ts == null || ma20Ts == null) ? "NULL" : ma60Ts.getDecimalValue().compareTo(ma20Ts.getDecimalValue()) + "");
//		});
//		po.setCustColList(tableData);
	}
}
