package com.willy.sias.db.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "PRICE_SECOND_TW")
@IdClass(PriceSecondTwPOKey.class)
@AllArgsConstructor
@NoArgsConstructor
public class PriceSecondTwPO {
	@Id
    @Column(name = "\"SID\"")
	private int sid;
	@Id
    @Column(name = "\"DATA_DATE")
	private Date dataDate;
	@Column(name = "\"OPEN\"")
	private BigDecimal open;
	@Column(name = "\"HIGH\"")
	private BigDecimal high;
	@Column(name = "\"LOW\"")
	private BigDecimal low;
	@Column(name = "\"CLOSE\"")
	private BigDecimal close;
	@Column(name = "\"VOLUME\"")
	private BigDecimal volume;
}
