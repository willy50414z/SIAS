package com.willy.sias.web;

import com.linecorp.bot.model.event.Event;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import com.willy.r.dao.WRChart;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.util.BeanFactory;
import com.willy.sias.util.CheckExeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@LineMessageHandler
@SpringBootApplication
@ComponentScan("com.willy")
@EnableTransactionManagement(proxyTargetClass = true)
public class StartWebService implements CommandLineRunner { 
  public static void main(String[] args) throws Exception {
    BeanFactory.context = SpringApplication.run(StartWebService.class, args);
  }

  //處理文字訊息
  @EventMapping
  public Message handleTextMessageEvent(MessageEvent<TextMessageContent> event) {
    String originalMessageText = event.getMessage().getText();
    return new TextMessage(originalMessageText);
  }

  //處理非文字訊息
  @EventMapping
  public void handleDefaultMessageEvent(Event event) {
    System.out.println("event: " + event);
  }

  @Autowired
  private DataTableDao dtDao;
  @Autowired
  private WRChart wrChart;

  @Override
  public void run(String... args) throws Exception {
    if (!CheckExeUtil.isExecute(new Exception())) {
      return;
    }
//    System.exit(0);
  }
}