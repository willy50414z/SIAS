package com.willy.sias.backtest.util;

import static org.junit.Assert.assertTrue;

import com.willy.sias.backtest.service.ProfitDTOService;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.CalendarPO;
import com.willy.sias.db.repository.CalendarRepository;
import com.willy.sias.util.EnumSet.PriceType;
import com.willy.util.log.WLog;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradeDetailDTO;
import com.willy.sias.util.EnumSet.TradeType;
import com.willy.sias.util.config.Const;
import com.willy.util.type.WType;

@SpringBootTest
@ComponentScan("com.willy")
@RunWith(SpringRunner.class)
public class TradeUtilTest {

  @Autowired
  TradeUtil tradeUtil;
  @Autowired
  ProfitDTOService ps;
  @MockBean
  DataTableDao dtDao;
  @MockBean
  CalendarRepository calendarRepo;

  @Before
  public void init() {
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210419"),
        WType.strToDate("20210419"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210419"), new BigDecimal("26.30"))));
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210421"),
        WType.strToDate("20210421"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210421"), new BigDecimal("28.35"))));
    Mockito.when(dtDao.findTsList(122, PriceType.CLOSE.getKey(), WType.strToDate("20210426"),
        WType.strToDate("20210426"))).thenReturn(
        Arrays.asList(new TimeSeriesDTO<>(WType.strToDate("20210426"), new BigDecimal("28.80"))));

    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210419"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210420"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210421"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210420"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210421"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210422"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210421"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210422"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210423"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210422"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210423"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210423"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210424"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210425"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210426"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210426"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210427"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210428"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
    Mockito.when(
        calendarRepo.findTop2ByDateGreaterThanAndDateTypeIsOrderByDate(WType.strToDate("20210427"),
            Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)).thenReturn(Arrays.asList(
        new CalendarPO(WType.strToDate("20210428"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE),
        new CalendarPO(WType.strToDate("20210429"), Const._CFG_ID_CALENDAR_STOCK_BUS_DATE)));
  }

//	@Test
//  public void testTradeWithoutPriceTypeShouldSuccess(){
//    tu.trade(494)
//  }

  @Test
  public void testTradeWithTradePriceShouldSuccess() {
    ProfitDTO profitDTO = new ProfitDTO();
    profitDTO.setOriginalFunds(new BigDecimal(1000000));

    tradeUtil.trade(122, WType.strToDate("20210419"), 3000, TradeType.buy, new BigDecimal("25.5"),
        profitDTO);//76565
    tradeUtil.trade(122, WType.strToDate("20210420"), 3000, TradeType.buy, new BigDecimal("27.5"),
        profitDTO);//82570
    tradeUtil.trade(122, WType.strToDate("20210421"), 3000, TradeType.sell, new BigDecimal("22.5"),
        profitDTO);//67241
    tradeUtil.trade(122, WType.strToDate("20210422"), 3000, TradeType.sell, new BigDecimal("30.5"),
        profitDTO);//91148
    tradeUtil.trade(122, WType.strToDate("20210423"), 3000, TradeType.buy, new BigDecimal("21.5"),
        profitDTO);//64555
    tradeUtil.trade(122, WType.strToDate("20210423"), 3000, TradeType.sell, new BigDecimal("23.5"),
        profitDTO);//70335
    tradeUtil.trade(122, WType.strToDate("20210424"), 3000, TradeType.shortSelling,
        new BigDecimal("29.5"), profitDTO);//88293
    tradeUtil.trade(122, WType.strToDate("20210424"), 3000, TradeType.buy, new BigDecimal("35.5"),
        profitDTO);//106591
    tradeUtil.trade(122, WType.strToDate("20210426"), 3000, TradeType.buy, new BigDecimal("28.5"),
        profitDTO);//85573
    tradeUtil.trade(122, WType.strToDate("20210427"), 3000, TradeType.sell, new BigDecimal("23.5"),
        profitDTO);//70229

    profitDTO.getTradeDetailList().forEach(x -> WLog.info(x.toString()));
  }

  @Test
  public void getTradeAmtTest_Except() throws ParseException {
    assertTrue(tradeUtil.getTradeAmt(0, BigDecimal.valueOf(84.5), TradeType.buy)
        .compareTo(Const._BIGDECIMAL_0) == 0);
    assertTrue(tradeUtil.getTradeAmt(2000, BigDecimal.valueOf(84.5), TradeType.marginBuy) == null);
    assertTrue(tradeUtil.getTradeAmt(2000, null, TradeType.marginBuy) == null);
  }

  // 測試手續費(6折)
  @Test
  public void getTradeAmtTest_HandlingFee() throws ParseException {
    assertTrue(
        tradeUtil.getHandlingFee(1000, new BigDecimal(315)).compareTo(new BigDecimal(269)) == 0);
    assertTrue(
        tradeUtil.getHandlingFee(1000, BigDecimal.valueOf(8.4)).compareTo(new BigDecimal(20)) == 0);
  }

  // 測試交易金額
  @Test
  public void getTradeAmtTest_Buy() throws ParseException {
    // 現買
    assertTrue(tradeUtil.getTradeAmt(2000, BigDecimal.valueOf(5.86), TradeType.buy)
        .compareTo(new BigDecimal(11740)) == 0);
    assertTrue(tradeUtil.getTradeAmt(2000, BigDecimal.valueOf(84.5), TradeType.buy)
        .compareTo(new BigDecimal(169144)) == 0);
  }

  @Test
  public void getTradeAmtTest_SellAmt() throws ParseException {
    assertTrue(tradeUtil.getTradeAmt(1000, new BigDecimal(315), TradeType.sell)
        .compareTo(new BigDecimal(313786)) == 0);
    assertTrue(tradeUtil.getTradeAmt(1000, BigDecimal.valueOf(22.9), TradeType.sell)
        .compareTo(new BigDecimal(22812)) == 0);
  }

  // 當沖賣
  @Test
  public void getTradeAmtTest_ShortSelling() throws ParseException {
    assertTrue(tradeUtil.getTradeAmt(3000, new BigDecimal(144), TradeType.shortSelling)
        .compareTo(new BigDecimal(430983)) == 0);
  }

  // 當沖賣
  @Test
  public void getTradeAmtTest_ShortBuying() throws ParseException {
    assertTrue(tradeUtil.getTradeAmt(3000, new BigDecimal(151), TradeType.shortBuying)
        .compareTo(new BigDecimal(453387)) == 0);
  }

  // 交易紀錄(買)
  @Test
  public void getTradeTest() throws ParseException {
    int tradeResult;
    ProfitDTO profitDTO = new ProfitDTO();
    List<TradeDetailDTO> tdList = new ArrayList<>();
    List<TradeDetailDTO> invList = new ArrayList<>();
    profitDTO.setOriginalFunds(new BigDecimal(
        1000000));// assertTrue(profitPo.getTradeDetailList().hashCode());assertTrue(profitPo.getInventoryList().hashCode());

    // 普買_同日普賣
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210419"), 3000, TradeType.buy,
        new BigDecimal("25.5"), profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("25.5"), new BigDecimal("-76565"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("25.5"), new BigDecimal("-76565"), WType.strToDate("20210421")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210419"), 2000, TradeType.sell,
        new BigDecimal("26.5"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210419"),
        new BigDecimal(2000), new BigDecimal("26.5"), new BigDecimal("52876"),
        WType.strToDate("20210421"), new BigDecimal(1833)));
    invList.clear();
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(1000),
            new BigDecimal("25.5"), new BigDecimal("-25522"), WType.strToDate("20210421")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    // 同日不同額普買
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210419"), 3000, TradeType.buy,
        new BigDecimal("27.5"), profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("27.5"), new BigDecimal("-82570"), WType.strToDate("20210421")));
    invList.clear();

    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(1000),
            new BigDecimal("25.5"), new BigDecimal("-25522"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("27.5"), new BigDecimal("-82570"), WType.strToDate("20210421")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    // 同日超額普賣
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210419"), 5000, TradeType.sell,
        new BigDecimal("28.5"), profitDTO);
//    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210419"),
//        new BigDecimal(4000), new BigDecimal("28.5"), new BigDecimal("113732"),
//        WType.strToDate("20210421"), new BigDecimal("5640")));
//    invList.clear();
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_INVENTORY_NOT_ENOUGH);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210419"), 4000, TradeType.sell,
        new BigDecimal("28.5"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210419"),
        new BigDecimal(4000), new BigDecimal("28.5"), new BigDecimal("113732"),
        WType.strToDate("20210421"), new BigDecimal("5640")));
    invList.clear();
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    // 增加庫存
    tradeUtil.trade(11809, WType.strToDate("20210419"), 5000, TradeType.buy, new BigDecimal("28.5"),
        profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210419"), 5000, TradeType.buy, new BigDecimal("20.5"),
        profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210419"), 5000, TradeType.buy, new BigDecimal("21.5"),
        profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("28.5"), new BigDecimal("-142621"), WType.strToDate("20210421")));
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("20.5"), new BigDecimal("-102587"), WType.strToDate("20210421")));
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("21.5"), new BigDecimal("-107591"), WType.strToDate("20210421")));
    invList.clear();
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("28.5"), new BigDecimal("-142621"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("20.5"), new BigDecimal("-102587"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("21.5"), new BigDecimal("-107591"), WType.strToDate("20210421")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210419")).compareTo(new BigDecimal(1000000))
            == 0);

    // 隔日普賣
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210420"), 7000, TradeType.sell,
        new BigDecimal("22.5"), profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.sell, 11809, WType.strToDate("20210420"), new BigDecimal(7000),
            new BigDecimal("22.5"), new BigDecimal("156894"), WType.strToDate("20210422"),
            new BigDecimal(11271)));
    invList.clear();
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("28.5"), new BigDecimal("-142621"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("21.5"), new BigDecimal("-64555"), WType.strToDate("20210421")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210420")).compareTo(new BigDecimal(1000000))
            == 0);

    // 隔日沖賣 -> 沒存貨
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210421"), 3000, TradeType.shortSelling,
        new BigDecimal("22.5"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.clear();
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(5000),
            new BigDecimal("28.5"), new BigDecimal("-142621"), WType.strToDate("20210421")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210419"), new BigDecimal(3000),
            new BigDecimal("21.5"), new BigDecimal("-64555"), WType.strToDate("20210421")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210421")).compareTo(new BigDecimal(654674))
            == 0);

    // 隔日超額普賣 -> 僅存貨內普賣
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210422"), 10000, TradeType.sell,
        new BigDecimal("19.9"), profitDTO);
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_INVENTORY_NOT_ENOUGH);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210422")).compareTo(new BigDecimal(811568))
            == 0);

    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210422"), 8000, TradeType.sell,
        new BigDecimal("19.9"), profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.sell, 11809, WType.strToDate("20210422"), new BigDecimal(8000),
            new BigDecimal("19.9"), new BigDecimal("158587"), WType.strToDate("20210426"),
            new BigDecimal(-48589)));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210422")).compareTo(new BigDecimal(811568))
            == 0);

    // 普通沖買
    tradeUtil.trade(11809, WType.strToDate("20210423"), 3000, TradeType.shortBuying,
        new BigDecimal("15.1"), profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210423"), 3000, TradeType.shortBuying,
        new BigDecimal("11.2"), profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210423"), 3000, TradeType.shortBuying,
        new BigDecimal("12.3"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("15.1"), new BigDecimal("-45338"),
        WType.strToDate("20210427")));
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("11.2"), new BigDecimal("-33628"),
        WType.strToDate("20210427")));
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("12.3"), new BigDecimal("-36931"),
        WType.strToDate("20210427")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("15.1"), new BigDecimal("-45338"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("11.2"), new BigDecimal("-33628"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("12.3"), new BigDecimal("-36931"),
        WType.strToDate("20210427")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210423")).compareTo(new BigDecimal(878910))
            == 0);

    // 當天普賣 -> 轉沖賣
    tradeResult = tradeUtil.trade(11809, WType.strToDate("20210423"), 4000, TradeType.sell,
        new BigDecimal("13.1"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(4000), new BigDecimal("13.1"), new BigDecimal("52278"),
        WType.strToDate("20210427"), new BigDecimal(6340)));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(3000), new BigDecimal("15.1"), new BigDecimal("-45338"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("12.3"), new BigDecimal("-24621"),
        WType.strToDate("20210427")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210423")).compareTo(new BigDecimal(878910))
            == 0);

    // 當日超額沖賣 -> 僅存貨內沖賣，多的直接沖賣
    tradeUtil.trade(11809, WType.strToDate("20210423"), 7000, TradeType.shortSelling,
        new BigDecimal("13.1"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(5000), new BigDecimal("13.1"), new BigDecimal("65346"),
        WType.strToDate("20210427"), new BigDecimal(-4613)));
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    assertTrue(tradeResult == Const._TRADE_RESULT_CODE_SUCCESS);
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210423")).compareTo(new BigDecimal(878910))
            == 0);

    // 普通沖賣
    tradeUtil.trade(11809, WType.strToDate("20210426"), 3000, TradeType.shortSelling,
        new BigDecimal("15.1"), profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210426"), 3000, TradeType.shortSelling,
        new BigDecimal("11.2"), profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210426"), 3000, TradeType.shortSelling,
        new BigDecimal("12.3"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("15.1"), new BigDecimal("45195"),
        WType.strToDate("20210428")));
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("11.2"), new BigDecimal("33522"),
        WType.strToDate("20210428")));
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("12.3"), new BigDecimal("36814"),
        WType.strToDate("20210428")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("15.1"), new BigDecimal("45195"),
        WType.strToDate("20210428")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("11.2"), new BigDecimal("33522"),
        WType.strToDate("20210428")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("12.3"), new BigDecimal("36814"),
        WType.strToDate("20210428")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210426")).compareTo(new BigDecimal(1037497))
            == 0);

    // 當日普買 -> 轉沖買
    tradeUtil.trade(11809, WType.strToDate("20210426"), 4000, TradeType.buy, new BigDecimal("13.1"),
        profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(4000), new BigDecimal("13.1"), new BigDecimal("-52444"),
        WType.strToDate("20210428"), new BigDecimal(5023)));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("11.2"), new BigDecimal("33522"),
        WType.strToDate("20210428")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(2000), new BigDecimal("12.3"), new BigDecimal("24542"),
        WType.strToDate("20210428")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210426")).compareTo(new BigDecimal(1037497))
            == 0);

    // 沖買
    tradeUtil.trade(11809, WType.strToDate("20210426"), 3000, TradeType.shortBuying,
        new BigDecimal("13.8"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(3000), new BigDecimal("13.8"), new BigDecimal("-41435"),
        WType.strToDate("20210428"), new BigDecimal(-5719)));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210426"),
        new BigDecimal(2000), new BigDecimal("11.2"), new BigDecimal("22348"),
        WType.strToDate("20210428")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210426")).compareTo(new BigDecimal(1037497))
            == 0);

    // 沖買過量
    tradeUtil.trade(11809, WType.strToDate("20210426"), 3000, TradeType.shortBuying,
        new BigDecimal("14.2"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(2000), new BigDecimal("14.2"), new BigDecimal("-28424"),
        WType.strToDate("20210428"), new BigDecimal(-6076)));
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(1000), new BigDecimal("14.2"), new BigDecimal("-14220"),
        WType.strToDate("20210428")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("22.5"), new BigDecimal("67342"),
        WType.strToDate("20210423")));
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(1000), new BigDecimal("14.2"), new BigDecimal("-14220"),
        WType.strToDate("20210428")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));
    assertTrue(
        ps.getAcctBalance(profitDTO, WType.strToDate("20210426")).compareTo(new BigDecimal(1037497))
            == 0);

    // 可沖買存貨不足抵銷普買 -> 普買
    tradeUtil.trade(11809, WType.strToDate("20210421"), 5000, TradeType.buy, new BigDecimal("20.2"),
        profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210421"),
        new BigDecimal(3000), new BigDecimal("20.2"), new BigDecimal("-60651"),
        WType.strToDate("20210423"), new BigDecimal(6691)));
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210421"), new BigDecimal(2000),
            new BigDecimal("20.2"), new BigDecimal("-40434"), WType.strToDate("20210423")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(1000), new BigDecimal("14.2"), new BigDecimal("-14220"),
        WType.strToDate("20210428")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210421"), new BigDecimal(2000),
            new BigDecimal("20.2"), new BigDecimal("-40434"), WType.strToDate("20210423")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));

    // 普買金額>餘額
    assertTrue(tradeUtil.trade(11809, WType.strToDate("20210426"), 10000, TradeType.buy,
        new BigDecimal("100.0"), profitDTO) == Const._TRADE_RESULT_CODE_BALANCE_NOT_ENOUGH);
    assertTrue(tradeUtil.trade(11809, WType.strToDate("20210426"), 10000, TradeType.shortBuying,
        new BigDecimal("100.0"), profitDTO) == Const._TRADE_RESULT_CODE_BALANCE_NOT_ENOUGH);

    // 同日同價位買2張
    tradeUtil.trade(11809, WType.strToDate("20210427"), 3000, TradeType.buy, new BigDecimal("12.1"),
        profitDTO);
    tradeUtil.trade(11809, WType.strToDate("20210427"), 3000, TradeType.buy, new BigDecimal("12.1"),
        profitDTO);
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210427"), new BigDecimal(3000),
            new BigDecimal("12.1"), new BigDecimal("-36331"), WType.strToDate("20210429")));
    tdList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210427"), new BigDecimal(3000),
            new BigDecimal("12.1"), new BigDecimal("-36331"), WType.strToDate("20210429")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(1000), new BigDecimal("14.2"), new BigDecimal("-14220"),
        WType.strToDate("20210428")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210421"), new BigDecimal(2000),
            new BigDecimal("20.2"), new BigDecimal("-40434"), WType.strToDate("20210423")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210427"), new BigDecimal(6000),
            new BigDecimal("12.1"), new BigDecimal("-72662"), WType.strToDate("20210429")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));

    // 同日同價位沖買1張
    tradeUtil.trade(11809, WType.strToDate("20210426"), 1000, TradeType.shortBuying,
        new BigDecimal("14.2"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(1000), new BigDecimal("14.2"), new BigDecimal("-14220"),
        WType.strToDate("20210428")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(2000), new BigDecimal("14.2"), new BigDecimal("-28440"),
        WType.strToDate("20210428")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210421"), new BigDecimal(2000),
            new BigDecimal("20.2"), new BigDecimal("-40434"), WType.strToDate("20210423")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210427"), new BigDecimal(6000),
            new BigDecimal("12.1"), new BigDecimal("-72662"), WType.strToDate("20210429")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));

    // 同日同價位沖賣2張
    tradeUtil.trade(11809, WType.strToDate("20210423"), 2000, TradeType.shortSelling,
        new BigDecimal("13.1"), profitDTO);
    tdList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(2000), new BigDecimal("13.1"), new BigDecimal("26139"),
        WType.strToDate("20210427")));
    invList.clear();
    invList.add(new TradeDetailDTO(TradeType.shortSelling, 11809, WType.strToDate("20210423"),
        new BigDecimal(4000), new BigDecimal("13.1"), new BigDecimal("52278"),
        WType.strToDate("20210427")));
    invList.add(new TradeDetailDTO(TradeType.shortBuying, 11809, WType.strToDate("20210426"),
        new BigDecimal(2000), new BigDecimal("14.2"), new BigDecimal("-28440"),
        WType.strToDate("20210428")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210421"), new BigDecimal(2000),
            new BigDecimal("20.2"), new BigDecimal("-40434"), WType.strToDate("20210423")));
    invList.add(
        new TradeDetailDTO(TradeType.buy, 11809, WType.strToDate("20210427"), new BigDecimal(6000),
            new BigDecimal("12.1"), new BigDecimal("-72662"), WType.strToDate("20210429")));
    assertTrue(profitDTO.getTradeDetailList().equals(tdList));
    assertTrue(profitDTO.getInventoryList().equals(invList));

    // 測試沒上架的功能
    tradeUtil.trade(11809, WType.strToDate("20210423"), 2000, TradeType.marginBuy,
        new BigDecimal("13.1"), profitDTO);
  }
}
