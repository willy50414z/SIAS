package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Table(name="DIVIDEND_TW")
public class DividendTWPOKey implements Serializable{
	/**
	 * 股票代號
	 */
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 股利所屬年度
	 */
	@Column(name = "DIV_BELONGS")
	private String divBelongs;
	
	/**
	 * 權利分派基準日
	 */
	@Column(name = "PU_RIGHT_BASE_DATE")
	private Date puRightBaseDate;

	private static final long serialVersionUID = 1L;
}
