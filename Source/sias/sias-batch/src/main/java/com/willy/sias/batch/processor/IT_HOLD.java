package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;

@Setter
@Service
@ConfigurationProperties(prefix = "sias.batch.it-hold")
public class IT_HOLD extends BaseBatchProcessor {

	@Autowired
	private LPTransLog_TW_TWSERepository lpRepo;
	@Autowired
	private SysConfigRepository sysConfigRepo;
	@Autowired
	private CompanyCategoryRepository ccRepo;

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	protected void exec() throws Exception {
		// TODO Auto-generated method stub
		// 撈出要更新的ID
		// select distinct sid from lp where it_hold is null
		List<Integer> itHoldingNullSidList = lpRepo.findDistinctSIDByItHoldingIsNull();
		List<Integer> noDerivativeSidList = ccRepo.findSidWithoutDerivative();
		List<Integer> sidList = itHoldingNullSidList.stream().filter(sid1 -> noDerivativeSidList.contains(sid1))
				.collect(Collectors.toList());
		for (int sid : sidList) {
			WLog.info("start update IT_Holding, sid["+sid+"]");
			// 撈出IT_HOLD資料
			ITHoldDTO itHoldDto;
			try {
				itHoldDto = this.getITHoldDTO(sid);
			} catch(Exception e) {
				WLog.error("sid["+sid+"] get IT Hold Fail["+WType.exceptionToString(e)+"]");
				continue;
			}
			

			// 撈出對應的LP_PO
			List<LPTransLogTwPO> lpList = lpRepo.findBySidIsOrderByTransDate(sid);

			// 更新資料
			try {
				//更新基準日之後的資料
				List<LPTransLogTwPO> lpList1 = lpList.stream()
						.filter(lp -> WDate.afterOrEquals(lp.getTransDate(), itHoldDto.getDate()))
						.collect(Collectors.toList());

				BigDecimal itHold = null;
				for (LPTransLogTwPO lp : lpList1) {
					if (itHold == null) {
						itHold = itHoldDto.getItHold();
						lp.setItHolding(itHold);
						continue;
					}
					itHold = itHold.add(new BigDecimal(lp.getItNet()));
					lp.setItHolding(itHold);
				}

				//更新基準日之前的資料(有it_holding == null才要執行)
				if (lpList.stream().filter(
						lp -> WDate.beforeOrEquals(lp.getTransDate(), itHoldDto.getDate()) && lp.getItHolding() == null)
						.count() > 0) {
					lpList1 = lpList.stream().filter(lp -> WDate.beforeOrEquals(lp.getTransDate(), itHoldDto.getDate()))
							.collect(Collectors.toList());

					itHold = null;
					for (int i = lpList1.size() - 1; i > -1; i--) {
						if (itHold == null) {
							itHold = itHoldDto.getItHold();
						}
						lpList1.get(i).setItHolding(itHold);
						itHold = itHold.subtract(new BigDecimal(lpList1.get(i).getItNet()));
					}
				} else {
					lpList = lpList1;
				}
			}catch(Exception e) {
				WLog.error("sid["+sid+"] update data fail["+WType.exceptionToString(e)+"]");
			}

			// 回存
			lpRepo.saveAll(lpList);
			WLog.info("Finish update IT_Holding, sid["+sid+"]");
		}
	}

	private ITHoldDTO getITHoldDTO(int sid) throws Exception {

		// 先撈過去最新的IT_Holding，在往後推
		LPTransLogTwPO lp = lpRepo.findFirstBySidIsAndItHoldingNotNullOrderByTransDateDesc(sid);
		if (lp == null) {
			// 撈出sidCode
			SysConfigPO sysConfigPO = sysConfigRepo.findById(sid).orElse(null);
			if (sysConfigPO == null) {
				WLog.error("sid[" + sid + "] can't get sidCode");
				throw new IllegalArgumentException("sid[" + sid + "] can't get sidCode");
			}
			String sidCode = sysConfigPO.getCfgValue();
			Elements eles = new WJsoup()
					.get("https://concords.moneydj.com/z/zc/zcl/zcl.djhtm?a=" + sidCode + "&b=4", "UTF-8")
					.getElementsByTag("table").get(1).getElementsByTag("table").get(1).getElementsByTag("tr").get(46)
					.getElementsByTag("td");
			String strDate = eles.get(0).text();
			String strItHold = eles.get(6).text();
			return new ITHoldDTO(WType.strToDate(WDate.convertCE(WString.rmStr(strDate, "/"))),
					new BigDecimal(WString.rmStr(strItHold, ",")).multiply(new BigDecimal(1000)));
		} else {
			return new ITHoldDTO(lp.getTransDate(), lp.getItHolding());
		}
	}

}

@Data
@AllArgsConstructor
class ITHoldDTO {
	private Date date;
	private BigDecimal itHold;
}
