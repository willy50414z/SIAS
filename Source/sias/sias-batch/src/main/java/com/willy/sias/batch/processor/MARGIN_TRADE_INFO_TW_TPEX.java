package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.sias.db.po.MarginTradeInfoPO;
import com.willy.sias.db.repository.MarginTradeInfoRepository;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.StringUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.margintradeinfo-tw-tpex")
public class MARGIN_TRADE_INFO_TW_TPEX extends BaseBatchProcessor {
	@Autowired
	private MarginTradeInfoRepository mtiRepo;
	
	@Override
	public void cleanHisData() throws Exception {
		//已包含在readFileAndComposeToBeanList()
	}

	@Override
	protected void exec() throws Exception {
		txtFile.setEncoding(Const._ENCODING_UTF8);
		String urlDate = WDate.convertCEYear(WDate.getDateInfo(this.dataDate, Calendar.YEAR)) + WType.dateToStr(this.dataDate, "/MM/dd");
		this.putMapParam("urlDate", urlDate);
		//下載檔案
		filePath = FileUtil.download(parseMapParamToString(urlTempl), parseMapParamToString(csvFileNameTempl));
		
		//read and compose
		List<MarginTradeInfoPO> mtiList = readFileAndComposeToBeanList();
		
		//save
		mtiRepo.saveAll(mtiList);
	}
	
	private List<MarginTradeInfoPO> readFileAndComposeToBeanList() throws Exception{
		MarginTradeInfoPO mti;
		List<MarginTradeInfoPO> mtiList = new ArrayList<> ();
		Set<Integer> sidSet = new HashSet<> ();
		
		String[][] dataAr = txtFile.readToAr(filePath, "代號", "合計", true);
		this.mergeSid(dataAr);
		
		for(String[] rowDate : dataAr) {
			mti = new MarginTradeInfoPO();
			mti.setSid(sidMap.get(rowDate[0].trim()));
			mti.setDataDate(dataDate);
			mti.setFinBuy(Integer.valueOf(StringUtil.cleanDigitData(rowDate[3])));
			mti.setFinSell(Integer.valueOf(StringUtil.cleanDigitData(rowDate[4])));
			mti.setFinRepay(Integer.valueOf(StringUtil.cleanDigitData(rowDate[5])));
			mti.setFinBalance(Integer.valueOf(StringUtil.cleanDigitData(rowDate[6])));
			mti.setFinQuota(new BigDecimal(StringUtil.cleanDigitData(rowDate[9])));
			mti.setLendBuy(Integer.valueOf(StringUtil.cleanDigitData(rowDate[11])));
			mti.setLendSell(Integer.valueOf(StringUtil.cleanDigitData(rowDate[12])));
			mti.setLendRepay(Integer.valueOf(StringUtil.cleanDigitData(rowDate[13])));
			mti.setLendBalance(Integer.valueOf(StringUtil.cleanDigitData(rowDate[14])));
			mti.setLendQuota(new BigDecimal(StringUtil.cleanDigitData(rowDate[17])));
			mti.setMemo(rowDate[19]);
			mtiList.add(mti);
			sidSet.add(mti.getSid());
		}
		mtiRepo.deleteByDataDateIsAndSidIn(dataDate, sidSet);
		return mtiList;
	}

}
