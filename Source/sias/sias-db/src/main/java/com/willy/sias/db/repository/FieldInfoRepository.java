package com.willy.sias.db.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.willy.sias.db.po.FieldInfoPO;

@Repository
public interface FieldInfoRepository extends JpaRepository<FieldInfoPO, Integer> {
  public List<FieldInfoPO> findByFieldNameLike(String fieldName);
}
