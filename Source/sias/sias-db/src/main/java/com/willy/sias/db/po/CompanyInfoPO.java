package com.willy.sias.db.po;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * BatchLog
 * 
 * @author
 */
@Table(name = "COMPANY_INFO")
@IdClass(CompanyInfoPOKey.class)
@Entity
@Data
public class CompanyInfoPO implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "\"CHAIR_MAN\"")
	private String chairMan;
	@Column(name = "\"GEN_MANAGER\"")
	private String genManager;
	@Column(name = "\"EST_DATE\"")
	private Date estDate;
	@Column(name = "\"CAPITAL\"")
	private BigDecimal capital;
	@Column(name = "\"PUBL_SHARES\"")
	private BigDecimal publShares;
	@Column(name = "\"SE_DATE\"")
	private Date seDate;
	@Column(name = "\"OTC_DATE\"")
	private Date otcDate;
	@Column(name = "\"ES_DATE\"")
	private Date esDate;
	@Column(name = "\"PUB_DATE\"")
	private Date pubDate;
	
	@Id
	@EqualsAndHashCode.Exclude
	@Column(name = "\"DATA_DATE\"")
	private Date dataDate;
	@Id
	@EqualsAndHashCode.Exclude
	@Column(name = "\"SID\"")
	private Integer sid;
	
	
}