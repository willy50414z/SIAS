package com.willy.sias.backtest.service;

import com.willy.sias.util.config.Const;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FieldFilterServiceTest {

  @Autowired
  private FieldFilterService ffSvc;
  @Autowired
  private Jedis jedis;

  @Test
  public void testReplactProcessBlocksToRedisKeyWithProcessBlocksStrShouldSuccess() {
    String processBlocks = "[{14}(1,2,3) > 10] & [{15}([{18}()}],5,[{16}(6,[{17}(15,16,17)],8)])]";
    Map<String, String> redisKeyAndProcBlockMap = new HashMap<>();

    Assert.assertEquals(
        "${" + Const.PROC_BLOCK_KEY_PREFFIX + "0} & ${" + Const.PROC_BLOCK_KEY_PREFFIX + "4}",
        ffSvc.replactProcessBlocksToRedisKey(processBlocks, 0, redisKeyAndProcBlockMap));

    Assert.assertEquals(redisKeyAndProcBlockMap,
        new HashMap<String, String>() {{
          put(Const.PROC_BLOCK_KEY_PREFFIX + "3",
              "[{16}(6,${" + Const.PROC_BLOCK_KEY_PREFFIX + "1},8)]");
          put(Const.PROC_BLOCK_KEY_PREFFIX + "4",
              "[{15}(${" + Const.PROC_BLOCK_KEY_PREFFIX + "2},5,${" + Const.PROC_BLOCK_KEY_PREFFIX
                  + "3})]");
          put(Const.PROC_BLOCK_KEY_PREFFIX + "0", "[{14}(1,2,3) > 10]");
          put(Const.PROC_BLOCK_KEY_PREFFIX + "1", "[{17}(15,16,17)]");
          put(Const.PROC_BLOCK_KEY_PREFFIX + "2", "[{18}()}]");
        }});
  }
}
