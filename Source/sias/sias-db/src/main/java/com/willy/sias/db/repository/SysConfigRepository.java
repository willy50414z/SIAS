package com.willy.sias.db.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.willy.sias.db.po.SysConfigPO;

public interface SysConfigRepository extends JpaRepository<SysConfigPO, Integer> {
	List<SysConfigPO> findByParentIdIsOrderByCfgId(int parentId);
	List<SysConfigPO> findByCfgIdInOrderByCfgId(Set<Integer> cfgId);
	List<SysConfigPO> findByParentIdIsAndCfgDescLike(Integer parentId, String cfgDesc);
	List<SysConfigPO> findByParentIdIsAndCfgDescIn(Integer cfgId, List<String> cfgDesc);
	SysConfigPO findByParentIdIsAndCfgValueIs(int parentId,String fieldValue);
	List<SysConfigPO> findByParentIdIsAndCfgValueIn(int parentId,List<String> fieldValue);
	List<SysConfigPO> findByParentIdIsAndCfgValueStartingWithOrderByCfgValueDesc (int parentId, String fieldValueKey);
	
}