package com.willy.sias.backtest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MethodInfoDTO {
  private String svcType;
  private String svcMethodDesc;
  private String resultDesc;
  private String className;
  private String methodDesc;
}
