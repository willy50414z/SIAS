package com.willy.sias.web.vo;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import lombok.Data;

@Data
public class FieldInfoVO {
  private String fieldInfoJs;
  private LinkedList<String> processArgList;
  private LinkedHashSet<String> subCatFieldNameSet;
}
