package com.willy.sias.backtest.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.willy.sias.db.dto.TimeSeriesDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BreakUdTrackDTO {
	private Date baseDate;
	private int breakUdStatus;
	private List<TimeSeriesDTO<BigDecimal>> pressureTsList;
	private List<TimeSeriesDTO<BigDecimal>> supportTsList;
}
