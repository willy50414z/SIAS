<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
<title>歷史回測結果</title>
<%@ include file="/jsp/Header.jsp"%>
<script>
	$().ready(function() {
		$('#backTestResult').DataTable({
			"searching" : false,
			"drawCallback" : function(settings) {
				//換頁後重新綁定JS
			}
		});
		$("#resizableTextArea").resizable({
			animate : true
		});
		if("${errMsg}" != ''){
			alert("查詢失敗 : \r\n${errMsg}");
		}
		$( "#tradeDetailDialog" ).dialog({
		      height: 500,
		      width: 700,
		      autoOpen: false,
		      modal: true,
		      buttons: {
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    });
		$( ".btn-primary" ).on( "click", function() {
			$.ajax({
				type : "POST",
				url : getContextPath() + "/BackTestSummary/QueryTradeDetail",
				data : {
					testId : $(this).val()
				},
				success : function(tradeDetailVOList) {
					var tradeDetailHtml = "<table id = 'tradeDetailTable' style='width: 100%'><tr style='background-color: #E0E0E0'><th>交易日</th><th>買賣別</th><th>交易價</th><th>交易數量</th><th>交易金額</th><th>淨利</th></tr>";
					for(var tradeDetailIndex=0; tradeDetailIndex<tradeDetailVOList.length; tradeDetailIndex++){
						console.log(tradeDetailVOList[tradeDetailIndex]);
						console.log(tradeDetailVOList[tradeDetailIndex].buy);
						tradeDetailHtml += "<tr style='background-color: "+(tradeDetailVOList[tradeDetailIndex].buy?"":"#ECF5FF")+"'>";
						tradeDetailHtml += "<td>"+tradeDetailVOList[tradeDetailIndex].tradeDate+"</td>";
						tradeDetailHtml += "<td  style='"+(tradeDetailVOList[tradeDetailIndex].buy?"color:red'>現股買入":"color:green'>現股賣出")+"</td>";
						tradeDetailHtml += "<td>"+tradeDetailVOList[tradeDetailIndex].tradePrice+"</td>";
						tradeDetailHtml += "<td>"+tradeDetailVOList[tradeDetailIndex].tradeCount+"</td>";
						tradeDetailHtml += "<td>"+tradeDetailVOList[tradeDetailIndex].tradeAmt+"</td>";
						tradeDetailHtml += "<td>"+tradeDetailVOList[tradeDetailIndex].grossProfit+"</td></tr>";
					}
					tradeDetailHtml += "</table>";
					$("#tradeDetailContent").html(tradeDetailHtml);
					$( "#tradeDetailDialog" ).dialog( "open" );
				},
				error : function(data) {
					alert("查詢交易明細失敗");
				}
			});
	    });
	});
</script>
<style>
#searchRegion td {
	padding-top: 10px;
	padding-left: 5px;
}
#tradeDetailTable, #tradeDetailTable th, #tradeDetailTable td {
  border: 1px solid black;
  border-collapse: collapse;
}
</style>
</head>
<body>
	<form id="queryRegion">
		<table id="searchRegion" class="display" style="width: 50%">
			<tr>
				<td>測試ID:</td>
				<td><input name="testIdCreteria" class="form-control"
					type="text" placeholder="測試ID篩選條件" value="${testIdCreteria}"></td>
				<td>測試日期:</td>
				<td><input name="testDateCreteria" class="form-control"
					type="text" placeholder="測試日期篩選條件" value="${testDateCreteria}"></td>
			</tr>
			<tr>
				<td>回測起日:</td>
				<td><input name="testStartDateCreteria" class="form-control"
					type="text" placeholder="回測起日篩選條件" value="${testStartDateCreteria}"></td>
				<td>回測迄日:</td>
				<td><input name="testEndDateCreteria" class="form-control"
					type="text" placeholder="回測迄日篩選條件" value="${testEndDateCreteria}"></td>
			</tr>
			<tr>
				<td>策略名稱:</td>
				<td><input name="strategyNameCreteria" class="form-control"
					type="text" placeholder="策略名稱篩選條件" value="${strategyNameCreteria}"></td>
				<td>策略描述:</td>
				<td><input name="strategyDescCreteria" class="form-control"
					type="text" placeholder="策略描述篩選條件" value="${strategyDescCreteria}"></td>
			</tr>
			<tr>
				<td>毛利:</td>
				<td><input name="grossProfitCriteria" class="form-control"
					type="text" placeholder="毛利篩選條件"  value="${grossProfitCriteria}"></td>
				<td>毛利率:</td>
				<td><input name="grossProfitRatioCriteria" class="form-control"
					type="text" placeholder="毛利率篩選條件" value="${grossProfitRatioCriteria}"></td>
			</tr>
			<tr>
				<td>勝率:</td>
				<td><input name="profitRatioCriteria" class="form-control"
					type="text" placeholder="勝率篩選條件" value="${profitRatioCriteria}"></td>
				<td></td>
				<td><button id="searchBtn" type="submit"
						class="btn btn-success" style="float: right">Query</button></td>
			</tr>
		</table>
	</form>
	<br />
	<br />
	<br />
	<div id="resultRegion">
		<table id="backTestResult" class="display" style="width: 100%">
			<thead>
				<tr>
					<th>測試ID</th>
					<th>測試日期</th>
					<th>回測起日</th>
					<th>回測迄日</th>
					<th>策略名稱</th>
					<th>策略描述</th>
					<th>毛利</th>
					<th>毛利率</th>
					<th>勝率</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${backTestSummaryList}" var="backTestSummary">
					<tr>
						<td>${backTestSummary.testId}</td>
						<td>${backTestSummary.testDate}</td>
						<td>${backTestSummary.testStartDate}</td>
						<td>${backTestSummary.testEndDate}</td>
						<td>${backTestSummary.strategyName}</td>
						<td style="width: 300px">${backTestSummary.strategyDesc}</td>
						<td>${backTestSummary.grossProfit}</td>
						<td>${backTestSummary.grossProfitRatio}</td>
						<td>${backTestSummary.profitRatio}
						<td><button type="button" class="btn btn-primary"
								value="${backTestSummary.testId}">交易明細</button></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<div id="tradeDetailDialog" title="交易明細">
		<fieldset>
			<legend>交易明細</legend>
			<div id="tradeDetailContent"></div>
		</fieldset>
	  
	</div>
</body>
</html>