package com.willy.sias.db.dto;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class MACDStatusDTO {
	List<TimeSeriesDTO<MACDDTO>> macdTsList;

	/**
	 * DIF快線及MACD慢線在0軸上的天數
	 * >0 0軸上
	 * <0 0軸下
	 * =0 沒有同時在上或下
	 * @param date
	 * @return
	 */
	public int getDIFMACDStatus(Date date) {
		List<TimeSeriesDTO<MACDDTO>> partMacdTsList = macdTsList.stream()
				.filter(ts -> WDate.beforeOrEquals(ts.getDate(), date)).collect(Collectors.toList());
		if(partMacdTsList == null) {
			WLog.error("Can't get partMacdTsList for Date["+date+"]");
			return Const.ERROR_INT_RC;
		}
		if(partMacdTsList.size()<3) {
			WLog.error("Can't get enough partMacdTsList for Date["+date+"]partMacdTsList.size()["+partMacdTsList.size()+"]");
			return Const.ERROR_INT_RC;
		}
		
		int udZeroDays = 0;
		for (int i = partMacdTsList.size() - 1; i > -1; i--) {
			if(partMacdTsList.get(i).getValue().getDif() == null) {
				break;
			}
			if (partMacdTsList.get(i).getValue().getDif().compareTo(Const._BIGDECIMAL_0) >= 0
					&& partMacdTsList.get(i).getValue().getMacd().compareTo(Const._BIGDECIMAL_0) >= 0) {
				if (udZeroDays < 0) {
					break;
				}
				udZeroDays++;
			} else if (partMacdTsList.get(i).getValue().getDif().compareTo(Const._BIGDECIMAL_0) <= 0
					&& partMacdTsList.get(i).getValue().getMacd().compareTo(Const._BIGDECIMAL_0) <= 0) {
				if (udZeroDays > 0) {
					break;
				}
				udZeroDays--;
			}
		}
		return udZeroDays;
	}
	
	/**
	 * OSC柱目前趨勢
	 * >0 上升天數
	 * <0 下降天數
	 * @param date
	 * @return
	 */
	public int getOSCStatus(Date date) {
		List<TimeSeriesDTO<MACDDTO>> partMacdTsList = macdTsList.stream()
				.filter(ts -> WDate.beforeOrEquals(ts.getDate(), date)).collect(Collectors.toList());
		if(partMacdTsList == null) {
			WLog.error("Can't get partMacdTsList for Date["+date+"]");
			return Const.ERROR_INT_RC;
		}
		if(partMacdTsList.size()<3) {
			WLog.error("Can't get enough partMacdTsList for Date["+date+"]partMacdTsList.size()["+partMacdTsList.size()+"]");
			return Const.ERROR_INT_RC;
		}
		
		int upDays = 0;
		for (int i = partMacdTsList.size() - 1; i > 0; i--) {
			if(partMacdTsList.get(i - 1).getValue().getOsc() == null) {
				break;
			}
			if(partMacdTsList.get(i).getValue().getOsc().compareTo(partMacdTsList.get(i - 1).getValue().getOsc())>0) {
				if(upDays<0) {
					break;
				}
				upDays++;
			} else {
				if(upDays>0) {
					break;
				}
				upDays--;
			}
		}
		return upDays;
	}
}
