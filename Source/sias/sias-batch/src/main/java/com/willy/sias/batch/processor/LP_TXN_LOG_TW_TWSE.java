package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.willy.sias.db.po.LPTransLogTwPO;
import com.willy.sias.db.repository.LPTransLog_TW_TWSERepository;
import com.willy.sias.util.FileUtil;
import com.willy.sias.util.StringUtil;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties("sias.batch.lptranslog-tw-twse")
public class LP_TXN_LOG_TW_TWSE extends BaseBatchProcessor {
	@Autowired
	private WJDBC jdbc;
	@Autowired
	private LPTransLog_TW_TWSERepository lptRepo;
	@Transactional
	public void exec() throws Exception {
		// 下載資料
		filePath = FileUtil.download(this.parseMapParamToString(urlTempl), this.parseMapParamToString(csvFileNameTempl));

		// 匯入對應
		String[][] lptAr = txtFile.readToAr(filePath, "證券代號", "說明:",true);
		if(lptAr.length ==0) {
			WLog.info(this.dataDate + "當天非交易日");
			return;
		}
		
		this.mergeSid(lptAr);

		//匯入
		this.saveLPTransLog(lptAr);
	}
	
	public void updateItHolding() throws NumberFormatException, ParseException {
		//取得未update資訊
		String[][] unUpdateAr = jdbc.queryForAr("select sid from LP_TRANS_LOG_TW lp where lp.IT_HOLDING is null group by sid");
		//逐個SID撈出
		while(unUpdateAr.length>0) {
			int batchSize = 500;
			List<LPTransLogTwPO> lpListHeap = new ArrayList<> ();
			for(String[] unUpRow : unUpdateAr) {
				BigDecimal itHolding = new BigDecimal(0);
				List<LPTransLogTwPO> lpList =  lptRepo.findBySidIsOrderByTransDate(Integer.valueOf(unUpRow[0]));
				for(LPTransLogTwPO lp : lpList) {
					if(lp.getItHolding()==null) {
						itHolding = itHolding.add(new BigDecimal((lp.getItNet() == null ? 0 : lp.getItNet())));
						lp.setItHolding(itHolding);
					}else {
						itHolding = lp.getItHolding();
					}
				}
				batchSize--;
				if(batchSize == 0) {
					batchSize = 500;
					lptRepo.saveAll(lpListHeap);
					lpListHeap.clear();
				}else {
					lpListHeap.addAll(lpList);
				}
			}
		}
	}
	private void saveLPTransLog(String[][] lpTransLogAr) throws ParseException {
		LPTransLogTwPO lpt;
		List<LPTransLogTwPO> lptList = new ArrayList<LPTransLogTwPO> ();
		
		for (String[] lptLog : lpTransLogAr) {
			lpt = new LPTransLogTwPO();
			lpt.setSid(sidMap.get(lptLog[0]));
			lpt.setTransDate(dataDate);
			if(dataDate.after(WType.strToDate("20171215"))) {
				lpt.setFiNsBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[2])));
				lpt.setFiNsSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[3]))); 
				lpt.setFiSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[5])));
				lpt.setFiSelfSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[6])));
				lpt.setFiBuy(lpt.getFiNsBuy() + lpt.getFiSelfBuy());
				lpt.setFiSell(lpt.getFiNsSell() + lpt.getFiSelfSell());
				lpt.setItBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[8])));
				lpt.setItSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[9])));
				lpt.setDlSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[12])));
				lpt.setDlSelfSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[13])));
				lpt.setDlHedgingBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[15])));
				lpt.setDlHedgingSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[16])));
				lpt.setDlBuy(lpt.getDlHedgingBuy() + lpt.getDlSelfBuy());
				lpt.setDlSell(lpt.getDlHedgingSell() + lpt.getDlSelfSell());
			}else if(dataDate.after(WType.strToDate("20141130"))){
				lpt.setFiNsBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[2])));
				lpt.setFiNsSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[3])));
				lpt.setFiSelfBuy(0);
				lpt.setFiSelfSell(0);
				lpt.setFiBuy(lpt.getFiNsBuy() + lpt.getFiSelfBuy());
				lpt.setFiSell(lpt.getFiNsSell() + lpt.getFiSelfSell());
				lpt.setItBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[5])));
				lpt.setItSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[6])));
				lpt.setDlSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[9])));
				lpt.setDlSelfSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[10])));
				lpt.setDlHedgingBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[12])));
				lpt.setDlHedgingSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[13])));
				lpt.setDlBuy(lpt.getDlHedgingBuy() + lpt.getDlSelfBuy());
				lpt.setDlSell(lpt.getDlHedgingSell() + lpt.getDlSelfSell());
			}else {
				lpt.setFiNsBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[2])));
				lpt.setFiNsSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[3])));
				lpt.setFiSelfBuy(0);
				lpt.setFiSelfSell(0);
				lpt.setFiBuy(lpt.getFiNsBuy() + lpt.getFiSelfBuy());
				lpt.setFiSell(lpt.getFiNsSell() + lpt.getFiSelfSell());
				lpt.setItBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[5])));
				lpt.setItSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[6])));
				lpt.setDlSelfBuy(Integer.valueOf(StringUtil.cleanDigitData(lptLog[9])));
				lpt.setDlSelfSell(Integer.valueOf(StringUtil.cleanDigitData(lptLog[10])));
				lpt.setDlHedgingBuy(0);
				lpt.setDlHedgingSell(0);
				lpt.setDlBuy(lpt.getDlHedgingBuy() + lpt.getDlSelfBuy());
				lpt.setDlSell(lpt.getDlHedgingSell() + lpt.getDlSelfSell());
			}
			lptList.add(lpt);
		}
		lptRepo.saveAll(lptList);
	}
	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
//		lptRepo.deleteByTransDateEquals(dataDate);
	}
}
