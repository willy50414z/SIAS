package com.willy.sias.db.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;


@Table(name = "EXCHANGE")
@Entity
@Data
@IdClass(ExchangePOKey.class)
@NoArgsConstructor
@AllArgsConstructor
public class ExchangePO
{
	/**
	 * 標的代號
	 */
	@Id
	@Column(name = "SID")
	private Integer sid;

	/**
	 * 資料日期
	 */
	@Id
	@Column(name = "DATA_DATE")
	private Date dataDate;
	/**
	 * 開盤價
	 */
	@Column(name = "\"OPEN\"")
	private BigDecimal open;

	/**
	 * 最高價
	 */
	@Column(name = "HIGH")
	private BigDecimal high;

	/**
	 * 最低價
	 */
	@Column(name = "LOW")
	private BigDecimal low;

	/**
	 * 收盤價
	 */
	@Column(name = "\"CLOSE\"")
	private BigDecimal close;

	/**
	 * 成交量
	 */
	@Column(name = "\"VOL\"")
	private BigDecimal vol;
}
