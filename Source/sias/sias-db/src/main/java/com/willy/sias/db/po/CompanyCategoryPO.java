package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * CompanyCategory
 * @author 
 */
@Table(name="COMPANY_CATEGORY")
@Entity
@Getter
@Setter
public class CompanyCategoryPO implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
     *	 ID
     */
	@Id
	@EqualsAndHashCode.Include
    @Column(name = "\"ID\"")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
	 * 	 資料日期
	*/
    @Column(name = "\"DATA_DATE\"")
    private Date dataDate;

    /**
     * 	標的ID
     */
    @Column(name = "\"SID\"")
    private Integer sid;
    
    /**
     * 	分類ID
     */
    @Column(name = "\"CATEGORY_ID\"")
    private Integer categoryId;
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoryId == null) ? 0 : categoryId.hashCode());
		result = prime * result + ((dataDate == null) ? 0 : dataDate.hashCode());
		result = prime * result + ((sid == null) ? 0 : sid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompanyCategoryPO other = (CompanyCategoryPO) obj;
		if (categoryId == null) {
			if (other.categoryId != null)
				return false;
		} else if (!categoryId.equals(other.categoryId))
			return false;
		if (dataDate == null) {
			if (other.dataDate != null)
				return false;
		} else if (!dataDate.equals(other.dataDate))
			return false;
		if (sid == null) {
			if (other.sid != null)
				return false;
		} else if (!sid.equals(other.sid))
			return false;
		return true;
	}
}