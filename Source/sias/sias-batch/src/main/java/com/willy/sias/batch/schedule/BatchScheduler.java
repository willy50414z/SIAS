package com.willy.sias.batch.schedule;

import java.text.ParseException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.willy.sias.batch.processor.LP_TXN_LOG_TW_TWSE;
import com.willy.sias.batch.processor.STOCK_PRICE_TW_TPEX;
import com.willy.sias.batch.processor.STOCK_PRICE_TW_TWSE;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;

@Component
public class BatchScheduler {
	@Autowired
	private LP_TXN_LOG_TW_TWSE lptTwTwse;
	@Autowired
	private STOCK_PRICE_TW_TPEX spPrice_TPEX;
	@Autowired
	private STOCK_PRICE_TW_TWSE spPrice_TWSE;

	// 每天22:00執行
	@Scheduled(cron = "00 22 * * *")
	public void dailyDataCollectJob() {
		// 股價TPEX
		try {
			spPrice_TPEX.setStrDataDate(WDate.getNowDate());
			spPrice_TPEX.exec();
		} catch (Exception e) {
			WLog.error(e);
		}

		// 股價
		try {
			spPrice_TWSE.setStrDataDate(WDate.getNowDate());
			spPrice_TWSE.exec();
		} catch (Exception e) {
			WLog.error(e);
		}

		// 法人交易資料20120502開始
		try {
			lptTwTwse.setStrDataDate(WDate.getNowDate());
			lptTwTwse.exec();
		} catch (Exception e) {
			WLog.error(e);
		}

	}

	public void execAllBatch() throws ParseException {
		String strBatchStartDate = "20070422";
		Date batchDate = WType.strToDate(strBatchStartDate);
		// 執行今天以前的
		while (batchDate.before(WType.strToDate(WDate.getNowDate()))) {
			try {
				spPrice_TPEX.setStrDataDate(WType.dateToStr(batchDate));
				spPrice_TPEX.exec();
			} catch (Exception e) {
				WLog.error(e);
			}

			// 股價
			try {
				spPrice_TWSE.setStrDataDate(WType.dateToStr(batchDate));
				spPrice_TWSE.exec();
			} catch (Exception e) {
				WLog.error(e);
			}

			// 法人交易資料20120502開始
			try {
				lptTwTwse.setStrDataDate(WType.dateToStr(batchDate));
				lptTwTwse.exec();
			} catch (Exception e) {
				WLog.error(e);
			}

			batchDate = WDate.addDay(batchDate, 1);
		}
	}
}
