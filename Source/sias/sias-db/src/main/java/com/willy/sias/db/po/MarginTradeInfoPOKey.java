package com.willy.sias.db.po;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * LPTransLog_TW_TWSE
 * @author 
 */
@AllArgsConstructor
@NoArgsConstructor
@Table(name="MARGIN_TRADE_INFO")
public class MarginTradeInfoPOKey implements Serializable {
    /**
     * 資料日期
     */
	@Column(name = "DATA_DATE")
    private Date dataDate;

    /**
     * 標的代號
     */
    @Column(name = "SID")
    private Integer sid;

    private static final long serialVersionUID = 1L;
}