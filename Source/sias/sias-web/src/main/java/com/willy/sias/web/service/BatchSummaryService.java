package com.willy.sias.web.service;

import com.willy.sias.util.BeanFactory;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.willy.sias.batch.processor.BaseBatchProcessor;
import com.willy.sias.db.po.BatchLogPO;
import com.willy.sias.db.repository.BatchLogRepository;
import com.willy.sias.util.MailUtil;
import com.willy.sias.util.config.Const;
import com.willy.util.log.WLog;
import com.willy.util.type.WType;

@Service
public class BatchSummaryService {
	@Autowired
	private MailUtil mainUtil;
	@Autowired
	private BatchLogRepository blRepo;

	public void execOneTimeJob(String batchCode, Date dataDate) {
		String strDataDate = "";
		try {
			//將該BatchLog status更新成0
			BatchLogPO blpo = blRepo.findByBatchNameAndDataDate(batchCode, dataDate);
			if(blpo != null) {
				blpo.setStatus(0);
				blpo.setErrMsg("one time job rerun");
				blRepo.save(blpo);
			}
			
			//執行批次
			strDataDate = WType.dateToStr(dataDate);
			Class<?> batchPorcessorClass = Class.forName(Const.batchProcessorPackage + batchCode);
			BaseBatchProcessor batchPorcessor = (BaseBatchProcessor) BeanFactory.getBean(batchPorcessorClass);
			batchPorcessor.setStrDataDate(strDataDate);
			batchPorcessor.process();
			
			//發送Mail
			mainUtil.info("單次執行排程 BatchCode ["+batchCode+"] DataDate["+strDataDate+"]完成", "");
		} catch (Exception e) {
			WLog.error(e);
			mainUtil.error("單次執行排程 BatchCode ["+batchCode+"] DataDate["+strDataDate+"]失敗", "");
		} 
	}
}
