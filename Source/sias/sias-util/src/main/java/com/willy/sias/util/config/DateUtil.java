package com.willy.sias.util.config;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

@Service
public class DateUtil {
	public HashMap<Date, List<Date>> getSameDayDateInDifferentYears(int startYear, List<Date> dateList)
			throws NumberFormatException, ParseException {
		// 預設endDate是今年
		return getSameDayDateInDifferentYears(startYear, Integer.valueOf(WType.dateToStr(new Date(), "YYYY")),
				dateList);
	}

	/**
	 * 取得所有年份同一天DateList
	 * 
	 * @param startYear
	 * @param endYear
	 * @param dateList
	 * @return
	 * @throws ParseException
	 */
	public HashMap<Date, List<Date>> getSameDayDateInDifferentYears(int startYear, int endYear, List<Date> dateList) {
		Set<String> addedDay = new HashSet<String>();
		HashMap<Date, List<Date>> dateMap = new HashMap<Date, List<Date>>();
		List<Date> dateListInDiffYears;
		for (Date date : dateList) {
			// 取得日MMdd
			String date_MMdd = WType.dateToStr(date, "MMdd");
			if (addedDay.contains(date_MMdd)) {
				continue;
			} else {
				addedDay.add(date_MMdd);
			}
			WLog.debug("基準日期: " + WString.toString(date));
			dateListInDiffYears = new ArrayList<>();

			// 遍覽年分
			for (int year = startYear; year <= endYear; year++) {
				dateListInDiffYears.add(WType.strToDate(year + date_MMdd));
			}
			WLog.debug("不同年份同日日期: " + WString.toString(dateListInDiffYears));
			dateMap.put(date, dateListInDiffYears);
		}
		return dateMap;
	}

	public <T> HashMap<Integer, List<T>> groupListByMonth(Map<Date, List<T>> listMap) throws ParseException {
		HashMap<Integer, List<T>> monthListMap = new HashMap<>();
		for (Date date : listMap.keySet()) {
			int month = WDate.getDateInfo(date, Calendar.MONTH);
			if (monthListMap.containsKey(month)) {
				List<T> list = monthListMap.get(month);
				list.addAll(listMap.get(date));
				monthListMap.put(month, list);
			} else {
				monthListMap.put(month, listMap.get(date));
			}
		}
		return monthListMap;
	}

	/**
	 * 取得交易日
	 * 
	 * @param allTradeDateList
	 * @param targetDate
	 * @param calNum           0:最近的下一個交易日 1:最近的下一個交易日 -1:最近的前一個交易日
	 * @return
	 */
	public Date getTradeDate(List<Date> allTradeDateList, Date targetDate, int calNum) {
		List<Date> tradeDateList;
		if (calNum >= 0) {
			tradeDateList = allTradeDateList.stream().filter(x -> x.after(targetDate) || x.equals(targetDate))
					.collect(Collectors.toList());
			return tradeDateList.get(Math.max(calNum - 1, 0));
		} else {
			tradeDateList = allTradeDateList.stream().filter(x -> x.before(targetDate)).collect(Collectors.toList());
			if (tradeDateList.size() + calNum > 0) {
				return tradeDateList.get(tradeDateList.size() + calNum);
			} else {
				return null;
			}
		}
	}

	public Date getTradeDate(Set<Date> allTradeDateList, Date targetDate, int calNum) {
		List<Date> tradeDateList;
		if (calNum >= 0) {
			tradeDateList = allTradeDateList.stream().filter(x -> x.after(targetDate) || x.equals(targetDate))
					.collect(Collectors.toList());
			return tradeDateList.get(Math.max(calNum - 1, 0));
		} else {
			tradeDateList = allTradeDateList.stream().filter(x -> x.before(targetDate)).collect(Collectors.toList());
			if (tradeDateList.size() + calNum > 0) {
				return tradeDateList.get(tradeDateList.size() + calNum);
			} else {
				return null;
			}
		}
	}
}
