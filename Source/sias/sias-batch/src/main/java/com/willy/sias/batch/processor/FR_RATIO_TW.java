package com.willy.sias.batch.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.FRRatioTWPO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.CompanyCategoryRepository;
import com.willy.sias.db.repository.FRRatioTWRepository;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.EnumSet.ParentKey;
import com.willy.sias.util.config.Const;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;

import lombok.Setter;

@Setter
@Service
@ConfigurationProperties("sias.batch.fr-ratio-tw")
public class FR_RATIO_TW extends BaseBatchProcessor {
	@Autowired
	private WJsoup jsoup;
	private String urlTempl;
	@Autowired
	private SysConfigRepository sysConfigRepo;
	@Autowired
	private FRRatioTWRepository frRatioRepo;
	@Autowired
	private CompanyCategoryRepository ccRepo;
	private String year;
	
	@Override
	protected void exec() throws Exception {
		year = "" + WDate.convertCEYear(WDate.getDateInfo(dataDate, Calendar.YEAR));
		this.putMapParam("year", year);

		// 取得SID
		List<Integer> sidList = ccRepo.findSidWithoutDerivative();
		List<SysConfigPO> sysConfigList = sysConfigRepo.findByParentIdIsOrderByCfgId(Const._CFG_ID_FIN_RATIO_TYPE);
		HashMap<Integer, String> sidSidCodeMap = cfgDao.findKeyAndFieldvalueMapByParentkey(ParentKey.SID.getKey());
		List<FRRatioTWPO> dbPoList = frRatioRepo.findByFrDateLike(WDate.convertCEYear(year)+"%");
		Set<Integer> sidSet = dbPoList.stream().map(x -> x.getSid()).collect(Collectors.toSet());
		FRRatioTWPO frPo;
		List<FRRatioTWPO> frPoList = new ArrayList<>();
		int quarter = 0;
		for(int sid : sidList) {
			if(sidSet.contains(sid)) {
				WLog.info("skip sid["+sid+"], which has been run.");
			}
			int rowIndex = 0;
			int yearAdj = 2;
			this.putMapParam("sidCode", sidSidCodeMap.get(sid).toString());
			WLog.info("start query SID["+this.getMapParam("sidCode")+"] FR Ratio");
			Elements  trElements = jsoup.getElement(this.parseMapParamToString(urlTempl)).getElementsByTag("tr");
			for (Element trElement : trElements) {
				quarter = (quarter == 4) ? 1 : (quarter + 1);
				Elements tdElements = trElement.getElementsByTag("td");
				String txtValue = WString.cleanDigitData(tdElements.get(0).text());
				if (WString.isNumeric(txtValue) && !txtValue.equals("0")) {
					yearAdj = 2 - rowIndex / 4;
					rowIndex++;
					for (int i = 0; i < 5; i++) {
						frPo = new FRRatioTWPO();
						frPo.setFrDate((WDate.convertCEYear(Integer.valueOf(year)) - yearAdj) + "0" + quarter);
						frPo.setFrKey(sysConfigList.get(i).getCfgId());
						frPo.setFrValue(new BigDecimal(WString.cleanDigitData(tdElements.get(i).text())));
						frPo.setSid(sid);
						frPoList.add(frPo);
					}
				}
			}
			frRatioRepo.saveAll(frPoList);
			Thread.sleep(3000);
		}
	}

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
		
	}

}