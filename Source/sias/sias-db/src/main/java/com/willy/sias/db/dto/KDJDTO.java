package com.willy.sias.db.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KDJDTO {
	private BigDecimal k;
	private BigDecimal d;
	private BigDecimal j;
}
