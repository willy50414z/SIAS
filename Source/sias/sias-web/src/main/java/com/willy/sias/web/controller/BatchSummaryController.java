package com.willy.sias.web.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.willy.sias.util.EnumSet.BatchName;
import com.willy.sias.web.service.BatchSummaryService;
import com.willy.sias.web.vo.BatchSummaryVO;
import com.willy.util.log.WLog;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

@Controller
@RequestMapping("/BatchSummary")
public class BatchSummaryController {
	
	private final String jspName = "BatchSummary";
	@Autowired
	private BatchSummaryService bsService;
	
	@RequestMapping("/")
	public ModelAndView visit() throws ParseException {
		ModelAndView view = new ModelAndView("frames/MainFrame");
		view.addObject("homePageUrl", "viewBatchSummary");
		return view;
	}

	@RequestMapping("/viewBatchSummary")
	public ModelAndView viewBackTestMain() throws ParseException {
		ModelAndView view = new ModelAndView(jspName);
		BatchSummaryVO bs;
		List<BatchSummaryVO> batchSummaryList = new ArrayList<> ();
		List<String> batchCodeList = new ArrayList<>();
		for(BatchName batchName : BatchName.values()) {
			bs = new BatchSummaryVO(batchName.toString(), batchName.getBatchDesc(), batchName.getFrequenceDesc());
			batchSummaryList.add(bs);
			batchCodeList.add(batchName.toString() + "\t-\t" + batchName.getBatchDesc());
		}
		view.addObject("batchSummaryList", batchSummaryList);
		return view;
	}
	@RequestMapping("/exeBatch")
	public void exeBatch(String batchCode, String strDate) throws ParseException {
		try {
			bsService.execOneTimeJob(batchCode, WType.strToDate(WString.rmStr(strDate, "/")));
		} catch (Exception e) {
			WLog.error(e);
		} 
	}
}
