package com.willy.sias.backtest.strategy;

import com.willy.sias.backtest.dto.ProfitDTO;
import com.willy.sias.backtest.dto.TradePlanDTO;
import com.willy.sias.backtest.service.BasicService;
import com.willy.sias.backtest.service.StatisticService;
import com.willy.sias.backtest.service.TechIdxService;
import com.willy.sias.backtest.util.TradeUtil;
import com.willy.sias.db.dao.DataTableDao;
import com.willy.util.jdbc.WJDBC;
import com.willy.util.log.WLog;
import com.willy.util.redis.WRedis;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.UncategorizedSQLException;

@Setter
@Getter
public abstract class TradeStrategy implements Callable<Integer> {

  @Autowired
  TradeUtil tu;
  @Autowired
  DataTableDao dtDao;
  @Autowired
  WJDBC jdbc;
  @Autowired
  TechIdxService techService;
  @Autowired
  StatisticService statSvc;
  @Autowired
  BasicService bs;
  @Autowired
  TechIdxService tiSvc;
  @Autowired
  WRedis redis;



  private int sid;
  private List<Date> tradeDateList;
  private static List<TradePlanDTO> tradePlanList = new ArrayList<>();

  // 交易判斷Log
  public StringBuilder tradeJudgeLog = new StringBuilder();

  public abstract void collectTradePlanList();

  public abstract void collectTradePlanInfo(ProfitDTO po);

  public String getStrategyDesc() {
    return null;
  }

  public String checkArgs() {
    return null;
  }

  void dropExpiredTable(int saveCount) {
    try {
      String[][] tableNameAr = jdbc.queryForAr("SELECT TOP (SELECT COUNT(1)-" + saveCount
          + " FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'BACKTEST%') TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'BACKTEST%' ORDER BY TABLE_NAME");
      if (tableNameAr == null) {
        return;
      }
      for (String[] tableName : tableNameAr) {
        WLog.info("DROP TABLE[" + tableName[0] + "]");
        jdbc.jdbcTemplate.execute("DROP TABLE " + tableName[0]);
      }
    } catch (UncategorizedSQLException e) {
      // TOP N 或 FETCH 資料列計數值不可以是負值 錯誤不需理會
    } catch (Exception e) {
      WLog.error(e);
    }
  }

  public Integer run() {
    try {
      this.collectTradePlanList();
      return 1;
    } catch (Exception e) {
      return -1;
    }
  }

  void addTradePlanList(TradePlanDTO tp) {
    tradePlanList.add(tp);
  }

  public static List<TradePlanDTO> getTradePlanList() {
    return tradePlanList;
  }

  public Integer call() throws Exception {
    try {
      this.collectTradePlanList();
      cleanCache();
    } catch (Exception e) {
      WLog.error(e);
      return -1;
    }
    return 1;
  }

  //delete cache by sid which is tested complete
  private void cleanCache(){
    List<String> testSidCacheKeyList = redis.getKeys().stream().filter(key -> isSidCacheKey(key)).collect(
        Collectors.toList());
    if(testSidCacheKeyList.size() > 0) {
      String[] cacheKeys = new String[testSidCacheKeyList.size()];
      testSidCacheKeyList.toArray(cacheKeys);
      redis.del(cacheKeys);
    }
  }

  private boolean isSidCacheKey(String cacheKey) {
    try{
      return Integer.valueOf(cacheKey.split("::")[1].split(",")[1]) == this.sid;
    } catch (Exception e) {
      return false;
    }
  }
}
