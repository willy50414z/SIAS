package com.willy.sias.util;

import com.willy.util.log.WLog;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class BeanFactory {
  public static ApplicationContext context;

  @Autowired
  public ApplicationContext appContext;

  @PostConstruct
  private void initStaticDao () {
    context = appContext;
  }

  public static Object getBean(String beanName) {
    return context.getBean(beanName);
  }

  public static <T> T getBean(Class<T> beanClass) {
    return context.getBean(beanClass);
  }
}
