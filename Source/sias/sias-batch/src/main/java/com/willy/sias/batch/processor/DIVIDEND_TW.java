package com.willy.sias.batch.processor;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.willy.net.web.WJsoup;
import com.willy.sias.db.po.DividendTWPO;
import com.willy.sias.db.repository.DividendTWRepository;
import com.willy.sias.util.FileUtil;
import com.willy.util.date.WDate;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Service
@Setter
@ConfigurationProperties(prefix = "sias.batch.dividend-tw")
public class DIVIDEND_TW extends BaseBatchProcessor {
	@Autowired
	private WJsoup js;
	@Autowired
	private DividendTWRepository divRepo;
	
	@Value("${sias.downloadFile.dir}")
	private String downloadPath;
	//除權息公告查詢網址
	private String dividendQryUrl = "https://mops.twse.com.tw/mops/web/t108sb27";
	private final String[] targetMarketType=new String[] {"sii", "otc"};

	@Override
	public void cleanHisData() throws Exception {
		// TODO Auto-generated method stub
		String year = this.strDataDate.substring(0, 4);
		Date startDate = WType.strToDate(year + "0101");
		Date endDate = WType.strToDate((Integer.valueOf(year) + 1) + "0101");
		putMapParam("year", year);
		divRepo.deleteByDataDateBetween(startDate, endDate);
	}

	@Override
	protected void exec() throws Exception {
		// TODO Auto-generated method stub
		for(String marketType : targetMarketType) {
			//下載檔案
			filePath = downloadDivCSV(getMapParam("year"), marketType);
			
			//讀取檔案
			String[][] divAr = txtFile.readToAr(filePath, "公司代號", "公司代號", true);
			
			this.mergeSid(divAr);
			
			//compose
			divRepo.saveAll(compose(divAr));
		}
	}
	
	private Set<DividendTWPO> compose(String[][] divAr) throws ParseException{
		DividendTWPO div;
		Integer year = Integer.valueOf(this.getMapParam("year"));
		Set<DividendTWPO> divSet = new HashSet<> ();
		for(String[] rowDate : divAr) {
			if(year>2005 && year<2016) {
				divSet.add(composeDividendTWPO2006To2015(rowDate));
			}else if(year>2015 && year<2019) {
				divSet.add(composeDividendTWPO2016To2018(rowDate));
			}else if(year>2018) {
				div = composeDividendTWPOAfter2019(rowDate);
				if(div != null) {
					divSet.add(div);
				}
				
			}
		}
		return divSet;
	}
	
	private DividendTWPO composeDividendTWPO2006To2015(String[] rowDate) throws ParseException {
		DividendTWPO div = new DividendTWPO();
		div.setSid(sidMap.get(rowDate[0]));
		div.setDivBelongs(rowDate[2].trim());
		div.setDataDate(WType.strToDate(rowDate[20]+rowDate[21], "yyyy/MM/ddHH:mm:ss"));
		div.setSDivRev(new BigDecimal(WString.cleanDigitData(rowDate[4])));
		div.setSDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[5])));
		div.setExSDivDate(StringUtils.isEmpty(rowDate[6]) ? null : WType.strToDate(rowDate[6]));
		div.setPuRightBaseDate(StringUtils.isEmpty(rowDate[3]) ? null : WType.strToDate(rowDate[3]));
		div.setCDivRev(new BigDecimal(WString.cleanDigitData(rowDate[11])));
		div.setCDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[12])));
		div.setExCDivDate(StringUtils.isEmpty(rowDate[13]) ? null : WType.strToDate(rowDate[13]));
		div.setPuCDivDate(StringUtils.isEmpty(rowDate[14]) ? null : WType.strToDate(rowDate[14]));
		return div;
	}
	
	private DividendTWPO composeDividendTWPO2016To2018(String[] rowDate) throws ParseException {
		DividendTWPO div = new DividendTWPO();
		div.setSid(sidMap.get(rowDate[0]));
		div.setDivBelongs(rowDate[2].trim());
		div.setDataDate(WType.strToDate(rowDate[14]+rowDate[15], "yyyy/MM/ddHH:mm:ss"));
		div.setSDivRev(new BigDecimal(WString.cleanDigitData(rowDate[4])));
		div.setSDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[5])));
		div.setExSDivDate(StringUtils.isEmpty(rowDate[6]) ? null : WType.strToDate(rowDate[6]));
		div.setPuRightBaseDate(StringUtils.isEmpty(rowDate[3]) ? null : WType.strToDate(rowDate[3]));
		div.setCDivRev(new BigDecimal(WString.cleanDigitData(rowDate[7])));
		div.setCDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[8])));
		div.setExCDivDate(StringUtils.isEmpty(rowDate[9]) ? null : WType.strToDate(rowDate[9]));
		div.setPuCDivDate(StringUtils.isEmpty(rowDate[10]) ? null : WType.strToDate(rowDate[10]));
		return div;
	}
	
	private DividendTWPO composeDividendTWPOAfter2019(String[] rowDate) throws ParseException {
		DividendTWPO div = null;
		if(rowDate[11].equals("") || rowDate[11].trim().equals("0")) {
			div = new DividendTWPO();
			div.setSid(sidMap.get(rowDate[0]));
			div.setDivBelongs(rowDate[2].trim());
			div.setDataDate(WType.strToDate(rowDate[15]+rowDate[16], "yyyy/MM/ddHH:mm:ss"));
			div.setSDivRev(new BigDecimal(WString.cleanDigitData(rowDate[4])));
			div.setSDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[5])));
			div.setExSDivDate(StringUtils.isEmpty(rowDate[6]) ? null : WType.strToDate(rowDate[6]));
			div.setPuRightBaseDate(StringUtils.isEmpty(rowDate[3]) ? null : WType.strToDate(rowDate[3]));
			div.setCDivRev(new BigDecimal(WString.cleanDigitData(rowDate[7])));
			div.setCDivLegres(new BigDecimal(WString.cleanDigitData(rowDate[8])));
			div.setExCDivDate(StringUtils.isEmpty(rowDate[9]) ? null : WType.strToDate(rowDate[9]));
			div.setPuCDivDate(StringUtils.isEmpty(rowDate[10]) ? null : WType.strToDate(rowDate[10]));
		}else {
			return div;
		}
		return div;
	}

	private String downloadDivCSV(String year, String marketType)
			throws IOException, InterruptedException, ExecutionException {
		String csvFileName = parseMapParamToString(csvFileNameTempl);
		if(!new File(downloadPath + csvFileName).exists()) {
			js.putFormData("encodeURIComponent", "1");
			js.putFormData("step", "1");
			js.putFormData("firstin", "1");
			js.putFormData("off", "1");
			js.putFormData("keyword4", "");
			js.putFormData("code1", "");
			js.putFormData("TYPEK2", "");
			js.putFormData("checkbtn", "");
			js.putFormData("queryName", "");
			js.putFormData("TYPEK", marketType);
			js.putFormData("co_id_1", "");
			js.putFormData("co_id_2", "");
			js.putFormData("year", WDate.convertCEYear(year));
			js.putFormData("month", "");
			js.putFormData("b_date", "");
			js.putFormData("e_date", "");
			js.putFormData("type", "");
			Document doc = js.getBody(dividendQryUrl);
			Elements inputs = doc.getElementsByTag("input");
			for (Element input : inputs) {
				if (input.attr("name").equals("filename")) {
					putMapParam("csvFileName", input.val());
					FileUtil.download(parseMapParamToString(urlTempl), csvFileName);
					break;
				}
			}
		}
		return downloadPath + csvFileName;
	}
}
