package com.willy.sias.db.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.willy.sias.db.dto.KBarDTO;
import com.willy.sias.db.dto.TicksDTO;
import com.willy.sias.db.dto.TimeSeriesDTO;
import com.willy.sias.db.po.SysConfigPO;
import com.willy.sias.db.repository.SysConfigRepository;
import com.willy.sias.util.config.Const;
import com.willy.util.cache.WCache;
import com.willy.util.date.WDate;
import com.willy.util.log.WLog;
import com.willy.util.redis.WRedis;
import com.willy.util.string.WString;
import com.willy.util.type.WType;

import lombok.Setter;

@Component
@Setter
@ConfigurationProperties(prefix = "shiaoji")
public class ShiaojiDao {
	private String kBarUrl;
	private String ticksUrl;

	@Value("${shiaoji.personId}")
	private String personId;
	@Value("${shiaoji.active}")
	private boolean active;
	@Autowired
	private SysConfigRepository scRepo;
	@Autowired
	private WRedis redis;
	private Connection con;
	private Response rs;
	private String result = "";
	

	public ShiaojiDao(@Value("${shiaoji.loginUrl}") String loginUrl, @Value("${shiaoji.kBarUrl}") String kBarUrl,
			@Value("${shiaoji.personId}") String personId, @Value("${shiaoji.dwp}") String dwp) throws Exception {
		if(!active) {
			return;
		}
		boolean isRqLogin = false;
		// 測試連線是否成功
		WCache.cache.put("sidCode", "2330");
		WCache.cache.put("startDate", "2021-08-25");
		WCache.cache.put("endDate", "2021-08-25");
		for (int i = 0; i < 6; i++) {
			try {
				con = Jsoup.connect(WString.parseParamsToStrByCache(kBarUrl)).ignoreContentType(true).timeout(30000);
				rs = con.method(Method.GET).execute();
				result = new String(rs.bodyAsBytes(), Const._ENCODING_UTF8);
				new JSONObject(result);
				return;
			} catch (Exception e) {
				if (!isRqLogin) {
					// 登入
					con = Jsoup.connect(loginUrl).ignoreContentType(true).timeout(30000);
					con.data("person_id", personId, "passwd", dwp);
					rs = con.method(Method.POST).execute();
					result = new String(rs.bodyAsBytes(), Const._ENCODING_UTF8);
					new JSONObject(result);
				}
			}
			WLog.info("Login Test Fail[" + i + "], next test will start after 5 second");
			Thread.sleep(5000);
		}
	}

	// 每分鐘成交筆數
	public JSONObject getKBar(Integer sid, Date startDate, Date endDate) throws Exception {
		SysConfigPO sc = scRepo.findById(sid).orElse(null);
		if(sc == null || StringUtils.isBlank(sc.getCfgValue())) {
			throw new IllegalArgumentException("can't get stockCode from SysConfig, SID[" + sid + "]");
		} else {
			return getKBar(sc.getCfgValue(), startDate, endDate);
		}
	}
	public JSONObject getKBar(String stockCode, Date startDate, Date endDate) throws Exception {
		String redisKey = getRedisKey(stockCode, startDate, endDate);
		if(redis.exists(redisKey)) {
			return (JSONObject) redis.get(redisKey);
		}
		WCache.cache.put("sidCode", stockCode);
		WCache.cache.put("startDate", WType.dateToStr(startDate, WDate.dateFormat_yyyyMMdd_Dash));
		WCache.cache.put("endDate", WType.dateToStr(endDate, WDate.dateFormat_yyyyMMdd_Dash));
		String reqUrl = WString.parseParamsToStrByCache(kBarUrl);
		WLog.info("reqUrl : " + reqUrl);
		con = Jsoup.connect(reqUrl).ignoreContentType(true).timeout(30000).maxBodySize(0);
		rs = con.method(Method.GET).execute();
		result = new String(rs.bodyAsBytes(), Const._ENCODING_UTF8);
		return new JSONObject(result);
	}
	
	//取得單分鐘成交股價資訊
	public List<TimeSeriesDTO<KBarDTO>> getKBarList(Integer sid, Date startDate, Date endDate) throws Exception {
		List<TimeSeriesDTO<KBarDTO>> kbarTsList = new ArrayList<>();
		SysConfigPO sc = scRepo.findById(sid).orElse(null);
		JSONObject js = getKBar(sc.getCfgValue(), startDate, endDate);
		for (int i = 0; i < js.getString("ts").split(":").length - 1; i++) {
			Date date = new Date(
					Long.parseLong(js.getJSONObject("ts").get(String.valueOf(i)).toString()) - (8 * 60 * 60 * 1000));
			BigDecimal open = new BigDecimal(js.getJSONObject("Open").get(String.valueOf(i)).toString());
			BigDecimal high = new BigDecimal(js.getJSONObject("High").get(String.valueOf(i)).toString());
			BigDecimal low = new BigDecimal(js.getJSONObject("Low").get(String.valueOf(i)).toString());
			BigDecimal close = new BigDecimal(js.getJSONObject("Close").get(String.valueOf(i)).toString());
			BigDecimal vol = new BigDecimal(js.getJSONObject("Volume").get(String.valueOf(i)).toString());
			kbarTsList.add(new TimeSeriesDTO<KBarDTO>(date, new KBarDTO(open, high, low, close, vol)));
		}
		kbarTsList.sort(new Comparator<TimeSeriesDTO<KBarDTO>>() {
			@Override
			public int compare(TimeSeriesDTO<KBarDTO> o1, TimeSeriesDTO<KBarDTO> o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		return kbarTsList;
	}

	// 逐筆成交資料
	public JSONObject getTicks(String stockCode, Date date) throws Exception {
		String redisKey = getRedisKey(stockCode, date, null);
		if(redis.exists(redisKey)) {
			return (JSONObject) redis.get(redisKey);
		}
		WCache.cache.put("sidCode", stockCode);
		WCache.cache.put("date", WType.dateToStr(date, WDate.dateFormat_yyyyMMdd_Dash));
		con = Jsoup.connect(WString.parseParamsToStrByCache(ticksUrl)).ignoreContentType(true).timeout(30000)
				.maxBodySize(0);
		rs = con.method(Method.GET).execute();
		result = new String(rs.bodyAsBytes(), Const._ENCODING_UTF8);
		return new JSONObject(result);
	}

	//取得單日成交資訊
	public List<TimeSeriesDTO<TicksDTO>> getTicksList(Integer sid, Date date) throws Exception {
		// TicksDTO
		SysConfigPO sc = scRepo.findById(sid).orElse(null);
		JSONObject js = getTicks(sc.getCfgValue(), date);
		List<TimeSeriesDTO<TicksDTO>> ticksTsList = new ArrayList<>();
		for (int i = 0; i < js.getString("ts").split(":").length - 1; i++) {
			ticksTsList.add(new TimeSeriesDTO<TicksDTO>(
					new Date(Long.parseLong(js.getJSONObject("ts").get(String.valueOf(i)).toString())
							- (8 * 60 * 60 * 1000)),
					new TicksDTO(new BigDecimal(js.getJSONObject("ask_volume").get(String.valueOf(i)).toString()),
							new BigDecimal(js.getJSONObject("ask_price").get(String.valueOf(i)).toString()),
							new BigDecimal(js.getJSONObject("close").get(String.valueOf(i)).toString()),
							new BigDecimal(js.getJSONObject("bid_price").get(String.valueOf(i)).toString()),
							new BigDecimal(js.getJSONObject("bid_volume").get(String.valueOf(i)).toString()),
							new BigDecimal(js.getJSONObject("volume").get(String.valueOf(i)).toString()))));
		}
		ticksTsList.sort(new Comparator<TimeSeriesDTO<TicksDTO>>() {
			@Override
			public int compare(TimeSeriesDTO<TicksDTO> o1, TimeSeriesDTO<TicksDTO> o2) {
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		return ticksTsList;
	}
	
	/**
	 * 用來組儲存進redis的key
	 * @param sid
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	private String getRedisKey(String stockCode, Date startDate, Date endDate) {
		StackTraceElement st = new Exception().getStackTrace()[1];
		return st.getFileName() + "_" + st.getMethodName() + "_" + st.getLineNumber() + "_" + stockCode + "_" + (startDate == null ? "null" : startDate.getTime()) + "_" + (endDate == null ? "null" : endDate.getTime());
	}
}
